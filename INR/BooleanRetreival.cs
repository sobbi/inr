﻿using System;
using System.Collections.Generic;

namespace INR
{
    public class BooleanRetreival
    {

        public static LinkedList<PostingListElement> parseAndCalculate(string query)
        {
            string[] ors = query.ToLower().Replace("(", "").Replace(")", "").Split(new string[] { "\\or" }, StringSplitOptions.RemoveEmptyEntries);
            LinkedList<PostingListElement> resultAll = new LinkedList<PostingListElement>();
            for (int i = 0; i < ors.Length; i++)
            {
                string[] ands = ors[i].Split(new string[] { "\\and" }, StringSplitOptions.RemoveEmptyEntries);
                LinkedList<PostingListElement> resultAnds = PostingFile.getPostingList(ands[0].Trim()).list;

                for (int j = 1; j < ands.Length; j++)
                {
                    LinkedList<PostingListElement> currentAnd = PostingFile.getPostingList(ands[j].Replace("\\not", "").Trim()).list;
                    if (ands[j].Trim().StartsWith("\\not"))
                    {
                        resultAnds = BooleanRetreival.andNot(resultAnds, currentAnd);
                    }
                    else
                    {
                        resultAnds = BooleanRetreival.and(resultAnds, currentAnd);
                    }
                }
                resultAll = BooleanRetreival.or(resultAll, resultAnds);
            }
            return resultAll;
        }

        /*public static LinkedList<LinkedList<string>> parse(string query)
        {
            LinkedList<LinkedList<string>> result = new LinkedList<LinkedList<string>>();
            string[] ors = query.Split(new string[] { " \\or " }, StringSplitOptions.None);
            for (int i = 0; i < ors.Length; i++)
            {
                LinkedList<string> currentStatement = new LinkedList<string>();
                string[] ands = ors[i].Split(new string[] { "\\and" }, StringSplitOptions.None);
                for(int j = 0; j < ands.Length; j++)
                {
                    ands[j] = ands[j].Trim();
                    currentStatement.AddLast(ands[j]);
                }
                result.AddLast(currentStatement);
            }
            /*Just a Test 
            LinkedListNode<LinkedList<string>> p = result.First;
            while (p != null)
            {
                LinkedListNode<string> pp = p.Value.First;
                while (pp != null)
                {
                    Console.Write(pp.Value);
                    pp = pp.Next;
                    if (pp != null)
                    {
                        Console.Write(" and ");
                    }
                } 
                p = p.Next;
                if (p != null)
                {
                    Console.Write(" or ");
                }
            }
            *//*TestEnd

                return result;
        }*/

        public static LinkedList<PostingListElement> and(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b)
        {
            LinkedList<PostingListElement> result = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;
            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId == p2.Value.docId)
                {
                    result.AddLast(p1.Value);
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else if (p1.Value.docId < p2.Value.docId)
                {
                    p1 = p1.Next;
                }
                else
                {
                    p2 = p2.Next;
                }
            }
            return result;
        }

        public static LinkedList<PostingListElement> andNot(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b)
        {
            Console.WriteLine("calling andNot");
            LinkedList<PostingListElement> result = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;
            while (p1 != null && p2 != null)
            {
                if (p2.Value.docId == p1.Value.docId)
                {
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else if (p1.Value.docId < p2.Value.docId)
                {
                    result.AddLast(p1.Value);
                    p1 = p1.Next;
                }
                else
                {
                    p2 = p2.Next;
                }
            }
            while (p1 != null)
            {
                result.AddLast(p1.Value);
                p1 = p1.Next;
            }
            return result;
        }

        public static LinkedList<PostingListElement> or(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b)
        {
            LinkedList<PostingListElement> answer = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;

            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId < p2.Value.docId)
                {
                    answer.AddLast(p1.Value);
                    p1 = p1.Next;
                }
                else if (p1.Value.docId > p2.Value.docId)
                {
                    answer.AddLast(p2.Value);
                    p2 = p2.Next;
                }
                else
                {
                    answer.AddLast(p1.Value);
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
            }

            while (p1 != null)
            {
                answer.AddLast(p1.Value);
                p1 = p1.Next;
            }
            while (p2 != null)
            {
                answer.AddLast(p2.Value);
                p2 = p2.Next;
            }
            return answer;
        }
    }
}
