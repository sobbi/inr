﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INR
{

    public class ClusteringParser
    {
        public static Dictionary<int, double> parse(string query, int b2)
        {
            List<string> terms = query.ToLower().Trim().Split(' ').ToList();
            Dictionary<Cluster, double> bestFit = Clustering.getBestFittingClusters(Clustering.clusters, terms, b2);
            Dictionary<int, double> clusterScores = new Dictionary<int, double>();
            foreach (KeyValuePair<Cluster, double> kv in bestFit)
            {
                Console.WriteLine("Best cluster has docId " + kv.Key.leaderDocId);
                clusterScores = clusterScores.Concat(Cosine.FastCosineScore(terms, kv.Key)).Distinct().ToDictionary(p => p.Key, p => p.Value);
            }
            clusterScores = clusterScores.OrderByDescending(c => c.Value).Take(10).ToDictionary(p => p.Key, p => p.Value);
            foreach (KeyValuePair<int, double> s in clusterScores)
            {
                Console.WriteLine("Document " + s.Key + " == SCORE: " + s.Value);
            }
            return clusterScores;
        }
    }

    public class Clustering
    {
        public static HashSet<Cluster> clusters;
        public static double[,] clusterDistances;

        static Random random = new Random();

        public static HashSet<Cluster> initClusters(int b1)
        {
            Console.WriteLine("Selecting LeaderNodes");
            clusters = selectLeaders();
            //calculateClusterDistances();
            assignFollowersToClusters(clusters, b1);
            return clusters;
        }

        private static void calculateClusterDistances()
        {
            Console.WriteLine("Calculate Distances Between Clusters");
            DateTime startTime = DateTime.Now;
            clusterDistances = new double[clusters.Count, clusters.Count];
            for (int i = 0; i < clusters.Count; i++)
            {
                for (int j = 0; j < clusters.Count; j++)
                {
                    clusterDistances[i, j] = clusters.ElementAt(i).calcSimToLeader(clusters.ElementAt(j).leaderDocId);
                }
            }
            DateTime endTime = DateTime.Now;
            Console.WriteLine("Time Taken to calculate ClusterLeaderDistances: " + (endTime - startTime));
        }

        private static HashSet<Cluster> selectLeaders()
        {
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            int numberOfLeaders = (int)Math.Sqrt(numberOfDocuments);
            HashSet<Cluster> leaders = new HashSet<Cluster>();
            while (leaders.Count < numberOfLeaders)
            {
                leaders.Add(new Cluster(random.Next(0, numberOfDocuments)));
            }
            return leaders;
        }

        private static void assignFollowersToClusters(HashSet<Cluster> clusters, int b1 = 1)
        {
            Console.WriteLine("Assign follower to leaders");
            DateTime startTime = DateTime.Now;
            Parallel.For(0, Tokenizer.wordsInDocs.Count, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount - 1 }, docId =>
            {
                if (!clusters.Contains(new Cluster(docId)))
                {
                    Dictionary<Cluster, double> bestFittingCluster = getBestFittingClusters(clusters, docId);
                    foreach(KeyValuePair<Cluster, double> c in bestFittingCluster.Take(b1))
                    {
                        c.Key.followerDocIds.Add(docId);
                    }
                }
            });
            DateTime endTime = DateTime.Now;
            Console.WriteLine("Time taken to assign followers to leaders: " + (endTime - startTime));
        }

        private static Dictionary<Cluster, double> getBestFittingClusters(HashSet<Cluster> clusters, int docId)
        {
            DateTime startTime = DateTime.Now;
            DateTime calcTime = new DateTime();
            Dictionary<Cluster, double> bestResults = new Dictionary<Cluster, double>();
            for (int i = 0; i < clusters.Count; i++)
            {
                DateTime startCalcTime = DateTime.Now;
                bestResults.Add(clusters.ElementAt(i), clusters.ElementAt(i).calcSimToLeader(docId));
                DateTime endCalcTime = DateTime.Now;
                calcTime += endCalcTime - startCalcTime;
            }
            DateTime endTime = DateTime.Now;
            return bestResults.OrderBy(t => t.Value).ToDictionary(p => p.Key, p => p.Value);
        }

        public static Dictionary<Cluster, double> getBestFittingClusters(HashSet<Cluster> clusters, List<string> query, int b2)
        {
            Dictionary<Cluster, double> bestCluster = new Dictionary<Cluster, double>();
            foreach (Cluster c in clusters)
            {
                bestCluster.Add(c, Cosine.calcSingleFastCosineScore(query, c.leaderDocId));
            }
            return bestCluster.OrderBy(t => t.Value).Take(b2).ToDictionary(p => p.Key, p => p.Value);
        }
    }

    public class Cluster
    {

        public int leaderDocId;
        public List<int> followerDocIds;

        public Cluster(int leaderDocId)
        {
            this.leaderDocId = leaderDocId;
            this.followerDocIds = new List<int>();
        }

        public double calcSimToLeader(int docId)
        {
            return Cosine.fastSim(leaderDocId, docId);
        }

        public int getNearestClusterMember(int docId)
        {
            Tuple<int, double> mostSimilarResult = null;
            foreach (int followerDocId in followerDocIds)
            {
                double currentScore = Cosine.fastSim(docId, followerDocId);
                if (mostSimilarResult == null || currentScore > mostSimilarResult.Item2)
                {
                    mostSimilarResult = new Tuple<int, double>(followerDocId, currentScore);
                }
            }
            double leaderSimilarity = calcSimToLeader(docId);
            if (leaderSimilarity > mostSimilarResult.Item1)
            {
                mostSimilarResult = new Tuple<int, double>(leaderDocId, leaderSimilarity);
            }
            return mostSimilarResult.Item1;
        }

        public override bool Equals(object obj)
        {
            var cluster = obj as Cluster;
            return cluster != null &&
                   leaderDocId == cluster.leaderDocId;
        }

        public override int GetHashCode()
        {
            return 616977039 + leaderDocId.GetHashCode();
        }


    }
}
