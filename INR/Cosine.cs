﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INR
{
    public class Cosine
    {

        private static Dictionary<int, double> docLens = new Dictionary<int, double>();

        public static void calcWtds()
        {
            DateTime dateTimeStart = DateTime.UtcNow;
            int[] docLengths = new int[Tokenizer.wordsInDocs.Count];
            for (int i = 0; i < Tokenizer.wordsInDocs.Count; i++)
            {
                docLengths[i] = Tokenizer.wordsInDocs[i].Count;
            }
            for (int termId = 0; termId < PostingFile.postingFile.Values.Count; termId++)
            {
                LinkedListNode<PostingListElement> e1 = PostingFile.postingFile.Values[termId].list.First;
                while (e1 != null)
                {
                    int currentDocId = e1.Value.docId;
                    e1.Value.wtd = calcWtd(currentDocId, e1, 10, docLengths);
                    if(docLens.ContainsKey(currentDocId))
                    {
                        docLens[currentDocId] += Math.Pow(e1.Value.wtd, 2);
                    }
                    else
                    {
                        docLens.Add(currentDocId, Math.Pow(e1.Value.wtd, 2);
                    }
                    e1 = e1.Next;
                }
            }
            for (int i = 0; i < Tokenizer.wordsInDocs.Count; i++)
            {
                if (docLens.ContainsKey(i))
                {
                    docLens[i] = Math.Sqrt(docLens[i]);
                }
            }
            DateTime dateTimeEnd = DateTime.UtcNow;
            Console.WriteLine("Time taken for calculating Wtds: " + (dateTimeEnd - dateTimeStart) + "seconds");
        }

        public static List<Tuple<int, double>> CosineScore(int docId)
        {
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            double[] scores = new double[numberOfDocuments];
            int[] documentLengths = new int[numberOfDocuments];

            for (int id = 0; id < numberOfDocuments; id++)
            {
                documentLengths[id] = Tokenizer.wordsInDocs[id].Count;
            }

            Console.WriteLine("Calc similarity");
            for (int doc2 = 0; doc2 < numberOfDocuments; doc2++)
            {
                scores[doc2] = sim(docId, doc2);
            }

            List<Tuple<int, double>> scoreTuple = new List<Tuple<int, double>>();

            for (int i = 0; i < scores.Length; i++)
            {
                scoreTuple.Add(new Tuple<int, double>(i, scores[i]));
            }
            scoreTuple.Sort((t1, t2) => t1.Item2.CompareTo(t2.Item2));
            scoreTuple.Reverse();
            return scoreTuple;
        }

        public static double sim(int doc1, int doc2)
        {
            double zaehler = 0;
            double wtc1 = 0;
            double wtc2 = 0;
            List<WordInDoc> list1 = Tokenizer.wordsInDocs[doc1];
            List<WordInDoc> list2 = Tokenizer.wordsInDocs[doc2];
            List<WordInDoc> concatList = list1.Concat(list2).Distinct().ToList<WordInDoc>();

            for (int i = 0; i < concatList.Count; i++)
            {
                LinkedListNode<PostingListElement> e1 = PostingFile.postingFile.Values[concatList[i].id].list.First;
                double tmp1 = 0;
                double tmp2 = 0;
                while (e1 != null)
                {
                    if (e1.Value.docId == doc1)
                    {
                        tmp1 = e1.Value.wtd;
                        wtc1 += Math.Pow(tmp1, 2);
                    }
                    if (e1.Value.docId == doc2)
                    {
                        tmp2 = e1.Value.wtd;
                        wtc2 += Math.Pow(tmp2, 2);
                    }
                    e1 = e1.Next;
                }
                zaehler += tmp1 * tmp2;
            }
            double nenner = Math.Sqrt(wtc1) * Math.Sqrt(wtc2);
            return zaehler / nenner;
        }

        public static double fastSim(int doc1, int doc2)
        {
            double zaehler = 0;
            double wtc1 = 0;
            double wtc2 = 0;
            List<WordInDoc> list1 = Tokenizer.wordsInDocs[doc1];
            List<WordInDoc> list2 = Tokenizer.wordsInDocs[doc2];
            List<WordInDoc> concatList = list1.Concat(list2).Distinct().ToList<WordInDoc>();

            for (int i = 0; i < concatList.Count; i++)
            {
                PostingList currentPL = PostingFile.postingFile.Values[concatList[i].id];
                if (currentPL.docs.ContainsKey(doc1) && currentPL.docs.ContainsKey(doc2))
                {
                    double tmp1 = currentPL.docs[doc1].wtd;
                    double tmp2 = currentPL.docs[doc2].wtd;
                    wtc1 += Math.Pow(tmp1, 2);
                    wtc2 += Math.Pow(tmp2, 2);
                    zaehler += tmp1 * tmp2;
                }
            }
            double nenner = Math.Sqrt(wtc1) * Math.Sqrt(wtc2);
            return zaehler / nenner;
        }

        private static double calcWtd(int docId, LinkedListNode<PostingListElement> e1, int k, int[] documentLengths)
        {
            int documentFreq = e1.List.Count;
            int termFreqInDoc = e1.Value.positions.Count;
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            int avgDocLen = documentLengths.Sum() / documentLengths.Length;
            double result = (termFreqInDoc / ((double)termFreqInDoc + ((double)k * ((double)documentLengths[docId]) / (double)avgDocLen))) * Math.Log(numberOfDocuments / documentFreq);
            return result;
        }

        private static double calcWtd(string term, int termFreqInQuery, int querySize, int k, int[] documentLengths)
        {
            int docFreq = PostingFile.postingFile[term].list.Count + 1;
            int numberOfDocuments = Tokenizer.wordsInDocs.Count + 1;
            int avgDocLen = (documentLengths.Sum() + querySize) / (documentLengths.Length + 1);
            double result = (termFreqInQuery / ((double)termFreqInQuery + ((double)k * (double)querySize / (double)avgDocLen))) * Math.Log(numberOfDocuments / docFreq);
            return result;
        }

        public static List<Tuple<int, double>> FastCosineScore(List<string> terms)
        {
            Console.WriteLine("FastConsineScore started");
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            double[] scores = new double[numberOfDocuments];
            int[] documentLengths = new int[numberOfDocuments];

            for (int docId = 0; docId < numberOfDocuments; docId++)
            {
                documentLengths[docId] = Tokenizer.wordsInDocs[docId].Count;
            }
            foreach (string term in terms)
            {
                LinkedListNode<PostingListElement> current;
                try
                {
                    current = PostingFile.postingFile[term].list.First;
                }
                catch (KeyNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                    return new List<Tuple<int, double>>();
                }
                int documentFreq = current.List.Count;
                while (current != null)
                {
                    int tftd = current.Value.positions.Count;
                    if (tftd > 0)
                    {
                        scores[current.Value.docId] += (1d + Math.Log10(tftd)) * Math.Log(numberOfDocuments / documentFreq);
                    }
                    current = current.Next;
                }
            }

            for (int docId = 0; docId < numberOfDocuments; docId++)
            {
                scores[docId] = scores[docId] / docLens[docId];
            }

            List<Tuple<int, double>> scoresTuple = new List<Tuple<int, double>>();
            for (int i = 0; i < scores.Length; i++)
            {
                scoresTuple.Add(new Tuple<int, double>(i, scores[i]));
            }

            scoresTuple.Sort((t1, t2) => t1.Item2.CompareTo(t2.Item2));
            scoresTuple.Reverse();

            Console.WriteLine("FastConsineScore finished");
            return scoresTuple;
        }

        public static Dictionary<int, double> FastCosineScore(List<string> terms, Cluster cluster)
        {
            Console.WriteLine("FastConsineScore started");
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            Dictionary<int, double> scores = new Dictionary<int, double>();
            Dictionary<int, int> documentLengths = new Dictionary<int, int>();

            foreach (int docId in cluster.followerDocIds)
            {
                documentLengths.Add(docId, Tokenizer.wordsInDocs[docId].Count);
                scores.Add(docId, 0);
            }
            documentLengths.Add(cluster.leaderDocId, Tokenizer.wordsInDocs[cluster.leaderDocId].Count);
            scores.Add(cluster.leaderDocId, 0);

            foreach (string term in terms)
            {
                PostingList currentPL = null;
                try
                {
                    currentPL = PostingFile.postingFile[term];
                }
                catch (KeyNotFoundException)
                {
                    continue;
                }

                int documentFreq = currentPL.docs.Count;
                foreach (KeyValuePair<int, PostingListElement> e in currentPL.docs)
                {
                    int tftd = e.Value.positions.Count;
                    if (tftd > 0)
                    {
                        if (cluster.followerDocIds.Contains(e.Value.docId) || e.Value.docId == cluster.leaderDocId)
                        {
                            scores[e.Value.docId] += (1d + Math.Log10(tftd)) * Math.Log(numberOfDocuments / documentFreq);
                        }
                    }
                }
            }

            foreach(int docId in cluster.followerDocIds)
            {
                scores[docId] = scores[docId] / docLens[docId];
            }
            scores[cluster.leaderDocId] = scores[cluster.leaderDocId] / docLens[cluster.leaderDocId];

            scores.OrderByDescending(s => s.Value);
            Console.WriteLine("FastConsineScore finished");
            return scores;
        }

        public static double calcSingleFastCosineScore(List<string> terms, int docId)
        {
            double score = 0;
            int numberOfDocuments = Tokenizer.wordsInDocs.Count;
            foreach (string term in terms)
            {
                PostingList currentPL = null;
                try
                {
                    currentPL = PostingFile.postingFile[term];
                }
                catch (KeyNotFoundException)
                {
                    continue;
                }

                int documentFreq = currentPL.docs.Count;
                if (currentPL.docs.ContainsKey(docId))
                {
                    int tftd = currentPL.docs[docId].positions.Count;
                    if (tftd > 0)
                    {
                        score += (1d + Math.Log10(tftd)) * Math.Log(numberOfDocuments / documentFreq);
                    }
                }
            }
            return score/docLens[docId];
        }

        internal static List<Tuple<int, double>> CosineScore(List<string> terms, HashSet<Cluster> clusters)
        {
            List<Tuple<int, double>> resultScores = new List<Tuple<int, double>>();
            double[] scores = new double[clusters.Count];

            for (int i = 0; i < clusters.Count; i++)
            {
                scores[i] = sim(terms, clusters.ElementAt(i).leaderDocId);
            }

            List<Tuple<int, double>> scoreTuple = new List<Tuple<int, double>>();

            for (int i = 0; i < scores.Length; i++)
            {
                scoreTuple.Add(new Tuple<int, double>(i, scores[i]));
            }
            scoreTuple.Sort((t1, t2) => t1.Item2.CompareTo(t2.Item2));
            scoreTuple.Reverse();

            return scoreTuple;

        }

        private static double sim(List<string> terms, int leaderDocId)
        {
            List<string> docWords = Tokenizer.wordsInDocs[leaderDocId].ConvertAll(w => w.term).ToList();
            List<string> concatList = terms.Concat(docWords).Distinct().ToList();
            double zaehler = 0;
            double wtc2 = 0;
            double wtc = 0;
            for (int i = 0; i < concatList.Count; i++)
            {
                if (PostingFile.postingFile.ContainsKey(concatList[i]))
                {
                    PostingList currentPL = PostingFile.postingFile[concatList[i]];
                    if (currentPL.docs.ContainsKey(leaderDocId))
                    {
                        double tmp = 0;
                        tmp = currentPL.docs[leaderDocId].wtd;
                        wtc += Math.Pow(tmp, 2);
                        double qwtc = calcWtd(concatList[i], terms.Where(t => t.Equals(concatList[i])).Count(), terms.Count, 10, Tokenizer.wordsInDocs.ToList().ConvertAll(w => w.Value.Count).ToArray());
                        zaehler += tmp * qwtc;
                        wtc2 += Math.Pow(qwtc, 2);
                    }
                }
            }
            double nenner = Math.Sqrt(wtc) * Math.Sqrt(wtc2);
            return zaehler / nenner;
        }
    }
}
