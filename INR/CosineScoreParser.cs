﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace INR
{
    public class CosineScoreParser
    {

        public static List<Tuple<int, double>> parseAndExecute(string query)
        {
            int docId = -1;
            bool isDoc = int.TryParse(query, out docId);
            if(isDoc)
            {
                return Cosine.CosineScore(docId);
            }
            List<string> terms = parseQuery(query);
            return Cosine.FastCosineScore(terms);
        }


        private static List<string> parseQuery(string query)
        {
            query = query.TrimEnd();
            query = query.ToLower();
            string[] terms = query.Split();
            return terms.ToList();
        }

    }
}
