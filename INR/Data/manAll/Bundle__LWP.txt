Bundle::LWP(3pm)      User Contributed Perl Documentation     Bundle::LWP(3pm)



NAME
       Bundle::LWP - install all libwww-perl related modules

SYNOPSIS
	perl -MCPAN -e 'install Bundle::LWP'

CONTENTS
       MIME::Base64	  - Used in authentication headers

       Digest::MD5	  - Needed to do Digest authentication

       URI 1.10 	  - There are URIs everywhere

       Net::FTP 2.58	  - If you want ftp://-support

       HTML::Tagset	  - Needed by HTML::Parser

       HTML::Parser	  - Need by HTML::HeadParser

       HTML::HeadParser   - To get the correct $res->base

       LWP		  - The reason why you need the modules above

DESCRIPTION
       This bundle defines all prereq modules for libwww-perl.	Bundles have
       special meaning for the CPAN module.  When you install the bundle
       module all modules mentioned in "CONTENTS" will be installed instead.

SEE ALSO
       "Bundles" in CPAN



perl v5.10.1			  2008-04-11		      Bundle::LWP(3pm)
