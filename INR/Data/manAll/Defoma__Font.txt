Defoma::Font(3pm)					     Defoma::Font(3pm)



NAME
       Defoma::Font - Defoma module to handle font and font-cache.

SYNOPSIS
       use Defoma::Font;

       defoma_font_register( category, font, hints...  );

       defoma_font_unregister( category, font );

       defoma_font_reregister( category, font, hints...  );

       defoma_font_if_register( category, font );

       @fonts = defoma_font_get_fonts( category );

       @hints = defoma_font_get_hints( category, font );

       %failedapps = defoma_font_get_failed ( category, font );


DESCRIPTION
       Defoma::Font  is  a Defoma module to handle registration/unregistration
       of fonts and font-caches. It provides functions listed above which  are
       supposed  to  be  called  from  Defoma-configuration  scripts  when the
       scripts want to register/unregister a font, and need  to  retrieve  the
       data recorded in a font-cache, which holds fonts and their hints.

       defoma_font_register  is  a  function used to register a specified font
       into a specified category with specified hints.	If the specified  font
       is  already registered, it returns non-zero. Otherwise the font and the
       hints are registered into the font-cache of the specified category, and
       further Defoma-configuration scripts get called which accept the speci‐
       fied category with register command.

       defoma_font_unregister is a function used  to  unregister  a  specified
       font  from  a specified category.  If the font is not registered in the
       category, it returns non-zero.  Otherwise further  Defoma-configuration
       scripts	get called which accept the specified category with unregister
       command, and the font and the hints are removed from the font-cache  of
       the category.

       defoma_font_reregister	is  a  function  which	actually  calls  defo‐
       ma_font_unregister and defoma_font_register in order.  If  a  specified
       font  if  not  registered  in  a specified category, unregisteration is
       skipped. If the font is already registered in another category, it  re‐
       turns non-zero.

       defoma_font_if_register	if  a function that checks if a specified font
       is registered in a specified category.  If the font is  registered,  it
       returns non-zero. Otherwise it returns zero.

       defoma_font_get_fonts  is  a  function  used  to obtain a list of fonts
       reigstered in the specified category.  It returns a list of fonts in an
       array.  (font1 font2 font3 ...)

       defoma_font_get_hints  is a function used to obtain hints of the speci‐
       fied font registered in the specified category.	It returns a  list  of
       hints in an array.

       defoma_font_get_failed  is  a  function used to obtain applications for
       which Defoma failed to configure the specified font.  It returns a hash
       whose keys are applications and whose values are error status.

EXAMPLES
       1. Register 'Helvetica-Bold' in postscript category with hints.

       $font = 'Helvetica-Bold';
       @hints = ('--Family', 'Helvetica', '--Weight', 'Bold',
		 '--Charset', 'ISO8859-1);

       defoma_font_register('postscript', $font, @hints);

       2. Unregister 'foo.pfa' from type1 category.

       $font = '/usr/share/fonts/foo.pfa';

       defoma_font_unregister('type1', $font);

       3. Unregister 'foo.pfa' from type1 category if registered.

       $font = '/usr/share/fonts/foo.pfa';

       if (defoma_font_if_register('type1', $font)) {
	 defoma_font_unregister('type1', $font);
       }

       4. Get a list of fonts and their hints.

       %hash = ();
       @fonts = defoma_font_get_fonts('x-postscript');
       foreach $font (@fonts) {
	 $hash{$font} = [];
	 @{$hash{$font}} = defoma_font_get_hints('x-postscript', $font);
       }




				March  4, 2001		     Defoma::Font(3pm)
