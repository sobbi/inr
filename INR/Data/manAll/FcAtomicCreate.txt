FcAtomicCreate(3)					     FcAtomicCreate(3)



NAME
       FcAtomicCreate - create an FcAtomic object

SYNOPSIS
       #include <fontconfig.h>

       FcAtomic * FcAtomicCreate(const FcChar8 *file);
       .fi

DESCRIPTION
       Creates	a  data  structure containing data needed to control access to
       file.  Writing is done to a separate file. Once that file is  complete,
       the  original configuration file is atomically replaced so that reading
       process always see a consistent and complete file without the  need  to
       lock for reading.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcAtomicCreate(3)
