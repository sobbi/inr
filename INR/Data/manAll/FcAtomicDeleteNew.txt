FcAtomicDeleteNew(3)					  FcAtomicDeleteNew(3)



NAME
       FcAtomicDeleteNew - delete new file

SYNOPSIS
       #include <fontconfig.h>

       void FcAtomicDeleteNew(FcAtomic *atomic);
       .fi

DESCRIPTION
       Deletes the new file. Used in error recovery to back out changes.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcAtomicDeleteNew(3)
