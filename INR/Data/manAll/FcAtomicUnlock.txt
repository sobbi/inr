FcAtomicUnlock(3)					     FcAtomicUnlock(3)



NAME
       FcAtomicUnlock - unlock a file

SYNOPSIS
       #include <fontconfig.h>

       void FcAtomicUnlock(FcAtomic *atomic);
       .fi

DESCRIPTION
       Unlocks the file.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcAtomicUnlock(3)
