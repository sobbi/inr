FcCharSetDestroy(3)					   FcCharSetDestroy(3)



NAME
       FcCharSetDestroy - Destroy a character set

SYNOPSIS
       #include <fontconfig.h>

       void FcCharSetDestroy(FcCharSet *fcs);
       .fi

DESCRIPTION
       FcCharSetDestroy  decrements  the reference count fcs. If the reference
       count becomes zero, all memory referenced is freed.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	   FcCharSetDestroy(3)
