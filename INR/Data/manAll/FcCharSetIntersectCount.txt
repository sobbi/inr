FcCharSetIntersectCount(3)			    FcCharSetIntersectCount(3)



NAME
       FcCharSetIntersectCount - Intersect and count charsets

SYNOPSIS
       #include <fontconfig.h>

       FcChar32 FcCharSetIntersectCount(const FcCharSet *a);
       (const FcCharSet *b);
       .fi

DESCRIPTION
       Returns the number of chars that are in both a and b.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009     FcCharSetIntersectCount(3)
