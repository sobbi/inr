FcCharSetNextPage(3)					  FcCharSetNextPage(3)



NAME
       FcCharSetNextPage - Continue enumerating charset contents

SYNOPSIS
       #include <fontconfig.h>

       FcChar32 FcCharSetNextPage(const FcCharSet *a);
       (FcChar32[FC_CHARSET_MAP_SIZE] map);
       (FcChar32 *next);
       .fi

DESCRIPTION
       Builds  an  array  of  bits  marking the Unicode coverage of a for page
       *next. Returns the base of the array. next contains the	next  page  in
       the font.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcCharSetNextPage(3)
