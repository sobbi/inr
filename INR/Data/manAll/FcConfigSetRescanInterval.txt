FcConfigSetRescanInterval(3)			  FcConfigSetRescanInterval(3)



NAME
       FcConfigSetRescanInterval - Set config rescan interval

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcConfigSetRescanInterval(FcConfig *config);
       (int rescanInterval);
       .fi

DESCRIPTION
       Sets the rescan interval. Returns FcFalse if the interval cannot be set
       (due to allocation failure). Otherwise  returns	FcTrue.   An  interval
       setting of zero disables automatic checks.  If config is NULL, the cur‐
       rent configuration is used.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009   FcConfigSetRescanInterval(3)
