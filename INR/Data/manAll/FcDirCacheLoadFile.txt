FcDirCacheLoadFile(3)					 FcDirCacheLoadFile(3)



NAME
       FcDirCacheLoadFile - load a cache file

SYNOPSIS
       #include <fontconfig.h>

       FcCache * FcDirCacheLoadFile(const FcChar8 *cache_file);
       (struct stat *file_stat);
       .fi

DESCRIPTION
       This  function loads a directory cache from cache_file. If file_stat is
       non-NULL, it will be filled with the results of stat(2)	on  the  cache
       file.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	 FcDirCacheLoadFile(3)
