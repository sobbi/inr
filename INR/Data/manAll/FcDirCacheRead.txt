FcDirCacheRead(3)					     FcDirCacheRead(3)



NAME
       FcDirCacheRead - read or construct a directory cache

SYNOPSIS
       #include <fontconfig.h>

       FcCache * FcDirCacheRead(const FcChar8 *dir);
       (FcBool force);
       (FcConfig *config);
       .fi

DESCRIPTION
       This  returns  a  cache for dir. If force is FcFalse, then an existing,
       valid cache file will be used. Otherwise, a new cache will  be  created
       by scanning the directory and that returned.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcDirCacheRead(3)
