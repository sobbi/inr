FcDirScan(3)							  FcDirScan(3)



NAME
       FcDirScan - scan a font directory without caching it

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcDirScan(FcFontSet *set);
       (FcStrSet *dirs);
       (FcFileCache *cache);
       (FcBlanks *blanks);
       (const FcChar8 *dir);
       (FcBool force);
       .fi

DESCRIPTION
       If  cache  is  not zero or if force is FcFalse, this function currently
       returns FcFalse. Otherwise, it scans an entire directory and  adds  all
       fonts  found to set.  Any subdirectories found are added to dirs. Call‐
       ing this function does not create any cache files. Use FcDirCacheRead()
       if caching is desired.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 		  FcDirScan(3)
