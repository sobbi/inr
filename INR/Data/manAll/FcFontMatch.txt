FcFontMatch(3)							FcFontMatch(3)



NAME
       FcFontMatch - Return best font

SYNOPSIS
       #include <fontconfig.h>

       FcPattern * FcFontMatch(FcConfig *config);
       (FcPattern *p);
       (FcResult *result);
       .fi

DESCRIPTION
       Finds  the  font  in sets most closely matching pattern and returns the
       result of FcFontRenderPrepare for that font and the  provided  pattern.
       This  function should be called only after FcConfigSubstitute and FcDe‐
       faultSubstitute have been called for p; otherwise the results will  not
       be correct.  If config is NULL, the current configuration is used.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 		FcFontMatch(3)
