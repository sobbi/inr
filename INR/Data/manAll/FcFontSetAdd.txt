FcFontSetAdd(3) 					       FcFontSetAdd(3)



NAME
       FcFontSetAdd - Add to a font set

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcFontSetAdd(FcFontSet *s);
       (FcPattern *font);
       .fi

DESCRIPTION
       Adds  a	pattern  to  a	font  set. Note that the pattern is not copied
       before being inserted into the set. Returns FcFalse if the pattern can‐
       not  be	inserted  into	the set (due to allocation failure). Otherwise
       returns FcTrue.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	       FcFontSetAdd(3)
