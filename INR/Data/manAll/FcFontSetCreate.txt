FcFontSetCreate(3)					    FcFontSetCreate(3)



NAME
       FcFontSetCreate - Create a font set

SYNOPSIS
       #include <fontconfig.h>

       FcFontSet * FcFontSetCreate(void);
       .fi

DESCRIPTION
       Creates an empty font set.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	    FcFontSetCreate(3)
