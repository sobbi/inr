FcFontSetMatch(3)					     FcFontSetMatch(3)



NAME
       FcFontSetMatch - Return the best font from a set of font sets

SYNOPSIS
       #include <fontconfig.h>

       FcPattern * FcFontSetMatch(FcConfig *config);
       (FcFontSet **sets);
       (intnsets);
       (FcPattern *pattern);
       (FcResult *result);
       .fi

DESCRIPTION
       Finds  the  font  in sets most closely matching pattern and returns the
       result of FcFontRenderPrepare for that font and the  provided  pattern.
       This  function should be called only after FcConfigSubstitute and FcDe‐
       faultSubstitute have been called for  pattern;  otherwise  the  results
       will  not  be correct.  If config is NULL, the current configuration is
       used.  Returns NULL if an error occurs during this process.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcFontSetMatch(3)
