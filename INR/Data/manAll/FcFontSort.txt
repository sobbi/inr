FcFontSort(3)							 FcFontSort(3)



NAME
       FcFontSort - Return list of matching fonts

SYNOPSIS
       #include <fontconfig.h>

       FcFontSet * FcFontSort(FcConfig *config);
       (FcPattern *p);
       (FcBool trim);
       (FcCharSet **csp);
       (FcResult *result);
       .fi

DESCRIPTION
       Returns	the list of fonts sorted by closeness to p. If trim is FcTrue,
       elements in the list which don't include Unicode coverage not  provided
       by earlier elements in the list are elided. The union of Unicode cover‐
       age of all of the fonts is returned in csp, if csp is  not  NULL.  This
       function  should be called only after FcConfigSubstitute and FcDefault‐
       Substitute have been called for p; otherwise the results  will  not  be
       correct.

       The  returned  FcFontSet  references  FcPattern structures which may be
       shared by the return value from multiple FcFontSort calls, applications
       must  not  modify these patterns. Instead, they should be passed, along
       with p to FcFontRenderPrepare which combines them into a complete  pat‐
       tern.

       The  FcFontSet returned by FcFontSort is destroyed by caling FcFontSet‐
       Destroy.  If config is NULL, the current configuration is used.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 		 FcFontSort(3)
