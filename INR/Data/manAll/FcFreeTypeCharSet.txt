FcFreeTypeCharSet(3)					  FcFreeTypeCharSet(3)



NAME
       FcFreeTypeCharSet - compute unicode coverage

SYNOPSIS
       #include <fontconfig.h>
       #include <fcfreetype.h>

       FcCharSet * FcFreeTypeCharSet(FT_Face face);
       (FcBlanks *blanks);
       .fi

DESCRIPTION
       Scans  a  FreeType  face  and returns the set of encoded Unicode chars.
       This scans several encoding tables to build as complete a list as  pos‐
       sible.	If  'blanks' is not 0, the glyphs in the font are examined and
       any blank glyphs not in 'blanks' are not placed in the returned	FcCha‐
       rSet.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcFreeTypeCharSet(3)
