FcGetLangs(3)							 FcGetLangs(3)



NAME
       FcGetLangs - Get list of languages

SYNOPSIS
       #include <fontconfig.h>

       FcStrSet * FcGetLangs(void);
       .fi

DESCRIPTION
       Returns a string set of all known languages.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 		 FcGetLangs(3)
