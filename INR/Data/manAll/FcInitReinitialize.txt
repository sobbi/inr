FcInitReinitialize(3)					 FcInitReinitialize(3)



NAME
       FcInitReinitialize - re-initialize library

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcInitReinitialize(void);
       .fi

DESCRIPTION
       Forces  the  default  configuration  file to be reloaded and resets the
       default configuration. Returns FcFalse if the configuration  cannot  be
       reloaded  (due  to  config  file  errors,  allocation failures or other
       issues) and leaves  the	existing  configuration  unchanged.  Otherwise
       returns FcTrue.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	 FcInitReinitialize(3)
