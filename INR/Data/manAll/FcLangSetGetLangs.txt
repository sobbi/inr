FcLangSetGetLangs(3)					  FcLangSetGetLangs(3)



NAME
       FcLangSetGetLangs - get the list of languages in the langset

SYNOPSIS
       #include <fontconfig.h>

       FcStrSet * FcLangSetGetLangs(const FcLangSet *ls);
       .fi

DESCRIPTION
       Returns a string set of all languages in langset.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcLangSetGetLangs(3)
