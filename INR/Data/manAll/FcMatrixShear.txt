FcMatrixShear(3)					      FcMatrixShear(3)



NAME
       FcMatrixShear - Shear a matrix

SYNOPSIS
       #include <fontconfig.h>

       void FcMatrixShear(FcMatrix *matrix);
       (double sh);
       (double sv);
       .fi

DESCRIPTION
       FcMatrixShare  shears  matrix  horizontally by sh and vertically by sv.
       This is done by multiplying by the matrix:

	 1  sh
	 sv  1


VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	      FcMatrixShear(3)
