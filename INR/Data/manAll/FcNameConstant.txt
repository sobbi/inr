FcNameConstant(3)					     FcNameConstant(3)



NAME
       FcNameConstant - Get the value for a symbolic constant

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcNameConstant(FcChar8 *string);
       (int *result);
       .fi

DESCRIPTION
       Returns	whether  a  symbolic  constant with name string is registered,
       placing the value of the constant in result if present.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcNameConstant(3)
