FcNameGetConstant(3)					  FcNameGetConstant(3)



NAME
       FcNameGetConstant - Lookup symbolic constant

SYNOPSIS
       #include <fontconfig.h>

       const FcConstant * FcNameGetConstant(FcChar8 *string);
       .fi

DESCRIPTION
       Return the FcConstant structure related to symbolic constant string.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcNameGetConstant(3)
