FcNameUnparse(3)					      FcNameUnparse(3)



NAME
       FcNameUnparse - Convert a pattern back into a string that can be parsed

SYNOPSIS
       #include <fontconfig.h>

       FcChar8 * FcNameUnparse(FcPattern *pat);
       .fi

DESCRIPTION
       Converts  the  given  pattern  into  the standard text format described
       above.  The return value is not static, but  instead  refers  to  newly
       allocated memory which should be freed by the caller using free().

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	      FcNameUnparse(3)
