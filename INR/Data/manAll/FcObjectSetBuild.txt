FcObjectSetBuild(3)					   FcObjectSetBuild(3)



NAME
       FcObjectSetBuild,   FcObjectSetVaBuild,	 FcObjectSetVapBuild  -  Build
       object set from args

SYNOPSIS
       #include <fontconfig.h>

       FcObjectSet * FcObjectSetBuild(const char *first);
       (...);

       FcObjectSet * FcObjectSetVaBuild(const char *first);
       (va_list va);

       void FcObjectSetVapBuild(FcObjectSet *result);
       (const char *first);
       (va_list va);
       .fi

DESCRIPTION
       These build an object set  from	a  null-terminated  list  of  property
       names.	FcObjectSetVapBuild  is  a macro version of FcObjectSetVaBuild
       which returns the result in the result variable directly.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	   FcObjectSetBuild(3)
