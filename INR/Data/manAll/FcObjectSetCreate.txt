FcObjectSetCreate(3)					  FcObjectSetCreate(3)



NAME
       FcObjectSetCreate - Create an object set

SYNOPSIS
       #include <fontconfig.h>

       FcObjectSet * FcObjectSetCreate(void);
       .fi

DESCRIPTION
       Creates an empty set.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	  FcObjectSetCreate(3)
