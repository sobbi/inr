FcPatternAddWeak(3)					   FcPatternAddWeak(3)



NAME
       FcPatternAddWeak - Add a value to a pattern with weak binding

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcPatternAddWeak(FcPattern *p);
       (const char *object);
       (FcValue value);
       (FcBool append);
       .fi

DESCRIPTION
       FcPatternAddWeak  is  essentially  the same as FcPatternAdd except that
       any values added to the list have binding weak instead of strong.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	   FcPatternAddWeak(3)
