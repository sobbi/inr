FcStrSetDestroy(3)					    FcStrSetDestroy(3)



NAME
       FcStrSetDestroy - destroy a string set

SYNOPSIS
       #include <fontconfig.h>

       void FcStrSetDestroy(FcStrSet *set);
       .fi

DESCRIPTION
       Destroys set.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	    FcStrSetDestroy(3)
