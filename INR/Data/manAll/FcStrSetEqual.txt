FcStrSetEqual(3)					      FcStrSetEqual(3)



NAME
       FcStrSetEqual - check sets for equality

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcStrSetEqual(FcStrSet *set_a);
       (FcStrSet *set_b);
       .fi

DESCRIPTION
       Returns	whether  set_a	contains  precisely the same strings as set_b.
       Ordering of strings within the two sets is not considered.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	      FcStrSetEqual(3)
