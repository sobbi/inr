FcStrSetMember(3)					     FcStrSetMember(3)



NAME
       FcStrSetMember - check set for membership

SYNOPSIS
       #include <fontconfig.h>

       FcBool FcStrSetMember(FcStrSet *set);
       (const FcChar8 *s);
       .fi

DESCRIPTION
       Returns whether s is a member of set.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	     FcStrSetMember(3)
