FcUtf8ToUcs4(3) 					       FcUtf8ToUcs4(3)



NAME
       FcUtf8ToUcs4 - convert UTF-8 to UCS4

SYNOPSIS
       #include <fontconfig.h>

       int FcUtf8ToUcs4(FcChar8 *src);
       (FcChar32 *dst);
       (int len);
       .fi

DESCRIPTION
       Converts the next Unicode char from src into dst and returns the number
       of bytes containing the char. src must be at least len bytes long.

VERSION
       Fontconfig version 2.8.0



			       18 November 2009 	       FcUtf8ToUcs4(3)
