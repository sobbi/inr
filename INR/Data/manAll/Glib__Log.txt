Glib::Log(3pm)						    User Contributed Perl Documentation 					     Glib::Log(3pm)



NAME
       Glib::Log -  A flexible logging mechanism

METHODS
   scalar = Glib::Log->set_always_fatal ($fatal_mask)
       ·   $fatal_mask (scalar)

   Glib->critical ($domain, $message)
       ·   $domain (string or undef)

       ·   $message (string)

   Glib->error ($domain, $message)
       ·   $domain (string or undef)

       ·   $message (string)

   scalar = Glib::Log->set_fatal_mask ($log_domain, $fatal_mask)
       ·   $log_domain (string)

       ·   $fatal_mask (scalar)

   integer = Glib::Log->set_handler ($log_domain, $log_levels, $log_func, $user_data=undef)
       ·   $log_domain (string or undef) name of the domain to handle with this callback.

       ·   $log_levels (Glib::LogLevelFlags) log levels to handle with this callback

       ·   $log_func (subroutine) handler function

       ·   $user_data (scalar)

   Glib->log ($log_domain, $log_level, $message)
       ·   $log_domain (string or undef)

       ·   $log_level (scalar)

       ·   $message (string)

   Glib->message ($domain, $message)
       ·   $domain (string or undef)

       ·   $message (string)

   Glib::Log->remove_handler ($log_domain, $handler_id)
       ·   $log_domain (string or undef)

       ·   $handler_id (integer) as returned by "set_handler"

   Glib->warning ($domain, $message)
       ·   $domain (string or undef)

       ·   $message (string)

ENUMS AND FLAGS
   flags Glib::LogLevelFlags
       ·   'recursion' / 'G_LOG_FLAG_RECURSION'

       ·   'fatal' / 'G_LOG_FLAG_FATAL'

       ·   'error' / 'G_LOG_LEVEL_ERROR'

       ·   'critical' / 'G_LOG_LEVEL_CRITICAL'

       ·   'warning' / 'G_LOG_LEVEL_WARNING'

       ·   'message' / 'G_LOG_LEVEL_MESSAGE'

       ·   'info' / 'G_LOG_LEVEL_INFO'

       ·   'debug' / 'G_LOG_LEVEL_DEBUG'

       ·   'fatal-mask' / 'G_LOG_FATAL_MASK'

SEE ALSO
       Glib

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Glib for a full notice.



perl v5.10.0								 2009-11-05							     Glib::Log(3pm)
