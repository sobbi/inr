Glib::Param::Enum(3pm)					    User Contributed Perl Documentation 				     Glib::Param::Enum(3pm)



NAME
       Glib::Param::Enum -  Wrapper for enum parameters in GLib

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::Enum

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::Enum

METHODS
   scalar = $pspec_enum->get_default_value
   string = $pspec_enum->get_enum_class
SEE ALSO
       Glib, Glib::ParamSpec, Glib::ParamSpec

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Glib for a full notice.



perl v5.10.0								 2009-11-05						     Glib::Param::Enum(3pm)
