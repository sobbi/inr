Glib::Param::String(3pm)				    User Contributed Perl Documentation 				   Glib::Param::String(3pm)



NAME
       Glib::Param::String -  Wrapper for string parameters in GLib

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::String

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::String

METHODS
   string = $pspec_string->get_default_value
SEE ALSO
       Glib, Glib::ParamSpec, Glib::ParamSpec

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Glib for a full notice.



perl v5.10.0								 2009-11-05						   Glib::Param::String(3pm)
