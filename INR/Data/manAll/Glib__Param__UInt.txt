Glib::Param::UInt(3pm)					    User Contributed Perl Documentation 				     Glib::Param::UInt(3pm)



NAME
       Glib::Param::UInt -  Wrapper for uint parameters in GLib

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::UInt

DESCRIPTION
       This page documents the extra accessors available for all of the unsigned integer type paramspecs: UChar, UInt, and ULong.  Perl really only
       supports full-size integers, so all of these methods return UVs; the distinction of integer size is important to the underlying C library and also
       determines the data value range.

HIERARCHY
	 Glib::ParamSpec
	 +----Glib::Param::UChar

	 Glib::ParamSpec
	 +----Glib::Param::UInt

	 Glib::ParamSpec
	 +----Glib::Param::ULong

METHODS
   unsigned = $pspec->get_default_value
   unsigned = $pspec->get_maximum
   unsigned = $pspec->get_minimum
SEE ALSO
       Glib, Glib::ParamSpec, Glib::ParamSpec

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Glib for a full notice.



perl v5.10.0								 2009-11-05						     Glib::Param::UInt(3pm)
