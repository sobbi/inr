Gnome2::Canvas::RE(3pm) 				    User Contributed Perl Documentation 				    Gnome2::Canvas::RE(3pm)



NAME
       Gnome2::Canvas::RE - base class for rectangles and ellipses

HIERARCHY
	 Glib::Object
	 +----Glib::InitiallyUnowned
	      +----Gtk2::Object
		   +----Gnome2::Canvas::Item
			+----Gnome2::Canvas::Shape
			     +----Gnome2::Canvas::RE

PROPERTIES
       'x1' (double : readable / writable)
       'x2' (double : readable / writable)
       'y1' (double : readable / writable)
       'y2' (double : readable / writable)

SEE ALSO
       Gnome2::Canvas, Glib::Object, Glib::InitiallyUnowned, Gtk2::Object, Gnome2::Canvas::Item, Gnome2::Canvas::Shape

COPYRIGHT
       Copyright (C) 2003-2004 by the Gtk2-Perl Team.

       This software is licensed under the LGPL; see Gnome2::Canvas for a full notice.



perl v5.10.1								 2010-03-06						    Gnome2::Canvas::RE(3pm)
