Gnome2::Config::Iterator(3pm)				    User Contributed Perl Documentation 			      Gnome2::Config::Iterator(3pm)



NAME
       Gnome2::Config::Iterator

METHODS
   list = $handle->next
       Returns the new GnomeConfigIterator, the key, and the value.

SEE ALSO
       Gnome2

COPYRIGHT
       Copyright (C) 2003-2004 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2 for a full notice.



perl v5.10.1								 2010-03-06					      Gnome2::Config::Iterator(3pm)
