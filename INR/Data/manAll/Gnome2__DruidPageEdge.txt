Gnome2::DruidPageEdge(3pm)				    User Contributed Perl Documentation 				 Gnome2::DruidPageEdge(3pm)



NAME
       Gnome2::DruidPageEdge - wrapper for GnomeDruidPageEdge

HIERARCHY
	 Glib::Object
	 +----Glib::InitiallyUnowned
	      +----Gtk2::Object
		   +----Gtk2::Widget
			+----Gtk2::Container
			     +----Gtk2::Bin
				  +----Gnome2::DruidPage
				       +----Gnome2::DruidPageEdge

INTERFACES
	 Glib::Object::_Unregistered::AtkImplementorIface
	 Gtk2::Buildable

METHODS
   widget = Gnome2::DruidPageEdge->new ($position)
       ·   $position (Gnome2::EdgePosition)

   widget = Gnome2::DruidPageEdge->new_aa ($position)
       ·   $position (Gnome2::EdgePosition)

   widget = Gnome2::DruidPageEdge->new_with_vals ($position, $antialiased, $title=undef, $text=undef, $logo=undef, $watermark=undef, $top_watermark=undef)
       ·   $position (Gnome2::EdgePosition)

       ·   $antialiased (boolean)

       ·   $title (string)

       ·   $text (string)

       ·   $logo (Gtk2::Gdk::Pixbuf or undef)

       ·   $watermark (Gtk2::Gdk::Pixbuf or undef)

       ·   $top_watermark (Gtk2::Gdk::Pixbuf or undef)

   $druid_page_edge->set_bg_color ($color)
       ·   $color (Gtk2::Gdk::Color)

   $druid_page_edge->set_logo_bg_color ($color)
       ·   $color (Gtk2::Gdk::Color)

   $druid_page_edge->set_logo ($logo_image)
       ·   $logo_image (Gtk2::Gdk::Pixbuf or undef)

   $druid_page_edge->set_text_color ($color)
       ·   $color (Gtk2::Gdk::Color)

   $druid_page_edge->set_text ($text)
       ·   $text (string)

   $druid_page_edge->set_textbox_color ($color)
       ·   $color (Gtk2::Gdk::Color)

   $druid_page_edge->set_title_color ($color)
       ·   $color (Gtk2::Gdk::Color)

   $druid_page_edge->set_title ($title)
       ·   $title (string)

   $druid_page_edge->set_top_watermark ($top_watermark_image)
       ·   $top_watermark_image (Gtk2::Gdk::Pixbuf or undef)

   $druid_page_edge->set_watermark ($watermark)
       ·   $watermark (Gtk2::Gdk::Pixbuf or undef)

ENUMS AND FLAGS
   enum Gnome2::EdgePosition
       ·   'start' / 'GNOME_EDGE_START'

       ·   'finish' / 'GNOME_EDGE_FINISH'

       ·   'other' / 'GNOME_EDGE_OTHER'

       ·   'last' / 'GNOME_EDGE_LAST'

SEE ALSO
       Gnome2, Glib::Object, Glib::InitiallyUnowned, Gtk2::Object, Gtk2::Widget, Gtk2::Container, Gtk2::Bin, Gnome2::DruidPage

COPYRIGHT
       Copyright (C) 2003-2004 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2 for a full notice.



perl v5.10.1								 2010-03-06						 Gnome2::DruidPageEdge(3pm)
