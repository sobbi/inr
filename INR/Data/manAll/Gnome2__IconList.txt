Gnome2::IconList(3pm)					    User Contributed Perl Documentation 				      Gnome2::IconList(3pm)



NAME
       Gnome2::IconList - wrapper for GnomeIconList

HIERARCHY
	 Glib::Object
	 +----Glib::InitiallyUnowned
	      +----Gtk2::Object
		   +----Gtk2::Widget
			+----Gtk2::Container
			     +----Gtk2::Layout
				  +----Gnome2::Canvas
				       +----Gnome2::IconList

INTERFACES
	 Glib::Object::_Unregistered::AtkImplementorIface
	 Gtk2::Buildable

METHODS
   widget = Gnome2::IconList->new ($icon_width, $adj, $flags)
       ·   $icon_width (integer)

       ·   $adj (Gtk2::Adjustment)

       ·   $flags (Gnome2::IconListFlags)

   integer = $gil->append ($icon_filename, $text)
       ·   $icon_filename (string)

       ·   $text (string)

   integer = $gil->append_pixbuf ($im, $icon_filename, $text)
       ·   $im (Gtk2::Gdk::Pixbuf)

       ·   $icon_filename (string)

       ·   $text (string)

   $gil->clear
   $gil->set_col_spacing ($pixels)
       ·   $pixels (integer)

   integer = $gil->find_icon_from_filename ($filename)
       ·   $filename (string)

   $gil->focus_icon ($idx)
       ·   $idx (integer)

   $gil->freeze
   $gil->set_hadjustment ($hadj)
       ·   $hadj (Gtk2::Adjustment)

   integer = $gil->get_icon_at ($x, $y)
       ·   $x (integer)

       ·   $y (integer)

   $gil->set_icon_border ($pixels)
       ·   $pixels (integer)

   string = $gil->get_icon_filename ($idx)
       ·   $idx (integer)

   visibility = $gil->icon_is_visible ($pos)
       ·   $pos (integer)

   object = $gil->get_icon_pixbuf_item ($idx)
       ·   $idx (integer)

   icontextitem = $gil->get_icon_text_item ($idx)
       ·   $idx (integer)

   $gil->set_icon_width ($w)
       ·   $w (integer)

   $gil->insert ($pos, $icon_filename, $text)
       ·   $pos (integer)

       ·   $icon_filename (string)

       ·   $text (string)

   $gil->insert_pixbuf ($pos, $im, $icon_filename, $text)
       ·   $pos (integer)

       ·   $im (Gtk2::Gdk::Pixbuf)

       ·   $icon_filename (string)

       ·   $text (string)

   integer = $gil->get_items_per_line
   $gil->moveto ($pos, $yalign)
       ·   $pos (integer)

       ·   $yalign (double)

   integer = $gil->get_num_icons
   $gil->remove ($pos)
       ·   $pos (integer)

   $gil->set_row_spacing ($pixels)
       ·   $pixels (integer)

   $gil->select_all
       Since: libgnomeui 2.8

   $gil->select_icon ($pos)
       ·   $pos (integer)

   list = $gil->get_selection
       Returns a list of integers.

   selectionmode = $gil->get_selection_mode
   $gil->set_selection_mode ($mode)
       ·   $mode (Gtk2::SelectionMode)

   $gil->set_separators ($sep)
       ·   $sep (string)

   $gil->set_text_spacing ($pixels)
       ·   $pixels (integer)

   $gil->thaw
   integer = $gil->unselect_all
   $gil->unselect_icon ($pos)
       ·   $pos (integer)

   $gil->set_vadjustment ($vadj)
       ·   $vadj (Gtk2::Adjustment)

SIGNALS
       select-icon (Gnome2::IconList, integer, Gtk2::Gdk::Event)
       unselect-icon (Gnome2::IconList, integer, Gtk2::Gdk::Event)
       focus-icon (Gnome2::IconList, integer)
       boolean = text-changed (Gnome2::IconList, integer, string)
       move-cursor (Gnome2::IconList, Gtk2::DirectionType, boolean)
       toggle-cursor-selection (Gnome2::IconList)

ENUMS AND FLAGS
   enum Gtk2::DirectionType
       ·   'tab-forward' / 'GTK_DIR_TAB_FORWARD'

       ·   'tab-backward' / 'GTK_DIR_TAB_BACKWARD'

       ·   'up' / 'GTK_DIR_UP'

       ·   'down' / 'GTK_DIR_DOWN'

       ·   'left' / 'GTK_DIR_LEFT'

       ·   'right' / 'GTK_DIR_RIGHT'

   enum Gtk2::SelectionMode
       ·   'none' / 'GTK_SELECTION_NONE'

       ·   'single' / 'GTK_SELECTION_SINGLE'

       ·   'browse' / 'GTK_SELECTION_BROWSE'

       ·   'multiple' / 'GTK_SELECTION_MULTIPLE'

       ·   'extended' / 'GTK_SELECTION_EXTENDED'

   enum Gtk2::Visibility
       ·   'none' / 'GTK_VISIBILITY_NONE'

       ·   'partial' / 'GTK_VISIBILITY_PARTIAL'

       ·   'full' / 'GTK_VISIBILITY_FULL'

SEE ALSO
       Gnome2, Glib::Object, Glib::InitiallyUnowned, Gtk2::Object, Gtk2::Widget, Gtk2::Container, Gtk2::Layout, Gnome2::Canvas

COPYRIGHT
       Copyright (C) 2003-2004 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2 for a full notice.



perl v5.10.1								 2010-03-06						      Gnome2::IconList(3pm)
