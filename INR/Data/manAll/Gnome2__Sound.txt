Gnome2::Sound(3pm)					    User Contributed Perl Documentation 					 Gnome2::Sound(3pm)



NAME
       Gnome2::Sound

METHODS
   integer = Gnome2::Sound->connection_get
   Gnome2::Sound->init ($hostname="localhost")
       ·   $hostname (string)

   Gnome2::Sound->play ($filename)
       ·   $filename (string)

   integer = Gnome2::Sound->sample_load ($sample_name, $filename)
       ·   $sample_name (string)

       ·   $filename (string)

   Gnome2::Sound->shutdown
SEE ALSO
       Gnome2

COPYRIGHT
       Copyright (C) 2003-2004 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2 for a full notice.



perl v5.10.1								 2010-03-06							 Gnome2::Sound(3pm)
