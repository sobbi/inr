Gnome2::URL(3pm)					    User Contributed Perl Documentation 					   Gnome2::URL(3pm)



NAME
       Gnome2::URL

METHODS
   boolean = Gnome2::URL->show ($url)
       ·   $url (string)

       May croak with a Glib::Error in $@ on failure.

   boolean = Gnome2::URL->show_with_env ($url, $env_ref)
       ·   $url (string)

       ·   $env_ref (scalar)

       May croak with a Glib::Error in $@ on failure.

       Since: libgnome 2.2

SEE ALSO
       Gnome2

COPYRIGHT
       Copyright (C) 2003-2004 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2 for a full notice.



perl v5.10.1								 2010-03-06							   Gnome2::URL(3pm)
