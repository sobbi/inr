Gnome2::VFS::Async(3pm) 				    User Contributed Perl Documentation 				    Gnome2::VFS::Async(3pm)



NAME
       Gnome2::VFS::Async - handles asynchronous file operations

METHODS
   handle = Gnome2::VFS::Async->create ($text_uri, $open_mode, $exclusive, $perm, $priority, $func, $data=undef)
       ·   $text_uri (string)

       ·   $open_mode (Gnome2::VFS::OpenMode)

       ·   $exclusive (boolean)

       ·   $perm (integer)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->create_symbolic_link ($uri, $uri_reference, $priority, $func, $data=undef)
       ·   $uri (Gnome2::VFS::URI)

       ·   $uri_reference (string)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->create_uri ($uri, $open_mode, $exclusive, $perm, $priority, $func, $data=undef)
       ·   $uri (Gnome2::VFS::URI)

       ·   $open_mode (Gnome2::VFS::OpenMode)

       ·   $exclusive (boolean)

       ·   $perm (integer)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->get_file_info ($uri_ref, $options, $priority, $func, $data=undef)
       ·   $uri_ref (scalar)

       ·   $options (Gnome2::VFS::FileInfoOptions)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->set_file_info ($uri, $info, $mask, $options, $priority, $func, $data=undef)
       ·   $uri (Gnome2::VFS::URI)

       ·   $info (Gnome2::VFS::FileInfo)

       ·   $mask (Gnome2::VFS::SetFileInfoMask)

       ·   $options (Gnome2::VFS::FileInfoOptions)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->find_directory ($near_ref, $kind, $create_if_needed, $find_if_needed, $permissions, $priority, $func, $data=undef)
       ·   $near_ref (scalar)

       ·   $kind (Gnome2::VFS::FindDirectoryKind)

       ·   $create_if_needed (boolean)

       ·   $find_if_needed (boolean)

       ·   $permissions (integer)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   integer = Gnome2::VFS::Async->get_job_limit
   Gnome2::VFS::Async->set_job_limit ($limit)
       ·   $limit (integer)

   handle = Gnome2::VFS::Async->load_directory ($text_uri, $options, $items_per_notification, $priority, $func, $data=undef)
       ·   $text_uri (string)

       ·   $options (Gnome2::VFS::FileInfoOptions)

       ·   $items_per_notification (integer)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->load_directory_uri ($uri, $options, $items_per_notification, $priority, $func, $data=undef)
       ·   $uri (Gnome2::VFS::URI)

       ·   $options (Gnome2::VFS::FileInfoOptions)

       ·   $items_per_notification (integer)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->open ($text_uri, $open_mode, $priority, $func, $data=undef)
       ·   $text_uri (string)

       ·   $open_mode (Gnome2::VFS::OpenMode)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   handle = Gnome2::VFS::Async->open_uri ($uri, $open_mode, $priority, $func, $data=undef)
       ·   $uri (Gnome2::VFS::URI)

       ·   $open_mode (Gnome2::VFS::OpenMode)

       ·   $priority (integer)

       ·   $func (scalar)

       ·   $data (scalar)

   list = Gnome2::VFS::Async->xfer ($source_ref, $target_ref, $xfer_options, $error_mode, $overwrite_mode, $priority, $func_update, $data_update,
       $func_sync, $data_sync=undef)
       ·   $source_ref (scalar)

       ·   $target_ref (scalar)

       ·   $xfer_options (Gnome2::VFS::XferOptions)

       ·   $error_mode (Gnome2::VFS::XferErrorMode)

       ·   $overwrite_mode (Gnome2::VFS::XferOverwriteMode)

       ·   $priority (integer)

       ·   $func_update (scalar)

       ·   $data_update (scalar)

       ·   $func_sync (scalar)

       ·   $data_sync (scalar)

       Returns a GnomeVFSResult and a GnomeVFSAsyncHandle.

ENUMS AND FLAGS
   flags Gnome2::VFS::FileInfoOptions
       ·   'default' / 'GNOME_VFS_FILE_INFO_DEFAULT'

       ·   'get-mime-type' / 'GNOME_VFS_FILE_INFO_GET_MIME_TYPE'

       ·   'force-fast-mime-type' / 'GNOME_VFS_FILE_INFO_FORCE_FAST_MIME_TYPE'

       ·   'force-slow-mime-type' / 'GNOME_VFS_FILE_INFO_FORCE_SLOW_MIME_TYPE'

       ·   'follow-links' / 'GNOME_VFS_FILE_INFO_FOLLOW_LINKS'

       ·   'get-access-rights' / 'GNOME_VFS_FILE_INFO_GET_ACCESS_RIGHTS'

       ·   'name-only' / 'GNOME_VFS_FILE_INFO_NAME_ONLY'

       ·   'get-acl' / 'GNOME_VFS_FILE_INFO_GET_ACL'

       ·   'get-selinux-context' / 'GNOME_VFS_FILE_INFO_GET_SELINUX_CONTEXT'

   enum Gnome2::VFS::FindDirectoryKind
       ·   'desktop' / 'GNOME_VFS_DIRECTORY_KIND_DESKTOP'

       ·   'trash' / 'GNOME_VFS_DIRECTORY_KIND_TRASH'

   flags Gnome2::VFS::OpenMode
       ·   'none' / 'GNOME_VFS_OPEN_NONE'

       ·   'read' / 'GNOME_VFS_OPEN_READ'

       ·   'write' / 'GNOME_VFS_OPEN_WRITE'

       ·   'random' / 'GNOME_VFS_OPEN_RANDOM'

       ·   'truncate' / 'GNOME_VFS_OPEN_TRUNCATE'

   flags Gnome2::VFS::SetFileInfoMask
       ·   'none' / 'GNOME_VFS_SET_FILE_INFO_NONE'

       ·   'name' / 'GNOME_VFS_SET_FILE_INFO_NAME'

       ·   'permissions' / 'GNOME_VFS_SET_FILE_INFO_PERMISSIONS'

       ·   'owner' / 'GNOME_VFS_SET_FILE_INFO_OWNER'

       ·   'time' / 'GNOME_VFS_SET_FILE_INFO_TIME'

       ·   'acl' / 'GNOME_VFS_SET_FILE_INFO_ACL'

       ·   'selinux-context' / 'GNOME_VFS_SET_FILE_INFO_SELINUX_CONTEXT'

       ·   'symlink-name' / 'GNOME_VFS_SET_FILE_INFO_SYMLINK_NAME'

   enum Gnome2::VFS::XferErrorMode
       ·   'abort' / 'GNOME_VFS_XFER_ERROR_MODE_ABORT'

       ·   'query' / 'GNOME_VFS_XFER_ERROR_MODE_QUERY'

   flags Gnome2::VFS::XferOptions
       ·   'default' / 'GNOME_VFS_XFER_DEFAULT'

       ·   'unused-1' / 'GNOME_VFS_XFER_UNUSED_1'

       ·   'follow-links' / 'GNOME_VFS_XFER_FOLLOW_LINKS'

       ·   'unused-2' / 'GNOME_VFS_XFER_UNUSED_2'

       ·   'recursive' / 'GNOME_VFS_XFER_RECURSIVE'

       ·   'samefs' / 'GNOME_VFS_XFER_SAMEFS'

       ·   'delete-items' / 'GNOME_VFS_XFER_DELETE_ITEMS'

       ·   'empty-directories' / 'GNOME_VFS_XFER_EMPTY_DIRECTORIES'

       ·   'new-unique-directory' / 'GNOME_VFS_XFER_NEW_UNIQUE_DIRECTORY'

       ·   'removesource' / 'GNOME_VFS_XFER_REMOVESOURCE'

       ·   'use-unique-names' / 'GNOME_VFS_XFER_USE_UNIQUE_NAMES'

       ·   'link-items' / 'GNOME_VFS_XFER_LINK_ITEMS'

       ·   'follow-links-recursive' / 'GNOME_VFS_XFER_FOLLOW_LINKS_RECURSIVE'

       ·   'target-default-perms' / 'GNOME_VFS_XFER_TARGET_DEFAULT_PERMS'

   enum Gnome2::VFS::XferOverwriteMode
       ·   'abort' / 'GNOME_VFS_XFER_OVERWRITE_MODE_ABORT'

       ·   'query' / 'GNOME_VFS_XFER_OVERWRITE_MODE_QUERY'

       ·   'replace' / 'GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE'

       ·   'skip' / 'GNOME_VFS_XFER_OVERWRITE_MODE_SKIP'

SEE ALSO
       Gnome2::VFS

COPYRIGHT
       Copyright (C) 2003-2007 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2::VFS for a full notice.



perl v5.10.1								 2010-03-06						    Gnome2::VFS::Async(3pm)
