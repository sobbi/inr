Gnome2::VFS::DNSSD::Resolve::Handle(3pm)		    User Contributed Perl Documentation 		   Gnome2::VFS::DNSSD::Resolve::Handle(3pm)



NAME
       Gnome2::VFS::DNSSD::Resolve::Handle - used for resolving services on the network

METHODS
   result = $handle->cancel
ENUMS AND FLAGS
   enum Gnome2::VFS::Result
       ·   'ok' / 'GNOME_VFS_OK'

       ·   'error-not-found' / 'GNOME_VFS_ERROR_NOT_FOUND'

       ·   'error-generic' / 'GNOME_VFS_ERROR_GENERIC'

       ·   'error-internal' / 'GNOME_VFS_ERROR_INTERNAL'

       ·   'error-bad-parameters' / 'GNOME_VFS_ERROR_BAD_PARAMETERS'

       ·   'error-not-supported' / 'GNOME_VFS_ERROR_NOT_SUPPORTED'

       ·   'error-io' / 'GNOME_VFS_ERROR_IO'

       ·   'error-corrupted-data' / 'GNOME_VFS_ERROR_CORRUPTED_DATA'

       ·   'error-wrong-format' / 'GNOME_VFS_ERROR_WRONG_FORMAT'

       ·   'error-bad-file' / 'GNOME_VFS_ERROR_BAD_FILE'

       ·   'error-too-big' / 'GNOME_VFS_ERROR_TOO_BIG'

       ·   'error-no-space' / 'GNOME_VFS_ERROR_NO_SPACE'

       ·   'error-read-only' / 'GNOME_VFS_ERROR_READ_ONLY'

       ·   'error-invalid-uri' / 'GNOME_VFS_ERROR_INVALID_URI'

       ·   'error-not-open' / 'GNOME_VFS_ERROR_NOT_OPEN'

       ·   'error-invalid-open-mode' / 'GNOME_VFS_ERROR_INVALID_OPEN_MODE'

       ·   'error-access-denied' / 'GNOME_VFS_ERROR_ACCESS_DENIED'

       ·   'error-too-many-open-files' / 'GNOME_VFS_ERROR_TOO_MANY_OPEN_FILES'

       ·   'error-eof' / 'GNOME_VFS_ERROR_EOF'

       ·   'error-not-a-directory' / 'GNOME_VFS_ERROR_NOT_A_DIRECTORY'

       ·   'error-in-progress' / 'GNOME_VFS_ERROR_IN_PROGRESS'

       ·   'error-interrupted' / 'GNOME_VFS_ERROR_INTERRUPTED'

       ·   'error-file-exists' / 'GNOME_VFS_ERROR_FILE_EXISTS'

       ·   'error-loop' / 'GNOME_VFS_ERROR_LOOP'

       ·   'error-not-permitted' / 'GNOME_VFS_ERROR_NOT_PERMITTED'

       ·   'error-is-directory' / 'GNOME_VFS_ERROR_IS_DIRECTORY'

       ·   'error-no-memory' / 'GNOME_VFS_ERROR_NO_MEMORY'

       ·   'error-host-not-found' / 'GNOME_VFS_ERROR_HOST_NOT_FOUND'

       ·   'error-invalid-host-name' / 'GNOME_VFS_ERROR_INVALID_HOST_NAME'

       ·   'error-host-has-no-address' / 'GNOME_VFS_ERROR_HOST_HAS_NO_ADDRESS'

       ·   'error-login-failed' / 'GNOME_VFS_ERROR_LOGIN_FAILED'

       ·   'error-cancelled' / 'GNOME_VFS_ERROR_CANCELLED'

       ·   'error-directory-busy' / 'GNOME_VFS_ERROR_DIRECTORY_BUSY'

       ·   'error-directory-not-empty' / 'GNOME_VFS_ERROR_DIRECTORY_NOT_EMPTY'

       ·   'error-too-many-links' / 'GNOME_VFS_ERROR_TOO_MANY_LINKS'

       ·   'error-read-only-file-system' / 'GNOME_VFS_ERROR_READ_ONLY_FILE_SYSTEM'

       ·   'error-not-same-file-system' / 'GNOME_VFS_ERROR_NOT_SAME_FILE_SYSTEM'

       ·   'error-name-too-long' / 'GNOME_VFS_ERROR_NAME_TOO_LONG'

       ·   'error-service-not-available' / 'GNOME_VFS_ERROR_SERVICE_NOT_AVAILABLE'

       ·   'error-service-obsolete' / 'GNOME_VFS_ERROR_SERVICE_OBSOLETE'

       ·   'error-protocol-error' / 'GNOME_VFS_ERROR_PROTOCOL_ERROR'

       ·   'error-no-master-browser' / 'GNOME_VFS_ERROR_NO_MASTER_BROWSER'

       ·   'error-no-default' / 'GNOME_VFS_ERROR_NO_DEFAULT'

       ·   'error-no-handler' / 'GNOME_VFS_ERROR_NO_HANDLER'

       ·   'error-parse' / 'GNOME_VFS_ERROR_PARSE'

       ·   'error-launch' / 'GNOME_VFS_ERROR_LAUNCH'

       ·   'error-timeout' / 'GNOME_VFS_ERROR_TIMEOUT'

       ·   'error-nameserver' / 'GNOME_VFS_ERROR_NAMESERVER'

       ·   'error-locked' / 'GNOME_VFS_ERROR_LOCKED'

       ·   'error-deprecated-function' / 'GNOME_VFS_ERROR_DEPRECATED_FUNCTION'

       ·   'error-invalid-filename' / 'GNOME_VFS_ERROR_INVALID_FILENAME'

       ·   'error-not-a-symbolic-link' / 'GNOME_VFS_ERROR_NOT_A_SYMBOLIC_LINK'

       ·   'num-errors' / 'GNOME_VFS_NUM_ERRORS'

SEE ALSO
       Gnome2::VFS

COPYRIGHT
       Copyright (C) 2003-2007 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2::VFS for a full notice.



perl v5.10.1								 2010-03-06				   Gnome2::VFS::DNSSD::Resolve::Handle(3pm)
