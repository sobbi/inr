Gnome2::VFS::FileInfo(3pm)				    User Contributed Perl Documentation 				 Gnome2::VFS::FileInfo(3pm)



NAME
       Gnome2::VFS::FileInfo - stores information about files, like stat

METHODS
   fileinfo = Gnome2::VFS::FileInfo->new ($hash_ref)
       ·   $hash_ref (scalar)

       Creates a new GnomeVFSFileInfo object from hash_ref for use with Gnome2::VFS::FileInfo::matches, for example.  Normally, you can always directly use
       a hash reference if you're asked for a GnomeVFSFileInfo.

   boolean = $a->matches ($b)
       ·   $b (Gnome2::VFS::FileInfo)

   string = $info->get_mime_type
SEE ALSO
       Gnome2::VFS

COPYRIGHT
       Copyright (C) 2003-2007 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2::VFS for a full notice.



perl v5.10.1								 2010-03-06						 Gnome2::VFS::FileInfo(3pm)
