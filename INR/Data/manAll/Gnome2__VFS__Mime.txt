Gnome2::VFS::Mime(3pm)					    User Contributed Perl Documentation 				     Gnome2::VFS::Mime(3pm)



NAME
       Gnome2::VFS::Mime - functions for getting information about applications and components associated with MIME types

METHODS
   boolean = Gnome2::VFS::Mime->id_in_application_list ($id, ...)
       ·   $id (string)

       ·   ... (list) of GnomeVFSMimeApplication's

   list = Gnome2::VFS::Mime->id_list_from_application_list (...)
       ·   ... (list) of GnomeVFSMimeApplication's

       Returns a list of application id's.

   string = Gnome2::VFS->get_mime_type_for_data ($data)
       ·   $data (scalar)

   string = Gnome2::VFS->get_mime_type_for_name_and_data ($filename, $data)
       ·   $filename (string)

       ·   $data (scalar)

       Since: vfs 2.14

   string = Gnome2::VFS->get_mime_type_for_name ($filename)
       ·   $filename (string)

       Since: vfs 2.14

   string = Gnome2::VFS->get_mime_type ($text_uri)
       ·   $text_uri (string)

   list = Gnome2::VFS::Mime->remove_application_from_list ($application_id, ...)
       ·   $application_id (string)

       ·   ... (list) of GnomeVFSMimeApplication's

       Returns a boolean indicating whether anything was removed and the resulting list of GnomeVFSMimeApplication's.

   string = Gnome2::VFS->get_slow_mime_type ($text_uri)
       ·   $text_uri (string)

       Since: vfs 2.14

SEE ALSO
       Gnome2::VFS

COPYRIGHT
       Copyright (C) 2003-2007 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2::VFS for a full notice.



perl v5.10.1								 2010-03-06						     Gnome2::VFS::Mime(3pm)
