Gnome2::VFS::Monitor(3pm)				    User Contributed Perl Documentation 				  Gnome2::VFS::Monitor(3pm)



NAME
       Gnome2::VFS::Monitor - Monitors volume mounts and unmounts

METHODS
   list = Gnome2::VFS::Monitor->add ($text_uri, $monitor_type, $func, $data=undef)
       ·   $text_uri (string)

       ·   $monitor_type (Gnome2::VFS::MonitorType)

       ·   $func (scalar)

       ·   $data (scalar)

       Returns a GnomeVFSResult and a GnomeVFSMonitorHandle.

ENUMS AND FLAGS
   enum Gnome2::VFS::MonitorType
       ·   'file' / 'GNOME_VFS_MONITOR_FILE'

       ·   'directory' / 'GNOME_VFS_MONITOR_DIRECTORY'

SEE ALSO
       Gnome2::VFS

COPYRIGHT
       Copyright (C) 2003-2007 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Gnome2::VFS for a full notice.



perl v5.10.1								 2010-03-06						  Gnome2::VFS::Monitor(3pm)
