Pango::AttrBackground(3pm)				    User Contributed Perl Documentation 				 Pango::AttrBackground(3pm)



NAME
       Pango::AttrBackground - Pango background color attribute

METHODS
   attribute = Pango::AttrBackground->new ($red, $green, $blue, ...)
       ·   $red (unsigned)

       ·   $green (integer)

       ·   $blue (unsigned)

       ·   ... (list)

SEE ALSO
       Pango

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						 Pango::AttrBackground(3pm)
