Pango::AttrScale(3pm)					    User Contributed Perl Documentation 				      Pango::AttrScale(3pm)



NAME
       Pango::AttrScale - Pango font size scale attribute

METHODS
   attribute = Pango::AttrScale->new ($scale, ...)
       ·   $scale (double)

       ·   ... (list)

   double = $attr->value (...)
       ·   ... (list)

SEE ALSO
       Pango

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						      Pango::AttrScale(3pm)
