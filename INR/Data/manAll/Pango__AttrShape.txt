Pango::AttrShape(3pm)					    User Contributed Perl Documentation 				      Pango::AttrShape(3pm)



NAME
       Pango::AttrShape - Pango shape attribute

METHODS
   attribute = Pango::AttrShape->new ($ink_rect, $logical_rect, ...)
       ·   $ink_rect (array reference)

       ·   $logical_rect (array reference)

       ·   ... (list)

   array reference = $attr->ink_rect (...)
       ·   ... (list)

   array reference = $attr->logical_rect (...)
       ·   ... (list)

SEE ALSO
       Pango

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						      Pango::AttrShape(3pm)
