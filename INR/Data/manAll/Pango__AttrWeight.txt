Pango::AttrWeight(3pm)					    User Contributed Perl Documentation 				     Pango::AttrWeight(3pm)



NAME
       Pango::AttrWeight - Pango font weight attribute

METHODS
   attribute = Pango::AttrWeight->new ($weight, ...)
       ·   $weight (Pango::Weight)

       ·   ... (list)

   weight = $attr->value (...)
       ·   ... (list)

ENUMS AND FLAGS
   enum Pango::Weight
       ·   'thin' / 'PANGO_WEIGHT_THIN'

       ·   'ultralight' / 'PANGO_WEIGHT_ULTRALIGHT'

       ·   'light' / 'PANGO_WEIGHT_LIGHT'

       ·   'book' / 'PANGO_WEIGHT_BOOK'

       ·   'normal' / 'PANGO_WEIGHT_NORMAL'

       ·   'medium' / 'PANGO_WEIGHT_MEDIUM'

       ·   'semibold' / 'PANGO_WEIGHT_SEMIBOLD'

       ·   'bold' / 'PANGO_WEIGHT_BOLD'

       ·   'ultrabold' / 'PANGO_WEIGHT_ULTRABOLD'

       ·   'heavy' / 'PANGO_WEIGHT_HEAVY'

       ·   'ultraheavy' / 'PANGO_WEIGHT_ULTRAHEAVY'

SEE ALSO
       Pango

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						     Pango::AttrWeight(3pm)
