Pango::Attribute(3pm)					    User Contributed Perl Documentation 				      Pango::Attribute(3pm)



NAME
       Pango::Attribute - Pango base class for attributes

HIERARCHY
	 Glib::Boxed
	 +----Pango::Attribute

METHODS
   integer = $attr->end_index (...)
       ·   ... (list)

   boolean = $attr1->equal ($attr2)
       ·   $attr2 (Pango::Attribute)

   integer = $attr->start_index (...)
       ·   ... (list)

SEE ALSO
       Pango, Glib::Boxed

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						      Pango::Attribute(3pm)
