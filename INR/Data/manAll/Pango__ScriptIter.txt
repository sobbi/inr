Pango::ScriptIter(3pm)					    User Contributed Perl Documentation 				     Pango::ScriptIter(3pm)



NAME
       Pango::ScriptIter - used to break a string of Unicode into runs by text

HIERARCHY
	 Glib::Boxed
	 +----Pango::ScriptIter

METHODS
   scriptiter = Pango::ScriptIter->new ($text)
       ·   $text (string)

       Since: pango 1.4

   boolean = $iter->next
       Since: pango 1.4

   list = $iter->get_range
       Returns the bounds and the script for the region pointed to by $iter.

       Since: pango 1.4

SEE ALSO
       Pango, Glib::Boxed

COPYRIGHT
       Copyright (C) 2003-2009 by the gtk2-perl team.

       This software is licensed under the LGPL.  See Pango for a full notice.



perl v5.10.0								 2009-11-17						     Pango::ScriptIter(3pm)
