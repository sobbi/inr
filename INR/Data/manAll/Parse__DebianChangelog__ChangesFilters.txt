blib::man::ChangesFilters(3pParse::DebianChangelblib::man::ChangesFilters(3pm)



NAME
       Parse::DebianChangelog::ChangesFilters - filters to be applied to
       Debian changelog entries

UeBERSICHT
BESCHREIBUNG
       This is currently only used internally by Parse::DebianChangelog and is
       not yet documented. There may be still API changes until this module is
       finalized.

SIEHE AUCH
       Parse::DebianChangelog

AUTOR
       Frank Lichtenheld, <frank@lichtenheld.de>

COPYRIGHT UND LIZENZ
       Copyright (C) 2005 Frank Lichtenheld

       Dieses Programm ist freie Software. Sie koennen es unter den
       Bedingungen der GNU General Public License, wie von der Free Software
       Foundation veroeffentlicht, weitergeben und/oder modifizieren, entweder
       gemaess Version 2 der Lizenz oder (nach Ihrer Option) jeder spaeteren
       Version.

       Die Veroeffentlichung dieses Programms erfolgt in der Hoffnung, dass es
       Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne
       die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FUeR
       EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public
       License.

       Sie sollten ein Exemplar der GNU General Public License zusammen mit
       diesem Programm erhalten haben. Falls nicht, schreiben Sie an die Free
       Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
       02110, USA.



Parse::DebianChangelog		  2010-04-26	blib::man::ChangesFilters(3pm)
