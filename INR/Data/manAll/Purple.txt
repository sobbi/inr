Purple(3pm)						    User Contributed Perl Documentation 						Purple(3pm)



NAME
       Purple - Perl extension to the libpurple instant messenger library.

SYNOPSIS
	 use Purple;

ABSTRACT
	 This module provides the interface for using perl scripts as plugins
	 in libpurple.

DESCRIPTION
       This module provides the interface for using perl scripts as plugins in Purple. With this, developers can write perl scripts that can be loaded in
       Purple as plugins. The scripts can interact with IMs, chats, accounts, the buddy list, libpurple signals, and more.

       The API for the perl interface is very similar to that of the Purple C API, which can be viewed at http://developer.pidgin.im/doxygen/ or in the
       header files in the Purple source tree.

FUNCTIONS
       @accounts = Purple::accounts
	   Returns a list of all accounts, online or offline.

       @chats = Purple::chats
	   Returns a list of all chats currently open.

       @connections = Purple::connections
	   Returns a list of all active connections.

       @conversations = Purple::conversations
	   Returns a list of all conversations, both IM and chat, currently open.

       @conv_windows = Purple::conv_windows
	   Returns a list of all conversation windows currently open.

       @ims = Purple::ims
	   Returns a list of all instant messages currently open.

SEE ALSO
       Purple C API documentation - http://developer.pidgin.im/doxygen/

       Purple website - http://pidgin.im/

AUTHOR
       Christian Hammond, <chipx86@gnupdate.org>

COPYRIGHT AND LICENSE
       Copyright 2003 by Christian Hammond

       This library is free software; you can redistribute it and/or modify it under the terms of the General Public License (GPL).  For more information,
       see http://www.fsf.org/licenses/gpl.txt



perl v5.10.1								 2010-02-16								Purple(3pm)
