RPC::XML::Function(3pm) 				    User Contributed Perl Documentation 				    RPC::XML::Function(3pm)



NAME
       RPC::XML::Function - Object class for RPC routines that do not check signatures

SYNOPSIS
	   require RPC::XML::Function;

	   ...
	   $method_1 = RPC::XML::Function->new(name => 'system.identity',
					       code => sub { ... });
	   $method_2 = RPC::XML::Function->new('/path/to/status.xpl');

DESCRIPTION
       The RPC::XML::Function is a class that derives from RPC::XML::Procedure (see RPC::XML::Procedure), while bypassing all the signature-specific logic
       associated with server-side methods in the RPC::XML suite.

       By doing this, the encapsulated code becomes responsible for how the server (and ultimately, the client) interprets returned values. For the classes
       that adhere to signatures, the signature includes the expected type of the returned value. If an object of this class anticipates that the data may
       be ambiguous (an intended string being interpreted as an integer, for example), the code it encapsulates should consider encoding the response with
       the data-classes documented in RPC::XML prior to return.

USAGE
       Only those routines different from RPC::XML::Procedure are listed:

       new(LIST)
	   The constructor for this class is identical to the super-class versions, except that it disregards any "signature" keys on the input list. The
	   return value upon success is a newly-blessed object reference, otherwise an error message is returned.

       signature
	   Returns "undef" only.

       clone
	   Acts as the parent "clone" method, save that in the absence of any signature data, the clone is in fact a perfect copy of the original.

       is_valid
	   Uses the same validity test, minus the checking of signature data (tests only for valid "name" and "code" keys).

       match_signature
	   Always returns the string, "scalar".

DIAGNOSTICS
       Unless otherwises specified, routines return the object reference itself upon a successful operation, and an error string (which is not a blessed
       reference) upon error.

BUGS
       Please report any bugs or feature requests to "bug-rpc-xml at rt.cpan.org", or through the web interface at
       <http://rt.cpan.org/NoAuth/ReportBug.html?Queue=RPC-XML>. I will be notified, and then you'll automatically be notified of progress on your bug as I
       make changes.

SUPPORT
       ·   RT: CPAN's request tracker

	   <http://rt.cpan.org/NoAuth/Bugs.html?Dist=RPC-XML>

       ·   AnnoCPAN: Annotated CPAN documentation

	   <http://annocpan.org/dist/RPC-XML>

       ·   CPAN Ratings

	   <http://cpanratings.perl.org/d/RPC-XML>

       ·   Search CPAN

	   <http://search.cpan.org/dist/RPC-XML>

       ·   Source code on GitHub

	   <http://github.com/rjray/rpc-xml>

COPYRIGHT & LICENSE
       This file and the code within are copyright (c) 2009 by Randy J. Ray.

       Copying and distribution are permitted under the terms of the Artistic License 2.0 (<http://www.opensource.org/licenses/artistic-license-2.0.php>)
       or the GNU LGPL 2.1 (<http://www.opensource.org/licenses/lgpl-2.1.php>).

CREDITS
       The XML-RPC standard is Copyright (c) 1998-2001, UserLand Software, Inc.  See <http://www.xmlrpc.com> for more information about the XML-RPC
       specification.

SEE ALSO
       RPC::XML, RPC::XML::Procedure, make_method

AUTHOR
       Randy J. Ray <rjray@blackperl.com>



perl v5.10.0								 2009-12-07						    RPC::XML::Function(3pm)
