RPC::XML::Method(3pm)					    User Contributed Perl Documentation 				      RPC::XML::Method(3pm)



NAME
       RPC::XML::Method - Object encapsulation of server-side RPC methods

SYNOPSIS
	   require RPC::XML::Method;

	   ...
	   $method_1 = RPC::XML::Method->new({ name => 'system.identity',
					       code => sub { ... },
					       signature => [ 'string' ] });
	   $method_2 = RPC::XML::Method->new('/path/to/status.xpl');

DESCRIPTION
       This package is no longer a distinct, separate entity. It has become an empty sub-class of RPC::XML::Procedure. Please see RPC::XML::Procedure for
       details on the methods and usage.

       By the time of 1.0 release of this software package, this file will be removed completely.

BUGS
       Please report any bugs or feature requests to "bug-rpc-xml at rt.cpan.org", or through the web interface at
       <http://rt.cpan.org/NoAuth/ReportBug.html?Queue=RPC-XML>. I will be notified, and then you'll automatically be notified of progress on your bug as I
       make changes.

SUPPORT
       ·   RT: CPAN's request tracker

	   <http://rt.cpan.org/NoAuth/Bugs.html?Dist=RPC-XML>

       ·   AnnoCPAN: Annotated CPAN documentation

	   <http://annocpan.org/dist/RPC-XML>

       ·   CPAN Ratings

	   <http://cpanratings.perl.org/d/RPC-XML>

       ·   Search CPAN

	   <http://search.cpan.org/dist/RPC-XML>

       ·   Source code on GitHub

	   <http://github.com/rjray/rpc-xml>

COPYRIGHT & LICENSE
       This file and the code within are copyright (c) 2009 by Randy J. Ray.

       Copying and distribution are permitted under the terms of the Artistic License 2.0 (<http://www.opensource.org/licenses/artistic-license-2.0.php>)
       or the GNU LGPL 2.1 (<http://www.opensource.org/licenses/lgpl-2.1.php>).

CREDITS
       The XML-RPC standard is Copyright (c) 1998-2001, UserLand Software, Inc.  See <http://www.xmlrpc.com> for more information about the XML-RPC
       specification.

SEE ALSO
       RPC::XML::Procedure

AUTHOR
       Randy J. Ray <rjray@blackperl.com>



perl v5.10.0								 2009-12-07						      RPC::XML::Method(3pm)
