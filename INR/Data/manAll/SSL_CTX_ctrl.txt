SSL_CTX_ctrl(3SSL)							  OpenSSL							 SSL_CTX_ctrl(3SSL)



NAME
       SSL_CTX_ctrl, SSL_CTX_callback_ctrl, SSL_ctrl, SSL_callback_ctrl - internal handling functions for SSL_CTX and SSL objects

SYNOPSIS
	#include <openssl/ssl.h>

	long SSL_CTX_ctrl(SSL_CTX *ctx, int cmd, long larg, void *parg);
	long SSL_CTX_callback_ctrl(SSL_CTX *, int cmd, void (*fp)());

	long SSL_ctrl(SSL *ssl, int cmd, long larg, void *parg);
	long SSL_callback_ctrl(SSL *, int cmd, void (*fp)());

DESCRIPTION
       The SSL_*_ctrl() family of functions is used to manipulate settings of the SSL_CTX and SSL objects. Depending on the command cmd the arguments larg,
       parg, or fp are evaluated. These functions should never be called directly. All functionalities needed are made available via other functions or
       macros.

RETURN VALUES
       The return values of the SSL*_ctrl() functions depend on the command supplied via the cmd parameter.

SEE ALSO
       ssl(3)



0.9.8k									 2001-10-20							 SSL_CTX_ctrl(3SSL)
