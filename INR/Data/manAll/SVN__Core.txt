native::Core(3perl)					    User Contributed Perl Documentation 					native::Core(3perl)



NAME
       SVN::Core - Core module of the subversion perl bindings

SYNOPSIS
	   use SVN::Core; # does apr_initialize and cleanup for you

	   # create a root pool and set it as default pool for later use
	   my $pool = SVN::Pool->new_default;

	   sub something {
	       # create a subpool of the current default pool
	       my $pool = SVN::Pool->new_default_sub;
	       # some svn operations...

	       # $pool gets destroyed and the previous default pool
	       # is restored when $pool's lexical scope ends
	   }

	   # svn_stream_t as native perl io handle
	   my $stream = $txn->root->apply_text('trunk/filea', undef);
	   print $stream $text;
	   close $stream;

	   # native perl io handle as svn_stream_t
	   SVN::Repos::dump_fs($repos, \*STDOUT, \*STDERR,
			       0, $repos->fs->youngest_rev, 0);

DESCRIPTION
       SVN::Core implements higher level functions of fundamental subversion functions.

FUNCTIONS
       SVN::Core::auth_open([auth provider array]);
	   Takes a reference to an array of authentication providers and returns an auth_baton.  If you use prompt providers you can not use this function,
	   but need to use the auth_open_helper.

       SVN::Core::auth_open_helper([auth provider array]);
	   Prompt providers return two values instead of one.  The 2nd parameter is a reference to whatever was passed into them as the callback.
	   auth_open_helper splits up these arguments, passing the provider objects into auth_open which gives it an auth_baton and putting the other ones
	   in an array.  The first return value of this function is the auth_baton, the second is a reference to an array containing the references to the
	   callbacks.

	   These callback arrays should be stored in the object the auth_baton is attached to.

OTHER OBJECTS
   svn_stream_t - SVN::Stream
       You can use native perl io handles (including io globs) as svn_stream_t in subversion functions. Returned svn_stream_t are also translated into perl
       io handles, so you could access them with regular print, read, etc.

       Note that some functions take a stream to read from or write to, but do not close the stream while still holding the reference to the io handle.  In
       this case the handle won't be destroyed properly.  You should always set up the correct default pool before calling such functions.

   svn_pool_t - SVN::Pool
       The perl bindings significantly simplify the usage of pools, while still being manually adjustable.

       For functions requiring a pool as the last argument (which are, almost all of the subversion functions), the pool argument is optional. The default
       pool is used if it is omitted. When "SVN::Core" is loaded, it creates a new default pool, which is also available from "SVN::Core->gpool".

       For callback functions providing a pool to your subroutine, you could also use $pool->default to make it the default pool in the scope.

       Methods

       new ([$parent])
	   Create a new pool. The pool is a root pool if $parent is not supplied.

       new_default ([$parent])
	   Create a new pool. The pool is a root pool if $parent is not supplied.  Set the new pool as default pool.

       new_default_sub
	   Create a new subpool of the current default pool, and set the resulting pool as new default pool.

       clear
	   Clear the pool.

       DESTROY
	   Destroy the pool. If the pool was the default pool, restore the previous default pool. This is normally called automatically when the SVN::Pool
	   object is no longer used and destroyed by the perl garbage collector.

   svn_error_t - SVN::Error
       By default the perl bindings handle exceptions for you.	The default handler automatically croaks with an appropriate error message.  This is likely
       sufficient for simple scripts, but more complex usage may demand handling of errors.

       You can override the default exception handler by changing the $SVN::Error::handler variable.  This variable holds a reference to a perl sub that
       should be called whenever an error is returned by a svn function.  This sub will be passed a svn_error_t object.   Its return value is ignored.

       If you set the $SVN::Error::handler to undef then each call will return an svn_error_t object as its first return in the case of an error, followed
       by the normal return values.  If there is no error then a svn_error_t will not be returned and only the normal return values will be returned.  When
       using this mode you should be careful only to call functions in array context.  For example: my ($ci) = $ctx->mkdir('http://svn/foo');  In this case
       $ci will be an svn_error_t object if an error occurs and a svn_client_commit_info object otherwise.  If you leave the parenthesis off around $ci
       (scalar context) it will be the commit_info object, which in the case of an error will be undef.

       If you plan on using explicit exception handling, understanding the exception handling system the C API uses is helpful.  You can find information
       on it in the HACKING file and the API documentation.  Looking at the implementation of SVN::Error::croak_on_error and SVN::Error::expanded_message
       may be helpful as well.

       $svn_error_t->apr_err()
	   APR error value, possibly SVN_ custom error.

       $svn_error_t->message()
	   Details from producer of error.

       $svn_error_t->child()
	   svn_error_t object of the error that's wrapped.

       $svn_error_t->pool()
	   The pool holding this error and any child errors it wraps.

       $svn_error_t->file()
	   Source file where the error originated.

       $svn_error_t->line()
	   Source line where the error originated.

       SVN::Error::strerror($apr_status_t)
	   Returns the english description of the status code.

       $svn_error_t->strerror()
	   Returns the english description of the apr_err status code set on the $svn_error_t.	This is short for:
	   SVN::Error::strerror($svn_error_t->apr_err());

       SVN::Error::create($apr_err, $child, $message);
	   Returns a new svn_error_t object with the error status specified in $apr_err, the child as $child, and error message of $message.

       SVN::Error::quick_wrap($child, $new_msg); or $child->quick_wrap($new_msg);
	   A quick n' easy way to create a wrappered exception with your own message before throwing it up the stack.

	   $child is the svn_error_t object you want to wrap and $new_msg is the new error string you want to set.

       SVN::Error::compose($chain, $new_error); or $chain->compose($new_error);
	   Add new_err to the end of $chain's chain of errors.

	   The $new_err chain will be copied into $chain's pool and destroyed, so $new_err itself becomes invalid after this function.

       SVN::Error::clear($svn_error_t); or $svn_error_t->clear();
	   Free the memory used by $svn_error_t, as well as all ancestors and descendants of $svn_error_t.

	   You must call this on every svn_error_t object you get or you will leak memory.

       SVN::Error::expanded_message($svn_error_t) or $svn_error_t->expanded_message()
	   Returns the error message by tracing through the svn_error_t object and its children and concatenating the error messages.  This is how the
	   internal exception handlers get their error messages.

       SVN::Error::is_error($value)
	   Returns true if value is of type svn_error.	Returns false if value is anything else or undefined.  This is useful for seeing if a call has
	   returned an error.

       SVN::Error::croak_on_error
	   Default error handler.  It takes an svn_error_t and extracts the error messages from it and croaks with those messages.

	   It can be used in two ways.	The first is detailed above as setting it as the automatic exception handler via setting $SVN::Error::handler.

	   The second is if you have $SVN::Error::handler set to undef as a wrapper for calls you want to croak on when there is an error, but you don't
	   want to write an explicit error handler. For example:

	   my $result_rev=SVN::Error::croak_on_error($ctx->checkout($url,$path,'HEAD',1));

	   If there is no error then croak_on_error will return the arguments passed to it unchanged.

       SVN::Error::confess_on_error
	   The same as croak_on_error except it will give a more detailed stack backtrace, including internal calls within the implementation of the perl
	   bindings.  This is useful when you are doing development work on the bindings themselves.

       SVN::Error::ignore_error
	   This is useful for wrapping around calls which you wish to ignore any potential error.  It checks to see if the first parameter is an error and
	   if it is it clears it.  It then returns all the other parameters.

   svn_log_changed_path_t
       $lcp->action()
	   'A'dd, 'D'elete, 'R'eplace, 'M'odify

       $lcp->copyfrom_path()
	   Source path of copy, or "undef" if there isn't any previous revision history.

       $lcp->copyfrom_rev()
	   Source revision of copy, or $SVN::Core::INVALID_REVNUM if there is no previous history.

   svn_node_kind_t - SVN::Node
       An enum of the following constants:

       $SVN::Node::none, $SVN::Node::file, $SVN::Node::dir, $SVN::Node::unknown.

   svn_opt_revision_t
   svn_config_t
       Opaque object describing a set of configuration options.

   svn_dirent_t
       $dirent->kind()
	   Node kind.  A number which matches one of these constants: $SVN::Node::none, $SVN::Node::file, $SVN::Node::dir, $SVN::Node::unknown.

       $dirent->size()
	   Length of file text, or 0 for directories.

       $dirent->has_props()
	   Does the node have properties?

       $dirent->created_rev()
	   Last revision in which this node changed.

       $dirent->time()
	   Time of created_rev (mod-time).

       $dirent->last_author()
	   Author of created rev.

   svn_auth_cred_simple_t
       $simple->username()
	   Username.

       $simple->password()
	   Password.

       $simple->may_save()
	   Indicates if the credentials may be saved (to disk).

   svn_auth_cred_username_t
       $username->username()
	   Username.

       $username->may_save()
	   Indicates if the credentials may be saved (to disk).

   svn_auth_cred_ssl_server_trust_t
       $strust->may_save()
	   Indicates if the credentials may be saved (to disk).

       $strust->accepted_failures()
	   Bit mask of the accepted failures.

   svn_auth_ssl_server_cert_info_t
       $scert->hostname()
	   Primary CN.

       $scert->fingerprint()
	   ASCII fingerprint.

       $scert->valid_from()
	   ASCII date from which the certificate is valid.

       $scert->valid_until()
	   ASCII date until which the certificate is valid.

       $scert->issuer_dname()
	   DN of the certificate issuer.

       $scert->ascii_cert()
	   Base-64 encoded DER certificate representation.

   svn_auth_cred_ssl_client_cert_t
       $ccert->cert_file()
	   Full paths to the certificate file.

       $ccert->may_save()
	   Indicates if the credentials may be saved (to disk).

   svn_auth_cred_ssl_client_cert_pw_t
       $ccertpw->password()
	   Certificate password.

       $ccertpw->may_save()
	   Indicates if the credentials may be saved (to disk).

CONSTANTS
   SVN::Auth::SSL
       $SVN::Auth::SSL::NOTYETVALID
	   Certificate is not yet valid.

       $SVN::Auth::SSL::EXPIRED
	   Certificate has expired.

       $SVN::Auth::SSL::CNMISMATCH
	   Certificate's CN (hostname) does not match the remote hostname.

       $SVN::Auth::SSL::UNKNOWNCA
	   Certificate authority is unknown (i.e. not trusted).

       $SVN::Auth::SSL::OTHER
	   Other failure. This can happen if neon has introduced a new failure bit that we do not handle yet.

   _p_svn_lock_t
       Objects of this class contain information about locks placed on files in a repository.  It has the following accessor methods:

       path
	   The full path to the file which is locked, starting with a forward slash ("/").

       token
	   A string containing the lock token, which is a unique URI.

       owner
	   The username of whoever owns the lock.

       comment
	   A comment associated with the lock, or undef if there isn't one.

       is_dav_comment
	   True if the comment was made by a generic DAV client.

       creation_date
	   Time at which the lock was created, as the number of microseconds since 00:00:00 January 1, 1970 UTC.  Divide it by 1_000_000 to get a Unix
	   time_t value.

       expiration_date
	   When the lock will expire.  Has the value '0' if the lock will never expire.

AUTHORS
       Chia-liang Kao <clkao@clkao.org>

COPYRIGHT
       Copyright (c) 2003 CollabNet.  All rights reserved.

       This software is licensed as described in the file COPYING, which you should have received as part of this distribution.  The terms are also
       available at http://subversion.tigris.org/license-1.html.  If newer versions of this license are posted there, you may use a newer version instead,
       at your option.

       This software consists of voluntary contributions made by many individuals.  For exact contribution history, see the revision history and logs,
       available at http://subversion.tigris.org/.



perl v5.10.0								 2008-08-13							native::Core(3perl)
