XStringToKeysym(3)						       XLIB FUNCTIONS							 XStringToKeysym(3)



NAME
       XStringToKeysym, XKeysymToString, XKeycodeToKeysym, XKeysymToKeycode, XConvertCase - convert keysyms

SYNTAX
       KeySym XStringToKeysym(char *string);

       char *XKeysymToString(KeySym keysym);

       KeySym XKeycodeToKeysym(Display *display, KeyCode keycode, int index);

       KeyCode XKeysymToKeycode(Display *display, KeySym keysym);

       void XConvertCase(KeySym keysym, KeySym *lower_return, KeySym *upper_return);

ARGUMENTS
       display	 Specifies the connection to the X server.

       index	 Specifies the element of KeyCode vector.

       keycode	 Specifies the KeyCode.

       keysym	 Specifies the KeySym that is to be searched for or converted.

       lower_return
		 Returns the lowercase form of keysym, or keysym.

       string	 Specifies the name of the KeySym that is to be converted.

       upper_return
		 Returns the uppercase form of keysym, or keysym.

DESCRIPTION
       Standard KeySym names are obtained from <X11/keysymdef.h> by removing the XK_ prefix from each name.  KeySyms that are not part of the Xlib standard
       also may be obtained with this function.  The set of KeySyms that are available in this manner and the mechanisms by which Xlib obtains them is
       implementation-dependent.

       If the KeySym name is not in the Host Portable Character Encoding, the result is implementation-dependent.  If the specified string does not match a
       valid KeySym, XStringToKeysym returns NoSymbol.

       The returned string is in a static area and must not be modified.  The returned string is in the Host Portable Character Encoding.  If the specified
       KeySym is not defined, XKeysymToString returns a NULL.

       The XKeycodeToKeysym function uses internal Xlib tables and returns the KeySym defined for the specified KeyCode and the element of the KeyCode vec‐
       tor.  If no symbol is defined, XKeycodeToKeysym returns NoSymbol.

       If the specified KeySym is not defined for any KeyCode, XKeysymToKeycode returns zero.

       The XConvertCase function returns the uppercase and lowercase forms of the specified Keysym, if the KeySym is subject to case conversion; otherwise,
       the specified KeySym is returned to both lower_return and upper_return.	Support for conversion of other than Latin and Cyrillic KeySyms is imple‐
       mentation-dependent.

SEE ALSO
       XLookupKeysym(3)
       Xlib - C Language X Interface



X Version 11								libX11 1.3.2							 XStringToKeysym(3)
