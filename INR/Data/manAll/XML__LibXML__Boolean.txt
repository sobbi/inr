XML::LibXML::Boolean(3pm)				    User Contributed Perl Documentation 				  XML::LibXML::Boolean(3pm)



NAME
       XML::LibXML::Boolean - Boolean true/false values

DESCRIPTION
       XML::LibXML::Boolean objects implement simple boolean true/false objects.

API
   XML::LibXML::Boolean->True
       Creates a new Boolean object with a true value.

   XML::LibXML::Boolean->False
       Creates a new Boolean object with a false value.

   value()
       Returns true or false.

   to_literal()
       Returns the string "true" or "false".



perl v5.10.0								 2009-09-23						  XML::LibXML::Boolean(3pm)
