XdbeFreeVisualInfo(3)							X FUNCTIONS						      XdbeFreeVisualInfo(3)



NAME
       XdbeFreeVisualInfo - frees information returned by XdbeGetVisualInfo().

SYNOPSIS
       #include <X11/extensions/Xdbe.h>

       void XdbeFreeVisualInfo(
	   XdbeScreenVisualInfo *visual_info)

DESCRIPTION
       This function frees the list of XdbeScreenVisualInfo returned by the function XdbeGetVisualInfo().

SEE ALSO
       DBE,  XdbeAllocateBackBufferName(),  XdbeBeginIdiom(), XdbeDeallocateBackBufferName(), XdbeEndIdiom(), XdbeGetBackBufferAttributes(), XdbeGetVisual‐
       Info(), XdbeQueryExtension(), XdbeSwapBuffers().




X Version 11							       libXext 1.1.1						      XdbeFreeVisualInfo(3)
