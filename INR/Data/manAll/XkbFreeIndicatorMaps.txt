XkbFreeIndicatorMaps(3) 					       XKB FUNCTIONS						    XkbFreeIndicatorMaps(3)



NAME
       XkbFreeIndicatorMaps - Frees memory used by the indicators member of an XkbDescRec structure

SYNOPSIS
       void XkbFreeIndicatorMaps (XkbDescPtr xkb);

ARGUMENTS
       - xkb  keyboard description structure

DESCRIPTION
       If the indicators member of the keyboard description record pointed to by xkb is not NULL, XkbFreeIndicatorMaps frees the memory associated with the
       indicators member of xkb.



X Version 11								libX11 1.3.2						    XkbFreeIndicatorMaps(3)
