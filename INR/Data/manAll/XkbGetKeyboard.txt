XkbGetKeyboard(3)						       XKB FUNCTIONS							  XkbGetKeyboard(3)



NAME
       XkbGetKeyboard -   Retrieves one or more components of a keyboard device description

SYNOPSIS
       XkbDescPtr XkbGetKeyboard (Display *display, unsigned int which, unsigned int device_spec);

ARGUMENTS
       display
	      connection to the X server

       device_spec
	      device ID, or XkbUseCoreKbd

       bits_to_change
	      determines events to be selected / deselected

       values_for_bits
	      1=>select, 0->deselect; for events in bits_to_change

DESCRIPTION
       XkbGetKeyboard  allocates and returns a pointer to a keyboard description. It queries the server for those components specified in the which parame‐
       ter for device device_spec and copies the results to the XkbDescRec it allocated. The remaining fields in the keyboard description are set to  NULL.
       The valid masks for which are those listed in Table 1.


	       Table 1 Mask Bits for XkbDescRec
       ──────────────────────────────────────────────────
       Mask Bit 	      XkbDescRec Field	 Value
       ──────────────────────────────────────────────────
       XkbControlsMask	      ctrls		 (1L<<0)
       XkbServerMapMask       server		 (1L<<1)
       XkbIClientMapMask      map		 (1L<<2)
       XkbIndicatorMapMask    indicators	 (1L<<3)
       XkbNamesMask	      names		 (1L<<4)
       XkbCompatMapMask       compat		 (1L<<5)
       XkbGeometryMask	      geom		 (1L<<6)
       XkbAllComponentsMask   All Fields	 (1L<<7)

       XkbGetKeyboard is used to read the current description for one or more components of a keyboard device. It calls XkbGetKeyboardByName as follows:

       XkbGetKeyboardByName(dpy, device_spec, NULL, which, which, False).

DIAGNOSTICS
       BadAlloc       Unable to allocate storage




X Version 11								libX11 1.3.2							  XkbGetKeyboard(3)
