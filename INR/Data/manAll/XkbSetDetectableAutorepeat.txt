XkbSetDetectableAutorepeat(3)					       XKB FUNCTIONS					      XkbSetDetectableAutorepeat(3)



NAME
       XkbSetDetectableAutorepeat - Sets DetectableAutorepeat

SYNTAX
       Bool XkbSetDetectableAutorepeat (Display *display, Bool detectable, Bool *supported_rtrn);

ARGUMENTS
       - display
	      connection to X server

       - detectable
	      True => set DetectableAutorepeat

       - supported_rtrn
	      backfilled True if DetectableAutorepeat supported

DESCRIPTION
       Auto-repeat  is	the  generation  of  multiple key events by a keyboard when the user presses a key and holds it down. Keyboard hardware and device-
       dependent X server software often implement auto-repeat by generating multiple KeyPress events with no intervening KeyRelease  event.  The  standard
       behavior of the X server is to generate a KeyRelease event for every KeyPress event. If the keyboard hardware and device-dependent software of the X
       server implement auto-repeat by generating multiple KeyPress events, the device-independent part of the X server by default synthetically  generates
       a KeyRelease event after each KeyPress event.  This provides predictable behavior for X clients, but does not allow those clients to detect the fact
       that a key is auto-repeating.

       Xkb allows clients to request detectable auto-repeat.  If a client requests and the server supports DetectableAutorepeat, Xkb  generates  KeyRelease
       events only when the key is physically released. If DetectableAutorepeat is not supported or has not been requested, the server synthesizes a KeyRe‐
       lease event for each repeating KeyPress event it generates.

       DetectableAutorepeat, unlike other controls, is not contained in the XkbControlsRec structure, nor can it be enabled or disabled via the EnabledCon‐
       trols control. Instead, query and set DetectableAutorepeat using XkbGetDetectableAutorepeat and XkbSetDetectableAutorepeat.

       DetectableAutorepeat is a condition that applies to all keyboard devices for a client's connection to a given X server; it cannot be selectively set
       for some devices and not for others. For this reason, none of the Xkb library functions involving DetectableAutorepeat involve a device specifier.

       This request affects all keyboard activity for the requesting client only; other clients still see the expected nondetectable auto-repeat  behavior,
       unless they have requested otherwise.

       XkbSetDetectableAutorepeat  sends  a  request  to the server to set DetectableAutorepeat on for the current client if detectable is True, and off if
       detectable is False; it then waits for a reply. If supported_rtrn is not NULL, XkbSetDetectableAutorepeat backfills supported_rtrn with True if	the
       server  supports  DetectableAutorepeat,	and False if it does not.  XkbSetDetectableAutorepeat returns the current state of DetectableAutorepeat for
       the requesting client: True if DetectableAutorepeat is set, and False otherwise.

RETURN VALUES
       True	      The XkbSetDetectableAutorepeat function returns True if DetectableAutorepeat is set.

       False	      The XkbSetDetectableAutorepeat function returns False if DetectableAutorepeat is not set.



X Version 11								libX11 1.3.2					      XkbSetDetectableAutorepeat(3)
