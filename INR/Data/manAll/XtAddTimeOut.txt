XtAddInput(3)							 XT COMPATIBILITY FUNCTIONS						      XtAddInput(3)



NAME
       XtAddInput, XtAddTimeOut, XtAddWorkProc - register input, timeout, and workprocs

SYNTAX
       XtInputId XtAddInput(int source, XtPointer condition, XtInputCallbackProc proc, XtPointer client_data);

       XtIntervalId XtAddTimeOut(unsigned long interval, XtInputCallbackProc proc, XtPointer client_data);

       XtWorkProcId XtAddWorkProc(XtWorkProc proc, XtPointer client_data);

ARGUMENTS
       client_data
		 Specifies the argument that is to be passed to the specified procedure when input is available

       condition Specifies the mask that indicates a read, write, or exception condition or some operating system dependent condition.

       proc	 Specifies the procedure that is to be called when input is available.

       source	 Specifies the source file descriptor on a UNIX-based system or other operating system dependent device specification.

DESCRIPTION
       XtAddInput has been replaced by XtAppAddInput.

       XtAddTimeOut has been replaced by XtAppAddTimeOut.

       XtAddWorkProc has been replaced by XtAppAddWorkProc.

SEE ALSO
       XtAppAddInput(3Xt), XtAppAddTimeOut(3Xt), XtAppAddWorkProc(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							      XtAddInput(3)
