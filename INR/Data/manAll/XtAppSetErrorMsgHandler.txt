XtAppErrorMsg(3)							XT FUNCTIONS							   XtAppErrorMsg(3)



NAME
       XtAppErrorMsg, XtAppSetErrorMsgHandler, XtAppSetWarningMsgHandler, XtAppWarningMsg - high-level error handlers

SYNTAX
       void XtAppErrorMsg(XtAppContext app_context, String name, String type, String class, String default, String *params, Cardinal *num_params);

       void XtAppSetErrorMsgHandler(XtAppContext app_context, XtErrorMsgHandler msg_handler);

       void XtAppSetWarningMsgHandler(XtAppContext app_context, XtErrorMsgHandler msg_handler);

       void XtAppWarningMsg(XtAppContext app_context, String name, String type, String class, String default, String *params, Cardinal *num_params);

ARGUMENTS
       app_context
		 Specifies the application context.

       class	 Specifies the resource class.

       default	 Specifies the default message to use.

       name	 Specifies the general kind of error.

       type	 Specifies the detailed name of the error.


       msg_handler
		 Specifies the new fatal error procedure, which should not return or the nonfatal error procedure, which usually returns.

       num_params
		 Specifies the number of values in the parameter list.

       params	 Specifies a pointer to a list of values to be stored in the message.

DESCRIPTION
       The XtAppErrorMsg function calls the high-level error handler and passes the specified information.

       The XtAppSetErrorMsgHandler function registers the specified  procedure, which is called when a fatal error occurs.

       The XtAppSetWarningMsgHandler function registers the specified procedure, which is called when a nonfatal error condition occurs.

       The XtAppWarningMsg function calls the high-level error handler and passes the specified information.

SEE ALSO
       XtAppGetErrorDatabase(3Xt), XtAppError(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							   XtAppErrorMsg(3)
