XtAppLock(3)								XT FUNCTIONS							       XtAppLock(3)



NAME
       XtAppLock, XtAppUnlock - lock and unlock application context

SYNTAX
       void XtAppLock(XtAppContext app_context);

       void XtAppUnlock(XtAppContext app_context);

ARGUMENTS
       app_context
		 Specifies the application context.

DESCRIPTION
       XtAppLock locks the application context including all its related displays and widgets.

       XtAppUnlock unlocks the application context.

SEE ALSO
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							       XtAppLock(3)
