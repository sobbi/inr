XtCallAcceptFocus(3)							XT FUNCTIONS						       XtCallAcceptFocus(3)



NAME
       XtCallAcceptFocus - calla widget's accept_focus procedure

SYNTAX
       Boolean XtCallAcceptFocus(Widget w, Time *time);

ARGUMENTS
       time	 Specifies the X time of the event that is causing the accept focus.

       w	 Specifies the widget.

DESCRIPTION
       The XtCallAcceptFocus function calls the specified widget's accept_focus procedure, passing it the specified widget and time, and returns what the
       accept_focus procedure returns.	If accept_focus is NULL, XtCallAcceptFocus returns False.

SEE ALSO
       XtSetKeyboardFocus(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7						       XtCallAcceptFocus(3)
