XtOwnSelection(3)							XT FUNCTIONS							  XtOwnSelection(3)



NAME
       XtOwnSelection, XtOwnSelectionIncremental, XtDisownSelection - set selection owner

SYNTAX
       Boolean XtOwnSelection(Widget w, Atom selection, Time time, XtConvertSelectionProc convert_proc, XtLoseSelectionProc lose_selection, XtSelection‐
	      DoneProc done_proc);

       Boolean XtOwnSelectionIncremental(Widget w, Atom selection, Time time, XtConvertSelectionIncrProc convert_callback, XtLoseSelectionIncrProc
	      lose_callback, XtSelectionDoneIncrProc done_callback, XtCancelConvertSelectionProc cancel_callback, XtPointer client_data);

       void XtDisownSelection(Widget w, Atom selection, Time time);

ARGUMENTS
       convert_proc
		 Specifies the procedure that is to be called whenever someone requests the current value of the selection.

       done_proc Specifies the procedure that is called after the requestor has received the selection or NULL if the owner is not interested in being
		 called back.

       lose_selection
		 Specifies the procedure that is to be called whenever the widget has lost selection ownership or NULL if the owner is not interested in
		 being called back.

       selection Specifies an atom that describes the type of the selection (for example, XA_PRIMARY, XA_SECONDARY, or XA_CLIPBOARD).

       time	 Specifies the timestamp that indicates when the selection ownership should commence or is to be relinquished.

       w	 Specifies the widget that wishes to become the owner or to relinquish ownership.

DESCRIPTION
       The XtOwnSelection function informs the Intrinsics selection mechanism that a widget believes it owns a selection.  It returns True if the widget
       has successfully become the owner and False otherwise.  The widget may fail to become the owner if some other widget has asserted ownership at a
       time later than this widget.  Note that widgets can lose selection ownership either because someone else asserted later ownership of the selection
       or because the widget voluntarily gave up ownership of the selection.  Also note that the lose_selection procedure is not called if the widget fails
       to obtain selection ownership in the first place.

       The XtOwnSelectionIncremental procedure informs the Intrinsics incremental selection mechanism that the specified widget wishes to own the selec‐
       tion. It returns True if the specified widget successfully becomes the selection owner or False otherwise. For more information about selection,
       target, and time, see Section 2.6 of the Inter-Client Communication Conventions Manual.

       A widget that becomes the selection owner using XtOwnSelectionIncremental may use XtDisownSelection to relinquish selection ownership.

       The XtDisownSelection function informs the Intrinsics selection mechanism that the specified widget is to lose ownership of the selection.  If the
       widget does not currently own the selection either because it lost the selection or because it never had the selection to begin with, XtDisown‐
       Selection does nothing.

       After a widget has called XtDisownSelection, its convert procedure is not called even if a request arrives later with a timestamp during the period
       that this widget owned the selection.  However, its done procedure will be called if a conversion that started before the call to XtDisownSelection
       finishes after the call to XtDisownSelection.

SEE ALSO
       XtAppGetSelectionTimeout(3Xt), XtGetSelectionValue(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							  XtOwnSelection(3)
