XtMalloc(3)								XT FUNCTIONS								XtMalloc(3)



NAME
       XtMalloc, XtCalloc, XtRealloc, XtFree, XtNew, XtNewString - memory management functions

SYNTAX
       char *XtMalloc(Cardinal size);

       char *XtCalloc(Cardinal num, Cardinal size);

       char *XtRealloc(char *ptr, Cardinal num);

       void XtFree(char *ptr);

       type *XtNew(type);

       String XtNewString(String string);

ARGUMENTS
       num	 Specifies the number of bytes or array elements.

       ptr	 Specifies a pointer to the old storage or to the block of storage that is to be freed.

       size	 Specifies the size of an array element (in bytes) or the number of bytes desired.

       string	 Specifies a previously declared string.

       type	 Specifies a previously declared data type.

DESCRIPTION
       The XtMalloc functions returns a pointer to a block of storage of at least the specified size bytes.  If there is insufficient memory to allocate
       the new block, XtMalloc calls XtErrorMsg.

       The XtCalloc function allocates space for the specified number of array elements of the specified size and initializes the space to zero.  If there
       is insufficient memory to allocate the new block, XtCalloc calls XtErrorMsg.

       The XtRealloc function changes the size of a block of storage (possibly moving it).  Then, it copies the old contents (or as much as will fit) into
       the new block and frees the old block.  If there is insufficient memory to allocate the new block, XtRealloc calls XtErrorMsg.  If ptr is NULL,
       XtRealloc allocates the new storage without copying the old contents; that is, it simply calls XtMalloc.

       The XtFree function returns storage and allows it to be reused.	If ptr is NULL, XtFree returns immediately.

       XtNew returns a pointer to the allocated storage.  If there is insufficient memory to allocate the new block, XtNew calls XtErrorMsg.  XtNew is a
       convenience macro that calls XtMalloc with the following arguments specified:

       ((type *) XtMalloc((unsigned) sizeof(type))

       XtNewString returns a pointer to the allocated storage.	If there is insufficient memory to allocate the new block, XtNewString calls XtErrorMsg.
       XtNewString is a convenience macro that calls XtMalloc with the following arguments specified:

       (strcpy(XtMalloc((unsigned) strlen(str) + 1), str))

SEE ALSO
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7								XtMalloc(3)
