XtGetResourceList(3)							XT FUNCTIONS						       XtGetResourceList(3)



NAME
       XtGetResourceList, XtGetConstraintResourceList - obtain resource list

SYNTAX
       void XtGetResourceList(WidgetClass class, XtResourceList *resources_return, Cardinal *num_resources_return);

       void XtGetConstraintResourceList(WidgetClass class, XtResourceList *resources_return, Cardinal *num_resources_return);

ARGUMENTS
       num_resources_return
		 Specifies a pointer to where to store the number of entries in the resource list.

       resources_return
		 Specifies a pointer to where to store the returned resource list.  The caller must free this storage using XtFree when done with it.

       widget_class
		 Specifies the widget class.

DESCRIPTION
       If XtGetResourceList is called before the widget class is initialized (that is, before the first widget of that class has been created), XtGet‐
       ResourceList returns the resource list as specified in the widget class record.	If it is called after the widget class has been initialized, XtGet‐
       ResourceList returns a merged resource list that contains the resources for all superclasses. The list returned by XtGetResourceList should be freed
       using XtFree when it is no longer needed.

       If XtGetConstraintResourceList is called before the widget class is initialized (that is, before the first widget of that class has been created),
       XtGetConstraintResourceList returns the resource list as specified in the widget class Constraint part record. If it is called after the widget
       class has been initialized, XtGetConstraintResourceList returns a merged resource list that contains the Constraint resources for all superclasses.
       If the specified class is not a subclass of constraintWidgetClass, *resources_return is set to NULL and *num_resources_return is set to zero. The
       list returned by XtGetConstraintResourceList should be freed using XtFree when it is no longer needed.

SEE ALSO
       XtGetSubresources(3Xt), XtOffset(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7						       XtGetResourceList(3)
