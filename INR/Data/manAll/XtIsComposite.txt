XtClass(3)								XT FUNCTIONS								 XtClass(3)



NAME
       XtClass, XtSuperclass, XtIsSubclass, XtCheckSubclass, XtIsObject, XtIsRectObj, XtIsWidget, XtIsComposite, XtIsConstraint, XtIsShell, XtIsOverride‐
       Shell, XtIsWMShell, XtIsVendorShell, XtIsTransientShell, XtIsTopLevelShell, XtIsApplicationShell, XtIsSessionShell - obtain and verify a widget's
       class

SYNTAX
       WidgetClass XtClass(Widget w);

       WidgetClass XtSuperclass(Widget w);

       Boolean XtIsSubclass(Widget w, WidgetClass widget_class);

       void XtCheckSubclass(Widget widget, WidgetClass widget_class, String message);

       Boolean XtIsObject(Widget w);

       Boolean XtIsRectObj(Widget w);

       Boolean XtIsWidget(Widget w);

       Boolean XtIsComposite(Widget w);

       Boolean XtIsConstraint(Widget w,

       Boolean XtIsShell(Widget w);

       Boolean XtIsOverrideShell(Widget w);

       Boolean XtIsWMShell(Widget w);

       Boolean XtIsVendorShell(Widget w);

       Boolean XtIsTransientShell(Widget w);

       Boolean XtIsTopLevelShell(Widget w);

       Boolean XtIsApplicationShell(Widget w);

       Boolean XtIsSessionShell(Widget w);

ARGUMENTS
       w	 Specifies the widget.

       widget_class
		 Specifies the widget class.

       message	 Specifies the message that is to be used.

DESCRIPTION
       The XtClass function returns a pointer to the widget's class structure.

       The XtSuperclass function returns a pointer to the widget's superclass class structure.

       The XtIsSubclass function returns True if the class of the specified widget is equal to or is a subclass of the specified class. The widget's class
       can be any number of subclasses down the chain and need not be an immediate subclass of the specified class. Composite widgets that need to restrict
       the class of the items they contain can use XtIsSubclass to find out if a widget belongs to the desired class of objects.

       The XtCheckSubclass macro determines if the class of the specified widget is equal to or is a subclass of the specified widget class.  The widget
       can be any number of subclasses down the chain and need not be an immediate subclass of the specified widget class.  If the specified widget is not
       a subclass, XtCheckSubclass constructs an error message from the supplied message, the widget's actual class, and the expected class and calls XtEr‐
       rorMsg.	XtCheckSubclass should be used at the entry point of exported routines to ensure that the client has passed in a valid widget class for the
       exported operation.

       XtCheckSubclass is only executed when the widget has been compiled with the compiler symbol DEBUG defined; otherwise, it is defined as the empty
       string and generates no code.

       To test if a given widget belongs to a subclass of an Intrinsics-defined class, the Intrinsics defines macros or functions equivalent to XtIsSub‐
       class for each of the built-in classes. These procedures are XtIsObject, XtIsRectObj, XtIsWidget, XtIsComposite, XtIsConstraint, XtIsShell, XtIs‐
       OverrideShell, XtIsWMShell, XtIsVendorShell, XtIsTransientShell, XtIsTopLevelShell, XtIsApplicationShell, and XtIsSessionShell.

       The

SEE ALSO
       XtAppErrorMsg(3Xt), XtDisplay(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7								 XtClass(3)
