XtOffset(3)								XT FUNCTIONS								XtOffset(3)



NAME
       XtOffset, XtOffsetOf, XtNumber - determine the byte offset or number of array elements

SYNTAX
       Cardinal XtOffset(Type pointer_type, Field field_name);

       Cardinal XtOffsetOf(Type structure_type, Field field_name);

       Cardinal XtNumber(ArrayVariable array);

ARGUMENTS
       array	 Specifies a fixed-size array.

       field_name
		 Specifies the name of the field for which to calculate the byte offset.

       pointer_type
		 Specifies a type that is declared as a pointer to the structure.

       structure_type
		 Specifies a type that is declared as a structure.

DESCRIPTION
       The XtOffset macro is usually used to determine the offset of various resource fields from the beginning of a widget and can be used at compile time
       in static initializations.

       The XtOffsetOf macro expands to a constant expression that gives the offset in bytes to the specified structure member from the beginning of the
       structure.  It is normally used to statically initialize resource lists and is more portable than XtOffset, which serves the same function.

       The XtNumber macro returns the number of elements in the specified argument lists, resources lists, and other counted arrays.

SEE ALSO
       XtGetResourceList(3Xt), XtSetArg(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7								XtOffset(3)
