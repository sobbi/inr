XtQueryGeometry(3)							XT FUNCTIONS							 XtQueryGeometry(3)



NAME
       XtQueryGeometry - query the preferred geometry of a child widget

SYNTAX
       XtGeometryResult XtQueryGeometry(Widget w, XtWidgetGeometry *intended, XtWidgetGeometry *preferred_return);

ARGUMENTS
       intended  Specifies any changes the parent plans to make to the child's geometry or NULL.

       preferred_return
		 Returns the child widget's preferred geometry.

       w	 Specifies the widget.

DESCRIPTION
       To discover a child's preferred geometry, the child's parent sets any changes that it intends to make to the child's geometry in the corresponding
       fields of the intended structure, sets the corresponding bits in intended.request_mode, and calls XtQueryGeometry.

       XtQueryGeometry clears all bits in the preferred_return->request_mode and checks the query_geometry field of the specified widget's class record.
       If query_geometry is not NULL, XtQueryGeometry calls the query_geometry procedure and passes as arguments the specified widget, intended, and pre‐
       ferred_return structures.  If the intended argument is NULL, XtQueryGeometry replaces it with a pointer to an XtWidgetGeometry structure with
       request_mode=0 before calling query_geometry.

SEE ALSO
       XtConfigureWidget(3Xt), XtMakeGeometryRequest(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							 XtQueryGeometry(3)
