XtGetSelectionTimeout(3)					 XT COMPATIBILITY FUNCTIONS					   XtGetSelectionTimeout(3)



NAME
       XtGetSelectionTimeout, XtSetSelectionTimeout - set and obtain selection timeout values

SYNTAX
       unsigned long XtGetSelectionTimeout(void);

       void XtSetSelectionTimeout(unsigned long timeout);

ARGUMENTS
       timeout	 Specifies the selection timeout in milliseconds.

DESCRIPTION
       The XtGetSelectionTimeout function has been superceded by XtAppGetSelectionTimeout.

       The XtSetSelectionTimeout function has been superceded by XtAppSetSelectionTimeout.

SEE ALSO
       XtAppGetSelectionTimeout(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7						   XtGetSelectionTimeout(3)
