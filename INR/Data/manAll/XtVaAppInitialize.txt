XtAppInitialize(3)							XT FUNCTIONS							 XtAppInitialize(3)



NAME
       XtAppInitialize, XtVaAppInitialize - initialize, open, or close a display

SYNTAX
       Widget XtAppInitialize(XtAppContext* app_context_return, String application_class, XrmOptionDescRec* options, Cardinal num_options, int*
	      argc_in_out, String* argv_in_out, String* fallback_resources, ArgList args, Cardinal num_args);

       Widget XtVaAppInitialize(XtAppContext* app_context_return, String application_class, XrmOptionDescRec* options, Cardinal num_options, int*
	      argc_in_out, String* argv_in_out, String* fallback_resources, ...);

ARGUMENTS
       app_context_return
		 Specifies the application context.

       application_class
		 Specifies the class name of this application, which usually is the generic name for all instances of this application.

       options	 Specifies how to parse the command line for any application-specific resources.  The options argument is passed as a parameter to XrmPar‐
		 seCommand.  For further information, see Xlib - C Language X Interface.

       num_options
		 Specifies the number of entries in the options list.

       argc_in_out
		 Specifies a pointer to the number of command line parameters.

       argv_in_out
		 Specifies the command line parameters.

       fallback_resources
		 Specifies resource values to be used if the application class resource file cannot be opened or read, or NULL.

       args	 Specifies the argument list to override any other resource specification for the created shell widget.

       num_args  Specifies the number of entries in the argument list.

       ...	 Specifies the variable argument list to override any other resource specification for the created shell widget.

DESCRIPTION
       The XtAppInitialize function calls XtToolkitInitialize followed by XtCreateApplicationContext, then calls XtOpenDisplay with display_string NULL and
       application_name NULL, and finally calls XtAppCreateShell with application_name NULL, widget_class applicationShellWidgetClass, and the specified
       args and num_args and returns the created shell. The modified argc and argv returned by XtDisplayInitialize are returned in argc_in_out and
       argv_in_out. If app_context_return is not NULL, the created application context is also returned. If the display specified by the command line can‐
       not be opened, an error message is issued and XtAppInitialize terminates the application. If fallback_resources is non-NULL, XtAppSetFallbackRe‐
       sources is called with the value prior to calling XtOpenDisplay.

       XtAppInitialize and XtVaAppInitialize have been superceded by XtOpenApplication and XtVaOpenApplication respectively.

SEE ALSO
       XtOpenApplication(3Xt), XtVaOpenApplication(3Xt)
       X Toolkit Intrinsics - C Language Interface
       Xlib - C Language X Interface



X Version 11								libXt 1.0.7							 XtAppInitialize(3)
