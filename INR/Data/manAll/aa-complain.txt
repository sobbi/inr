COMPLAIN(8)			   AppArmor			   COMPLAIN(8)



NAME
       aa-complain - set a AppArmor security profile to complain mode.

SYNOPSIS
       aa-complain <executable> [<executable> ...]

DESCRIPTION
       aa-complain is used to set the enforcement mode for one or more
       profiles to complain.  In this mode security policy is not enforced but
       rather access violations are logged to the system log.

BUGS
       None. Please report any you find to bugzilla at
       <http://bugzilla.novell.com>.

SEE ALSO
       apparmor(7), apparmor.d(5), aa-enforce(1), change_hat(2), and
       <http://forge.novell.com/modules/xfmod/project/?apparmor>.



Canonical, Ltd. 		  2010-03-11			   COMPLAIN(8)
