AIO_FSYNC(3)		   Linux Programmer's Manual		  AIO_FSYNC(3)



NAME
       aio_fsync - asynchronous file synchronization

SYNOPSIS
       #include <aio.h>

       int aio_fsync(int op, struct aiocb *aiocbp);

       Link with -lrt.

DESCRIPTION
       The  aio_fsync()  function  does a sync on all outstanding asynchronous
       I/O operations associated with aiocbp->aio_fildes.

       More precisely, if op is O_SYNC, then all currently queued  I/O	opera‐
       tions  shall  be  completed  as	if by a call of fsync(2), and if op is
       O_DSYNC, this call is the asynchronous analog  of  fdatasync(2).   Note
       that  this  is a request only — this call does not wait for I/O comple‐
       tion.

       Apart from aio_fildes the only field in the  structure  pointed	to  by
       aiocbp  that  is  used by this call is the aio_sigevent field (a struct
       sigevent) that indicates the desired type of asynchronous  notification
       at completion.  All other fields are ignored.

RETURN VALUE
       On  success  (the  sync	request was successfully queued) this function
       returns 0.  On error -1 is returned, and errno is set appropriately.

ERRORS
       EAGAIN Out of resources.

       EBADF  aio_fildes is not a valid file descriptor open for writing.

       EINVAL No synchronized I/O for this file is supported,  or  op  is  not
	      O_SYNC or O_DSYNC.

CONFORMING TO
       POSIX.1-2001.

SEE ALSO
       aio_cancel(3),	aio_error(3),	aio_read(3),  aio_return(3),  aio_sus‐
       pend(3), aio_write(3)

COLOPHON
       This page is part of release 3.23 of the Linux  man-pages  project.   A
       description  of	the project, and information about reporting bugs, can
       be found at http://www.kernel.org/doc/man-pages/.



				  2003-11-14			  AIO_FSYNC(3)
