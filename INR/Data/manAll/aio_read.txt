AIO_READ(3)		   Linux Programmer's Manual		   AIO_READ(3)



NAME
       aio_read - asynchronous read

SYNOPSIS
       #include <aio.h>

       int aio_read(struct aiocb *aiocbp);

       Link with -lrt.

DESCRIPTION
       The  aio_read()	function  requests  an asynchronous "n = read(fd, buf,
       count)"	 with	fd,   buf,   count   given   by    aiocbp->aio_fildes,
       aiocbp->aio_buf, aiocbp->aio_nbytes, respectively.  The return status n
       can be retrieved upon completion using aio_return(3).

       The data is read starting at the absolute file offset  aiocbp->aio_off‐
       set,  regardless of the current file position.  After this request, the
       value of the current file position is unspecified.

       The "asynchronous" means that this call returns as soon as the  request
       has been enqueued; the read may or may not have completed when the call
       returns.  One tests for completion using aio_error(3).

       If _POSIX_PRIORITIZED_IO is defined, and this file  supports  it,  then
       the  asynchronous operation is submitted at a priority equal to that of
       the calling process minus aiocbp->aio_reqprio.

       The field aiocbp->aio_lio_opcode is ignored.

       No data is read from a regular file beyond its maximum offset.

RETURN VALUE
       On success, 0 is returned.  On error the request is not enqueued, -1 is
       returned,  and  errno  is  set  appropriately.	If  an	error is first
       detected later, it will be reported via aio_return(3)  (returns	status
       -1)  and  aio_error(3)  (error status whatever one would have gotten in
       errno, such as EBADF).

ERRORS
       EAGAIN Out of resources.

       EBADF  aio_fildes is not a valid file descriptor open for reading.

       EINVAL One or more of aio_offset, aio_reqprio, aio_nbytes are invalid.

       ENOSYS This function is not supported.

       EOVERFLOW
	      The file is a regular file, we start reading before  end-of-file
	      and  want  at  least one byte, but the starting position is past
	      the maximum offset for this file.

CONFORMING TO
       POSIX.1-2001.

NOTES
       It is a good idea to zero out the control block before use.  This  con‐
       trol block must not be changed while the read operation is in progress.
       The buffer area being read into must not be accessed during the	opera‐
       tion  or  undefined  results may occur.	The memory areas involved must
       remain valid.

SEE ALSO
       aio_cancel(3),  aio_error(3),  aio_fsync(3),  aio_return(3),   aio_sus‐
       pend(3), aio_write(3)

COLOPHON
       This  page  is  part of release 3.23 of the Linux man-pages project.  A
       description of the project, and information about reporting  bugs,  can
       be found at http://www.kernel.org/doc/man-pages/.



				  2003-11-14			   AIO_READ(3)
