ALTER TEXT SEARCH TEMPLATE(7)	 SQL Commands	 ALTER TEXT SEARCH TEMPLATE(7)



NAME
       ALTER  TEXT  SEARCH  TEMPLATE  - change the definition of a text search
       template


SYNOPSIS
       ALTER TEXT SEARCH TEMPLATE name RENAME TO newname


DESCRIPTION
       ALTER TEXT SEARCH TEMPLATE changes the definition of a text search tem‐
       plate.  Currently,  the	only  supported functionality is to change the
       template's name.

       You must be a superuser to use ALTER TEXT SEARCH TEMPLATE.

PARAMETERS
       name   The name	(optionally  schema-qualified)	of  an	existing  text
	      search template.

       newname
	      The new name of the text search template.

COMPATIBILITY
       There is no ALTER TEXT SEARCH TEMPLATE statement in the SQL standard.

SEE ALSO
       CREATE TEXT SEARCH TEMPLATE [create_text_search_template(7)], DROP TEXT
       SEARCH TEMPLATE [drop_text_search_template(7)]



SQL - Language Statements	  2010-05-19	 ALTER TEXT SEARCH TEMPLATE(7)
