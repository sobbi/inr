APM(4)									APM(4)



NAME
       apm - Alliance ProMotion video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "apm"
	 ...
       EndSection

DESCRIPTION
       apm is an Xorg driver for Alliance ProMotion video cards. The driver is
       accelerated  for  supported  hardware/depth  combination.  It  supports
       framebuffer  depths of 8, 15, 16, 24 and 32 bits. For 6420, 6422, AT24,
       AT3D and AT25, all depths are fully accelerated except 24 bpp for which
       only screen to screen copy and rectangle filling is accelerated.

SUPPORTED HARDWARE
       The  apm  driver  supports  PCI	and  ISA  video cards on the following
       Alliance ProMotion chipsets

       ProMotion 6420

       ProMotion 6422

       AT24

       AT3D

       AT25

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration  details.   This
       section only covers configuration details specific to this driver.

       The  driver  auto-detects  the  chipset type, but the following ChipSet
       names may optionally be specified in the config file "Device"  section,
       and will override the auto-detection:

	   "6422", "at24", "at3d".

	   The AT25 is Chipset "at3d" and the 6420 is 6422.

	   The	driver will auto-detect the amount of video memory present for
	   all chips. The actual amount of video memory can also be  specified
	   with a VideoRam entry in the config file "Device" section.

	   The following driver Options are supported:

	   Option "HWCursor" "boolean"
		  Enable or disable the hardware cursor.  Default: on.

	   Option "NoAccel" "boolean"
		  Disable  or  enable  acceleration.  Default: acceleration is
		  enabled.

	   Option "NoLinear" "boolean"
		  Disable or enable use of linear frame buffer.  Default:  on.
		  Note: it may or may not work. Tell me if you need it.

	   Option "PciRetry" "boolean"
		  Enable or disable PCI retries.  Default: off.

	   Option "Remap_DPMS_On" "string"

	   Option "Remap_DPMS_Standby" "string"

	   Option "Remap_DPMS_Suspend" "string"

	   Option "Remap_DPMS_Off" "string"
		  Remaps  the  corresponding  DPMS  events. I've found that my
		  Hercules 128/3D swaps Off and Suspend events. You  can  cor‐
		  rect that with
			 Option "Remap_DPMS_Suspend" "Off"
			 Option "Remap_DPMS_Off" "Suspend"
		  in the Device section of the config file.

		  Option "SWCursor" "boolean"
			 Force the software cursor.  Default: off.

		  Option "ShadowFB" "boolean"
			 Enable  or  disable  use  of  the  shadow framebuffer
			 layer.  Default: off.

SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

AUTHORS
       Authors include: ...



X Version 11		     xf86-video-apm 1.2.2			APM(4)
