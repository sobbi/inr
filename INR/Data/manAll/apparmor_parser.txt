APPARMOR_PARSER(8)		   AppArmor		    APPARMOR_PARSER(8)



NAME
       apparmor_parser - loads AppArmor profiles into the kernel

SYNOPSIS
       apparmor_parser [-adrR] [--add] [--debug]  [--replace] [--remove]
			 [--preprocess] [--Include n] [--base n] [ --Complain
       ]

       apparmor_parser [-hv] [--help] [--version]

DESCRIPTION
       apparmor_parser is used to import new apparmor.d(5) profiles into the
       Linux kernel. The profiles restrict the operations available to
       processes by executable name.

       The profiles are loaded into the Linux kernel by the apparmor_parser
       program, which takes its input from standard input. The input supplied
       to apparmor_parser should be in the format described in apparmor.d(5).

OPTIONS
       -a, --add
	   Insert the AppArmor definitions given into the kernel. This is the
	   default action. This gives an error message if a AppArmor
	   definition by the same name already exists in the kernel, or if the
	   parser doesn't understand its input. It reports when an addition
	   succeeded.

       -r, --replace
	   This flag is required if an AppArmor definition by the same name
	   already exists in the kernel; used to replace the definition
	   already in the kernel with the definition given on standard input.

       -R, --remove
	   This flag is used to remove an AppArmor definition already in the
	   kernel.  Note that it still requires a complete AppArmor definition
	   as described in apparmor.d(5) even though the contents of the
	   definition aren't used.

       -C, --Complain
	   For the profile to load in complain mode.

       -B, --binary
	   Load a binary (cached) profile, as produced with the -S option.

       -N, --names
	   Produce a list of policies from a given set of profiles (implies
	   -K).

       -S, --stdout
	   Writes a binary (cached) profile to stdout (implies -K and -T).

       -b n, --base n
	   Set the base directory for resolving #include directives defined as
	   relative paths.

       -I n, --Include n
	   Add element n to the search path when resolving #include directives
	   defined as an absolute paths.

       -f n, --subdomainfs n
	   Set the location of the apparmor security filesystem (default is
	   "/sys/kernel/security/apparmor").

       -m n, --match-string n
	   Only use match features "n".

       -n n, --namespace-string n
	   Force a profile to load in the namespace "n".

       -X, --readimpliesX
	   In the case of profiles that are loading on systems were
	   READ_IMPLIES_EXEC is set in the kernel for a given process, load
	   the profile so that any "r" flags are processed as "mr".

       -k, --show-cache
	   Report the cache processing (hit/miss details) when loading or
	   saving cached profiles.

       -K, --skip-cache
	   Perform no caching at all: disables -W, implies -T.

       -T, --skip-read-cache
	   By default, if a profile's cache is found in /etc/apparmor.d/cache/
	   and the timestamp is newer than the profile, it will be loaded from
	   the cache.  This option disables this cache loading behavior.

       -W, --write-cache
	   Write out cached profiles to /etc/apparmor.d/cache/.  Off by
	   default.  In cases where abstractions have been changed, and the
	   parser is running with "--replace", it may make sense to also use
	   "--skip-read-cache" with the "--write-cache" option.

       -Q, --skip-kernel-load
	   Perform all actions except the actual loading of a profile into the
	   kernel.  This is useful for testing profile generation, caching,
	   etc, without making changes to the running kernel profiles.

       -q, --quiet
	   Do not report on the profiles as they are loaded, and not show
	   warnings.

       -v, --verbose
	   Report on the profiles as they are loaded, and show warnings.

       -V, --version
	   Print the version number and exit.

       -d, --debug
	   Given once, only checks the profiles to ensure syntactic
	   correctness.  Given twice, dumps its interpretation of the profile
	   for checking.

       -h, --help
	   Give a quick reference guide.

BUGS
       None known. If you find any, please report them to bugzilla at
       <http://bugzilla.novell.com>.

SEE ALSO
       apparmor(7), apparmor.d(5), subdomain.conf(5), change_hat(2), and
       <http://forge.novell.com/modules/xfmod/project/?apparmor>.



Canonical, Ltd. 		  2010-03-11		    APPARMOR_PARSER(8)
