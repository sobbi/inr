apport-bug(1)							 apport-bug(1)



NAME
       apport-bug,  apport-collect - file a bug report using Apport, or update
       an existing report


SYNOPSIS
       apport-bug

       apport-bug symptom | pid | package | program path | .apport/.crash file

       apport-collect report-number


DESCRIPTION
       apport-bug reports problems to your distribution's bug tracking system,
       using Apport to collect a lot of local information about your system to
       help the developers to fix the  problem	and  avoid  unnecessary  ques‐
       tion/answer turnarounds.

       You  should  always  start  with  running apport-bug without arguments,
       which will present a list of known symptoms.  This  will  generate  the
       most useful bug reports.

       If  there  is  no  matching symptom, you need to determine the affected
       program or package yourself. You can provide a package name or  program
       name to apport-bug, e. g.:

	   apport-bug firefox
	   apport-bug /usr/bin/unzip

       In  order to add more information to the bug report that could help the
       developers to fix the problem,  you  can  also  specify	a  process  ID
       instead:

	   $ pidof gnome-terminal
	   5139
	   $ apport-bug 5139

       As a special case, to report a bug against the Linux kernel, you do not
       need   to   use	 the   full   package	 name	 (such	  as	linux-
       image-2.6.28-4-generic); you can just use

	   apport-bug linux

       to report a bug against the currently running kernel.

       Finally,  you  can use this program to report a previously stored crash
       or bug report:

	   apport-bug /var/crash/_bin_bash.1000.crash
	   apport-bug /tmp/apport.firefox.332G9t.apport

       Bug reports can be written to a file by using the --save option	or  by
       using apport-cli.

       apport-bug detects whether KDE or Gnome is running and calls apport-gtk
       or apport-kde accordingly. If neither is available, or the session does
       not run under X11, it calls apport-cli for a command-line client.


UPDATING EXISTING REPORTS
       apport-collect collects the same information as apport-bug, but adds it
       to an already existing problem report. This is useful if the report was
       not originally filed through Apport, or by someone else, and the devel‐
       opers ask you for attaching information from your system as well.


OPTIONS
       Please see the apport-cli(1) manpage for possible options.


ENVIRONMENT
       APPORT_IGNORE_OBSOLETE_PACKAGES
	      Apport refuses to create bug  reports  if  the  package  or  any
	      dependency  is not current. If this environment variable is set,
	      this check is waived. Experts who will thoroughly check the sit‐
	      uation  before  filing  a  bug  report  can define this in their
	      ~/.bashrc or temporarily when calling the apport frontend (-cli,
	      -gtk, or -kde).


       APPORT_STAGING
	      When  reporting bugs to Launchpad (default in Ubuntu), use stag‐
	      ing.launchpad.net instead of the real production	instance.  You
	      should  use this when testing new hooks or new functionality, to
	      avoid sending uninteresting bug mail to developers.


SEE ALSO
       apport-cli(1)


AUTHOR
       apport and the accompanying tools are developed by  Martin  Pitt  <mar‐
       tin.pitt@ubuntu.com>.



Martin Pitt		      September 08, 2009		 apport-bug(1)
