APT-CDROM(8)			      APT			  APT-CDROM(8)



NAME
       apt-cdrom - APT-CDROM-Verwaltungswerkzeug

SYNOPSIS
       apt-cdrom [-hvrmfan] [-d=CD-ROM-Einhängepunkt]
		 [-o=Konfigurationszeichenkette] [-c=Datei] [[hinzufügen] |
		 [Identifikation]]

BESCHREIBUNG
       apt-cdrom wird benutzt, um eine neue CD-ROM zu APTs Liste der
       verfügbaren Quellen hinzuzufügen.  apt-cdrom kümmert sich um die
       festgestellte Struktur des Mediums, sowie die Korrektur für mehrere
       mögliche Fehlbrennungen und prüft die Indexdateien.

       Es ist notwendig, apt-cdrom zu benutzen, um CDs zum APT-System
       hinzuzufügen. Dies kann nicht manuell erfolgen. Weiterhin muss jedes
       Medium in einer Zusammenstellung aus mehreren CDs einzeln eingelegt und
       gescannt werden, um auf mögliche Fehlbrennungen zu testen.

       Außer wenn die Option -h oder --help angegeben wurde, muss einer der
       beiden Befehle unterhalb gegeben sein.

       add
	   add wird benutzt, um ein neues Medium zur Quellenliste
	   hinzuzufügen. Es wird das CD-ROM-Gerät aushängen, verlangen, dass
	   ein Medium eingelegt wird und dann mit den Einlesen und Kopieren
	   der Indexdateien fortfahren. Wenn das Medium kein angemessenes
	   disk-Verzeichnis hat, werden Sie nach einem aussagekräftigen Titel
	   gefragt.

	   APT benutzt eine CD-ROM-ID, um nachzuverfolgen, welches Medium
	   gerade im Laufwerk ist und verwaltet eine Datenbank mit diesen IDs
	   in /var/lib/apt/cdroms.list

       ident
	   Ein Fehlersuchwerkzeug, um die Identität des aktuellen Mediums
	   sowie den gespeicherten Dateinamen zu berichten.

OPTIONEN
       Alle Befehlszeilenoptionen können durch die Konfigurationsdatei gesetzt
       werden, die Beschreibung gibt die zu setzende Option an. Für boolesche
       Optionen können Sie die Konfigurationsdatei überschreiben, indem Sie
       etwas wie -f-, --no-f, -f=no oder etliche weitere Varianten benutzen.

       -d, --cdrom
	   Einhängepunkt. Gibt den Ort an, an dem die CD-ROM eingehängt wird.
	   Dieser Einhängepunkt muss in /etc/fstab eingetragen und angemessen
	   konfiguriert sein. Konfigurationselement: Acquire::cdrom::mount.

       -r, --rename
	   Ein Medium umbenennen. Ändert den Namen eines Mediums oder
	   überschreibt den Namen, der dem Medium gegeben wurde. Diese Option
	   wird apt-cdrom veranlassen, nach einem neuen Namen zu fragen.
	   Konfigurationselement: APT::CDROM::Rename.

       -m, --no-mount
	   Kein Einhängen. Hindert apt-cdrom am Ein- und Aushängen des
	   Einhängepunkts. Konfigurationselement: APT::CDROM::NoMount.

       -f, --fast
	   Schnelle Kopie. Unterstellt, dass die Paketdateien gültig sind und
	   prüft nicht jedes Paket. Diese Option sollte nur benutzt werden,
	   wenn apt-cdrom vorher für dieses Medium ausgeführt wurde und keine
	   Fehler festgestellt hat. Konfigurationselement: APT::CDROM::Fast.

       -a, --thorough
	   Gründliche Paketdurchsuchung. Diese Option könnte für einige alte
	   Debian-1.1/1.2-Medien nötig sein, die Paketdateien an seltsamen
	   Orten haben. Dies verlängert das Durchsuchen des Mediums deutlich,
	   nimmt aber alle auf.

       -n, --just-print, --recon, --no-act
	   Keine Änderungen. Die sources.list(5)-Datei nicht ändern und keine
	   Indexdateien schreiben. Alles wird jedoch immer noch geprüft.
	   Konfigurationselement: APT::CDROM::NoAct.

       -h, --help
	   Ein kurze Aufrufzusammenfassung zeigen.

       -v, --version
	   Die Version des Programms anzeigen.

       -c, --config-file
	   Konfigurationsdatei; Gibt eine Konfigurationssdatei zum Benutzen
	   an. Das Programm wird die Vorgabe-Konfigurationsdatei und dann
	   diese Konfigurationsdatei lesen. Lesen Sie apt.conf(5), um
	   Syntax-Informationen zu erhalten

       -o, --option
	   Eine Konfigurationsoption setzen; Dies wird eine beliebige
	   Konfigurationsoption setzen. Die Syntax lautet -o Foo::Bar=bar.  -o
	   und --option kann mehrfach benutzt werden, um verschiedene Optionen
	   zu setzen.

SIEHE AUCH
       apt.conf(5), apt-get(8), sources.list(5)

DIAGNOSE
       apt-cdrom gibt bei normalen Operationen 0 zurück, dezimal 100 bei
       Fehlern.

FEHLER
       APT-Fehlerseite[1]. Wenn Sie einen Fehler in APT berichten möchten,
       lesen Sie bitte /usr/share/doc/debian/bug-reporting.txt oder den
       reportbug(1)-Befehl. Verfassen Sie Fehlerberichte bitte auf Englisch.

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2009 von Chris Leick c.leick@vollbio.de
       angefertigt in Zusammenarbeit mit dem Debian German-l10n-Team
       debian-l10n-german@lists.debian.org.

       Note that this translated document may contain untranslated parts. This
       is done on purpose, to avoid losing content when the translation is
       lagging behind the original content.

AUTHORS
       Jason Gunthorpe

       APT-Team

NOTES
	1. APT-Fehlerseite
	   http://bugs.debian.org/src:apt



Linux			       14. Februar 2004 		  APT-CDROM(8)
