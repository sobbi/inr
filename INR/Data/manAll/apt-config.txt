APT-CONFIG(8)			      APT			 APT-CONFIG(8)



NAME
       apt-config - APT-Konfigurationsabfrageprogramm

SYNOPSIS
       apt-config [-hv] [-o=Konfigurationszeichenkette] [-c=Datei] {[shell] |
		  [Abbild]}

BESCHREIBUNG
       apt-config ist ein internes Programm, das von vielen Teilen des
       APT-Pakets benutzt wird, um durchgängige Konfigurierbarkeit
       bereitzustellen. Es greift auf die Hauptkonfigurationsdatei
       /etc/apt/apt.conf auf eine Art zu, die leicht für geskriptete
       Anwendungen zu benutzen ist.

       Außer, wenn die -h- oder --help-Option angegeben wurde, muss einer der
       Befehle unterhalb vorkommen.

       shell
	   shell wird benutzt, um aus einem Shellskript auf
	   Konfigurationsinformationen zuzugreifen. Es wird ein Paar aus
	   Argumenten angegeben – das erste als Shell-Variable und das zweite
	   als Konfigurationswert zum Abfragen. Als Ausgabe listet es eine
	   Serie von Shell-Zuweisungsbefehlen für jeden vorhandenen Wert auf.
	   In einen Shellskript sollte es wie folgt benutzt werden:

	       OPTS="-f"
	       RES=`apt-config shell OPTS MyApp::options`
	       eval $RES
	   Dies wird die Shell-Umgebungsvariable $OPT auf den Wert von
	   MyApp::options mit einer Vorgabe von -f setzen.

	   An das Konfigurationselement kann /[fdbi] angehängt werden. »f«
	   gibt Dateinamen zurück, »d« gibt Verzeichnisse zurück, »b« gibt
	   true oder false zurück und »i« gibt eine Ganzzahl zurück. Jede
	   Rückgabe ist normiert und intern geprüft.

       dump
	   Nur der Inhalt des Konfigurationsbereichs wird angezeigt.

OPTIONEN
       Alle Befehlszeilenoptionen können durch die Konfigurationsdatei gesetzt
       werden, die Beschreibung gibt die zu setzende Option an. Für boolesche
       Optionen können Sie die Konfigurationsdatei überschreiben, indem Sie
       etwas wie -f-, --no-f, -f=no oder etliche weitere Varianten benutzen.

       -h, --help
	   Ein kurze Aufrufzusammenfassung zeigen.

       -v, --version
	   Die Version des Programms anzeigen.

       -c, --config-file
	   Konfigurationsdatei; Gibt eine Konfigurationssdatei zum Benutzen
	   an. Das Programm wird die Vorgabe-Konfigurationsdatei und dann
	   diese Konfigurationsdatei lesen. Lesen Sie apt.conf(5), um
	   Syntax-Informationen zu erhalten

       -o, --option
	   Eine Konfigurationsoption setzen; Dies wird eine beliebige
	   Konfigurationsoption setzen. Die Syntax lautet -o Foo::Bar=bar.  -o
	   und --option kann mehrfach benutzt werden, um verschiedene Optionen
	   zu setzen.

SIEHE AUCH
       apt.conf(5)

DIAGNOSE
       apt-config gibt bei normalen Operationen 0 zurück, dezimal 100 bei
       Fehlern.

FEHLER
       APT-Fehlerseite[1]. Wenn Sie einen Fehler in APT berichten möchten,
       lesen Sie bitte /usr/share/doc/debian/bug-reporting.txt oder den
       reportbug(1)-Befehl. Verfassen Sie Fehlerberichte bitte auf Englisch.

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2009 von Chris Leick c.leick@vollbio.de
       angefertigt in Zusammenarbeit mit dem Debian German-l10n-Team
       debian-l10n-german@lists.debian.org.

       Note that this translated document may contain untranslated parts. This
       is done on purpose, to avoid losing content when the translation is
       lagging behind the original content.

AUTHORS
       Jason Gunthorpe

       APT-Team

NOTES
	1. APT-Fehlerseite
	   http://bugs.debian.org/src:apt



Linux			       29. Februar 2004 		 APT-CONFIG(8)
