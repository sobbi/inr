APT-EXTRACTTEMPLATE(1)		      APT		APT-EXTRACTTEMPLATE(1)



NAME
       apt-extracttemplates - Hilfsprogramm zum Extrahieren der
       DebConf-Konfiguration und Schablonen von Debian-Paketen

SYNOPSIS
       apt-extracttemplates [-hv] [-t=temporäres Verzeichnis] Datei...

BESCHREIBUNG
       apt-extracttemplates nimmt als Eingabe ein oder mehrere
       Debian-Paketdateien entgegen und schreibt alle verbundenen
       Konfigurationsskripte und Schablonendateien (in ein temporäres
       Verzeichnis) heraus. Für jedes übergebene Paket das
       Konfigurationsskripte und Schablonendateien enthält, wird eine
       Ausgabezeile in folgendem Format generiert:

       Paket Version Schablonendatei Konfigurationsskript

       Schablonendatei und Konfigurationsskript werden in das temporäre
       Verzeichnis geschrieben, das durch »-t« oder »--tempdir«
       (APT::ExtractTemplates::TempDir) Verzeichnis mit Dateinamen der Form
       package. template.XXXX und package.config.XXXX angegeben wurde

OPTIONEN
       Alle Befehlszeilenoptionen können durch die Konfigurationsdatei gesetzt
       werden, die Beschreibung gibt die zu setzende Option an. Für boolesche
       Optionen können Sie die Konfigurationsdatei überschreiben, indem Sie
       etwas wie -f-, --no-f, -f=no oder etliche weitere Varianten benutzen.

       -t, --tempdir
	   Temporäres Verzeichnis, in das die extrahierten
	   DebConf-Schablonendateien und Konfigurationsdateien geschrieben
	   werden. Konfigurationselement: APT::ExtractTemplates::TempDir

       -h, --help
	   Ein kurze Aufrufzusammenfassung zeigen.

       -v, --version
	   Die Version des Programms anzeigen.

       -c, --config-file
	   Konfigurationsdatei; Gibt eine Konfigurationssdatei zum Benutzen
	   an. Das Programm wird die Vorgabe-Konfigurationsdatei und dann
	   diese Konfigurationsdatei lesen. Lesen Sie apt.conf(5), um
	   Syntax-Informationen zu erhalten

       -o, --option
	   Eine Konfigurationsoption setzen; Dies wird eine beliebige
	   Konfigurationsoption setzen. Die Syntax lautet -o Foo::Bar=bar.  -o
	   und --option kann mehrfach benutzt werden, um verschiedene Optionen
	   zu setzen.

SIEHE AUCH
       apt.conf(5)

DIAGNOSE
       apt-extracttemplates gibt bei normalen Operationen 0 zurück, dezimal
       100 bei Fehlern.

FEHLER
       APT-Fehlerseite[1]. Wenn Sie einen Fehler in APT berichten möchten,
       lesen Sie bitte /usr/share/doc/debian/bug-reporting.txt oder den
       reportbug(1)-Befehl. Verfassen Sie Fehlerberichte bitte auf Englisch.

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2009 von Chris Leick c.leick@vollbio.de
       angefertigt in Zusammenarbeit mit dem Debian German-l10n-Team
       debian-l10n-german@lists.debian.org.

       Note that this translated document may contain untranslated parts. This
       is done on purpose, to avoid losing content when the translation is
       lagging behind the original content.

AUTHORS
       Jason Gunthorpe

       APT-Team

NOTES
	1. APT-Fehlerseite
	   http://bugs.debian.org/src:apt



Linux			       29. Februar 2004 	APT-EXTRACTTEMPLATE(1)
