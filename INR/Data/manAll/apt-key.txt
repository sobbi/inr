APT-KEY(8)			      APT			    APT-KEY(8)



NAME
       apt-key - APT-Schlüsselverwaltungsdienstprogramm

SYNOPSIS
       apt-key [--keyring filename] [command] [arguments...]

BESCHREIBUNG
       apt-key wird benutzt, um eine Liste von Schlüsseln zu verwalten, die
       APT benutzt, um Pakete zu authentifizieren. Pakete, die durch Benutzung
       dieser Schlüssel authentifiziert wurden, werden als vertrauenswürdig
       betrachtet.

BEFEHLE
       add Dateiname
	   Einen neuen Schlüssel zur Liste der vertrauenswürdigen Schlüssel
	   hinzufügen. Der Schlüssel wird aus Dateiname gelesen oder, wenn
	   Dateiname - ist, von der Standardeingabe.

       del Schlüssel-ID
	   Einen Schlüssel von der Liste der vertrauenswürdigen Schlüssel
	   entfernen.

       export Schlüssel-ID
	   Den Schlüssel Schlüssel-ID auf der Standardausgabe ausgeben.

       exportall
	   Alle vertrauenswürdigen Schlüssel auf der Standardausgabe ausgeben.

       list
	   Vertrauenswürdige Schlüssel auflisten.

       finger
	   Fingerabdrücke vertrauenswürdiger Schlüssel auflisten.

       adv
	   Erweitere Optionen an gpg weiterleiten. Mit adv --recv-key können
	   Sie den öffentlichen Schlüssel herunterladen.

       update
	   Den lokalen Schlüsselring mit dem Schlüsselring der
	   Debian-Archivschlüssel aktualisieren und aus dem Schlüsselring die
	   Archivschlüssel entfernen, die nicht länger gültig sind.

OPTIONEN
       Note that options need to be defined before the commands described in
       the previous section.

       --keyring filename
	   With this option it is possible to specify a specific keyring file
	   the command should operate on. The default is that a command is
	   executed on the trusted.gpg file as well as on all parts in the
	   trusted.gpg.d directory, through trusted.gpg is the primary keyring
	   which means that e.g. new keys are added to this one.

DATEIEN
       /etc/apt/trusted.gpg
	   Keyring of local trusted keys, new keys will be added here.
	   Configuration Item: Dir::Etc::Trusted.

       /etc/apt/trusted.gpg.d/
	   File fragments for the trusted keys, additional keyrings can be
	   stored here (by other packages or the administrator). Configuration
	   Item Dir::Etc::TrustedParts.

       /etc/apt/trustdb.gpg
	   Lokale Datenbank vertrauenswürdiger Archivschlüssel.

       /usr/share/keyrings/debian-archive-keyring.gpg
	   Schlüsselring vertrauenswürdiger Schlüssel des Debian-Archivs.

       /usr/share/keyrings/debian-archive-removed-keys.gpg
	   Schlüsselring entfernter vertrauenswürdiger Schlüssel des
	   Debian-Archivs.

SIEHE AUCH
       apt-get(8), apt-secure(8)

FEHLER
       APT-Fehlerseite[1]. Wenn Sie einen Fehler in APT berichten möchten,
       lesen Sie bitte /usr/share/doc/debian/bug-reporting.txt oder den
       reportbug(1)-Befehl. Verfassen Sie Fehlerberichte bitte auf Englisch.

AUTOR
       APT wurde vom APT-Team geschrieben apt@packages.debian.org.

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2009 von Chris Leick c.leick@vollbio.de
       angefertigt in Zusammenarbeit mit dem Debian German-l10n-Team
       debian-l10n-german@lists.debian.org.

       Note that this translated document may contain untranslated parts. This
       is done on purpose, to avoid losing content when the translation is
       lagging behind the original content.

AUTHOR
       Jason Gunthorpe

COPYRIGHT
       Copyright © 1998-2001 Jason Gunthorpe

NOTES
	1. APT-Fehlerseite
	   http://bugs.debian.org/src:apt



Linux			       28. Oktober 2008 		    APT-KEY(8)
