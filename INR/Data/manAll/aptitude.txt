APTITUDE(8)		aptitude command-line referenc		   APTITUDE(8)



NAME
       aptitude - Benutzerschnittstelle für den Paketmanager

SYNOPSIS
       aptitude [<Optionen>...] {autoclean | clean | forget-new | update |
		upgrade}

       aptitude [<Optionen>...] {changelog | dist-upgrade | download |
		forbid-version | hold | markauto | purge | reinstall | remove
		| show | unmarkauto} Pakete...

       aptitude [<Optionen>...] search Suchmuster...

       aptitude [-S <Dateiname>] [-u | -i]

       aptitude help

BESCHREIBUNG
       aptitude ist eine konsolenbasierte Benutzerschnittstelle für das
       Debian-GNU/Linux-Paketsystem.

       Es zeigt dem Benutzer die Liste der Pakete an und ermöglicht
       Paketmanagement wie das Installieren, Aktualisieren und Löschen von
       Paketen.  aptitude verfügt über einen visuellen Modus, kann aber auch
       von der Kommandozeile gesteuert werden.

KOMMANDOZEILENBEFEHLE
       Das erste Argument, das nicht mit einem Minus (“-”) beginnt, wird als
       Befehl an das Programm angesehen. Wenn kein Befehl übergeben wird,
       startet aptitude im visuellen Modus.

       Die folgenden Befehle sind verfügbar:

       install
	   Ein oder mehrere Pakete installieren. Die Pakete sollten nach dem
	   “install”-Befehl aufgelistet werden. Enthält ein Paketname eine
	   Tilde (“~”), so wird er als Suchmuster behandelt und jedes auf das
	   Muster passende Paket wird installiert (siehe “Search Patterns” in
	   der aptitude-Benutzeranleitung).

	   Um eine bestimmte Version des Pakets zu installieren, hängen Sie
	   “=<version>” an den Namen, zum Beispiel: “aptitude install
	   apt=3.1.4”. Um ein Paket aus einem bestimmten Archiv zu
	   installieren, hängen Sie “/<archiv>” an den Namen, zum Beispiel:
	   “aptitude install apt/experimental”.

	   Das Anhängen von “-”, “+”, “_”, oder “=” an den Paketnamen führt
	   dazu, dass das Paket entfernt, installiert, vollständig entfernt
	   (“purge”) oder gehalten wird. So können mehrere Befehle in einer
	   Befehlszeile gebündelt werden.


	   “install” ohne weitere Argumente wird eventuelle gespeicherten
	   anstehenden Befehle ausführen.

	       Note
	       Das “install”-Kommando ändert aptitudes Liste der anstehenden
	       Änderungen. Deswegen müssen Sie, wenn Sie zum Beispiel den
	       Befehl “aptitude install foo bar” eingeben und die Installation
	       abbrechen, danach “aptitude remove foo bar” ausführen, um den
	       Befehl zurückzunehmen.

       remove, purge, hold, reinstall
	   Diese Befehle verhalten sich ähnlich wie “install”, sie entfernen,
	   “purgen” oder halten jedoch die angegebenen Pakete, wenn nicht
	   anders spezifiziert.

	   Zum Beispiel löscht “aptitude remove '~ndeity'” alle Pakete, deren
	   Name “deity” enthält.

       markauto, unmarkauto
	   Pakete als automatisch bzw. manuell installiert markieren. Sie
	   können Pakete mit derselben Syntax angeben wie oben bei “install”
	   beschrieben. Beispielsweise wird “aptitude markauto '~slibs'” alle
	   Pakete im Bereich (s wie “section”) “libs” als automatisch
	   installiert markieren.

	   Weitere Informationen zu automatisch installierten Paketen finden
	   Sie unter “Managing Automatically Installed Packages” in der
	   aptitude-Benutzeranleitung.

       forbid-version
	   Verbieten, ein Paket auf eine bestimmte Version zu aktualisieren.
	   Dies wird aptitude daran hindern, das Paket automatisch auf die
	   angegebene Version zu aktualisieren, jedoch Upgrades auf spätere
	   Versionen zulassen. Standardmäßig wird aptitude die Version
	   verbieten, auf die normalerweise aktualisiert werden würde; Sie
	   können aber durch Anhängen von “=<version>” eine bestimmte Version
	   angeben. Beispiel: “aptitude forbid-version vim=1.2.3.broken-4”.

	   Dieser Befehl ist nützlich, um fehlerhafte Versionen eines Pakets
	   zu meiden, ohne das Paket manuell “zurückzuhalten” und wieder
	   freizugeben. Wenn Sie später die verbotene Version doch
	   installieren möchten, können Sie das Verbot mit dem
	   “install”-Kommando aufheben.

       update
	   Die Liste der verfügbaren Pakete von den apt-Quellen erneuern.
	   (Dies ist äquivalent zu “apt-get update”.)

       upgrade
	   Aktualisiert installierte Pakete auf die neueste Version.
	   Installierte Pakete werden nicht entfernt, solange sie noch
	   benötigt werden (siehe den Abschnitt “Managing Automatically
	   Installed Packages” in der aptitude-Benutzeranleitung); Pakete, die
	   zur Zeit nicht installiert sind, werden nicht installiert.

	   Wenn ein Paket nicht aktualisiert werden kann, ohne diese Regeln zu
	   verletzen, wird die derzeitige Version beibehalten.

       forget-new
	   Vergisst die Liste der “neuen” Pakete (äquivalent zur Taste “f” im
	   visuellen Modus).

       search
	   Sucht nach Paketen, die auf einen oder mehrere Ausdrücke passen.
	   Diese Ausdrücke sollten nach dem “search”-Befehl aufgelistet
	   werden. Alle passenden Pakete werden angezeigt. Es können alle
	   Suchmuster verwendet werden; zum Beispiel gibt “aptitude search
	   '~N'” die Liste der “neuen” Pakete aus. Suchmuster werden in
	   “Search Patterns” in der aptitude-Benutzeranleitung genauer
	   beschrieben.

       show
	   Detaillierte Informationen über eines oder mehrere Pakete ausgeben.
	   Die Pakete sollten nach dem “show”-Befehl angegeben werden.
	   Suchmuster sind zulässig.

       clean
	   Alle heruntergeladenen und zwischengespeicherten .deb-Dateien aus
	   dem Paketcache löschen. Der Paketcache liegt normalerweise unter
	   /var/cache/apt/archives.

       autoclean
	   Löscht alle zwischengespeicherten Paketdateien, die nicht mehr
	   heruntergeladen werden können. Dies verhindert das grenzenlose
	   Wachstum des Cacheverzeichnisses, ohne es vollständig zu leeren.

       changelog
	   Lädt das Debian-Änderungsprotokoll (“Changelog”) der angegebenen
	   Pakete herunter und zeigt es an.

	   Normalerweise wird der Changelog der aktuellen Version
	   heruntergeladen; Sie können eine bestimmte Version eines Pakets
	   durch Anhängen von =<version> an den Paketnamen auswählen; Sie
	   können die Version aus einem bestimmten Archiv durch Anhängen von
	   /<archiv> an den Paketnamen auswählen.

       download
	   Lädt die .deb-Datei des angegebenen Pakets herunter und speichert
	   sie im aktuellen Verzeichnis.

	   Normalerweise wird die aktuelle Version heruntergeladen; Sie können
	   eine bestimmte Version eines Pakets durch Anhängen von =<version>
	   an den Paketnamen auswählen; Sie können die Version aus einem
	   bestimmten Archiv durch Anhängen von /<archiv> an den Paketnamen
	   auswählen.

       help
	   Zeigt eine kurze Zusammenfassung der verfübaren Befehle und
	   Optionen an.

OPTIONEN
       Die folgenden Optionen können angegeben werden, um das Verhalten der
       obigen Befehle zu verändern. Nicht jeder Befehl wird jede Option
       beachten; manche Optionen ergeben für einige Befehle keinen Sinn.

       -D, --show-deps
	   Anzeigen einer kurzen Erläuterung, warum Pakete automatisch
	   installiert oder gelöscht werden.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Show-Deps.

       -d, --download-only
	   Die erforderlichen Paketdateien herunterladen und im Paketcache
	   speichern, aber nichts installieren oder löschen.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Download-Only.

       -F <format>, --display-format <format>
	   Das Format angeben, in dem die Ausgaben des search-Befehls
	   erfolgen. Zum Beispiel zeigt “%p %V %v” den Paketnamen gefolgt von
	   der installierten und der verfügbaren Version an (siehe
	   “Customizing how packages are displayed” in der
	   aptitude-Benutzeranleitung).

	   Dies entspricht der Einstellung
	   Aptitude::CmdLine::Package-Display-Format.

       -f
	   Aggressiver versuchen, die Abhängigkeiten kaputter Pakete zu
	   reparieren.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Fix-Broken.

       -P, --prompt
	   Immer nachfragen, auch wenn nur explizit angeforderte Änderungen
	   durchgeführt werden.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Always-Prompt.

       -R, --without-recommends
	   Empfehlungen nicht als Abhängigkeiten behandeln, wenn neue Pakete
	   installiert werden (dies überschreibt Einstellungen in
	   /etc/apt/apt.conf und ~/.aptitude/config).

	   Dies entspricht der Einstellung Aptitude::Recommends-Important

       -r, --with-recommends
	   Empfehlungen als Abhängigkeiten behandeln, wenn neue Pakete
	   installiert werden (dies überschreibt Einstellungen in
	   /etc/apt/apt.conf und ~/.aptitude/config).

	   Dies entspricht der Einstellung Aptitude::Recommends-Important

       -s, --simulate
	   Aufzählen, was aptitude mit den angegebenen Befehlen tun würde,
	   aber nichts wirklich durchführen. Dies erfordert keine root-Rechte.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Simulate.

       -t <release>, --target-release <release>
	   Das “Release”, aus dem Pakete installiert werden sollen. Zum
	   Beispiel zieht “aptitude -t unstable” Pakete aus der
	   unstable-Distribution vor.

	   Dies entspricht der Einstellung APT::Default-Release.

       -O <reihenfolge>, --sort <reihenfolge>
	   Die Reihenfolge angeben, in der die Ausgaben des search-Kommandos
	   erfolgen. Zum Beispiel sortiert “installsize” in aufsteigender
	   Reihenfolge der Installationsgröße (siehe “Customizing how packages
	   are sorted” in der aptitude-Benutzeranleitung).

       -o <schl>=<wert>
	   Eine Einstellung direkt setzen; zum Beispiel können Sie -o
	   Aptitude::Log=/tmp/my-log verwenden, um aptitudes Handeln nach
	   /tmp/my-log zu loggen. Weitere Informationen zu den Einstellungen
	   in den Konfigurationsdateien finden Sie in “Configuration file
	   reference” in der aptitude-Benutzeranleitung.

       -V, --show-versions
	   Anzeigen, welche Versionen installiert werden.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Show-Versions.

       -v, --verbose
	   Veranlasst einige Befehle (z.B.  show), mehr Informationen
	   anzuzeigen. Diese Option kann mehrfach angegeben werden um noch
	   mehr Informationen anzuzeigen.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Verbose.

       --version

	   aptitudes Versionsnummer anzeigen.

       --visual-preview
	   Die Vorschau nicht auf der Kommandozeile anzeigen, sondern die
	   visuelle Schnittstelle starten und deren Vorschaubildschirm
	   verwenden.

       -w <breite>, --width <breite>
	   Die Anzeigebreite, die für die Ausgabe des search-Befehls verwendet
	   wird, einstellen. (Per Vorgabe wird die Breite des Terminals
	   verwendet.)

	   Dies entspricht der Einstellung
	   Aptitude::CmdLine::Package-Display-Width

       -y, --assume-yes

	   “Ja” als Antwort für einfache Ja/Nein-Fragen annehmen. Dies hat
	   keinen Einfluss auf besonders gefährliche Aktionen, wie das
	   Entfernen von essentiellen Paketen. Setzt -P außer Kraft.

	   Dies entspricht der Einstellung Aptitude::CmdLine::Assume-Yes.

       -Z
	   Anzeigen, wieviel Plattenspeicher durch die Installation / das
	   Upgrade / das Löschen der einzelnen Pakete belegt oder freigegeben
	   wird.

	   Dies entspricht der Einstellung
	   Aptitude::CmdLine::Show-Size-Changes.

       Die folgenden Optionen sind Befehle für den visuellen Modus von
       aptitude. Sie werden intern verwendet und Sie sollten sie nicht selbst
       verwenden müssen.

       -S <dateiname>
	   Die erweiterten Statusinformationen aus <dateiname> laden, nicht
	   aus der Standarddatei.

       -u
	   Die Paketlisten aktualisieren, wenn das Programm startet. Sie
	   können diese Option und -i nicht kombinieren.

       -i
	   Den Vorschaubildschirm anzeigen, wenn das Programm startet
	   (äquivalent zur Taste “g”). Sie können diese Option nicht mit “-u”
	   kombinieren.

SIEHE AUCH
       apt-get(8), apt(8), /usr/share/doc/aptitude/html/<lang>/index.html from
       the package aptitude-doc-<lang>

AUTORS
       Daniel Burrows <dburrows@debian.org>
	   Author.

       Sebastian Kapfer <sebastian_kapfer@gmx.net>
	   Übersetzung

COPYRIGHT
       This manual page is free software; you can redistribute it and/or
       modify it under the terms of the GNU General Public License as
       published by the Free Software Foundation; either version 2 of the
       License, or (at your option) any later version.

       This manual page is distributed in the hope that it will be useful, but
       WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
       General Public License for more details.

AUTOR
       Diese Handbuchseite wurde von Daniel Burrows <dburrows@debian.org>
       verfasst.

       Deutsche Übersetzung von Sebastian Kapfer <sebastian_kapfer@gmx.net>.

       Bitte melden Sie Fehler.




[FIXME: source] 		  04/09/2010			   APTITUDE(8)
