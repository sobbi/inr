ARMSCII-8(7)		   Linux Programmer's Manual		  ARMSCII-8(7)



NAME
       armscii-8  -  Armenian  Character  Set  encoded	in octal, decimal, and
       hexadecimal

DESCRIPTION
       The Armenian Standard Code for  Information  Interchange,  8-bit  coded
       character set.

   ARMSCII-8 Characters
       The  following  table  displays	the characters in ARMSCII-8, which are
       printable and unlisted in the ascii(7) manual page.  The fourth	column
       will  only  show  the  proper  glyphs  in an environment configured for
       armscii-8.

       Oct   Dec   Hex	 Char	Description
       ────────────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       242   162   a2	  և	ARMENIAN SMALL LIGATURE ECH YIWN
       243   163   a3	  ։	ARMENIAN FULL STOP
       244   164   a4	  )	RIGHT PARENTHESIS
       245   165   a5	  (	LEFT PARENTHESIS
       246   166   a6	  »	RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
       247   167   a7	  «	LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
       250   168   a8	  —	EM DASH
       251   169   a9	  .	FULL STOP
       252   170   aa	  ՝	ARMENIAN COMMA
       253   171   ab	  ,	COMMA
       254   172   ac	  -	HYPHEN-MINUS
       255   173   ad	  ֊	ARMENIAN HYPHEN
       256   174   ae	  …	HORIZONTAL ELLIPSIS
       257   175   af	  ՜	ARMENIAN EXCLAMATION MARK
       260   176   b0	  ՛	ARMENIAN EMPHASIS MARK
       261   177   b1	  ՞	ARMENIAN QUESTION MARK
       262   178   b2	  Ա	ARMENIAN CAPITAL LETTER AYB
       263   179   b3	  ա	ARMENIAN SMALL LETTER AYB
       264   180   b4	  Բ	ARMENIAN CAPITAL LETTER BEN
       265   181   b5	  բ	ARMENIAN SMALL LETTER BEN
       266   182   b6	  Գ	ARMENIAN CAPITAL LETTER GIM
       267   183   b7	  գ	ARMENIAN SMALL LETTER GIM
       270   184   b8	  Դ	ARMENIAN CAPITAL LETTER DA
       271   185   b9	  դ	ARMENIAN SMALL LETTER DA
       272   186   ba	  Ե	ARMENIAN CAPITAL LETTER ECH
       273   187   bb	  ե	ARMENIAN SMALL LETTER ECH
       274   188   bc	  Զ	ARMENIAN CAPITAL LETTER ZA
       275   189   bd	  զ	ARMENIAN SMALL LETTER ZA
       276   190   be	  Է	ARMENIAN CAPITAL LETTER EH
       277   191   bf	  է	ARMENIAN SMALL LETTER EH
       300   192   c0	  Ը	ARMENIAN CAPITAL LETTER ET
       301   193   c1	  ը	ARMENIAN SMALL LETTER ET
       302   194   c2	  Թ	ARMENIAN CAPITAL LETTER TO
       303   195   c3	  թ	ARMENIAN SMALL LETTER TO
       304   196   c4	  Ժ	ARMENIAN CAPITAL LETTER ZHE
       305   197   c5	  ժ	ARMENIAN SMALL LETTER ZHE
       306   198   c6	  Ի	ARMENIAN CAPITAL LETTER INI
       307   199   c7	  ի	ARMENIAN SMALL LETTER INI
       310   200   c8	  Լ	ARMENIAN CAPITAL LETTER LIWN
       311   201   c9	  լ	ARMENIAN SMALL LETTER LIWN
       312   202   ca	  Խ	ARMENIAN CAPITAL LETTER XEH
       313   203   cb	  խ	ARMENIAN SMALL LETTER XEH
       314   204   cc	  Ծ	ARMENIAN CAPITAL LETTER CA
       315   205   cd	  ծ	ARMENIAN SMALL LETTER CA

       316   206   ce	  Կ	ARMENIAN CAPITAL LETTER KEN
       317   207   cf	  կ	ARMENIAN SMALL LETTER KEN
       320   208   d0	  Հ	ARMENIAN CAPITAL LETTER HO
       321   209   d1	  հ	ARMENIAN SMALL LETTER HO
       322   210   d2	  Ձ	ARMENIAN CAPITAL LETTER JA
       323   211   d3	  ձ	ARMENIAN SMALL LETTER JA
       324   212   d4	  Ղ	ARMENIAN CAPITAL LETTER GHAD
       325   213   d5	  ղ	ARMENIAN SMALL LETTER GHAD
       326   214   d6	  Ճ	ARMENIAN CAPITAL LETTER CHEH
       327   215   d7	  ճ	ARMENIAN SMALL LETTER CHEH
       330   216   d8	  Մ	ARMENIAN CAPITAL LETTER MEN
       331   217   d9	  մ	ARMENIAN SMALL LETTER MEN
       332   218   da	  Յ	ARMENIAN CAPITAL LETTER YI
       333   219   db	  յ	ARMENIAN SMALL LETTER YI
       334   219   dc	  Ն	ARMENIAN CAPITAL LETTER NOW
       335   220   dd	  ն	ARMENIAN SMALL LETTER NOW
       336   221   de	  Շ	ARMENIAN CAPITAL LETTER SHA
       337   222   df	  շ	ARMENIAN SMALL LETTER SHA
       340   223   e0	  Ո	ARMENIAN CAPITAL LETTER VO
       341   224   e1	  ո	ARMENIAN SMALL LETTER VO
       342   225   e2	  Չ	ARMENIAN CAPITAL LETTER CHA
       343   226   e3	  չ	ARMENIAN SMALL LETTER CHA
       344   227   e4	  Պ	ARMENIAN CAPITAL LETTER PEH
       345   228   e5	  պ	ARMENIAN SMALL LETTER PEH
       346   229   e6	  Ջ	ARMENIAN CAPITAL LETTER JHEH
       347   230   e7	  ջ	ARMENIAN SMALL LETTER JHEH
       350   231   e8	  Ռ	ARMENIAN CAPITAL LETTER RA
       351   232   e9	  ռ	ARMENIAN SMALL LETTER RA
       352   233   ea	  Ս	ARMENIAN CAPITAL LETTER SEH
       353   234   eb	  ս	ARMENIAN SMALL LETTER SEH
       354   235   ec	  Վ	ARMENIAN CAPITAL LETTER VEW
       355   236   ed	  վ	ARMENIAN SMALL LETTER VEW
       356   237   ee	  Տ	ARMENIAN CAPITAL LETTER TIWN
       357   238   ef	  տ	ARMENIAN SMALL LETTER TIWN
       360   239   f0	  Ր	ARMENIAN CAPITAL LETTER REH
       361   240   f1	  ր	ARMENIAN SMALL LETTER REH
       362   241   f2	  Ց	ARMENIAN CAPITAL LETTER CO
       363   242   f3	  ց	ARMENIAN SMALL LETTER CO
       364   243   f4	  Ւ	ARMENIAN CAPITAL LETTER YIWN
       365   244   f5	  ւ	ARMENIAN SMALL LETTER YIWN
       366   245   f6	  Փ	ARMENIAN CAPITAL LETTER PIWR
       367   246   f7	  փ	ARMENIAN SMALL LETTER PIWR
       370   247   f8	  Ք	ARMENIAN CAPITAL LETTER KEH
       371   248   f9	  ք	ARMENIAN SMALL LETTER KEH
       372   249   fa	  Օ	ARMENIAN CAPITAL LETTER OH
       373   250   fb	  օ	ARMENIAN SMALL LETTER OH
       374   251   fc	  Ֆ	ARMENIAN CAPITAL LETTER FEH
       375   252   fd	  ֆ	ARMENIAN SMALL LETTER FEH
       376   253   fe	  ՚	ARMENIAN APOSTROPHE

SEE ALSO
       ascii(7)


COLOPHON
       This page is part of release 3.23 of the Linux  man-pages  project.   A
       description  of	the project, and information about reporting bugs, can
       be found at http://www.kernel.org/doc/man-pages/.



Linux				  2009-01-21			  ARMSCII-8(7)
