ARP(8)			Handbuch für Linuxprogrammierer 		ARP(8)



NAME
       arp - Manipulation des ARP-Caches

SYNOPSIS
       arp [-vn] [-H Typ] [-i Schnittstelle] -a [Rechnername]

       arp [-v] [-i if] -d Rechnername [pub]

       arp [-v] [-H Typ] [-i Schnittstelle] -s Rechnername hw_adr [temp]

       arp [-v] [-H Typ] [-i Interface] -s Rechnername hw_adr [netmask nm] pub

       arp  [-v]  [-H Typ] [-i Schnittstelle] -Ds Rechnername ifa [netmask nm]
       pub

       arp [-vnD] [-H Typ] [-i Schnittstelle] -f [Dateiname]


BESCHREIBUNG
       Arp kann den ARP-Cache des Kernels auf verschiedene Arten manipulieren.
       Die   hauptsächliche  Verwendung  ist  es  Adresszuordnungseinträge  zu
       löschen und von Hand neue zu erzeugen.  Zum Zweck der  Fehlersuche  ist
       moeglich  mit  dem  arp	Programm den Inhalt des ARP-Caches vollständig
       auszugeben.

OPTIONEN
       -v, --verbose
	      Ausführlichere Ausgaben.

       -n, --numeric
	      macht numerische Adressausgaben anstatt zu versuche, den symbol‐
	      ischen Rechner-, Port- oder Benutzernamen zu ermitteln.

       -H type, --hw-type type
	      Beim  Setzen  oder Auslesen des ARP-Caches schränkt diese Option
	      ein, auf welcher Klasse von Einträgen arp operieren  soll.   Der
	      Standardwert  dieses Arguments ist ether (d.h. Hardwarecode 0x01
	      für IEEE 802.3 10Mbps Ethernet).	 Andere  mögliche  Werte  sind
	      Netzwerkstechnologien  so  wie  z.B.   ARCnet  (arcnet) , PROnet
	      (pronet) , AX.25 (ax25) and NET/ROM (netrom).

       -a [Rechnername], --display [Rechnername]
	      Zeigt die Einträge der angegebenen Rechner an.  Wird kein  host‐
	      name Argument verwendet, so werden alle Einträge aufgelistet.

       -d Rechnername, --delete Rechnername
	      Alle  Einträge  für  den	angegebenen Host entfernen.  Dies kann
	      z.B.  benutzt werden, wenn ein System angehalten wird.

       -D, --use-device
	      Die Hardwareadresse der Netzwerksschnittstelle ifa verwenden.

       -i If, --device Schnittstelle
	      Eine Netzwerksschnittstelle auswählen.  Es werden  nur  Einträge
	      für  die	angegebene Schnittstelle ausgedruckt.  Beim Setzen von
	      von   permanenten   oder	 temporären   Einträgen   wird	 diese
	      Schnittstelle  mit  dem  Eintrag	assoziiert.  Wird diese Option
	      nicht  verwendet,  so  versucht  der  Kernel   auf   Basis   der
	      Routentabelle  eine Schnittstelle auszuwählen.  Für pub Einträge
	      ist die angegebene Schnittstelle diejenige, auf der ARP-Anfragen
	      beantwortet werden.
	      ANMERKUNG: Diese Schnittstelle muß eine andere sein als die, auf
	      die die IP-Datagramme weitergeleitet werden.

       -s Rechnername hw_addr, --set Rechnername
	      Erzeugt manuel einen ARP Adresseintrag für den Rechner  Rechner‐
	      name  in	dem  die Hardwareadresse auf hw_addr gesetzt ist.  Das
	      genaue Format der Hardwareadresse ist  abhängig  von  der  Hard‐
	      wareklasse aber für die meisten Klassen kann man davon ausgehen,
	      daß die übliche Darstellung verwendet wird.  Für die  Ethernetk‐
	      lasse  sind dies sechs hexadezimale, von Doppelpunkten getrennte
	      Bytes.  Beim Zufügen von Proxy-ARP-Enträgen (das	sind  die  mit
	      der gesetzten publizieren Flagge) kann Netmaske für ARP-Einträge
	      für ganze Subnetze angegeben  werde.   Von  dieser  Praxis  wird
	      abgeraten.   Sie	wird  von  älteren Kerneln unterstützt, da sie
	      gelegentlich nützlich ist.  Wird die If the  temp  Flagge  nicht
	      angegeben,  so  werden die erzeugten Einträge nicht dauerhaft in
	      den ARP-Cache eingetragen.
	      ANMERKUNG: Ab der Kernelversion 2.2.0 ist es nicht mehr  möglich
	      ARP-Einträge  für ganze Teilnetze zu erzeugen. Statt dessen wird
	      automatisches Proxy  ARP	durchgeführt,  d.h.  wenn  eine  Route
	      existiert  und Forwarding eingeschaltet ist wird automatisch ein
	      temporärer Proxyarpeintrag erzeugt.  Siehe auch arp(7) für  mehr
	      Details.

       -f Dateiname, --file Dateiname
	      Ähnlich  der -s Option, außer, daß diesmal die Adressinformation
	      aus der Datei Dateiname verwendet  wird.	 Dies  kann  verwendet
	      werden,  wenn  ARP-Einträge  für	etliche Rechner erzeugt werden
	      müssen.  Der Name dieser Datei ist oft  /etc/ethers,  aber  dies
	      ist nicht offizieil standardisiert. Wenn kein Dateinamen angeben
	      ist wird /etc/ethers benutzt.

	      Das  Format  der	Datei  ist  einfach;  es  enthält  nur	ASCII-
	      Textzeilen, die aus einem Rechnernamen und einer Hardwareadresse
	      getrennt von einem Zwischenraum bestehen.  Zusätzlich können die
	      Flaggen pub, temp and netmask angegeben werden.

       Überall,  wo  Rechnername  erwartet  wird, kann auch eine IP-Adresse in
       Form eines durch Punkte getrennten Dezimalquadrupels angegeben werden.

       Aus Kompatiblitätsgründen können Rechnername  und  die  Hardwareadresse
       auch vertauscht werden.

       Jeder vollständige Eintrag wird im ARP-Cache mit der C Flagge markiert.
       Permanente Einträge werden mit M und zu publizierende Einträge mit  der
       P Flagge.

DATEIEN
       /proc/net/arp,
       /etc/networks
       /etc/hosts
       /etc/ethers

SIEHE AUCH
       ethers(5), rarp(8), route(8), ifconfig(8), netstat(8)

AUTOREN
       Fred   N.   van	 Kempen,   <waltje@uwalt.nl.mugnet.org>   mit	vielen
       Verbesserungen	vom   Verwalter   der	Net-Tools   Bernd    Eckenfels
       <net-tools@lina.inka.de>.

Übersetzung
       Ralf Bächle <ralf@gnu.org>



net-tools			 6. März 1999				ARP(8)
