BEGIN(7)			 SQL Commands			      BEGIN(7)



NAME
       BEGIN - start a transaction block


SYNOPSIS
       BEGIN [ WORK | TRANSACTION ] [ transaction_mode [, ...] ]

       where transaction_mode is one of:

	   ISOLATION LEVEL { SERIALIZABLE | REPEATABLE READ | READ COMMITTED | READ UNCOMMITTED }
	   READ WRITE | READ ONLY


DESCRIPTION
       BEGIN  initiates  a  transaction block, that is, all statements after a
       BEGIN command will  be  executed  in  a	single	transaction  until  an
       explicit  COMMIT  [commit(7)]  or  ROLLBACK [rollback(7)] is given.  By
       default (without BEGIN), PostgreSQL executes transactions in ``autocom‐
       mit''  mode, that is, each statement is executed in its own transaction
       and a commit is implicitly performed at the end of  the	statement  (if
       execution was successful, otherwise a rollback is done).

       Statements  are	executed  more quickly in a transaction block, because
       transaction start/commit requires significant CPU  and  disk  activity.
       Execution of multiple statements inside a transaction is also useful to
       ensure consistency when making several related changes: other  sessions
       will  be  unable  to  see  the  intermediate states wherein not all the
       related updates have been done.

       If the isolation level or read/write mode is specified, the new	trans‐
       action  has  those characteristics, as if SET TRANSACTION [set_transac‐
       tion(7)] was executed.

PARAMETERS
       WORK

       TRANSACTION
	      Optional key words. They have no effect.

       Refer to SET TRANSACTION [set_transaction(7)] for  information  on  the
       meaning of the other parameters to this statement.

NOTES
       START  TRANSACTION [start_transaction(7)] has the same functionality as
       BEGIN.

       Use COMMIT [commit(7)] or ROLLBACK [rollback(7)] to terminate a	trans‐
       action block.

       Issuing	BEGIN  when  already inside a transaction block will provoke a
       warning message. The state of the transaction is not affected.  To nest
       transactions  within a transaction block, use savepoints (see SAVEPOINT
       [savepoint(7)]).

       For reasons of backwards compatibility, the commas  between  successive
       transaction_modes can be omitted.

EXAMPLES
       To begin a transaction block:

       BEGIN;


COMPATIBILITY
       BEGIN  is a PostgreSQL language extension. It is equivalent to the SQL-
       standard command START TRANSACTION [start_transaction(7)], whose refer‐
       ence page contains additional compatibility information.

       Incidentally,  the  BEGIN  key  word is used for a different purpose in
       embedded SQL. You are advised  to  be  careful  about  the  transaction
       semantics when porting database applications.

SEE ALSO
       COMMIT	[commit(7)],   ROLLBACK   [rollback(7)],   START   TRANSACTION
       [start_transaction(7)], SAVEPOINT [savepoint(7)]



SQL - Language Statements	  2010-05-19			      BEGIN(7)
