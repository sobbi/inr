BLOCKDEV(8)							   BLOCKDEV(8)



NAME
       blockdev - call block device ioctls from the command line

SYNOPSIS
       blockdev [options] commands devices
       blockdev --report [devices]

DESCRIPTION
       The  utility  blockdev  allows one to call block device ioctls from the
       command line.

OPTIONS
       -V     Print version and exit.

       -q     Be quiet.

       -v     Be verbose.

       --report
	      Print a report for devices.  Note that the partition StartSec is
	      in 512-byte sectors.

COMMANDS
       --setro
	      Set read-only.

       --setrw
	      Set read-write.

       --getro
	      Get read-only. Print 1 if the device is read-only, 0 otherwise.

       --getss
	      Print sectorsize in bytes - usually 512.

       --getbsz
	      Print blocksize in bytes.

       --setbsz N
	      Set blocksize to N bytes.

       --getsize
	      Print  device  size in sectors (BLKGETSIZE). Deprecated in favor
	      of the --getsz option.

       --getsize64
	      Print device size in bytes (BLKGETSIZE64)

       --getsz
	      Get size in 512-byte sectors (BLKGETSIZE64 / 512).

       --setra N
	      Set readahead to N 512-byte sectors.

       --getra
	      Print readahead (in 512-byte sectors).

       --setfra N
	      Set filesystem readahead (same like --setra on 2.6 kernels).

       --getfra
	      Get filesystem readahead.

       --flushbufs
	      Flush buffers.

       --rereadpt
	      Reread partition table.

AUTHOR
       blockdev was written by Andries E. Brouwer.

AVAILABILITY
       The blockdev command is part of the util-linux-ng package and is avail‐
       able from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.




				   Jun 2007			   BLOCKDEV(8)
