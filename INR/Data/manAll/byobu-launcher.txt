byobu-launcher(1)		     byobu		     byobu-launcher(1)



NAME
       byobu-launcher - Byobu Launcher


DESCRIPTION
       byobu-launcher  is  a simple application that will launch "screen" in a
       byobu configuration, reconnecting to an existing detached  session  (if
       available).

       This  script  exists  for  simple  insertion  and removal from a user's
       $HOME/.screenrc file, by the byobu utility.


SEE ALSO
       screen(1), byobu(1)

       http://launchpad.net/byobu


AUTHOR
       This manpage was written by  Dustin  Kirkland  <kirkland@canonical.com>
       for  Ubuntu systems (but may be used by others).  Permission is granted
       to copy, distribute and/or modify this document under the terms of  the
       GNU  General  Public  License, Version 3 published by the Free Software
       Foundation.

       On Debian systems, the complete text of the GNU General Public  License
       can be found in /usr/share/common-licenses/GPL.



byobu				  16 Jan 2009		     byobu-launcher(1)
