byobu-reconnect-sockets(1)	     byobu	    byobu-reconnect-sockets(1)



NAME
       byobu-reconnect-sockets	- Sourcable script that updates GPG_AGENT_INFO
       and DBUS_SESSION_BUS_ADDRESS in the environment


DESCRIPTION
       byobu-reconnect-sockets is a sourcable bit  of  shell  code  that  will
       update  the  GPG_AGENT_INFO  and  DBUS_SESSION_BUS_ADDRESS  environment
       variables in the current shell, such that you may restablish connection
       to gpg-agent(1) and dbus-daemon(1).

       This is often useful when reattaching to a detached Byobu session.


       http://launchpad.net/byobu


AUTHOR
       This  manpage  and  the	utility were written by Dustin Kirkland <kirk‐
       land@canonical.com> for Ubuntu systems (but may	be  used  by  others).
       Permission  is  granted to copy, distribute and/or modify this document
       under the terms of the GNU General Public License, Version 3  published
       by the Free Software Foundation.

       On  Debian systems, the complete text of the GNU General Public License
       can be found in /usr/share/common-licenses/GPL.



byobu				  7 Dec 2009	    byobu-reconnect-sockets(1)
