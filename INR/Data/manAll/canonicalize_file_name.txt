CANONICALIZE_FILE_NAME(3)  Linux Programmer's Manual CANONICALIZE_FILE_NAME(3)



NAME
       canonicalize_file_name -  return the canonicalized filename

SYNOPSIS
       #define _GNU_SOURCE
       #include <stdlib.h>

       char *canonicalize_file_name(const char *path);

DESCRIPTION
       The  call  canonicalize_file_name(path) is equivalent to the call real‐
       path(path, NULL).

CONFORMING TO
       The function is a GNU extension.

SEE ALSO
       realpath(3), feature_test_macros(7)

COLOPHON
       This page is part of release 3.23 of the Linux  man-pages  project.   A
       description  of	the project, and information about reporting bugs, can
       be found at http://www.kernel.org/doc/man-pages/.



GNU				  2005-07-14	     CANONICALIZE_FILE_NAME(3)
