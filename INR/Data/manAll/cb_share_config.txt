man(1)			   cb_share_config man page			man(1)



NAME
       cb_share_config - Import/export parts of Code::Blocks configuration

SYNOPSIS
       cb_share_config

DESCRIPTION
       cb_share_config	 allows   you	to   share  (import/export)  parts  of
       Code::Blocks configuration. It is useful, for example, when you want to
       partially transfer settings between computers or even between different
       configurations.

AUTHOR
       Martin Halle

HISTORY
       2007 - Initial version



1.0				 16 July 2007				man(1)
