CBRT(3) 		   Linux Programmer's Manual		       CBRT(3)



NAME
       cbrt, cbrtf, cbrtl - cube root function

SYNOPSIS
       #include <math.h>

       double cbrt(double x);
       float cbrtf(float x);
       long double cbrtl(long double x);

       Link with -lm.

   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):

       cbrt(): _BSD_SOURCE || _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
       _ISOC99_SOURCE; or cc -std=c99
       cbrtf(), cbrtl(): _BSD_SOURCE || _SVID_SOURCE || _XOPEN_SOURCE >= 600
       || _ISOC99_SOURCE; or cc -std=c99

DESCRIPTION
       The  cbrt()  function returns the (real) cube root of x.  This function
       cannot fail; every representable real value has	a  representable  real
       cube root.

RETURN VALUE
       These functions return the cube root of x.

       If  x  is  +0,  -0,  positive infinity, negative infinity, or NaN, x is
       returned.

ERRORS
       No errors occur.

CONFORMING TO
       C99, POSIX.1-2001.

SEE ALSO
       pow(3), sqrt(3)

COLOPHON
       This page is part of release 3.23 of the Linux  man-pages  project.   A
       description  of	the project, and information about reporting bugs, can
       be found at http://www.kernel.org/doc/man-pages/.



GNU				  2008-08-05			       CBRT(3)
