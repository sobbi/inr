CHFN(1) 		 Dienstprogramme für Benutzer		       CHFN(1)



NAME
       chfn - ändert den Benutzernamen und sonstige Informationen

SYNOPSIS
       chfn [-f Voller_Name] [-r Zimmer_Nr] [-w Tel_Arbeit] [-h Tel_Privat]
	    [-o sonstiges] [Benutzer]

BESCHREIBUNG
       Der Befehl chfn ändert den vollen Namen, die Büronummer und
       Büroerweiterung sowie private Telefonnummer für ein Benutzerkonto.
       Diese Informationen werden typischerweise von finger(1) und anderen
       Programmen verwendet. Ein normaler Benutzer darf nur die Felder für
       seines eigenen Kontos ändern und muss dabei zudem die Einschränkungen
       in /etc/login.defs beachten. So ist es Benutzern standardmäßig nicht
       möglich, ihren vollen Namen zu ändern. Root kann die Felder aller
       Konten verändern. Außerdem darf nur Root mittels der Option -o die
       undefinierten Teile des GECOS-Feldes ändern.

       These fields must not contain any colons. Except for the other field,
       they should not contain any comma or equal sign. It is also recommended
       to avoid non-US-ASCII characters, but this is only enforced for the
       phone numbers. The other field is used to store accounting information
       used by other applications.

       Wenn keine Option ausgewählt wird, arbeitet chfn interaktiv. Dabei wird
       der Benutzer nach den aktuellen Werten für alle Felder befragt. Bei
       Eingabe eines neuen Wertes wird dieser verwendet, bei Eingabe einer
       Leerzeile stattdessen der Originalwert beibehalten. Der aktuelle Wert
       wird zwischen einem Paar von [ ] angezeigt. Ohne Optionen fragt chfn
       nach einem Benutzerkonto.

CONFIGURATION
       The following configuration variables in /etc/login.defs change the
       behavior of this tool:

       CHFN_RESTRICT (string)
	   This parameter specifies which values in the gecos field of the
	   /etc/passwd file may be changed by regular users using the chfn
	   program. It can be any combination of letters f, r, w, h, for Full
	   name, Room number, Work phone, and Home phone, respectively. For
	   backward compatibility, yes is equivalent to rwh and no is
	   equivalent to frwh. If not specified, only the superuser can make
	   any changes. The most restrictive setting is better achieved by not
	   installing chfn SUID.

DATEIEN
       /etc/login.defs
	   Konfiguration des Shadow-Passwort-Systems

       /etc/passwd
	   Informationen zu den Benutzerkonten.

SIEHE AUCH
       chsh(1), login.defs(5), passwd(5).



Dienstprogramme für Benutzer	  26.01.2010			       CHFN(1)
