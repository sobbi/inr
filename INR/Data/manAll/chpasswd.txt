CHPASSWD(8)		  System Management Commands		   CHPASSWD(8)



NAME
       chpasswd - update passwords in batch mode

SYNOPSIS
       chpasswd [options]

DESCRIPTION
       The chpasswd command reads a list of user name and password pairs from
       standard input and uses this information to update a group of existing
       users. Each line is of the format:

       user_name:password


       The supplied passwords must be in clear-text.

       PAM is used to update the password in the system database according to
       the PAM chpasswd configuration.

       When chpasswd fails to update a password, it continues updating the
       passwords of the next users, and will return an error code on exit.

       This command is intended to be used in a large system environment where
       many accounts are created at a single time.

OPTIONS
       The options which apply to the chpasswd command are:

       -S, --stdout
	   Report encrypted passwords to stdout instead of updating password
	   file.

       -h, --help
	   Display help message and exit.

CAVEATS
       Remember to set permissions or umask to prevent readability of
       unencrypted files by other users.

FILES
       /etc/pam.d/chpasswd
	   PAM configuration for chpasswd.

SEE ALSO
       passwd(1), newusers(8), useradd(8).



System Management Commands	  01/26/2010			   CHPASSWD(8)
