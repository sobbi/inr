COLON(P)		   POSIX Programmer's Manual		      COLON(P)



NAME
       colon - null utility

SYNOPSIS
       : [argument ...]

DESCRIPTION
       This  utility  shall  only  expand command arguments. It is used when a
       command is needed, as in the then condition of an if command, but noth‐
       ing is to be done by the command.

OPTIONS
       None.

OPERANDS
       See the DESCRIPTION.

STDIN
       Not used.

INPUT FILES
       None.

ENVIRONMENT VARIABLES
       None.

ASYNCHRONOUS EVENTS
       Default.

STDOUT
       Not used.

STDERR
       The standard error shall be used only for diagnostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       Zero.

CONSEQUENCES OF ERRORS
       Default.

       The following sections are informative.

APPLICATION USAGE
       None.

EXAMPLES
	      : ${X=abc}
	      if     false
	      then   :
	      else   echo $X
	      fi
	      abc

       As  with  any  of the special built-ins, the null utility can also have
       variable assignments and redirections associated with it, such as:


	      x=y : > z

       which sets variable x to the value y (so that  it  persists  after  the
       null utility completes) and creates or truncates file z.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Special Built-In Utilities

COPYRIGHT
       Portions  of  this text are reprinted and reproduced in electronic form
       from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology
       --  Portable  Operating	System	Interface (POSIX), The Open Group Base
       Specifications Issue 6, Copyright (C) 2001-2003	by  the  Institute  of
       Electrical  and	Electronics  Engineers, Inc and The Open Group. In the
       event of any discrepancy between this version and the original IEEE and
       The  Open Group Standard, the original IEEE and The Open Group Standard
       is the referee document. The original Standard can be  obtained	online
       at http://www.opengroup.org/unix/online.html .



IEEE/The Open Group		     2003			      COLON(P)
