COMPUTER-JANITOR(8)	 Ubuntu system administration	   COMPUTER-JANITOR(8)



NAME
       computer-janitor - clean up a system installation

SYNOPSIS
       computer-janitor find
       computer-janitor cleanup [--all] [CRUFT]...
       computer-janitor ignore [CRUFT]...
       computer-janitor unignore
       computer-janitor-gtk [options] [CRUFT]...

DESCRIPTION
       computer-janitor  and  computer-janitor-gtk  find and remove cruft from
       your system.  The first one is a command line program, the latter has a
       graphical user interface.

       Cruft  is anything that shouldn't be on the system, but is.  Stretching
       the definition, it is also things that should be  on  the  system,  but
       aren't.	Examples:

       ·      Packages	that were installed because something else depended on
	      them, but now nothing depends on them anymore.   Typically  this
	      includes large numbers of libraries.

       ·      Packages	that are no longer supported by the current release of
	      the operating system.  This can, for  example,  be  applications
	      whose development have stopped and that have no support, includ‐
	      ing no security support,	anymore.   Keeping  such  applications
	      installed can be dangerous.

       ·      Configuration tweaks that are missing from the system, but which
	      would be there if the system was installed  from	scratch.   For
	      example,	mount  options for filesystems: the relatime option is
	      an example.

       computer-janitor has four modes of operation, invoked by the first non-
       option word in the argument list.

       ·      find  searches  for  cruft  and prints out a list of them.  Each
	      piece of cruft is also tagged with it's state: ignored or remov‐
	      able.

       ·      cleanup actually removes the cruft.  It will remove those pieces
	      of cruft you name on the command line.  If you  want  to	remove
	      everything identified by find that is marked removable, you need
	      to use the --all option.

       ·      ignore and unignore mark cruft as ignored or removable,  respec‐
	      tively.

OPTIONS
       --all  With  the cleanup operation, remove all pieces of cruft that are
	      not ignored.

       --state-file=FILE
	      Use FILE as the name of the state file, for  persistent  storage
	      of  the  state  (ignored	or  removable) of each piece of cruft.
	      Default is /var/lib/computer-janitor/state.dat.	Use  /dev/null
	      to disable persistent storage of state.

       --no-act
	      Don't  actually remove anything, just pretend to do so.  This is
	      useful for testing stuff.

       --verbose
	      Be verbose: cause find to write a description of each  piece  of
	      cruft  it  finds, to help the user understand why understand why
	      the cruft is cruft.

EXIT STATUS
       computer-janitor will return an exit code of 0 for successful operation
       (no  errors).   It  will  return  a non-zero exit code if there are any
       errors.	It is not an error to find cruft, or to not find cruft.

ENVIRONMENT
       COMPUTER_JANITOR_DEBUG
	      Set this to yes to set the logging level to  DEBUG  rather  than
	      INFO.

       COMPUTER_JANITOR_PLUGINS
	      Set  this to the directory in which computer-janitor should look
	      for plugins.  The default is to look for them in /usr/share/com‐
	      puter-janitor/plugins.

FILES
       /var/lib/computer-janitor/state.dat
	      This  file  stores the state of cruft: ignored or removable.  It
	      only stores the state of cruft that have been state manipulated.
	      The state of cruft not mentioned in this file is removable.

       /etc/computer-janitor.d
	      This directory contains whitelist files, which specify that cer‐
	      tain things are never cruft.  A whitelist file has a  name  that
	      ends  with  .whitelist, and contains one name of cruft per line.
	      (Empty lines and lines beginning with # are ignored.)

EXAMPLE
       To find all cruft on the system:

	      computer-janitor find

       To remove a specific piece of cruft:

	      computer-janitor cleanup hello

       To mark a piece of cruft as  ignored,  so  that	it  isn't  removed  by
       cleanup:

	      computer-janitor ignore hello

       To remove the ignored status:

	      computer-janitor unignore hello

       To remove all cruft that isn't ignored:

	      computer-janitor cleanup --all

SEE ALSO
       computer-janitor-gtk(8).



Ubuntu				  2008-07-25		   COMPUTER-JANITOR(8)
