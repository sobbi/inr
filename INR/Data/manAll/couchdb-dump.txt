COUCHDB-DUMP(1) 		 User Commands		       COUCHDB-DUMP(1)



NAME
       couchdb-dump - a CouchDB dump utility

SYNOPSIS
       couchdb-dump [options] dburl

OPTIONS
       --version
	      show program's version number and exit

       -h, --help
	      show this help message and exit

       --json-module=JSON_MODULE
	      the  JSON  module  to  use ("simplejson", "cjson", or "json" are
	      supported)

       -u USERNAME, --username=USERNAME
	      the username to use for authentication

       -p PASSWORD, --password=PASSWORD
	      the password to use for authentication



couchdb-dump 0.6		  August 2009		       COUCHDB-DUMP(1)
