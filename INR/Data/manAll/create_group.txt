CREATE GROUP(7) 		 SQL Commands		       CREATE GROUP(7)



NAME
       CREATE GROUP - define a new database role


SYNOPSIS
       CREATE GROUP name [ [ WITH ] option [ ... ] ]

       where option can be:

	     SUPERUSER | NOSUPERUSER
	   | CREATEDB | NOCREATEDB
	   | CREATEROLE | NOCREATEROLE
	   | CREATEUSER | NOCREATEUSER
	   | INHERIT | NOINHERIT
	   | LOGIN | NOLOGIN
	   | [ ENCRYPTED | UNENCRYPTED ] PASSWORD 'password'
	   | VALID UNTIL 'timestamp'
	   | IN ROLE rolename [, ...]
	   | IN GROUP rolename [, ...]
	   | ROLE rolename [, ...]
	   | ADMIN rolename [, ...]
	   | USER rolename [, ...]
	   | SYSID uid


DESCRIPTION
       CREATE GROUP is now an alias for CREATE ROLE [create_role(7)].

COMPATIBILITY
       There is no CREATE GROUP statement in the SQL standard.

SEE ALSO
       CREATE ROLE [create_role(7)]



SQL - Language Statements	  2010-05-19		       CREATE GROUP(7)
