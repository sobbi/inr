CREATE LANGUAGE(7)		 SQL Commands		    CREATE LANGUAGE(7)



NAME
       CREATE LANGUAGE - define a new procedural language


SYNOPSIS
       CREATE [ PROCEDURAL ] LANGUAGE name
       CREATE [ TRUSTED ] [ PROCEDURAL ] LANGUAGE name
	   HANDLER call_handler [ VALIDATOR valfunction ]


DESCRIPTION
       Using  CREATE LANGUAGE, a PostgreSQL user can register a new procedural
       language with a PostgreSQL database. Subsequently, functions and  trig‐
       ger procedures can be defined in this new language.

       CREATE  LANGUAGE  effectively  associates the language name with a call
       handler that is responsible for executing functions written in the lan‐
       guage.  Refer  to  in the documentation for more information about lan‐
       guage call handlers.

       There are two forms of the CREATE LANGUAGE command.  In the first form,
       the  user supplies just the name of the desired language, and the Post‐
       greSQL server consults the pg_pltemplate system	catalog  to  determine
       the  correct parameters. In the second form, the user supplies the lan‐
       guage parameters along with the language name.  The second form can  be
       used  to  create  a  language that is not defined in pg_pltemplate, but
       this approach is considered obsolescent.

       When the server finds an entry in the  pg_pltemplate  catalog  for  the
       given  language	name, it will use the catalog data even if the command
       includes language parameters. This behavior simplifies loading  of  old
       dump  files,  which are likely to contain out-of-date information about
       language support functions.

       Ordinarily, the user must have the PostgreSQL  superuser  privilege  to
       register  a new language. However, the owner of a database can register
       a new language within that database if the language is  listed  in  the
       pg_pltemplate  catalog  and is marked as allowed to be created by data‐
       base owners (tmpldbacreate is true). The default is that  trusted  lan‐
       guages  can  be created by database owners, but this can be adjusted by
       superusers by modifying the contents of pg_pltemplate.  The creator  of
       a  language  becomes  its  owner  and  can later drop it, rename it, or
       assign it to a new owner.

PARAMETERS
       TRUSTED
	      TRUSTED specifies that the call  handler	for  the  language  is
	      safe,  that is, it does not offer an unprivileged user any func‐
	      tionality to bypass access restrictions. If  this  key  word  is
	      omitted when registering the language, only users with the Post‐
	      greSQL superuser privilege can use this language to  create  new
	      functions.

       PROCEDURAL
	      This is a noise word.

       name   The  name  of  the new procedural language. The language name is
	      case insensitive. The name must be unique among the languages in
	      the database.

	      For  backward  compatibility, the name can be enclosed by single
	      quotes.

       HANDLER call_handler
	      call_handler is the name of  a  previously  registered  function
	      that  will  be  called  to execute the procedural language func‐
	      tions. The call handler for a procedural language must be  writ‐
	      ten in a compiled language such as C with version 1 call conven‐
	      tion and registered with PostgreSQL  as  a  function  taking  no
	      arguments and returning the language_handler type, a placeholder
	      type that is simply used to identify the function as a call han‐
	      dler.

       VALIDATOR valfunction
	      valfunction is the name of a previously registered function that
	      will be called when a new function in the language  is  created,
	      to validate the new function.  If no validator function is spec‐
	      ified, then a new function will not be checked when it  is  cre‐
	      ated.   The  validator  function	must take one argument of type
	      oid, which will be the OID of the  to-be-created	function,  and
	      will typically return void.

	      A  validator  function would typically inspect the function body
	      for syntactical correctness, but it can also look at other prop‐
	      erties  of the function, for example if the language cannot han‐
	      dle certain argument types. To signal an	error,	the  validator
	      function	should use the ereport() function. The return value of
	      the function is ignored.

       The TRUSTED option and the support function name(s) are ignored if  the
       server has an entry for the specified language name in pg_pltemplate.

NOTES
       The  createlang(1)  program  is a simple wrapper around the CREATE LAN‐
       GUAGE command. It eases installation of procedural languages  from  the
       shell command line.

       Use  DROP  LANGUAGE  [drop_language(7)],  or better yet the droplang(1)
       program, to drop procedural languages.

       The system catalog  pg_language	(see  in  the  documentation)  records
       information  about  the currently installed languages. Also, createlang
       has an option to list the installed languages.

       To create functions in a procedural language,  a  user  must  have  the
       USAGE  privilege for the language. By default, USAGE is granted to PUB‐
       LIC (i.e., everyone) for trusted languages.  This  can  be  revoked  if
       desired.

       Procedural  languages  are  local  to individual databases.  However, a
       language can be installed into the template1 database, which will cause
       it to be available automatically in all subsequently-created databases.

       The  call  handler  function  and  the validator function (if any) must
       already exist if the server does not have an entry for the language  in
       pg_pltemplate.  But  when  there  is  an  entry, the functions need not
       already exist; they will be automatically defined if not present in the
       database.  (This might result in CREATE LANGUAGE failing, if the shared
       library that implements the language is not available in the  installa‐
       tion.)

       In  PostgreSQL versions before 7.3, it was necessary to declare handler
       functions as returning the placeholder type opaque,  rather  than  lan‐
       guage_handler.	To  support loading of old dump files, CREATE LANGUAGE
       will accept a function declared as returning opaque, but it will  issue
       a  notice  and  change  the  function's	declared  return  type to lan‐
       guage_handler.

EXAMPLES
       The preferred way of creating any of the standard procedural  languages
       is just:

       CREATE LANGUAGE plpgsql;


       For  a language not known in the pg_pltemplate catalog, a sequence such
       as this is needed:

       CREATE FUNCTION plsample_call_handler() RETURNS language_handler
	   AS '$libdir/plsample'
	   LANGUAGE C;
       CREATE LANGUAGE plsample
	   HANDLER plsample_call_handler;


COMPATIBILITY
       CREATE LANGUAGE is a PostgreSQL extension.

SEE ALSO
       ALTER  LANGUAGE	[alter_language(7)],  CREATE  FUNCTION	 [create_func‐
       tion(7)],  DROP	LANGUAGE  [drop_language(7)], GRANT [grant(7)], REVOKE
       [revoke(7)], createlang [createlang(1)], droplang [droplang(1)]



SQL - Language Statements	  2010-05-19		    CREATE LANGUAGE(7)
