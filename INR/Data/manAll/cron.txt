CRON(8) 							       CRON(8)



NAME
       cron - daemon to execute scheduled commands (Vixie Cron)

SYNOPSIS
       cron [-f] [-l] [-L loglevel]

DESCRIPTION
       cron  is  started automatically from /etc/init.d on entering multi-user
       runlevels.

OPTIONS
       -f      Stay in foreground mode, don't daemonize.

       -l      Enable LSB compliant names for /etc/cron.d files

       -L loglevel
	       Sets the loglevel for cron. The standard logging level (1) will
	       log  the start of all the cron jobs. A higher loglevel (2) will
	       cause cron to log also the end of all cronjobs,	which  can  be
	       useful  to  audit  the  behaviour of tasks run by cron. Logging
	       will be disabled if the loglevel is set to zero (0).

NOTES
       cron searches its spool	area  (/var/spool/cron/crontabs)  for  crontab
       files  (which  are named after accounts in /etc/passwd); crontabs found
       are loaded into memory.	Note that crontabs in  this  directory	should
       not be accessed directly - the crontab command should be used to access
       and update them.

       cron also reads /etc/crontab, which is in a slightly  different	format
       (see  crontab(5)).   Additionally, cron reads the files in /etc/cron.d:
       it treats  the  files  in  /etc/cron.d  as  in  the  same  way  as  the
       /etc/crontab  file  (they  follow the special format of that file, i.e.
       they  include  the  user  field).  However,  they  are  independent  of
       /etc/crontab:  they  do	not, for example, inherit environment variable
       settings from it. The intended purpose of  this	feature  is  to  allow
       packages  that  require	finer  control	of  their  scheduling than the
       /etc/cron.{daily,weekly,monthly} directories to add a crontab  file  to
       /etc/cron.d. Such files should be named after the package that supplies
       them. Files must conform to the same naming convention as used by  run-
       parts(8):  they	must  consist solely of upper- and lower-case letters,
       digits, underscores, and hyphens. If the -l option is  specified,  then
       they must conform to the LSB namespace specification, exactly as in the
       --lsbsysinit option in run-parts.

       Like /etc/crontab, the files in the /etc/cron.d directory are monitored
       for changes. In general, the admin should not use /etc/cron.d/, but use
       the standard system crontab /etc/crontab.

       cron then wakes up every minute, examining all stored crontabs,	check‐
       ing  each  command  to  see  if it should be run in the current minute.
       When executing commands, any output is  mailed  to  the	owner  of  the
       crontab (or to the user named in the MAILTO environment variable in the
       crontab, if such exists).  The children copies of  cron	running  these
       processes  have their name coerced to uppercase, as will be seen in the
       syslog and ps output.

       Additionally, cron checks each minute to see if its  spool  directory's
       modtime	(or  the  modtime on /etc/crontab) has changed, and if it has,
       cron will then examine the modtime on all  crontabs  and  reload  those
       which have changed.  Thus cron need not be restarted whenever a crontab
       file is modified.  Note that the crontab(1) command updates the modtime
       of the spool directory whenever it changes a crontab.

       Special	considerations	exist when the clock is changed by less than 3
       hours, for example at the beginning and end of daylight	savings  time.
       If  the time has moved forwards, those jobs which would have run in the
       time that was skipped will be run soon after the  change.   Conversely,
       if  the	time has moved backwards by less than 3 hours, those jobs that
       fall into the repeated time will not be re-run.

       Only jobs that run at a particular time (not specified as @hourly,  nor
       with  '*' in the hour or minute specifier) are affected. Jobs which are
       specified with wildcards are run based on the new time immediately.

       Clock changes of more than 3 hours are considered to be corrections  to
       the clock, and the new time is used immediately.

       cron  logs its action to the syslog facility 'cron', and logging may be
       controlled using the standard syslogd(8) facility.

SEE ALSO
       crontab(1), crontab(5)

AUTHOR
       Paul Vixie <paul@vix.com>



4th Berkeley Distribution	31 October 2006 		       CRON(8)
