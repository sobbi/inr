snmp.conf(5)			  Apple Inc.			  snmp.conf(5)



NAME
       snmp.conf - SNMP-Konfigurationsdatei für CUPS

BESCHREIBUNG
       Die  Datei  snmp.conf konfiguriert das SNMP-Drucker-Ermittlungs-Backend
       von CUPS und befindet sich normalerweise im Verzeichnis /etc/cups. Jede
       Zeile  in  der  Datei kann eine Konfigurationsdirektive, eine Leerzeile
       oder ein Kommentar sein. Kommentarzeilen beginnen mit dem Zeichen »#«.

       Das SNMP-Backend benutzt das SNMPv1-Protokoll,  um  Netzwerkdrucker  zu
       suchen, Informationen vom MIB-Host zu sammeln, zusammen mit intelligen‐
       ten Port-Untersuchungen, um für jeden Drucker die korrekte  Geräte-URI,
       Hersteller  und Modell zu bestimmen. Zukünftige Versionen von CUPS wer‐
       den wahrscheinlich auch den neuen Port-Monitor MIB unterstützen.

DIREKTIVEN
       Die folgenden Direktiven werden vom SNMP-Backend verstanden.  Für  eine
       detaillierte Beschreibung schauen Sie bitte in die Online-Hilfe:

       Address @IF(Name)

       Address @LOCAL

       Address Adresse
	    Sendet  SNMP-Übertragungsanfragen an die angegebene(n) Adresse(n).
	    Die Vorgabeadresse ist »@LOCAL«, die  an  alle  LAN-Schnittstellen
	    überträgt.

       Community Name
	    Spezifiziert  die  SNMP-Gemeinschaft  zum Abfragen. Die Vorgabege‐
	    meinschaft ist »public«.

       DebugLevel N
	    Setzt die Fehlersucheprotokollierstufe auf N;  0  deaktiviert  das
	    Fehlersucheprotokollieren,	1  aktiviert  grundlegendes  Protokol‐
	    lieren, 2 zeigt SNMP-Werte und 3 zeigt rohe hexadezimale Daten.

       HostNameLookups on

       HostNameLookups off
	    Spezifiziert, ob die Druckeradressen in  Rechnernamen  konvertiert
	    werden oder als numerische IP-Adressen verbleiben sollen. Der Vor‐
	    gabewert ist »off«.

       MaxRunTime Sekunden
	    Spezifiziert die maximale Zeit in Sekunden, für die das SNMP-Back‐
	    end das Netz nach Druckern durchsuchen wird.

SIEHE AUCH
       http://localhost:631/help

COPYRIGHT
       Copyright 2007-2009 by Apple Inc.



31. Juli 2006			     CUPS			  snmp.conf(5)
