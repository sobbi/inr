cupstestppd(1)			  Apple Inc.			cupstestppd(1)



NAME
       cupstestppd - überprüfe Konformität von PPD-Dateien

SYNOPSIS
       cupstestppd  [  -R  Wurzelverz ] [ -W Kategorie ] [ -q ] [-r] [ -v[v] ]
       Dateiname.ppd[.gz] [ ... DateinameN.ppd[.gz] ]
       cupstestppd [ -R Wurzelverz ] [ -W Kategorie ] [ -q ] [-r] [ -v[v] ] -

BESCHREIBUNG
       cupstestppd überprüft die Konformität  von  PPD-Dateien	zu  der  Adobe
       PostScript Printer Description-Dateiformatspezifikation Version 4.3. Es
       kann auch zur Auflistung der in einer PPD-Datei	unterstützen  Optionen
       und  verfügbaren  Schriften verwandt werden. Das Ergebnis des Tests und
       alle anderen Ausgaben werden an die Standardausgabe gesandt.

       Die erste Form von cupstestppd überprüft eine oder mehrere  PPD-Dateien
       auf  der Kommandozeile. Die zweite Form überprüft die auf der Standard‐
       eingabe übergebene PPD-Datei.

OPTIONEN
       cupstestppd unterstützt die folgenden Optionen:

       -R Wurzelverz
	    Spezifiziert ein alternatives Wurzelverzeichnis für  die  filter-,
	    pre-filter- und andere unterstützte Dateiüberprüfungen.

       -W constraints
	    Berichtet alle UIConstraint-Fehler als Warnungen.

       -W defaults
	    Berichtet alle Standard-Optionsfehler, außer größenbezogene Optio‐
	    nen, als Warnungen.

       -W filters
	    Berichtet alle Filterfehler als Warnungen.

       -W Profile
	    Alle Profilfehler als Warnungen berichten.

       -W Größen
	    Alle Mediengrößenfehler als Warnungen berichten.

       -W translations
	    Berichtet alle Übersetzungsfehler als Warnungen.

       -W all
	    Berichtet alle vorhergehenden Fehler als Warnungen.

       -W none
	    Berichtet alle bisherigen Fehler als Fehler

       -q
	    Spezifiziert, dass keine Information angezeigt werden soll.

       -r
	    Weicht die PPD-Konformitätsanforderungen  auf,  so	dass  typische
	    Leerzeichen-,  Steuerzeichen  und  Formatierungsprobleme nicht als
	    schwere Fehler behandelt werden.

       -v
	    Spezifiziert, dass detaillierte Konformitätstestergebnisse anstatt
	    des knappen »PASS/FAIL/ERROR« Statusses angezeigt werden sollen.

       -vv
	    Spezifiziert,  dass alle Informationen in der PPD-Datei zusätzlich
	    zu den detaillierten Konformitätstestergebnissen angezeigt	werden
	    sollen.

       Die Optionen -q, -v und -vv schließen sich gegenseitig paarweise aus.

EXIT-STATUS
       cupstestppd  liefert  Null bei Erfolg und nicht-Null im Fehlerfall. Die
       Fehlercodes lauten wie folgt:

       1
	    Fehlerhafte Kommandozeilen-Argumente oder fehlender PPD-Dateiname.

       2
	    PPD-Datei kann nicht geöffnet oder gelesen werden.

       3
	    Die PPD-Datei enthält Formatfehler, die nicht übersprungen	werden
	    können.

       4
	    Die PPD-Datei folgt nicht der Adobe PPD-Spezifikation.

BEISPIELE
       Der  folgende Befehl überprüft alle PPD-Dateien unterhalb des aktuellen
       Verzeichnisses und gibt die Namen aller	Dateien  aus,  die  nicht  der
       Spezifikation folgen:

	   find . -name \*.ppd \! -exec cupstestppd -q '{}' \; -print

       Der  nächste  Befehl überprüft alle PPD-Dateien unterhalb des aktuellen
       Verzeichnisse und gibt detaillierte Konformitätstestergebnisse für  die
       Dateien aus, die keine Konformität aufweisen.

	   find . -name \*.ppd \! -exec cupstestppd -q '{}' \; \
	       -exec cupstestppd -v '{}' \;


SIEHE AUCH
       lpadmin(8),
       http://localhost:631/help
       Adobe PostScript Printer Description File Format Specification, Version
       4.3.

COPYRIGHT
       Copyright 2007-2009 by Apple Inc.



2. März 2009			     CUPS			cupstestppd(1)
