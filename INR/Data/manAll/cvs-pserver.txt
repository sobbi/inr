
CVS-PSERVER(8)							CVS-PSERVER(8)



NAME
       cvs - The GNU Concurrent Versions System

SYNOPSIS
       cvs-pserver

DESCRIPTION
       This  manual page documents briefly the cvs-pserver command.  This man‐
       ual page was written for the Debian GNU/Linux distribution.

       cvs-pserver is a simple wrapper script that allows  easier  control  of
       the CVS pserver. It is configured using the file /etc/cvs-pserver.conf.
       The options that can be set in that file are:

       CVS_PSERV_REPOS
	  This should contain a colon-separated list of all the CVS  reposito‐
       ries you wish to use.

       CVS_TMP_DIR
	  Is  the location of the temporary space that you wish cvs to use for
       certain operations like import. If unset, CVS will fall back to $TMPDIR
       then /tmp.

       CVS_PSERV_LIMIT_MEM

       CVS_PSERV_LIMIT_DATA

       CVS_PSERV_LIMIT_CORE

       CVS_PSERV_LIMIT_CPU
	  These  may  be  used	to set process limits on cvs resource usage if
       desired.

       CVS_EXT_PASSWD_FILE
	  Is the location of an (optional) external  password  file,  used  to
       override the CVSROOT/passwd file for authentication if set.


FILES
       /etc/cvs-cron.conf

       /etc/cvs-pserver.conf


AUTHOR
       This manual page was written by Tollef Fog Heen <tfheen@debian.org> and
       then updated  by  Steve	McIntyre  <93sam@debian.org>  for  the	Debian
       GNU/Linux system.



								CVS-PSERVER(8)
