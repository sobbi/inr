CYTUNE(8)		   Linux Programmer's Manual		     CYTUNE(8)



NAME
       cytune - Tune driver parameters for Cyclades-Z multiport serial card

SYNOPSIS
       cytune  [-q  [-i  interval]]  [{-s|-S} value] [-g|-G] [{-t|-T} timeout]
       tty...

DESCRIPTION
       cytune queries and modifies the interruption threshold for the Cyclades
       driver.	 Each  serial  line  on a Cyclades card has a 12-byte FIFO for
       input (and another 12-byte FIFO for output).  The "threshold" specifies
       how  many input characters must be present in the FIFO before an inter‐
       ruption is raised.  When a Cyclades tty is opened,  this  threshold  is
       set to a default value based on baud rate:

		  Baud	      Threshold

	      50-4800		 10
	      9600		  8
	      19200		  4
	      38400		  2
	      57600-150000	  1

       If  the threshold is set too low, the large number of interruptions can
       load the machine  and  decrease	overall  system  throughput.   If  the
       threshold is set too high, the FIFO buffer can overflow, and characters
       will be lost.  Slower machines, however, may not be able to  deal  with
       the  interrupt  load,  and  will require that the threshold be adjusted
       upwards.

       If the cyclades driver was compiled with ENABLE_MONITORING defined, the
       cytune command can be used with the -q option to report interrupts over
       the monitoring interval and characters transferred over the  monitoring
       interval.  It will also report the state of the FIFO.  The maximum num‐
       ber of characters in the FIFO when an interrupt occurred, the instanta‐
       neous  count of characters in the FIFO, and how many characters are now
       in the FIFO are reported.  This output might look like this:

	      /dev/cubC0: 830 ints, 9130 chars; fifo: 11 threshold, 11 max, 11
	      now
		 166.259866 interrupts/second, 1828.858521 characters/second

       This  output  indicates that for this monitoring period, the interrupts
       were always being handled within one character time, because max  never
       rose above threshold.  This is good, and you can probably run this way,
       provided that a large number of samples come out this  way.   You  will
       lose  characters if you overrun the FIFO, as the Cyclades hardware does
       not seem to support the RTS RS-232 signal line for hardware  flow  con‐
       trol from the DCE to the DTE.

       In  query  mode	cytune will produce a summary report when ended with a
       SIGINT or when the threshold or timeout is changed.

       There may be a responsiveness vs. throughput  tradeoff.	 The  Cyclades
       card, at the higher speeds, is capable of putting a very high interrupt
       load on the system.  This will reduce the amount of CPU time  available
       for  other tasks on your system.  However, the time it takes to respond
       to a single character may be increased if you increase  the  threshold.
       This  might  be noticed by monitoring ping(8) times on a SLIP link con‐
       trolled by a Cyclades card.  If your SLIP link is  generally  used  for
       interactive work such as telnet(1), you may want to leave the threshold
       low, so that characters are responded to as quickly  as	possible.   If
       your  SLIP link is generally used for file transfer, WWW, and the like,
       setting the FIFO to a high value is likely to reduce the load  on  your
       system  while  not  significantly affecting throughput.	Alternatively,
       see the -t or -T options to adjust the time  that  the  cyclades  waits
       before flushing its buffer.  Units are 5ms.

       If  you	are  running a mouse on a Cyclades port, it is likely that you
       would want to maintain the threshold and timeout at a low value.

OPTIONS
       -s value
	      Set the current threshold to value characters.  Note that if the
	      tty  is  not  being  held open by another process, the threshold
	      will be reset on the next open.  Only values between 1  and  12,
	      inclusive, are permitted.

       -t value
	      Set  the current flush timeout to value units.  Note that if the
	      tty is not being held open by  another  process,	the  threshold
	      will  be reset on the next open.	Only values between 0 and 255,
	      inclusive, are permitted.  Setting  value  to  zero  forces  the
	      default,	currently  0x20  (160ms),  but soon to be 0x02 (10ms).
	      Units are 5 ms.

       -g     Get the current threshold and timeout.

       -S value
	      Set the default threshold to value characters.  When the tty  is
	      next  opened,  this  value  will be used instead of the default.
	      Only values between 1 and 12, inclusive, are permitted.

       -T value
	      Set the default flush timeout to value units.  When the  tty  is
	      next opened, this value will be used instead of the default.  If
	      value is zero, then the the value will default to 0x20  (160ms),
	      soon to be 0x02 (10ms).

       -G     Get the default threshold and flush timeout values.

       -q     Gather  statistics about the tty.  The results are only valid if
	      the Cyclades driver has  been  compiled  with  ENABLE_MONITORING
	      defined.	This is probably not the default.

       -i interval
	      Statistics will be gathered every interval seconds.

BUGS
       If  you	run two copies of cytune at the same time to report statistics
       about the same port, the 'ints', 'chars', and 'max' value will be reset
       and not reported correctly.  cytune should prevent this, but does not.

FILES
       /dev/ttyC[0-8]
       /dev/cubC[0-8]

SEE ALSO
       setserial(8)

AVAILABILITY
       The  cytune  command is part of the util-linux-ng package and is avail‐
       able from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



				   4 Mar 1995			     CYTUNE(8)
