d2i_X509_REQ(3SSL)		    OpenSSL		    d2i_X509_REQ(3SSL)



NAME
       d2i_X509_REQ, i2d_X509_REQ, d2i_X509_REQ_bio, d2i_X509_REQ_fp,
       i2d_X509_REQ_bio, i2d_X509_REQ_fp - PKCS#10 certificate request
       functions.

SYNOPSIS
	#include <openssl/x509.h>

	X509_REQ *d2i_X509_REQ(X509_REQ **a, const unsigned char **pp, long length);
	int i2d_X509_REQ(X509_REQ *a, unsigned char **pp);

	X509_REQ *d2i_X509_REQ_bio(BIO *bp, X509_REQ **x);
	X509_REQ *d2i_X509_REQ_fp(FILE *fp, X509_REQ **x);

	int i2d_X509_REQ_bio(X509_REQ *x, BIO *bp);
	int i2d_X509_REQ_fp(X509_REQ *x, FILE *fp);

DESCRIPTION
       These functions decode and encode a PKCS#10 certificate request.

       Othewise these behave in a similar way to d2i_X509() and i2d_X509()
       described in the d2i_X509(3) manual page.

SEE ALSO
       d2i_X509(3)

HISTORY
       TBA



0.9.8k				  2005-07-13		    d2i_X509_REQ(3SSL)
