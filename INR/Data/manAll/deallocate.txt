DEALLOCATE(7)			 SQL Commands			 DEALLOCATE(7)



NAME
       DEALLOCATE - deallocate a prepared statement


SYNOPSIS
       DEALLOCATE [ PREPARE ] { name | ALL }


DESCRIPTION
       DEALLOCATE  is  used to deallocate a previously prepared SQL statement.
       If you do not explicitly deallocate a prepared statement, it is deallo‐
       cated when the session ends.

       For more information on prepared statements, see PREPARE [prepare(7)].

PARAMETERS
       PREPARE
	      This key word is ignored.

       name   The name of the prepared statement to deallocate.

       ALL    Deallocate all prepared statements.

COMPATIBILITY
       The  SQL  standard  includes a DEALLOCATE statement, but it is only for
       use in embedded SQL.

SEE ALSO
       EXECUTE [execute(7)], PREPARE [prepare(7)]



SQL - Language Statements	  2010-05-19			 DEALLOCATE(7)
