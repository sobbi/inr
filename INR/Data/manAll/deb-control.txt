deb-control(5)			    Debian			deb-control(5)



NAME
       deb-control - Dateiformat der Hauptsteuerdatei von Debian-Paketen

ÜBERSICHT
       control

BESCHREIBUNG
       Jedes  Debian-Paket  enthält  die  Hauptsteuerdatei »control«, die eine
       Reihe von Feldern oder Kommentaren (wenn die  Zeile  mit  »#«  beginnt)
       enthält. Jedes Feld beginnt mit einer Markierung, wie Package oder Ver‐
       sion (Groß-/Kleinschreibung egal), gefolgt von  einem  Doppelpunkt  und
       dem  Körper  des  Feldes.  Felder werden nur durch die Feldmarkierungen
       abgegrenzt.  Mit  anderen  Worten,  Feldtexte  können  mehrere	Zeilen
       überspannen,  aber die Installationswerkzeuge werden im Allgemeinen die
       Zeilen bei der Verarbeitung des Feldkörpers  zusammenfassen  (mit  Aus‐
       nahme des Description-Feldes, sehen Sie dazu unten).

NOTWENDIGE FELDER
       Package: <Paketname>
	      Der  Wert dieses Feldes bestimmt den Paketnamen und wird von den
	      meisten  Installationswerkzeugen	verwendet,  um	Dateinamen  zu
	      generieren.

       Version: <Versions-Zeichenkette>
	      Typischerweise  ist das die Original-Paketversionsnummer, in der
	      Form,  die  der  Programmautor  verwendet.  Es  kann  auch  eine
	      Debian-Revisionsnummer enthalten (für nicht aus Debian stammende
	      Pakete). Das genaue Format und der  Sortieralgorithmus  sind  in
	      deb-version(5) beschrieben.

       Maintainer: <Vollständiger Name E-Mail>
	      Sollte in dem Format »Joe Bloggs <jbloggs@foo.com>« sein und ist
	      typischerweise die Person, die das Paket erstellt hat, im Gegen‐
	      satz zum Autor der Software, die paketiert wurde.

       Description: <Kurzbeschreibung>
	       <Langbeschreibung>
	      Das Format der Paketbeschreibung ist eine kurze knappe Zusammen‐
	      fassung auf der ersten Zeile (nach dem »Description«-Feld).  Die
	      folgenden  Zeilen  sollten als längere, detailliertere Beschrei‐
	      bung verwendet werden. Jede Zeile der Langbeschreibung muss  von
	      einem   Leerzeichen  begonnen  werden,  und  Leerzeilen  in  der
	      Langbeschreibung müssen einen einzelnen ».« hinter dem  einleit‐
	      enden Leerzeichen enthalten.

OPTIONALE FELDER
       Section: <Bereich>
	      Dies  ist  ein  allgemeines  Feld,  das dem Paket eine Kategorie
	      gibt, basierend auf der Software,  die  es  installiert.	Einige
	      übliche Bereiche sind »utils«, »net«, »mail«, »text«, »x11« usw.

       Priority: <Priorität>
	      Setzt  die Bedeutung dieses Pakets in Bezug zu dem Gesamtsystem.
	      Übliche Prioritäten  sind  »required«,  »standard«,  »optional«,
	      »extra« usw.

       In Debian haben die Section- und Priority-Felder einen definierten Satz
       an akzeptierten Werten, basierend auf  dem  Richtlinien-Handbuch.  Eine
       Liste  dieser Werte kann aus der neusten Version des Pakets debian-pol‐
       icy erhalten werden.

       Essential: <yes|no>
	      Dieses Feld wird normalerweise nur benötigt,  wenn  die  Antwort
	      »yes«  lautet.  Es  bezeichnet  ein Paket, das für den ordnungs‐
	      gemäßen Betrieb des  Systems  benötigt  wird.  Dpkg  oder  jedes
	      andere  Installationswerkzeug wird es nicht erlauben, ein Essen‐
	      tial-Paket zu entfernen (zumindestens nicht ohne die  Verwendung
	      einer der »force«-Optionen).

       Architecture: <arch|all>
	      Die  Architektur	spezifiziert  den  Hardwaretyp	für den dieses
	      Paket kompiliert wurde.  Geläufige  Architekturen  sind  »i386«,
	      »m68k«,  »sparc«, »alpha«, »powerpc« usw. Beachten Sie, dass die
	      Option all für Pakete gedacht  ist,  die	Architektur-unabhängig
	      sind.  Einige Beispiele hierfür sind Shell- und Perl-Skripte und
	      Dokumentation.

       Origin: <Name>
	      Der Name der Distribution, aus  der  dieses  Paket  ursprünglich
	      stammt.

       Bugs: <URL>
	      Die  URL	der Fehlerdatenbank für dieses Paket. Das derzeit ver‐
	      wendete  Format  ist  <BTS_Art>://<BTS_Adresse>  wie   in   deb‐
	      bugs://bugs.debian.org.

       Homepage: <URL>
	      Die URL des Original- (Upstream-)Projekts.

       Tag: <Liste von Markierungen>
	      Liste  der  unterstützten  Markierungen (»Tags«), die die Eigen‐
	      schaften des Pakets beschreiben. Die Beschreibung und die  Liste
	      der  unterstützten Markierungen kann in dem Paket debtags gefun‐
	      den werden.

       Source: <Quell-Name>
	      Der Name des Quellpakets, aus dem  dieses  Binärpaket  abstammt,
	      falls es sich vom Namen dieses Paketes unterscheidet.

       Depends: <Paketliste>
	      Liste  von Paketen, die benötigt werden, damit dieses Paket eine
	      nicht-triviale Menge an Funktionen anbieten kann. Die  Paketver‐
	      waltungssoftware	wird es nicht erlauben, dass ein Paket instal‐
	      liert wird, falls die in seinem Depends-Feld aufgeführten Pakete
	      nicht  installiert  sind (zumindestens nicht ohne Verwendung der
	      »Force«-Optionen).     Bei     einer     Installation	werden
	      Postinst-Skripte	von Paketen, die im Feld »Depends:« aufgeführt
	      sind, vor den Postinst-Skripten  der  eigentlichen  Pakete  aus‐
	      geführt.	Bei der gegenteiligen Operation, der Paket-Entfernung,
	      wird das Prerm-Skript eines Paketes vor den  Prerm-Skripten  der
	      Pakete ausgeführt, die im Feld »Depends:« aufgeführt sind.

       Pre-Depends: <Paketliste>
	      Liste  an  Paketen die installiert und konfiguriert sein müssen,
	      bevor dieses Paket installiert werden kann. Dies wird  normaler‐
	      weise  in  dem Fall verwendet, wo dieses Paket ein anderes Paket
	      zum Ausführen seines preinst-Skriptes benötigt.

       Recommends: <Paketliste>
	      Liste an Paketen, die in allen,  abgesehen  von  ungewöhnlichen,
	      Installationen  zusammen	angefunden  würden.  Die  Paketverwal‐
	      tungssoftware wird den Benutzer warnen, falls er ein Paket  ohne
	      die im Recommends-Feld aufgeführten Pakete installiert.

       Suggests: <Paketliste>
	      Liste  an Paketen die einen Bezug zu diesem haben und vielleicht
	      seine Nützlichkeit erweitern  könnten,  aber  ohne  die  das  zu
	      installierende Paket perfekt sinnvoll ist.

       Die Syntax der Depends, Pre-Depends, Recommends und Suggests-Felder ist
       eine Liste von Gruppen von alternativen Paketen. Jede Gruppe  ist  eine
       Liste  von  durch vertikale Striche (oder »Pipe«-Symbole) »|« getrennte
       Pakete. Die Gruppen werden durch Kommata getrennt. Kommata  müssen  als
       »UND«,  vertikale  Striche  als	»ODER«	gelesen werden, wobei die ver‐
       tikalen Striche stärker binden. Jeder Paketname wird  optional  gefolgt
       von einer Versionsnummer-Spezifikation in Klammern.

       Eine Versionsnummer kann mit »>>« beginnen, in diesem Falle passen alle
       neueren Versionen, und kann die	Debian-Paketrevision  (getrennt  durch
       einen  Bindestrich)  enthalten  oder  auch nicht. Akzeptierte Versions‐
       beziehungen sind »>>« für größer als, »<<« für kleiner  als,  »>=«  für
       größer  als  oder  identisch zu, »<=« für kleiner als oder identisch zu
       und »=« für identisch zu.

       Breaks: <Paketliste>
	      Liste Paketen auf, die von diesem Paket beschädigt  werden,  zum
	      Beispiel	in  dem  sie  Fehler  zugänglich machen, wenn sich das
	      andere  Paket  auf  dieses  Paket  verlässt.  Die   Paketverwal‐
	      tungssoftware  wird es beschädigten Paketen nicht erlauben, sich
	      zu konfigurieren; im Allgemeinen wird das Problem behoben, indem
	      ein  Upgrade des im Breaks-Feld aufgeführten Pakets durchgeführt
	      wird.

       Conflicts: <Paketliste>
	      Liste an Paketen, die mit diesem in Konflikt  stehen,  beispiel‐
	      sweise  indem  beide  Dateien  den gleichen Namen enthalten. Die
	      Paketverwaltungssoftware wird es nicht erlauben, Pakete, die  in
	      Konflikt	stehen, gleichzeitig zu installieren. Zwei in Konflikt
	      stehende Pakete sollten jeweils eine Conflicts-Zeile  enthalten,
	      die das andere Paket erwähnen.

       Replaces: <Paketliste>
	      Liste  an  Paketen,  von denen dieses Dateien ersetzt. Dies wird
	      dazu verwendet, um diesem Paket zu erlauben, Dateien  von  einem
	      anderen  Paket  zu  ersetzen  und  wird  gewöhnlich mit dem Con‐
	      flicts-Feld verwendet, um die Entfernung des anderen Paketes  zu
	      erlauben, falls dieses auch die gleichen Dateien wie das im Kon‐
	      flikt stehende Paket hat.

       Provides: <Paketliste>
	      Dies ist eine Liste von virtuellen  Paketen,  die  dieses  Paket
	      bereitstellt.  Gewöhnlich  wird  dies  verwendet,  wenn  mehrere
	      Pakete alle den gleichen	Dienst	bereitstellen.	Beispielsweise
	      können  Sendmail	und  Exim als Mailserver dienen, daher stellen
	      sie ein gemeinsames Paket (»mail-transport-agent«)  bereit,  von
	      dem andere Pakete abhängen können. Dies erlaubt es Sendmail oder
	      Exim als gültige Optionen  zur  Erfüllung  der  Abhängigkeit  zu
	      dienen.	Dies   verhindert,   dass   Pakete,   die   von  einem
	      E-Mail-Server abhängen, alle Paketnamen für  alle  E-Mail-Server
	      wissen und »|« zur Unterteilung der Liste verwenden müssen.

       Die  Syntax  von  Conflicts,  Replaces  und Provides ist eine Liste von
       Paketnamen, getrennt durch Kommata  (und  optionalen  Leerzeichen).  Im
       Conflicts-Feld  sollte  das  Komma  als	»ODER«	gelesen  werden.  Eine
       optionale Version kann auch mit der gleichen Syntax wie	oben  für  die
       Conflicts- und Replaces-Felder angegeben werden.

BEISPIEL
       # Comment
       Package: grep
       Essential: yes
       Priority: required
       Section: base
       Maintainer: Wichert Akkerman <wakkerma@debian.org>
       Architecture: sparc
       Version: 2.4-1
       Pre-Depends: libc6 (>= 2.0.105)
       Provides: rgrep
       Conflicts: rgrep
       Description: GNU grep, egrep und fgrep.
	Die GNU-Familie der Grep-Werkzeuge könnte die »schnellste im Westen« sein.
	GNU Grep basiert auf einem schellen »lazy-state deterministic matcher«
	(rund zweimal so schnell wie der standardmäßige Unix-Egrep) hybridisiert
	mit einer Boyer-Moore-Gosper-Suche für eine feste Zeichenkette, die
	unmöglichen Text von der Betrachtung durch den vollen »Matcher« verhindert
	ohne notwendigerweise jedes Zeichen anzuschauen. Das Ergebnis ist
	typischerweise um ein mehrfaches Schneller als Unix Grep oder Egrep.
	(Reguläre Ausdrücke, die Rückreferenzierungen enthalten, werden allerdings
	langsamer laufen.)

ÜBERSETZUNG
       Die  deutsche  Übersetzung  wurde  2004, 2006-2009 von Helge Kreutzmann
       <debian@helgefjell.de>, 2007 von Florian Rehnisch  <eixman@gmx.de>  und
       2008  von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese Übersetzung
       ist Freie Dokumentation; lesen Sie die GNU General Public License  Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       deb(5), deb-version(5), debtags(1), dpkg(1), dpkg-deb(1).



Debian-Projekt			  2007-10-08			deb-control(5)
