deb-extra-override(5)	      dpkg-Hilfsprogramme	 deb-extra-override(5)



NAME
       deb-extra-override - Debian-Archive Zusatz-override-Datei

ÜBERSICHT
       override

BESCHREIBUNG
       Während der Großteil der Informationen über ein binäres oder Quellpaket
       in der Datei  control/.dsc  gefunden  werden  kann,  kann  alles  davon
       überschrieben   werden,	 wenn	es  in	die  Dateien  Packages/Sources
       exportiert wird. Die Zusatz-override-Datei enthält  diese  »Überschrei‐
       bungen« (engl. overrides).

       Die  Zusatz-override-Datei hat ein einfaches, durch Leerzeichen getren‐
       ntes Format. Kommentare sind erlaubt (angezeigt durch ein #).

	    Paket Feldname Wert

       Paket ist der Name des Binär-/Quellpakets.

       Feldname ist der Name des Feldes, das überschrieben wird. Wert ist  der
       in  das	Feld  zu  setzende Wert. Er kann Leerzeichen enthalten, da die
       Zeile in nicht mehr als drei Spalten aufgeteilt wird, wenn sie  ausgew‐
       ertet wird.

       Die  Zusatz-override-Dateien,  die  für	die Erstellung der offiziellen
       Paketliste verwendet werden, können im  indices-Verzeichnis  auf  jedem
       Debian-Spiegel gefunden werden.

ÜBERSETZUNG
       Die  deutsche  Übersetzung  wurde  2004, 2006-2009 von Helge Kreutzmann
       <debian@helgefjell.de>, 2007 von Florian Rehnisch  <eixman@gmx.de>  und
       2008  von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese Übersetzung
       ist Freie Dokumentation; lesen Sie die GNU General Public License  Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       dpkg-scanpackages(1), dpkg-scansources(1), apt-ftparchive(1).



Debian-Projekt			  2009-08-16		 deb-extra-override(5)
