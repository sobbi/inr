deb-substvars(5)	      dpkg-Hilfsprogramme	      deb-substvars(5)



NAME
       deb-substvars - Substitutionsvariablen in Debian-Quellen

ÜBERSICHT
       substvars

BESCHREIBUNG
       Bevor  dpkg-source,  dpkg-gencontrol und dpkg-genchanges ihre Steuerin‐
       formationen (zu der Quellsteuer-Datei .dsc für dpkg-source und  zu  der
       Standardausgabe	für  dpkg-gencontrol  und  dpkg-genchanges) schreiben,
       führen sie einige Variablensubstitutionen in der Ausgabedatei durch.

       Eine Variablensubstitution hat die Form ${Variablenname}.  Variablenna‐
       men  bestehen  aus  alphanumerischen Zeichen (womit sie auch beginnen),
       Gedankenstrichen  und  Doppelpunkten.  Variablensubstitutionen	werden
       wiederholt  durchgeführt, bis keine übrig geblieben sind; der komplette
       Text des Feldes nach der Substitution wird erneut auf weitere Substitu‐
       tionen geprüft.

       Nachdem	alle  Substitutionen  erfolgt  sind,  wird jedes Auftreten der
       Zeichenkette ${}  (die  keine  erlaubte	Substitution  ist)  durch  das
       $-Zeichen ersetzt.

       Obwohl  die  Variablensubstitution  bei	allen  Feldern der Steuerdatei
       vorgenommen wird,  werden  einige  dieser  Felder  während  des	Bauens
       benötigt  und  verwendet, wenn die Substitution noch nicht erfolgt ist.
       Daher können Sie Variablen nicht in den	Feldern  Package,  Source  und
       Architecture verwenden.

       Variablen  können  über	die  allgemeine  -V-Option gesetzt werden. Sie
       können auch in der debian/substvars (bzw. in der  über  die  Option  -T
       gesetzten  Datei) angegeben werden. Diese Datei besteht auch Zeilen der
       Form Name=Wert. Leerzeichen am Zeilenende, leere Zeilen und Zeilen, die
       mit dem #-Symbol starten (Kommentare) werden ignoriert.

       Zusätzlich sind die folgenden Standardvariablen verfügbar:

       Arch   Die aktuelle Bau-Architektur (von dpkg --print-architecture).

       source:Version
	      Die Quellpaket-Version.

       source:Upstream-Version
	      Die  Paketversion der Originalautoren, einschließlich der Epoche
	      der Debian-Version, falls vorhanden.

       binary:Version
	      Die Binärpaketversion (die z.B. in einem binNMU von  source:Ver‐
	      sion abweichen kann).

       Source-Version
	      Die  Quellpaketversion (aus der changelog-Datei). Diese Variable
	      ist jetzt veraltet, da ihre Bedeutung von ihrer  Funktion  abwe‐
	      icht,  bitte verwenden Sie source:Version oder binary:Version wo
	      zutreffend.

       Installed-Size
	      Die Gesamtgröße der vom Paket installierten Dateien. Dieser Wert
	      wird in das entsprechende Feld der »control«-Datei kopiert; wird
	      es gesetzt, verändert es den Wert  dieses  Feldes.  Falls  diese
	      Variable	 nicht	 gesetzt  ist,	wird  dpkg-gencontrol  »du  -k
	      debian/tmp« verwenden, um den Standardwert zu ermitteln.

       Extra-Size
	      Zusätzlicher Plattenplatz, der verwendet wird,  wenn  das  Paket
	      installiert  ist.  Falls diese Variable gesetzt ist, wird dieser
	      Wert zu der Installed-Size-Variablen hinzuaddiert (egal  ob  sie
	      explizit	gesetzt  oder  der Standardwert verwendet wird), bevor
	      sie in das Feld Installed-Size der »control«-Datei kopiert wird.

       F:Feldname
	      Der Wert des Ausgabefeldes  Feldname  (der  in  der  kanonischen
	      Groß-/Kleinschreibung  angegeben werden muss). Das Setzen dieser
	      Variablen hat nur einen Effekt an den Stellen, wo diese explizit
	      expandiert werden.

       Format Die Formatversion der .changes-Datei, die von dieser Version der
	      Quellpaketierskripte erstellt wird.  Falls  Sie  diese  Variable
	      setzen,	werden	 die   Inhalte	 des   Format-Feldes   in  der
	      .changes-Datei auch geändert.

       Newline, Space, Tab
	      Diese Variablen enthalten das  jeweils  korrespondieren  Zeichen
	      (Zeilenumbruch,  Leerzeichen  und  Tabulator  in	dieser Reihen‐
	      folge).

       shlibs:dependencyfield
	      Variableneinstellungen  mit  Namen  dieser   Form   werden   von
	      dpkg-shlibdeps erstellt.

       dpkg:Upstream-Version
	      Die Original- (Upstream-)Version von Dpkg.

       dpkg:Version
	      Die komplette Version von Dpkg.

       Falls auf eine Variable Bezug genommen wird, diese aber nicht definiert
       ist, wird es eine Warnung erstellen und ein leerer Wert	wird  angenom‐
       men.

DATEIEN
       debian/substvars
	      Liste von Substitutionsvariablen und -werten.

FEHLER
       Die  Stelle,  an der das Überschreiben von Feldern passiert, verglichen
       mit bestimmten Standard-Ausgabe-Feldeinstellungen, ist eher konfus.


ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2004,  2006-2009	von  Helge  Kreutzmann
       <debian@helgefjell.de>,	2007  von Florian Rehnisch <eixman@gmx.de> und
       2008 von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese  Übersetzung
       ist  Freie Dokumentation; lesen Sie die GNU General Public License Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       dpkg(1),  dpkg-genchanges(1),  dpkg-gencontrol(1),   dpkg-shlibdeps(1),
       dpkg-source(1).

AUTOR
       Copyright © 1995-1996 Ian Jackson
       Copyright © 2000 Wichert Akkerman

       Dies  ist Freie Software; lesen Sie die GNU General Public License Ver‐
       sion 2 oder neuer für die Kopierbedingungen. Es gibt KEINE Haftung.



Debian-Projekt			  2009-07-15		      deb-substvars(5)
