DEBCONF-COPYDB.DE.1(1)		    Debconf		DEBCONF-COPYDB.DE.1(1)



NAME
       debconf-copydb - kopiere eine Debconf-Datenbank

ÜBERSICHT
	debconf-copydb QuellDB ZielDB [--pattern=Muster] [--owner-pattern=Muster] [--config=Foo:bar]

BESCHREIBUNG
       debconf-copydb kopiert Einträge aus einer existierenden Debconf-
       Datenbank in eine andere, möglicherweise neue Datenbank. Die zwei
       Datenbanken dürfen verschiedene Formate haben, in diesem Falle wird
       automatisch eine Konvertierung durchgeführt.

OPTIONEN
       QuellDB
	   Der Name der Quelldatenbank. Wird typischerweise in Ihrer
	   debconf.conf- (oder .debconfrc-)Datei definiert.

       ZielDB
	   Der Name der Zieldatenbank. Sie kann in Ihrer debconf.conf oder
	   .debconfrc definiert sein, oder Sie könnten sie auf der
	   Kommandozeile definieren (siehe unten).

       -p Muster, --pattern Muster
	   Falls dies angegeben ist, werden nur Einträge aus QuellDB kopiert,
	   deren Namen auf Muster passen.

       --owner-pattern Muster
	   Falls dies angegeben ist, werden nur Einträge aus QuellDB kopiert,
	   deren Eigentümer auf Muster passen.

       -c foo:bar, --config Foo:bar
	   Setze Option Foo auf bar. Dies ist ähnlich zu Folgendem:

	     Foo: bar

	   in debconf.conf, außer dass Sie möglicherweise das Leerzeichen auf
	   der Befehlszeile auslassen (oder zitieren: "Foo: bar"). Muss
	   generell mehrere Male benutzt werden, um einen vollen
	   Konfigurations-Abschnitt zu formen. Während in der debconf.conf
	   Leerzeilen benutzt werden, um Abschnitte zu trennen, nimmt dieses
	   Programm an, dass »Name:dbname« den Beginn eines neuen Abschnittes
	   anzeigt.

BEISPIELE
	 debconf-copydb configdb backup

       Kopiere alles aus configdb nach backup, unter der Annahme, dass Sie die
       Datenbank backup bereits in der debconf.conf definiert haben.

	 debconf-copydb configdb newdb --pattern='^slrn/' \
	       --config=Name:newdb --config=Driver:File \
	       --config=Filename:newdb.dat

       Kopiere slrn-bezogene Daten aus configdb nach newdb. newdb ist nicht in
       der rc-Datei definiert, so dass mit den Schaltern --config die
       Datenbank im Vorbeigehen aufgesetzt wird.

	 debconf-copydb configdb stdout -c Name:stdout -c Driver:Pipe \
	       -c InFd:none --pattern='^foo/'

       Gebe alle Einträge in der Debconf-Datenbank mit Bezug zum Paket foo
       aus.

	 debconf-copydb configdb pipe --config=Name:pipe \
		       --config=Driver:Pipe --config=InFd:none | \
	       ssh remotehost debconf-copydb pipe configdb \
		       --config=Name:pipe --config=Driver:Pipe

       Dies benutzt den Pipe-Treiber für besondere Anlässe, um eine Datenbank
       auf ein anderes System zu kopieren.

SIEHE AUCH
       debconf.conf(5)

AUTOR
       Joey Hess <joeyh@debian.org>

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2008 von Florian Rehnisch
       <eixman@gmx.de> und 2008-2009 Helge Kreutzmann <debian@helgefjell.de>
       angefertigt. Diese Übersetzung ist Freie Dokumentation; lesen Sie die
       GNU General Public License Version 2 oder neuer für die
       Kopierbedingungen.  Es gibt KEINE HAFTUNG.



				  2010-04-09		DEBCONF-COPYDB.DE.1(1)
