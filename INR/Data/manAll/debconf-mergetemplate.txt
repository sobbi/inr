DEBCONF-MERGETEMPLATE.DE.1(1)	    Debconf	 DEBCONF-MERGETEMPLATE.DE.1(1)



NAME
       debconf-mergetemplate - füge mehrere Debconf-Vorlagen-Dateien zusammen

ÜBERSICHT
	debconf-mergetemplate [Optionen] [Vorlagen.ll ...] Vorlagen

BESCHREIBUNG
       Hinweis: Dieses Hilfswerkzeug ist veraltet. Sie sollten auf die
       Verwendung des Programms po2debconf von Po-debconf umstellen.

       Dieses Programm ist nützlich, falls Sie mehrere Debconf-Vorlage-Dateien
       haben, die Sie zu einer großen Datei zusammen fügen möchten. Alle
       angegebenen Dateien werden eingelesen, zusammengeführt und auf die
       Standardausgabe ausgegeben.

       Dies kann besonders nützlich sein, falls Sie mit übersetzten Vorlage-
       Dateien hantieren. In diesem Falle könnten Sie Ihre Haupt-Vorlage-Datei
       haben und mehrere andere Dateien, die Ihnen von den Übersetzerinnen zur
       Verfügung gestellt wurden. Diese Dateien enthalten übersetzte Felder
       und möglicherweise haben die Übersetzerinnen die englischen Versionen
       der Felder zu ihrer Referenz in den Dateien belassen.

       Also wollen Sie alle übersetzten Vorlage-Dateien mit Ihrer Haupt-
       Vorlage-Datei zusammenfügen. Jegliche Felder, die in den übersetzten
       Dateien eindeutig sind, müssen zu den richtigen Vorlagen hinzugefügt
       werden, aber jegliche Felder, die sie gemeinsam haben, sollten durch
       die Felder in der Haupt-Datei ersetzt werden (welche aktueller sein
       könnte).

       Dieses Programm behandelt diesen Fall korrekt. Führen Sie einfach jede
       der übersetzten Vorlagen-Dateien und dann als letzte Ihre Haupt-
       Vorlagen-Datei auf.

OPTIONEN
       --outdated
	   Füge selbst veraltete Übersetzungen ein. Voreinstellung ist, sie
	   mit einer Warnmeldung zu verwerfen.

       --drop-old-templates
	   Falls eine Übersetzung eine vollständige Vorlage enthält, die nicht
	   in der Master-Datei ist (und somit möglicherweise eine alte Vorlage
	   ist), verwerfe die ganze Vorlage.

SIEHE AUCH
       debconf-getlang(1)

AUTOR
       Joey Hess <joeyh@debian.org>

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2008 von Florian Rehnisch
       <eixman@gmx.de> und 2008-2009 Helge Kreutzmann <debian@helgefjell.de>
       angefertigt. Diese Übersetzung ist Freie Dokumentation; lesen Sie die
       GNU General Public License Version 2 oder neuer für die
       Kopierbedingungen.  Es gibt KEINE HAFTUNG.



				  2010-04-09	 DEBCONF-MERGETEMPLATE.DE.1(1)
