DEBCONF-SET-SELECTIONS.DE.1(1)	    Debconf	DEBCONF-SET-SELECTIONS.DE.1(1)



NAME
       debconf-set-selections - füge neue Vorgabewerte in die
       Debconf-Datenbank ein

ÜBERSICHT
	debconf-set-selections Datei
	debconf-get-selections | ssh neuerhost debconf-set-selections

BESCHREIBUNG
       debconf-set-selections kann benutzt werden, um die Debconf-Datenbank
       mit Antworten vorzuladen, oder um Antworten in der Datenbank zu ändern.
       Jede Frage wird als gesehen markiert, um zu vermeiden, dass Debconf sie
       interaktiv stellt.

       Liest aus einer Datei, falls ein Dateiname angegeben ist, ansonsten von
       der Standardeingabe.

WARNUNG
       Benutzen Sie diesen Befehl nur, um Debconf-Werte für Pakete, die
       installiert sind oder werden sollen, zu laden. Ansonsten kommen Sie
       dahin, dass Sie Werte für nicht-installierte Pakete haben, die nicht
       verschwinden, oder zu schlimmeren Problemen, die gemeinsam benutzte
       Werte betreffen. Es empfiehlt sich, dass dies nur zum Laden der
       Datenbank benutzt wird, falls die ursprüngliche Maschine eine
       identische Installation hat.

DATENFORMAT
       Die Daten sind eine Folge von Zeilen. Zeilen, die mit dem Zeichen »#«
       beginnen, sind Kommentare. Leerzeilen werden ignoriert. Alle anderen
       Zeilen setzen den Wert einer Frage, und sollten vier jeweils durch ein
       Leerzeichen getrennte Werte enthalten. Der erste Wert ist der Name des
       Pakets, dem die Frage gehört. Der zweite ist der Name der Frage, der
       dritte ist der Typ dieser Frage, und der vierte Wert (bis zum
       Zeilenende) ist der Wert, der als Antwort auf die Frage zu benutzen
       ist.

       Alternativ kann der dritte Wert »seen« (gesehen) sein; dann
       kontrolliert die Vorladezeile nur, ob die Frage in Debconfs Datenbank
       als gesehen markiert wird. Beachten Sie, dass das Vorladen des Werts
       für eine Frage diese als gesehen markiert, also brauchen Sie, um den
       Vorgabewert einer Frage zu überschreiben, ohne diese Frage als gesehen
       zu markieren, zwei Zeilen.

       Zeilenfortsetzung kann benutzt werden, indem man die erste Zeile mit
       einem Rückwärtsschrägstrich (\) enden lässt.

BEISPIELE
	# Erzwinge Debconf-Priority kritisch.
	debconf debconf/priority select critical

	# Ändere Vorgabe für Benutzerschnittstelle auf Readline, aber erlaube Benutzer auszuwählen
	debconf debconf/frontend select readline
	debconf debconf/frontend seen false

OPTIONEN
       --verbose, -v
	   wortreiche Ausgabe

       --checkonly, -c
	   Prüfe nur das Format der Eingabedatei, schreibe keine Änderungen in
	   die Datenbank.

SIEHE AUCH
       debconf-get-selections(1) (verfügbar im Paket debconf-utils)

AUTOR
       Petter Reinholdtsen <pere@hungry.com>

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2008 von Florian Rehnisch
       <eixman@gmx.de> und 2008-2009 Helge Kreutzmann <debian@helgefjell.de>
       angefertigt. Diese Übersetzung ist Freie Dokumentation; lesen Sie die
       GNU General Public License Version 2 oder neuer für die
       Kopierbedingungen.  Es gibt KEINE HAFTUNG.



				  2010-04-09	DEBCONF-SET-SELECTIONS.DE.1(1)
