DH_INSTALLINIT(1)		   Debhelper		     DH_INSTALLINIT(1)



NAME
       dh_installinit - install upstart jobs or init scripts into package
       build directories

SYNOPSIS
       dh_installinit [debhelper options] [--name=name] [-n] [-R] [-r] [-d]
       [-O] [-- params]

DESCRIPTION
       dh_installinit is a debhelper program that is responsible for
       installing upstart job files or init scripts with associated defaults
       files into package build directories, and in the former case providing
       compatibility handling for non-upstart systems.

       It also automatically generates the postinst and postrm and prerm
       commands needed to set up the symlinks in /etc/rc*.d/ and to start and
       stop the init scripts.

FILES
       debian/package.upstart
	   If this exists, it is installed into etc/init/package.conf in the
	   package build directory.

       debian/package.init
	   Otherwise, if this exists, it is installed into etc/init.d/package
	   in the package build directory.

       debian/package.default
	   If this exists, it is installed into etc/default/package in the
	   package build directory.

OPTIONS
       -n, --noscripts
	   Do not modify postinst/postrm/prerm scripts.

       -o, --onlyscripts
	   Only modify postinst/postrm/prerm scripts, do not actually install
	   any init script, default files, or upstart job.  May be useful if
	   the init script or upstart job is shipped and/or installed by
	   upstream in a way that doesn't make it easy to let dh_installinit
	   find it.

	   If no upstart job file is installed in the target directory when
	   dh_installinit --onlyscripts is called, this program will assume
	   that an init script is being installed and not provide the
	   compatibility symlinks or upstart dependencies.

       -R, --restart-after-upgrade
	   Do not stop the init script until after the package upgrade has
	   been completed. This is different than the default behavior, which
	   stops the script in the prerm, and starts it again in the postinst.

	   This can be useful for daemons that should not have a possibly long
	   downtime during upgrade. But you should make sure that the daemon
	   will not get confused by the package being upgraded while it's
	   running before using this option.

       -r, --no-restart-on-upgrade
	   Do not stop init script on upgrade.

       --no-start
	   Do not start the init script on install or upgrade, or stop it on
	   removal.  Only call update-rc.d. Useful for rcS scripts.

       -d, --remove-d
	   Remove trailing "d" from the name of the package, and use the
	   result for the filename the upstart job file is installed as in
	   etc/init/ , or for the filename the init script is installed as in
	   etc/init.d and the default file is installed as in etc/default/ .
	   This may be useful for daemons with names ending in "d". (Note:
	   this takes precedence over the --init-script parameter described
	   below.)

       -uparams --update-rcd-params=params
       -- params
	   Pass "params" to update-rc.d(8). If not specified, "defaults" will
	   be passed to update-rc.d(8).

       --name=name
	   Install the upstart job file or the init script (and default file)
	   using the filename name instead of the default filename, which is
	   the package name.  When this parameter is used, dh_installinit
	   looks for and installs files named debian/package.name.upstart,
	   debian/package.name.init and debian/package.name.default, instead
	   of the usual debian/package.upstart, debian/package.init and
	   debian/package.default.

       --init-script=scriptname
	   Use "scriptname" as the filename the init script is installed as in
	   etc/init.d/ (and also use it as the filename for the defaults file,
	   if it is installed). If you use this parameter, dh_installinit will
	   look to see if a file in the debian/ directory exists that looks
	   like "package.scriptname" and if so will install it as the init
	   script in preference to the files it normally installs.

	   This parameter is deprecated, use the --name parameter instead.
	   This parameter will be ignored completely for upstart jobs.

       --upstart-only
	   Only install an upstart job file, and do not include maintainer
	   script code to replace an init script with that upstart job.

	   This parameter is intended for use when the "package.upstart" file
	   is new and only to be used on Upstart-based systems.

       --error-handler=function
	   Call the named shell function if running the init script fails. The
	   function should be provided in the prerm and postinst scripts,
	   before the #DEBHELPER# token.

NOTES
       Note that this command is not idempotent. dh_prep(1) should be called
       between invocations of this command. Otherwise, it may cause multiple
       instances of the same text to be added to maintainer scripts.

SEE ALSO
       debhelper(7)

       This program is a part of debhelper.

AUTHORS
       Joey Hess <joeyh@debian.org>

       Steve Langasek <steve.langasek@canonical.com>

       Scott James Remnant <scott@canonical.com>



7.4.15ubuntu1			  2010-02-22		     DH_INSTALLINIT(1)
