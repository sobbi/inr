DH_INSTALLMIME(1)		   Debhelper		     DH_INSTALLMIME(1)



NAME
       dh_installmime - install mime files into package build directories

SYNOPSIS
       dh_installmime [debhelper options] [-n]

DESCRIPTION
       dh_installmime is a debhelper program that is responsible for
       installing mime files into package build directories.

       It also automatically generates the postinst and postrm commands needed
       to interface with the debian mime-support and shared-mime-info
       packages. These commands are inserted into the maintainer scripts by
       dh_installdeb(1).

FILES
       debian/package.mime
	   Installed into usr/lib/mime/packages/package in the package build
	   directory.

       debian/package.sharedmimeinfo
	   Installed into /usr/share/mime/packages/package.xml in the package
	   build directory.

OPTIONS
       -n, --noscripts
	   Do not modify postinst/postrm scripts.

NOTES
       Note that this command is not idempotent. dh_prep(1) should be called
       between invocations of this command. Otherwise, it may cause multiple
       instances of the same text to be added to maintainer scripts.

SEE ALSO
       debhelper(7)

       This program is a part of debhelper.

AUTHOR
       Joey Hess <joeyh@debian.org>



7.4.15ubuntu1			  2009-11-17		     DH_INSTALLMIME(1)
