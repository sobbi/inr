DH_PYTHON(1)			   Debhelper			  DH_PYTHON(1)



NAME
       dh_python - calculates python dependencies and adds postinst and prerm
       python scripts (deprecated)

SYNOPSIS
       dh_python [debhelper options] [-n] [-V version] [module dirs ...]

DESCRIPTION
       Note: This program is deprecated. You should use dh_pysupport or
       dh_pycentral instead. This program will do nothing if debian/pycompat
       or a Python-Version control file field exists.

       dh_python is a debhelper program that is responsible for generating the
       ${python:Depends} substitutions and adding them to substvars files. It
       will also add a postinst and a prerm script if required.

       The program will look at python scripts and modules in your package,
       and will use this information to generate a dependency on python, with
       the current major version, or on pythonX.Y if your scripts or modules
       need a specific python version. The dependency will be substituted into
       your package's control file wherever you place the token
       "${python:Depends}".

       If some modules need to be byte-compiled at install time, appropriate
       postinst and prerm scripts will be generated. If already byte-compiled
       modules are found, they are removed.

       If you use this program, your package should build-depend on python.

OPTIONS
       module dirs
	   If your package installs python modules in non-standard
	   directories, you can make dh_python check those directories by
	   passing their names on the command line. By default, it will check
	   /usr/lib/site-python, /usr/lib/$PACKAGE, /usr/share/$PACKAGE,
	   /usr/lib/games/$PACKAGE, /usr/share/games/$PACKAGE and
	   /usr/lib/python?.?/site-packages.

	   Note: only /usr/lib/site-python, /usr/lib/python?.?/site-packages
	   and the extra names on the command line are searched for binary
	   (.so) modules.

       -V version
	   If the .py files your package ships are meant to be used by a
	   specific pythonX.Y version, you can use this option to specify the
	   desired version, such as 2.3. Do not use if you ship modules in
	   /usr/lib/site-python.

       -n, --noscripts
	   Do not modify postinst/postrm scripts.

CONFORMS TO
       Debian policy, version 3.5.7

       Python policy, version 0.3.7

SEE ALSO
       debhelper(7)

       This program is a part of debhelper.

AUTHOR
       Josselin Mouette <joss@debian.org>

       most ideas stolen from Brendan O'Dea <bod@debian.org>



7.4.15ubuntu1			  2009-12-29			  DH_PYTHON(1)
