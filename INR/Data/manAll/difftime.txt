DIFFTIME(3)		   Linux Programmer's Manual		   DIFFTIME(3)



NAME
       difftime - calculate time difference

SYNOPSIS
       #include <time.h>

       double difftime(time_t time1, time_t time0);

DESCRIPTION
       The  difftime()	function returns the number of seconds elapsed between
       time time1 and time time0, represented as a double.  The two times  are
       specified in calendar time, which represents the time elapsed since the
       Epoch (00:00:00 on January 1, 1970, Coordinated Universal Time (UTC)).

CONFORMING TO
       SVr4, 4.3BSD, C89, C99.

NOTES
       On a POSIX system, time_t is an arithmetic type,  and  one  could  just
       define

	      #define difftime(t1,t0) (double)(t1 - t0)

       when  the  possible  overflow  in the subtraction is not a concern.  On
       other systems, the data type time_t might use some other encoding where
       subtraction doesn't work directly.

SEE ALSO
       date(1), gettimeofday(2), time(2), ctime(3), gmtime(3), localtime(3)

COLOPHON
       This  page  is  part of release 3.23 of the Linux man-pages project.  A
       description of the project, and information about reporting  bugs,  can
       be found at http://www.kernel.org/doc/man-pages/.



GNU				  2002-02-28			   DIFFTIME(3)
