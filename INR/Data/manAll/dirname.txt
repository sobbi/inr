DIRNAME(1)			 User Commands			    DIRNAME(1)



NAME
       dirname - strip non-directory suffix from file name

SYNOPSIS
       dirname NAME
       dirname OPTION

DESCRIPTION
       Print  NAME  with  its trailing /component removed; if NAME contains no
       /'s, output `.' (meaning the current directory).

       --help display this help and exit

       --version
	      output version information and exit

EXAMPLES
       dirname /usr/bin/sort
	      Output "/usr/bin".

       dirname stdio.h
	      Output ".".

AUTHOR
       Written by David MacKenzie and Jim Meyering.

REPORTING BUGS
       Report dirname bugs to bug-coreutils@gnu.org
       GNU coreutils home page: <http://www.gnu.org/software/coreutils/>
       General help using GNU software: <http://www.gnu.org/gethelp/>

COPYRIGHT
       Copyright © 2009 Free Software Foundation, Inc.	 License  GPLv3+:  GNU
       GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
       This  is  free  software:  you  are free to change and redistribute it.
       There is NO WARRANTY, to the extent permitted by law.

SEE ALSO
       basename(1), readlink(1)

       The full documentation for dirname is maintained as a  Texinfo  manual.
       If  the	info and dirname programs are properly installed at your site,
       the command

	      info coreutils 'dirname invocation'

       should give you access to the complete manual.



GNU coreutils 7.4		  March 2010			    DIRNAME(1)
