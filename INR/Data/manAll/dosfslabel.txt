dosfslabel(8)							 dosfslabel(8)



NAME
       dosfslabel - set or get a MS-DOS filesystem label


SYNOPSIS
       dosfslabel device [label]



DESCRIPTION
       This manual page documents briefly the dosfslabel command.

       dosfslabel  set or gets	a MS-DOS filesystem label from a given device.
       If label is omitted, then the label name of  the  specified  device  is
       written on the standard output.

       label can't be longer than 11 characters.


OPTIONS
       -h,--help
	      Displays a help message.

       -V,--version
	      Shows version.


SEE ALSO
       mkfs.msdos(8), mkdosfs(8)



AUTHORS
       dosfslabel is Copyright 2007 Red Hat, Inc.

       Some portions are Copyright 1998 Roman Hodek.

       Some portions are Copyright 1993 Werner Almesberger.

       This  manual page was written by François Wendling <frwendling@free.fr>
       for the Debian GNU/Linux system (but may be used by others).



				13 August 2008			 dosfslabel(8)
