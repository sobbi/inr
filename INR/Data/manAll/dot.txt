DOT(P)			   POSIX Programmer's Manual			DOT(P)



NAME
       dot - execute commands in the current environment

SYNOPSIS
       . file

DESCRIPTION
       The  shell shall execute commands from the file in the current environ‐
       ment.

       If file does not contain a slash, the shell shall use the  search  path
       specified  by PATH to find the directory containing file. Unlike normal
       command search, however, the file searched for by the dot utility  need
       not  be	executable.  If  no  readable file is found, a non-interactive
       shell shall abort; an interactive shell shall write a  diagnostic  mes‐
       sage  to  standard  error, but this condition shall not be considered a
       syntax error.

OPTIONS
       None.

OPERANDS
       See the DESCRIPTION.

STDIN
       Not used.

INPUT FILES
       See the DESCRIPTION.

ENVIRONMENT VARIABLES
       See the DESCRIPTION.

ASYNCHRONOUS EVENTS
       Default.

STDOUT
       Not used.

STDERR
       The standard error shall be used only for diagnostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       Returns the value of the last command executed, or a zero  exit	status
       if no command is executed.

CONSEQUENCES OF ERRORS
       Default.

       The following sections are informative.

APPLICATION USAGE
       None.

EXAMPLES
	      cat foobar
	      foo=hello bar=world. foobar
	      echo $foo $bar
	      hello world

RATIONALE
       Some older implementations searched the current directory for the file,
       even if the value of PATH disallowed it.   This	behavior  was  omitted
       from  this  volume of IEEE Std 1003.1-2001 due to concerns about intro‐
       ducing the susceptibility to trojan horses that the user might be  try‐
       ing to avoid by leaving dot out of PATH .

       The  KornShell  version of dot takes optional arguments that are set to
       the positional parameters. This is a valid extension that allows a  dot
       script to behave identically to a function.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Special Built-In Utilities

COPYRIGHT
       Portions  of  this text are reprinted and reproduced in electronic form
       from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology
       --  Portable  Operating	System	Interface (POSIX), The Open Group Base
       Specifications Issue 6, Copyright (C) 2001-2003	by  the  Institute  of
       Electrical  and	Electronics  Engineers, Inc and The Open Group. In the
       event of any discrepancy between this version and the original IEEE and
       The  Open Group Standard, the original IEEE and The Open Group Standard
       is the referee document. The original Standard can be  obtained	online
       at http://www.opengroup.org/unix/online.html .



IEEE/The Open Group		     2003				DOT(P)
