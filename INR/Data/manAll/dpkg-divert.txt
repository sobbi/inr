dpkg-divert(8)		      dpkg-Hilfsprogramme		dpkg-divert(8)



NAME
       dpkg-divert - Hinwegsetzen über die Paketversion einer Datei

ÜBERSICHT
       dpkg-divert [Optionen] Befehl

BESCHREIBUNG
       dpkg-divert  ist  ein  Hilfswerkzeug,  um  die  Liste  der  Umleitungen
       einzurichten und zu aktualisieren.

       Datei-Diversionen (Umleitungen) sind eine Möglichkeit, dpkg(1) dazu  zu
       zwingen,  eine  Datei  nicht  an  ihren Standardplatz, sondern an einem
       umgeleiteten Ort zu  installieren.  Umleitungen	können	innerhalb  der
       Debian-Paketskripte verwendet werden, um eine Datei beiseitezuschieben,
       wenn sie einen Konflikt auslöst. Systemadministratoren können sie  auch
       verwenden,  um  sich über die Konfigurationsdateien einiger Pakete hin‐
       wegzusetzen, oder wann immer einige Dateien (die nicht als  »Conffiles«
       markiert  sind)	von Dpkg erhalten werden müssen, wenn eine neuere Ver‐
       sion eines Paketes, die diese Dateien enthält, installiert wird.


BEFEHLE
       [--add] Datei
	      Fügt eine Umleitung für Datei hinzu.

       --remove Datei
	      Entferne eine Umleitung für Datei.

       --list Glob-Muster
	      Liste Umleitungen auf, die auf Glob-Muster passen.

       --listpackage Datei
	      Gibt den Namen des Pakets aus, die Datei	umleitet.  Gibt  LOCAL
	      aus,  falls  die	Datei lokal umgeleitet wurde und nichts, falls
	      Datei nicht umgeleitet wurde.

       --truename Datei
	      Zeige den echten Namen für eine umgeleitete Datei an.

OPTIONEN
       --admindir Verzeichnis
	      Setze  das  Dpkg-Datenverzeichnis  auf  Verzeichnis   (Standard:
	      /var/lib/dpkg).

       --divert Umleitziel
	      Umleitziel  ist der Zielort, wo die Versionen der Datei, wie sie
	      von anderen Paketen bereitgestellt wird, hin umgeleitet werden.

       --local
	      Gibt an, dass alle Versionen dieser Datei umgeleitet sind.  Dies
	      bedeutet,  dass  es  keine  Ausnahmen gibt, und egal was für ein
	      Paket auch installiert ist, die Datei ist umgeleitet. Dies  kann
	      vom Administrator verwendet werden, um eine lokal geänderte Ver‐
	      sion zu installieren.

       --package Paket
	      Paket ist der Name des Pakets,  dessen  Kopie  von  Datei  nicht
	      umgeleitet  wird,  d.h.  Datei  wird für alle Pakete außer Paket
	      umgeleitet.

       --quiet
	      Ruhiger Modus, d.h. keine langatmige Ausgabe.

       --rename
	      Schiebe  die  Dateien  tatsächlich   beiseite   (oder   zurück).
	      dpkg-divert  wird den Arbeitsvorgang abbrechen, falls die Zield‐
	      atei bereits existiert.

       --test Testmodus, d.h. führe keine Änderungen durch, sondern zeige  sie
	      lediglich.

       --help Zeige den Bedienungshinweis und beende.

       --version
	      Gebe die Version aus und beende sich.

BEMERKUNGEN
       Beim Hinzufügen ist die Standardeinstellung --local und --divert <Orig‐
       inalname>.distrib. Beim Entfernen müssen,  falls  angegeben,  --package
       oder --local und --divert übereinstimmen.

       Verzeichnisse können mit dpkg-divert nicht umgeleitet werden.

       Vorsicht sollte beim Umleiten von Laufzeit-Bibliotheken walten gelassen
       werden, da ldconfig(8) einen symbolischen Link, basierend  auf  dem  in
       der  Bibliothek eingebetteten DT_SONAME-Feld, anlegt. Da ldconfig keine
       Umleitungen anerkennt (nur Dpkg tut dies), kann der Symlink am Ende auf
       die umgeleitete Bibliothek zeigen, falls die umgeleitete Bibliothek den
       gleichen SONAME wie die nicht umgeleitete hat.

BEISPIELE
       Um  alle   Kopien   von	 /usr/bin/example   auf   /usr/bin/example.foo
       umzuleiten,  d.h.  alle	Pakete,  die  /usr/bin/example	zur  Verfügung
       stellen, dazu anzuleiten, diese Datei  stattdessen  als	/usr/bin/exam‐
       ple.foo	 zu   installieren   und  die  Umbenennung,  falls  notwendig,
       durchzuführen:

       dpkg-divert --divert /usr/bin/example.foo --rename /usr/bin/example

       Um diese Umleitung zu entfernen:

       dpkg-divert --rename --remove /usr/bin/example


       Um für jedes Paket - außer Ihrem eigenen wibble-Paket -, das  versucht,
       /usr/bin/example   zu  installieren,  dieses  auf  /usr/bin/example.foo
       umzuleiten:

       dpkg-divert --package  wibble  --divert	/usr/bin/example.foo  --rename
	      /usr/bin/example

       Um diese Umleitung zu entfernen:

       dpkg-divert --package wibble --rename --remove /usr/bin/example

DATEIEN
       /var/lib/dpkg/diversions
	      Datei,  die  die	aktuelle  Liste  von  Umleitungen  des Systems
	      enthält. Sie befindet sich  im  Dpkg-Administrationsverzeichnis,
	      zusammen mit anderen für Dpkg wichtigen Dateien, wie status oder
	      available.
	      Hinweis: dpkg-divert erhält die alte Kopie dieser Datei, mit der
	      Endung -old, bevor es sie mit der neuen ersetzt.

ÜBERSETZUNG
       Die  deutsche  Übersetzung  wurde  2004, 2006-2009 von Helge Kreutzmann
       <debian@helgefjell.de>, 2007 von Florian Rehnisch  <eixman@gmx.de>  und
       2008  von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese Übersetzung
       ist Freie Dokumentation; lesen Sie die GNU General Public License  Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       dpkg(1).

AUTOR
       Copyright © 1995 Ian Jackson

       Dies  ist Freie Software; lesen Sie die GNU General Public License Ver‐
       sion 2 oder neuer für die Kopierbedingungen. Es gibt KEINE Haftung.



Debian-Projekt			  2008-08-18			dpkg-divert(8)
