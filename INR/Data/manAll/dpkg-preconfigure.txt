DPKG-PRECONFIGURE.DE.8(8)	    Debconf	     DPKG-PRECONFIGURE.DE.8(8)



NAME
       dpkg-preconfigure - lässt Pakete vor ihrer Installation Fragen stellen

ÜBERSICHT
	dpkg-preconfigure [optionen] paket.deb

	dpkg-preconfigure --apt

BESCHREIBUNG
       dpkg-preconfigure lässt Pakete Fragen stellen, bevor sie installiert
       werden. Es arbeitet auf einem Satz von Debian-Paketen, und von allen
       Paketen, die Debconf benutzen, wird das Skript »config« ausgeführt, so
       dass sie das System examinieren und Fragen stellen können.

OPTIONEN
       -fTyp, --frontend=Typ
	   Wählt die zu verwendende Benutzerschnittstelle aus.

       -pWert, --priority=Wert
	   Setzt Sie die niedrigste Frage-Priorität, an der Sie interessiert
	   sind. Jegliche Fragen mit einer Priorität unterhalb der gewählten
	   Priorität werden ignoriert und deren Vorgabewerte werden benutzt.

       --terse
	   Enables terse output mode. This affects only some frontends.

       --apt
	   Arbeite im APT-Modus. Dieser erwartet eine Folge von Dateinamen von
	   Paketen auf der Standardeingabe, statt sie als Parameter übergeben
	   zu bekommen. Typischerweise wird dies benutzt, um APT dpkg-
	   preconfigure für alle Pakete laufen zu lassen, bevor sie
	   installiert werden. Um dies zu tun, fügen Sie etwas wie das
	   Folgende in die /etc/apt/apt.conf ein:

	    // Vor-Konfigurieren aller Pakete bevor
	    // sie installiert werden.
	    DPkg::Pre-Install-Pkgs {
		   "dpkg-preconfigure --apt --priority=low";
	    };

       -h, --help
	   Zeige Hilfe zur Benutzung an.

SIEHE AUCH
       debconf(7)

AUTOR
       Joey Hess <joeyh@debian.org>

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2008 von Florian Rehnisch
       <eixman@gmx.de> und 2008-2009 Helge Kreutzmann <debian@helgefjell.de>
       angefertigt. Diese Übersetzung ist Freie Dokumentation; lesen Sie die
       GNU General Public License Version 2 oder neuer für die
       Kopierbedingungen.  Es gibt KEINE HAFTUNG.



				  2010-04-09	     DPKG-PRECONFIGURE.DE.8(8)
