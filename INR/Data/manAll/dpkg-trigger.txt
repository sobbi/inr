dpkg-trigger(1) 	     dpkg-Programmsammlung	       dpkg-trigger(1)



NAME
       dpkg-trigger - ein Paket-Trigger-Hilfswerkzeug

ÜBERSICHT
       dpkg-trigger [Option...] Trigger-Name
       dpkg-trigger [Option...] Befehl

BESCHREIBUNG
       dpkg-trigger  ist ein Werkzeug, um Trigger (deutsch: Auslöser) explizit
       zu aktivieren und den laufenden dpkg auf  deren	Unterstützung  hin  zu
       überprüfen.

       Dies  kann von Betreuerskripten in komplexen und if-then-else-Situatio‐
       nen verwandt werden, wo die Datei-Trigger oder die Steuerung von Datei‐
       direktiven  durch  explizite activate-Trigger nicht ausreichen. Es kann
       auch zum Testen und von Systemadministratoren verwandt werden (beachten
       Sie  aber, dass Trigger nicht tatsächlich durch dpkg-trigger ausgeführt
       werden).

       Nicht erkannte Triggernamen-Syntaxen sind für dpkg-trigger ein Fehler.

BEFEHLE
       --check-supported
	      Überprüfe, ob der laufende dpkg Trigger  unterstützt  (normaler‐
	      weise  vom  Postinst  aufgerufen).  Endet mit 0, falls ein Trig‐
	      ger-fähiger dpkg ausgeführt wurde, oder mit 1 und einer  Fehler‐
	      meldung  nach  Stderr, falls nicht. Normalerweise ist es allerd‐
	      ings besser, einfach den gewünschten Trigger mit dpkg-trigger zu
	      aktivieren.

       -h, --help
	      Zeige den Bedienungshinweis und beende.

       --version
	      Gebe die Version aus und beende sich.

       --licence, --license
	      Zeigt die Kopier-Lizenzierungsbedingungen und beendet sich.

OPTIONEN
       --admindir=Verz
	      Ändert  den  Ablageort  der  dpkg-Datenbank. Der Standardort ist
	      /var/lib/dpkg.

       --by-package=Paket
	      Überschreibe den Trigger-Erwartenden  (normalerweise  wird  dies
	      von  dpkg  durch	die Umgebungsvariable DPKG_MAINTSCRIPT_PACKAGE
	      der Betreuerskripte gesetzt, wobei das Paket  benannt  wird,  zu
	      dem das Skript gehört und dies wird standardmäßig verwandt).

       --no-await
	      Diese  Option  arrangiert,  dass	das  aufrufende Paket T (falls
	      zutreffend) nicht auf die Abarbeitung des Triggers warten  muss,
	      das  bzw.  die  interessierte(n)	Paket(e) I werden nicht zu der
	      Liste der auf Bearbeitung wartenden Trigger  von	T  hinzugefügt
	      und  der Status von T bleibt unverändert. T kann als installiert
	      betrachtet werden, obwohl I den Trigger noch  nicht  verarbeitet
	      haben könnte.

       --no-act
	      Nur testen, nichts tatsächlich ändern.

ÜBERSETZUNG
       Die  deutsche  Übersetzung  wurde  2004, 2006-2009 von Helge Kreutzmann
       <debian@helgefjell.de>, 2007 von Florian Rehnisch  <eixman@gmx.de>  und
       2008  von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese Übersetzung
       ist Freie Dokumentation; lesen Sie die GNU General Public License  Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       dpkg(1), deb-triggers(5), /usr/share/doc/dpkg/triggers.txt.gz.




Debian-Projekt			  2009-03-15		       dpkg-trigger(1)
