dpkg(1) 		     dpkg-Programmsammlung		       dpkg(1)



NAME
       dpkg - Paketverwalter für Debian

ÜBERSICHT
       dpkg [Optionen] Aktionen

WARNUNG
       Dieses  Handbuch ist für Benutzer gedacht, die die Kommandozeilenoptio‐
       nen und Paketzustände von  dpkg	detaillierter  verstehen  wollen,  als
       durch dpkg --help beschrieben ist.

       Es  sollte  nicht  von  Paketbetreuern  verwendet werden, die verstehen
       wollen, wie dpkg ihr Paket installieren wird. Die Beschreibung von  den
       Tätigkeiten  von  dpkg  beim Installieren und Entfernen von Paketen ist
       besonders unzulänglich.

BESCHREIBUNG
       dpkg ist ein Werkzeug, um Debian-Pakete zu installieren, zu  bauen  und
       zu verwalten. Die primäre und benutzerfreundlichere Oberfläche für dpkg
       ist aptitude(1). dpkg selbst wird komplett über	Kommandozeilenoptionen
       gesteuert,  die	aus genau einer Aktion und Null oder mehreren Optionen
       bestehen. Der Aktionsparameter teilt dpkg mit, was zu tun ist, und  die
       Optionen steuern die Aktionen in irgendeiner Weise.

       dpkg  kann  auch  als  Oberfläche für dpkg-deb(1) verwendet werden. Die
       folgenden Aktionen gehören zu dpkg-deb, und wenn sie  angetroffen  wer‐
       den, startet dpkg einfach dpkg-deb mit den übergebenen Parametern:
	   -b, --build,
	   -c, --contents,
	   -I, --info,
	   -f, --field,
	   -e, --control,
	   -x, --extract,
	   -X, --vextract und
	   --fsys-tarfile.
       Bitte lesen Sie dpkg-deb(1) für weitere Informationen über diese Aktio‐
       nen.

INFORMATIONEN ÜBER PAKETE
       dpkg verwaltet einige nützliche Informationen über  verfügbare  Pakete.
       Die  Informationen sind in drei Klassen unterteilt: Status, Auswahlsta‐
       tus und Schalter. Diese Werte sind  hauptsächlich  zur  Änderung  durch
       dselect gedacht.

   PAKETZUSTÄNDE
       nicht-installiert
	      Das Paket ist nicht auf Ihrem System installiert.

       Config-Dateien
	      Nur die Konfigurationsdateien des Pakets existieren auf dem Sys‐
	      tem.

       halb-installiert
	      Die Installation des Paketes wurde  begonnen,  aber  aus	irgen‐
	      deinem Grund nicht abgeschlossen.

       entpackt
	      Das Paket ist entpackt, aber nicht konfiguriert.

       halb-konfiguriert
	      Das  Paket  ist  entpackt  und die Konfiguration wurde begonnen,
	      aber aus irgendeinem Grund nicht abgeschlossen.

       triggers-awaited
	      Das Paket erwartet Trigger-Verarbeitung durch ein anderes Paket.

       triggers-pending
	      Das Paket wurde getriggert.

       installiert
	      Das Paket ist entpackt und konfigurierte korrekt.

   PAKETAUSWAHL-STATUS
       installiere
	      Das Paket ist zur Installation ausgewählt.

       halten Ein Paket, das mit halten markiert  ist,	wird  von  dpkg  nicht
	      behandelt,  es  sei  denn  Sie  erzwingen  dies  mit  der Option
	      --force-hold.

       deinstalliere
	      Das Paket ist zur Deinstallation	ausgewählt  (d.h.  wir	wollen
	      alle Dateien außer den Konfigurationsdateien entfernen).

       vollständig löschen
	      Das  Paket  ist  zum  vollständigen Löschen (»purge«) ausgewählt
	      (d.h. wir wollen	alle  Dateien  inklusive  der  Konfigurations‐
	      dateien entfernen).

   PAKET-SCHALTER
       Neuinst. notw
	      Ein  mit	Neuinst.  notw	markiertes  Paket  ist defekt und muss
	      erneut installiert werden. Diese Pakete  können  nicht  entfernt
	      werden,	es  sei  denn,	Sie  erzwingen	dies  mit  der	Option
	      --force-remove-reinstreq.

AKTIONEN
       -i, --install Paketdatei...
	      Installiere das Paket.  Falls  die  --recursive  oder  -R-Option
	      angegeben  ist,  muss sich Paketdatei stattdessen auf ein Verze‐
	      ichnis beziehen.

	      Die Installation besteht aus folgenden Schritten:

	      1. Extrahiere die Steuerdateien aus dem neuen Paket.

	      2. Falls eine andere Version des gleichen  Pakets  vor  der  der
	      neuen  Installation installiert war, dann führe das prerm-Skript
	      des alten Paketes aus.

	      3. Führe das preinst-Skript aus, falls dies  vom	Paket  bereit‐
	      gestellt wird.

	      4. Entpacke die neuen Dateien und sichere gleichzeitig die alten
	      Dateien,	so  dass  diese,  falls  etwas	schief	geht,  wieder‐
	      hergestellt werden können.

	      5.  Falls  eine  andere  Version des gleichen Pakets vor der der
	      neuen Installation installiert war, dann führe das postrm-Skript
	      des alten Paketes aus. Beachten Sie, dass dieses Skript nach dem
	      preinst-Skript des neuen Pakets ausgeführt wird, da neue Dateien
	      zeitgleich zu der Entfernung alter Dateien geschrieben werden.

	      6.  Konfiguriere	das  Paket.  Lesen Sie --configure für detail‐
	      lierte Informationen wie dies geschieht.

       --unpack Paketdatei ...
	      Entpacke das  Paket,  aber  konfiguriere	es  nicht.  Falls  die
	      --recursive  oder  -R-Option angegeben ist, muss sich Paketdatei
	      stattdessen auf ein Verzeichnis beziehen.

       --configure Paket ...|-a|--pending
	      Rekonfiguriere ein entpacktes Paket.  Falls  -a  oder  --pending
	      anstelle	von  Paket  angegeben ist, werden alle entpackten aber
	      nicht konfigurierten Pakete konfiguriert.

	      Die Konfiguration besteht aus folgenden Schritten:

	      1. Entpacke die Conffiles und  sichere  gleichzeitig  die  alten
	      Conffiles,  so  dass  diese,  falls  etwas  schief geht, wieder‐
	      hergestellt werden können.

	      2. Führe das postinst-Skript aus, falls dies vom	Paket  bereit‐
	      gestellt wird.

       --triggers-only Paket...|-a|--pending
	      Verarbeite  nur Trigger. Alle ausstehenden Trigger werden verar‐
	      beitet. Falls Paketnamen übergeben werden, werden nur die  Trig‐
	      ger  dieser Pakete verarbeitet, jedes genau einmal wo notwendig.
	      Die Verwendung dieser Option kann  Pakete  in  die  unzulässigen
	      Stati  triggers-awaited  und triggers-pending bringen. Durch die
	      Ausführung von »dpkg --configure	--pending«  kann  dies	später
	      behoben werden.

       -r, --remove, -P, --purge Paket ...|-a|--pending
	      Entferne	ein  installiertes  Paket.  -r oder --remove entfernen
	      alles außer Conffiles. Dies könnte die  Rekonfiguration  vermei‐
	      den,  falls  das	Paket später wieder reinstalliert wird. (Conf‐
	      files sind die Konfigurationsdateien, die  in  der  debian/conf‐
	      files-Steuerdatei  aufgeführt  sind).  -P  oder --purge entfernt
	      alles, auch Conffiles. Falls -a  oder  --pending	anstatt  eines
	      Paketnamens  angegeben werden, dann werden alle Pakete, die ent‐
	      packt aber zur Entfernung oder  vollständigen  Löschung  in  der
	      Datei	/var/lib/dpkg/status	 markiert    sind,    entfernt
	      beziehungsweise vollständig gelöscht. Hinweis: Einige Konfigura‐
	      tionsdateien  können  dpkg  nicht  bekannt  sein, da sie separat
	      durch die Konfigurationsskripte angelegt und  verwaltet  werden.
	      In  diesem Fall wird dpkg sie nicht selbst entfernen sondern das
	      Skript postrm (das von dpkg aufgerufen  wird)  des  Pakets  muss
	      sich   während  des  vollständigen  Löschens  um	das  Entfernen
	      kümmern.

	      Entfernung eines Paketes besteht aus den folgenden Schritte:

	      1. Führe das prerm-Skript aus

	      2. Entferne die installierten Dateien

	      3. Führe das postrm-Skript aus

       --update-avail, --merge-avail Packages-Datei
	      Aktualisiere dpkgs  und  dselects  Verständnis  darüber,	welche
	      Pakete  verfügbar  sind.	Mit der Aktion --merge-avail wird alte
	      Information mit der Information aus  der	Packages-Datei	zusam‐
	      mengeführt. Mit der Aktion --update-avail wird die alte Informa‐
	      tion durch die Information aus der Packages-Datei  ersetzt.  Die
	      mit  Debian  vertriebene	Packages-Datei heißt einfach Packages.
	      dpkg hält seine Aufzeichnungen über die  verfügbaren  Pakete  in
	      /var/lib/dpkg/available.

	      Ein  einfacher Befehl, um die Datei available in einem Rutsch zu
	      holen und zu aktualisieren ist  dselect  update.	Beachten  Sie,
	      dass  diese  Datei  nahezu  nutzlos ist, falls Sie nicht dselect
	      sondern eine APT-basierte Oberfläche verwenden: APT verfügt über
	      sein eigenes System, die verfügbaren Pakete zu überwachen.

       -A, --record-avail Paketdatei ...
	      Aktualisiere  mit  den  Informationen  aus  dem Paket Paketdatei
	      dpkgs und dselects Verständnis darüber, welche Pakete  verfügbar
	      sind.  Falls  die --recursive oder -R-Option angegeben ist, muss
	      sich Paketdatei stattdessen auf ein Verzeichnis beziehen.

       --forget-old-unavail
	      Jetzt veraltet und ohne  Funktion,  da  dpkg  automatisch  nicht
	      installierte nicht verfügbare Pakete vergisst.

       --clear-avail
	      Lösche  die  existierenden  Informationen darüber, welche Pakete
	      verfügbar sind.

       -C, --audit
	      Suche nach Pakete, die in Ihrem System nur teilweise installiert
	      wurden.  dpkg  wird  Vorschläge  machen, was mit ihnen geschehen
	      soll, um sie in Funktion zu bringen.

       --get-selections [Paket-Name-Muster...]
	      Hole Liste von Paketauswahlen und schreibe sie auf die Standard‐
	      ausgabe.	Ohne  Muster  werden  nicht-installierte  Pakete (d.h.
	      solche, die vorher »vollständig gelöscht« wurden)  werden  nicht
	      angezeigt.

       --set-selections
	      Setze  die Paketauswahl durch Einlesen einer Datei von der Stan‐
	      dardeingabe. Diese Datei sollte im  Format  »<Paket>  <Zustand>«
	      sein,  wobei  Zustand  einer  aus »install«, »hold«, »deinstall«
	      oder »purge« ist. Leerzeilen und Kommentarzeilen (beginnend  mit
	      »#«) sind auch erlaubt.

       --clear-selections
	      Setze  den  erbetenen Zustand von jedem nicht-essenziellen Paket
	      auf  »Deinstallation«.  Dies  ist  dazu  gedacht,   direkt   vor
	      --set-selections verwendet zu werden, um jedes Paket, dass nicht
	      in der Liste von --set-selections vorkommt, zu deinstallieren.

       --yet-to-unpack
	      Sucht nach Paketen dir zur Installation ausgewählt  wurden,  die
	      aber aus irgendeinem Grund noch nicht installiert wurden.


       --print-architecture
	      Gebe die Architektur der Pakete aus, die dpkg installiert
	      (beispielsweise »i386«).

       --compare-versions Ver1 Op Ver2
	      Vergleiche Versionsnummern, wobei Op ein binärer Operator
	      ist.   dpkg   liefert  Erfolg  (Nullergebnis)  falls  die
	      angegebene  Bedingung  erfüllt  ist  und	 einen	 Fehler
	      (nicht-Null-Ergebnis)  andernfalls.  Es gibt zwei Gruppen
	      von Operatoren, die sich in  der	Behandlung  von  leeren
	      Ver1  oder  Ver2	unterscheiden.	Die folgenden behandeln
	      leere Versionen als jünger als jede andere Version: lt le
	      eq  ne  ge gt. Die folgenden behandeln eine leere Version
	      als älter als jede Version: lt-nl le-nl ge-nl gt-nl.  Die
	      folgenden  sind  nur  aus  Kompatibilität mit der Steuer‐
	      dateisyntax bereitgestellt: < << <= = >= >> >.

       --command-fd <n>
	      Akzeptiert eine  Reihe  von  Befehlen  an  Eingabe-Datei‐
	      deskriptor  <n>.	Hinweis:  Zusätzliche  auf  der Komman‐
	      dozeile und durch diesen Dateideskriptor gesetzte  Optio‐
	      nen  werden  nicht  für nachfolgende Befehle, die im gle‐
	      ichen Lauf ausgeführt werden, zurückgesetzt.

       --help Zeige eine kurze Hilfenachricht an.

       --force-help
	      Gebe Hilfe über die --force-Sache-Optionen.

       -Dh, --debug=help
	      Gib Hilfe über Debugging-Optionen.

       --licence, --license
	      Zeige die dpkg-Lizenz an.

       --version
	      Zeige dpkg Versionsinformationen an.

       dpkg-deb-Aktionen
	      Lesen Sie dpkg-deb(1) für weitere Informationen über  die
	      folgenden Aktionen.

	      -b, --build Verzeichnis [Archiv|Verzeichnis]
		  Baue ein deb-Paket.
	      -c, --contents Archiv
		  Liste den Inhalt eines deb-Paketes auf.
	      -e, --control Dateiname [Verzeichnis]
		  Extrahiere Steuerinformationen von einem Paket.
	      -x, --extract Archiv Verzeichnis
		  Extrahiere die vom Paket enthaltenen Dateien.
	      -f, --field Archiv [Steuerfeld] ...
		  Zeige das/die Steuerfeld(er) eines Paketes an.
	      --fsys-tarfile Archiv
		  Zeige die von einem Debian-Paket enthaltene Dateisystem-Tardatei an.
	      -I, --info Archiv [Steuerdatei]
		  Zeige Informationen über ein Paket.


       dpkg-query-Aktionen
	      Lesen  Sie  dpkg-query(1)  für weitere Informationen über
	      die folgenden Aktionen.


	      -l, --list Paketnamen-Muster ...
		  Liste auf das übergebene Suchmuster passende Pakete auf.
	      -s, --status Paketname ...
		  Berichte den Status des spezifizierten Pakets.
	      -L, --listfiles Paketname ...
		  Liste die aus Paketname auf Ihrem System installierten Dateien auf.
	      -S, --search Dateinamen-Suchmuster ...
		  Suche nach einem Dateinamen in installierten Paketen.
	      -p, --print-avail Paketname
		  Zeige Details über Paketname, wie in /var/lib/dpkg/available
		  gefunden. Benutzer von APT-basierten Oberflächen sollten stattdessen
		  apt-cache show Paketname verwenden.

OPTIONEN
       Alle Optionen können auf der Kommandozeile, in der dpkg-Konfigu‐
       rationsdatei  /etc/dpkg/dpkg.cfg oder in den Dateien im Konfigu‐
       rationsverzeichnis /etc/dpkg/dpkg.cfg.d/ angegeben werden.  Jede
       Zeile in der Konfigurationsdatei ist entweder eine Option (exakt
       die  gleiche  wie  die  Befehlszeilenoption  nur  ohne  führende
       Gedankenstriche) oder ein Kommentar (falls sie mit # beginnt).

       --abort-after=Zahl
	      Ändere  nach  wie vielen Fehlern dpkg abbrechen wird. Der
	      Standardwert ist 50.

       -B, --auto-deconfigure
	      Wenn ein Paket entfernt  wird  besteht  die  Möglichkeit,
	      dass  ein  anderes installiertes Paket von dem entfernten
	      Paket abhängt. Die Angabe dieser Option führt zur automa‐
	      tischen Dekonfiguration des Paketes, das von dem entfern‐
	      ten Paket abhängt.

       -DOktal, --debug=Oktal
	      Schalte  Debugging  ein.	Oktal	wird   durch   bitweise
	      Oder-Verknüpfung	der  gewünschten Werte von der nachfol‐
	      genden Liste gebildet  (beachten	Sie,  dass  sich  diese
	      Werte   in   zukünftigen	 Veröffentlichungen   verändern
	      können).	-Dh  oder  --debug=help  zeigen  diese	 Debug‐
	      ging-Werte an.

		Nummer	Beschreibung
		    1	Allgemein hilfreiche Fortschrittsinformationen
		    2	Aufruf und Status der Betreuerskripte
		   10	Ausgabe für jede verarbeitete Datei
		  100	 Umfangreiche  Ausgabe	für  jede  verarbeitete
	      Datei
		   20	Ausgabe für jede Konfigurationsdatei
		  200	Umfangreiche Ausgabe für  jede	Konfigurations‐
	      datei
		   40	Abhängigkeiten und Konflikte
		  400	Umfangreiche Abhängigkeiten/Konflikte-Ausgabe
		10000	Trigger-Aktivierung und -Verarbeitung
		20000	Umfangreiche Ausgabe bezüglich Trigger
		40000	Alberne Menge an Ausgabe bezüglich Trigger
		 1000	 Umfangreiches	Gelaber beispielsweise über das
	      dpkg/info-Verzeichnis
		 2000	Verrückte Mengen an Gelaber

       --force-Sachen, --no-force-Sachen, --refuse-Sachen

	      Erzwinge oder verweigere (no-force  und  refuse  bedeuten
	      das   gleiche)   bestimmte   Sachen.   Sachen   ist  eine
	      Komma-separierte	Liste  von  Dingen,  die  im  folgenden
	      beschrieben  sind.  --force-help zeigt eine Nachricht an,
	      die diese beschreibt.  Mit  (*)  markierte  Dinge  werden
	      standardmäßig erzwungen.

	      Warnung.	Diese  Optionen sind hauptsächlich für den Ein‐
	      satz durch Experten gedacht. Der Einsatz ohne  komplettes
	      Verständnis   der   Effekte   kann  Ihr  gesamtes  System
	      zerstören.

	      all: Schaltet alle »force«-Optionen ein (oder aus).

	      downgrade(*): Installiere ein  Paket,  selbst  wenn  eine
	      neuere Version davon bereits installiert ist.

	      Warnung:	     Derzeit	   führt       Dpkg	  keine
	      Abhängigkeitsüberprüfung	bei  der  Installation	älterer
	      Versionen  (als  bereits	installiert)  durch (sog. Down‐
	      grade) und wird Sie daher nicht warnen, falls dadurch die
	      Abhängigkeit eines anderen Pakets nicht mehr erfüllt ist.
	      Dies kann ernsthafte Seiteneffekte haben,  ein  Downgrade
	      einer  essenziellen  Systemkomponente  kann  Ihr gesamtes
	      System unbrauchbar machen. Verwenden Sie diese Option mit
	      Vorsicht.

	      configure-any:  Konfiguriere  auch  jedes entpackte, aber
	      unkonfigurierte Paket von dem das aktuelle Paket abhängt.

	      hold: Verarbeite auch Pakete, die mit  »halten«  markiert
	      sind.

	      remove-reinstreq:  Entferne  ein	Paket,	selbst falls es
	      defekt ist und zur  Neuinstallation  markiert  ist.  Dies
	      kann  beispielsweise  dazu  führen, dass Teile des Pakets
	      auf dem System bleiben und von dpkg vergessen werden.

	      remove-essential: Entferne, selbst falls	das  Paket  als
	      essenziell  betrachtet wird. Essenzielle Pakete enthalten
	      hauptsächlich sehr grundlegende  Unix-Befehle.  Diese  zu
	      entfernen kann dazu führen, dass das gesamte System nicht
	      mehr arbeitet - verwenden Sie diese Option daher mit Vor‐
	      sicht.

	      depends:	Verwandle alle Abhängigkeitsprobleme in Warnun‐
	      gen.

	      depends-version: Ignoriere Versionen bei der Prüfung  von
	      Abhängigkeiten.

	      breaks:  Installiere, selbst falls dies ein anderes Paket
	      beschädigt.

	      conflicts: Installiere, selbst wenn es mit einem	anderen
	      Paket  in  Konflikt  steht.  Dies ist gefährlich, da dies
	      gewöhnlich dazu führt, dass einige Dateien  überschrieben
	      werden.

	      confmiss:  Installiere immer ein fehlendes Conffile. Dies
	      ist gefährlich, da es bedeutet, dass eine  Änderung  (die
	      Entfernung) an der Datei nicht erhalten wird.

	      confnew:	Falls  eine Conffile modifiziert wurde, instal‐
	      liere immer die neue Version ohne Rückfrage, es sei denn,
	      --force-confdef ist ebenfalls angegeben, in welchem Falle
	      die Standardaktion bevorzugt wird.

	      confold: Falls eine Conffile modifiziert	wurde,	behalte
	      immer  die  alte	Version  ohne  Rückfrage,  es sei denn,
	      --force-confdef ist ebenfalls angegeben, in welchem Falle
	      die Standardaktion bevorzugt wird.

	      confdef: Falls eine Conffile verändert wurde, wähle immer
	      die Standardaktion. Falls es keine  Standardaktion  gibt,
	      halte  an  um  den  Benutzer  zu	fragen,  es  sei  denn,
	      --force-confnew  oder  --force-confold   sind   ebenfalls
	      angegeben,  in  welchem Falle dies verwendet wird, um die
	      endgültige Aktion zu bestimmen.

	      overwrite: Überschreibe die Datei  aus  einem  Paket  mit
	      einer Datei aus einem anderen Paket.

	      overwrite-dir  Überschreibe  das	Verzeichnis  aus  einem
	      Paket mit einer Datei aus einem anderen Paket.

	      overwrite-diverted:   Überschreibe    eine    umgeleitete
	      (»diverted«) Datei mit einer nicht umgeleiteten.

	      architecture:  Verarbeite  sogar	Pakete mit der falschen
	      Architektur.

	      bad-path: Im PATH fehlen wichtige Programme,  daher  sind
	      Probleme wahrscheinlich.

	      not-root:  Versuche  Sachen  zu  (de)installieren, selbst
	      falls nicht root.

	      bad-verify: Installiere ein Paket selbst wenn die Authen‐
	      tizitätsprüfung fehlschlägt.


       --ignore-depends=Paket,...
	      Ignoriere  Abhängigkeitsüberprüfungen  für ein bestimmtes
	      Paket (tatsächlich  wird	die  Überprüfung  durchgeführt,
	      aber nur Warnungen über Konflikte werden angezeigt, sonst
	      nichts).

       --new, --old
	      Wähle das alte oder neue Binärpaketformat. Dies ist  eine
	      dpkg-deb(1)-Option.

       --nocheck
	      Lese  oder  überprüfe  den  Inhalt  der  Steuerdatei beim
	      Paketbau nicht. Dies ist eine dpkg-deb(1)-Option.

       --no-act, --dry-run, --simulate
	      Erledige alles, was gemacht werden  soll,  aber  schreibe
	      keine  Änderungen.  Dies	wird verwendet um zu sehen, was
	      mit der  spezifizierten  Änderung  passieren  würde  ohne
	      tatsächlich etwas zu modifizieren.

	      Stellen Sie sicher, dass --no-act vor dem Aktions-Parame‐
	      ter steht, oder Sie könnten mit unerwünschten Ergebnissen
	      enden.  (Beispielsweise  wird  dpkg  --purge foo --no-act
	      zuerst das Paket foo bereinigen und dann	versuchen,  das
	      Paket  --no-act  zu bereinigen, obwohl Sie wahrscheinlich
	      davon ausgingen, dass tatsächlich  gar  nichts  passieren
	      sollte)

       -R, --recursive
	      Behandle	rekursiv  alle	regulären  Dateien, die auf das
	      Muster *.deb passen und im  angegeben  Verzeichnis  sowie
	      allen Unterverzeichnis liegen. Dies kann mit den Aktionen
	      -i, -A, --install, --unpack und --avail verwendet werden.

       -G     Installiere ein Paket nicht, falls  bereits  eine  neuere
	      Version  des  gleichen  Paketes installiert ist. Dies ist
	      ein Alias für --refuse-downgrade.

       --admindir=Verz
	      Ändere  das  voreingestellte  administrative  Verzeichnis
	      (standardmäßig   /var/lib/dpkg),	in  dem  viele	Dateien
	      liegen, die Informationen über  den  Status  von	instal‐
	      lierten und deinstallierten Pakete usw. liegen.

       --instdir=Verz
	      Ändere   das   voreingestellte  Installationsverzeichnis.
	      Dieses Verzeichnis gibt an, wo Pakete installiert werden.
	      instdir  (standardmäßig  /) ist auch das Verzeichnis, das
	      an chroot(2) vor dem Aufruf der Installationsskripte  des
	      Paketes  übergeben  wird,  was bedeutet, dass die Skripte
	      instdir als ein Wurzelverzeichnis sehen.

       -root=Verz
	      Durch Ändern von root wird instdir auf Verz und  admindir
	      auf Verz/var/lib/dpkg geändert.

       -O, --selected-only
	      Bearbeite nur die Pakete, die zur Installation ausgewählt
	      sind. Die eigentliche Markierung erfolgt mit dselect oder
	      durch  dpkg,  wenn  es  Pakete bearbeitet. Beispielsweise
	      wird ein Paket bei der Entfernung als »zur Deinstallation
	      ausgewählt« markiert.

       -E, --skip-same-version
	      Installiere  das	Paket  nicht, falls die gleiche Version
	      des Pakets bereits installiert ist.

       --pre-invoke=Befehl
       --post-invoke=Befehl
	      Setzt einen Aufruf-Hook Befehl, der via »sh -c« vor  oder
	      nach dem Dpkg-Aufruf der Dpkg-Aktionen unpack, configure,
	      install, triggers-only, remove und purge ausgeführt wird.
	      Diese  Option kann mehrfach angegeben werden. Die Reihen‐
	      folge der Optionen wird erhalten, wobei Einträge aus  den
	      Konfigurationsdateien   Vorrang	einnehmen.   Die  Umge‐
	      bungsvariable DPKG_HOOK_ACTION wird für die Hooks auf die
	      aktuelle	 Dpkg-Aktion   gesetzt.   Hinweis:  Oberflächen
	      könnten  Dpkg  mehrere  Male  pro  Ausführung   aufrufen,
	      wodurch  die  Hooks  öfter als erwartet ausgeführt werden
	      könnten.

       --status-fd n
	      Schicke maschinenlesbare Paketstatus- und Fortschrittsin‐
	      formationen  an  den Dateideskriptor n. Diese Option kann
	      mehrfach angegeben werden. Die Information besteht typis‐
	      cherweise aus einem Datensatz pro Zeile in folgendem For‐
	      mat:

	      status: Paket: Status
		     Paketstatus geändert; Status entsprechend der Sta‐
		     tusdatei.

	      status: Paket : error : ausführliche-Fehlermeldung
		     Ein  Fehler  ist  aufgetreten.  Unglücklicherweise
		     kann zum Zeitpunkt der Erstellung dieser  Informa‐
		     tionen  ausführliche-Fehlermeldung  Zeilenumbrüche
		     enthalten. In  Standorteinstellungen  (»locales«),
		     in  denen	Übersetzer sorgfältig gearbeitet haben,
		     wird  allerdings  jeder  Zeilenumbruch  von  einem
		     Leerzeichen gefolgt.

	      status:  Datei  : conffile-prompt : »echt-alt« »echt-neu«
	      benutzer-edit dist-edit
		     Dem Benutzer wird eine Conffile-Frage gestellt.

	      processing: Stufe: Paket
		     Versandt  genau  bevor   eine   Verarbeitungsstufe
		     beginnt.  Stufe  ist  eine der folgenden: upgrade,
		     install (beide werden vor dem Entpacken versandt),
		     configure, trigproc, disappear, remove, purge.

       --log=Dateiname
	      Protokolliere Statusänderungsaktualisierungen und -Aktio‐
	      nen  in	Dateiname   anstatt   in   die	 Standard-Datei
	      /var/log/dpkg.log.  Falls diese Option mehrfach übergeben
	      wird, wird der  letzte  Dateiname  verwendet.  Protokoll‐
	      nachrichten sind von der Form »JJJJ-MM-TT HH:MM:SS status
	      <Zustand>   <Pkt>   <installierte-Version>«   für    Sta‐
	      tusänderungsaktualisierungen;	»JJJJ-MM-TT    HH:MM:SS
	      <Aktion>	<Pkt>  <installierte-Version>  <verfügbare-Ver‐
	      sion>«  für  Aktionen,  wobei  <Aktion> entweder install,
	      upgrade, remove oder purge ist; und »YYYY-MM-DD  HH:MM:SS
	      conffile	  <Dateiname>	 <Entscheidung>«    für   Conf‐
	      file-Änderungen  ist,   wobei   <Entscheidung>   entweder
	      install oder keep ist.

       --no-debsig
	      Versuche nicht, Paketsignaturen zu überprüfen.

       --no-triggers
	      Führe  keine Trigger in diesem Durchlauf aus (Aktivierun‐
	      gen werden dennoch aufgezeichnet). Falls dies mit  --con‐
	      figure  Paket  oder  --triggers-only Paket verwandt wird,
	      wird das	Postinst  des  benannten  Pakets  dennoch  aus‐
	      geführt, selbst falls nur ein Trigger-Lauf notwendig ist.
	      Die  Verwendung  dieser  Option  kann   Pakete   in   die
	      unzulässigen  Stati triggers-awaited und triggers-pending
	      bringen.	Durch  die  Ausführung	von  »dpkg  --configure
	      --pending« kann dies später behoben werden.

       --triggers
	      Annulliert ein vorheriges --no-triggers.

DATEIEN
       /etc/dpkg/dpkg.cfg
	      Konfigurationsdatei  mit Standardeinstellungen der Optio‐
	      nen.

       /var/log/dpkg.log
	      Standard-Protokolldatei (lesen Sie  /etc/dpkg/dpkg.cfg(5)
	      und die Option --log).

       Die  anderen  unten aufgeführten Dateien sind in ihrem Standard‐
       verzeichnis, lesen Sie den Text	zur  Option  --admindir  um  zu
       sehen, wie sie den Ort dieser Dateien ändern können.

       /var/lib/dpkg/available
	      Liste der verfügbaren Pakete.

       /var/lib/dpkg/status
	      Statusse	der  verfügbaren  Pakete.  Diese  Datei enthält
	      Informationen  darüber,  ob  ein	Paket  zur   Entfernung
	      markiert ist oder nicht, ob es installiert ist oder nicht
	      usw. Lesen Sie den Abschnitt  INFORMATIONEN  ÜBER  PAKETE
	      für weitere Informationen.

	      Die Statusdatei wird täglich nach /var/backups gesichert.
	      Dies kann hilfreich sein, falls sie aufgrund von	Proble‐
	      men mit dem Dateisystem verloren gegangen oder beschädigt
	      worden ist.

       Die folgenden Dateien sind Komponenten von  Binärpaketen.  Lesen
       Sie deb(5) für weitere Informationen über sie:

       control

       conffiles

       preinst

       postinst

       prerm

       postrm

UMGEBUNGSVARIABLEN
       DPKG_NO_TSTP
	      Definieren   sie	diese  zu  irgendetwas,  falls	Sie  es
	      bevorzugen, dass dpkg eine neue Shell startet statt  sich
	      selber  zu  suspendieren, während es einen »Shell-Escape«
	      durchführt.

       SHELL  Das Programm, das dpkg ausführen wird, wenn es eine  neue
	      Shell startet.

       COLUMNS
	      Setzt  die  Anzahl  von  Spalten die dpkg verwenden soll,
	      wenn es formatierten Text anzeigt.  Derzeit  nur	von  -l
	      verwendet.

       DPKG_RUNNING_VERSION
	      Wird  von  dpkg  für die Betreuer-Skript-Umgebung auf die
	      Version der aktuell laufenden Instanz von dpkg gesetzt.

       DPKG_MAINTSCRIPT_PACKAGE
	      Wird von dpkg für die Betreuer-Skript-Umgebung auf den in
	      Arbeit befindlichen Paketnamen gesetzt.

       DPKG_MAINTSCRIPT_ARCH
	      Wird  von  dpkg  für die Betreuer-Skript-Umgebung auf die
	      Architektur gesetzt, für die das Paket gebaut wurde.

BEISPIELE
       Um Pakete mit Bezug zum vi(1)-Editor aufzulisten:
	    dpkg -l '*vi*'

       Um die Einträge von zwei Paketen in  /var/lib/dpkg/available  zu
       sehen:
	    dpkg --print-avail elvis vim | less

       Wenn Sie die Liste der Pakete selbst durchsuchen wollen:
	    less /var/lib/dpkg/available

       Um ein installiertes Elvis-Paket zu entfernen:
	    dpkg -r elvis

       Um ein Paket zu installieren, müssen Sie es erst in einem Archiv
       oder auf einer CD-ROM finden. Die »available«-Datei zeigt,  dass
       das vim-Paket im Bereich »editors« ist:
	    cd /cdrom/pool/main/v/vim
	    dpkg -i vim_4.5-3.deb

       Um eine lokale Kopie der Paketauswahl-Zustände zu erstellen:
	    dpkg --get-selections >meine_auswahl

       Sie können diese Datei zu einem anderen Computer bringen und sie
       dort mit folgenden Befehlen installieren:
	    dpkg --clear-selections
	    dpkg --set-selections <meine_auswahl

       Beachten Sie, dass dies nichts wirklich	installiert  oder  ent‐
       fernt,  sondern	lediglich  den Auswahlzustand der angeforderten
       Pakete setzt. Sie werden eine andere Anwendung benötigen, um die
       angeforderten  Pakete tatsächlich herunterzuladen und zu instal‐
       lieren. Führen Sie beispielsweise apt-get dselect-upgrade aus.

       Gewöhnlich werden Sie feststellen, dass dselect(1)  eine  beque‐
       mere Art ist, den Paketauswahlzustand zu ändern.

ZUSÄTZLICHE FUNKTIONALITÄT
       Zusätzliche Funktionalität kann durch die Installation jedes der
       folgenden Pakete erhalten werden: apt, aptitude und debsums.

ÜBERSETZUNG
       Die deutsche Übersetzung wurde 2004, 2006-2009 von Helge Kreutz‐
       mann  <debian@helgefjell.de>,  2007  von  Florian Rehnisch <eix‐
       man@gmx.de> und 2008 von Sven Joachim <svenjoac@gmx.de> angefer‐
       tigt.  Diese  Übersetzung ist Freie Dokumentation; lesen Sie die
       GNU  General  Public  License  Version  2  oder	neuer  für  die
       Kopierbedingungen.  Es gibt KEINE HAFTUNG.

SIEHE AUCH
       aptitude(1),  apt(1),  dselect(1),  dpkg-deb(1),  dpkg-query(1),
       deb(5), deb-control(5), dpkg.cfg(5) und dpkg-reconfigure(8).

FEHLER
       --no-act gibt gewöhnlich  weniger  Informationen  als  hilfreich
       sein könnten.

AUTOREN
       Lesen  Sie  /usr/share/doc/dpkg/THANKS  für die Liste der Leute,
       die zu dpkg beigetragen haben.



Debian-Projekt			  2009-11-12			       dpkg(1)
