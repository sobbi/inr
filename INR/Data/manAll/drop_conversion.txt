DROP CONVERSION(7)		 SQL Commands		    DROP CONVERSION(7)



NAME
       DROP CONVERSION - remove a conversion


SYNOPSIS
       DROP CONVERSION [ IF EXISTS ] name [ CASCADE | RESTRICT ]


DESCRIPTION
       DROP CONVERSION removes a previously defined conversion.  To be able to
       drop a conversion, you must own the conversion.

PARAMETERS
       IF EXISTS
	      Do not throw an error if	the  conversion  does  not  exist.   A
	      notice is issued in this case.

       name   The  name  of the conversion. The conversion name can be schema-
	      qualified.

       CASCADE

       RESTRICT
	      These key words do not have  any	effect,  since	there  are  no
	      dependencies on conversions.

EXAMPLES
       To drop the conversion named myname:

       DROP CONVERSION myname;


COMPATIBILITY
       There is no DROP CONVERSION statement in the SQL standard.

SEE ALSO
       ALTER  CONVERSION [alter_conversion(7)], CREATE CONVERSION [create_con‐
       version(7)]



SQL - Language Statements	  2010-05-19		    DROP CONVERSION(7)
