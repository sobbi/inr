DROP GROUP(7)			 SQL Commands			 DROP GROUP(7)



NAME
       DROP GROUP - remove a database role


SYNOPSIS
       DROP GROUP [ IF EXISTS ] name [, ...]


DESCRIPTION
       DROP GROUP is now an alias for DROP ROLE [drop_role(7)].

COMPATIBILITY
       There is no DROP GROUP statement in the SQL standard.

SEE ALSO
       DROP ROLE [drop_role(7)]



SQL - Language Statements	  2010-05-19			 DROP GROUP(7)
