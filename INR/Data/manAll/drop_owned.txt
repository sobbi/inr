DROP OWNED(7)			 SQL Commands			 DROP OWNED(7)



NAME
       DROP OWNED - remove database objects owned by a database role


SYNOPSIS
       DROP OWNED BY name [, ...] [ CASCADE | RESTRICT ]


DESCRIPTION
       DROP OWNED drops all the objects in the current database that are owned
       by one of the specified roles. Any  privileges  granted	to  the  given
       roles on objects in the current database will also be revoked.

PARAMETERS
       name   The  name  of  a	role  whose objects will be dropped, and whose
	      privileges will be revoked.

       CASCADE
	      Automatically drop objects that depend on the affected objects.

       RESTRICT
	      Refuse to drop the objects owned by a role if any other database
	      objects  depend  on  one	of  the  affected objects. This is the
	      default.

NOTES
       DROP OWNED is often used to prepare for the  removal  of  one  or  more
       roles. Because DROP OWNED only affects the objects in the current data‐
       base, it is usually necessary to execute this command in each  database
       that contains objects owned by a role that is to be removed.

       Using  the  CASCADE  option  might  make the command recurse to objects
       owned by other users.

       The REASSIGN OWNED [reassign_owned(7)] command is an  alternative  that
       reassigns  the  ownership  of  all the database objects owned by one or
       more roles.

COMPATIBILITY
       The DROP OWNED statement is a PostgreSQL extension.

SEE ALSO
       REASSIGN OWNED [reassign_owned(7)], DROP ROLE [drop_role(7)]



SQL - Language Statements	  2010-05-19			 DROP OWNED(7)
