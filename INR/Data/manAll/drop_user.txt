DROP USER(7)			 SQL Commands			  DROP USER(7)



NAME
       DROP USER - remove a database role


SYNOPSIS
       DROP USER [ IF EXISTS ] name [, ...]


DESCRIPTION
       DROP USER is now an alias for DROP ROLE [drop_role(7)].

COMPATIBILITY
       The  DROP  USER	statement  is a PostgreSQL extension. The SQL standard
       leaves the definition of users to the implementation.

SEE ALSO
       DROP ROLE [drop_role(7)]



SQL - Language Statements	  2010-05-19			  DROP USER(7)
