DVIPDF(1)			  Ghostscript			     DVIPDF(1)



NAME
       dvipdf  -  konvertiert TeX-DVI-Dateien nach PDF mittels Ghostscript und
       dvips

SYNTAX
       dvipdf [ Optionen ] Eingabe.dvi [ Ausgabe.pdf ] ...

BESCHREIBUNG
       Das Skript ruf dvips(1) mit dem -q Parameter auf und leitet die Ausgabe
       über  eine  Pipe  an  gs(1)  weiter,  das  mit  den folgenden Paramtern
       aufgerufen wird:

		       -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite

       sowie mit -sOutputFile und allen in der Kommandozeile angegeben	Optio‐
       nen.

SIEHE AUCH
       gs(1), dvips(1)

VERSION
       Dieses  Dokument  wurde zuletzt für Ghostscript Version 7.21 durchgese‐
       hen.

AUTOR
       Artifex Software, Inc. sind die Hauptautoren  von  Ghostscript.	 Diese
       Manpage ist von George Ferguson.



7.21				  8.Juli 2002			     DVIPDF(1)
