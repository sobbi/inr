PS2PS(1)		     Ghostscript-Werkzeuge		      PS2PS(1)



NAME
       ps2ps, eps2eps - Ghostscript PostScript "Distiller"

SYNTAX
       ps2ps [ Optionen ] Eingabe.ps Ausgabe.ps
       eps2eps [ Optionen ] Eingabe.eps Ausgabe.eps

BESCHREIBT
       ps2ps  benutzt gs(1) um die PostScript(tm)-Dateien "Eingabe.ps" in ein‐
       facheres und (meist) schnelleres PostScript  in	"Ausgabe.ps"  zu  kon‐
       vertieren.   Normalerweise  darf  die  Ausgabe  PostScript-Level-2-Kon‐
       strukte benutzen, aber -dLanguageLevel=1 erzwingt die Ausgabe von Level
       1.

       eps2eps	führt  analoge Optimierungen bei Encapsulated PostScript (EPS)
       Dateien durch.

DATEIEN
       Rufen Sie "gs -h" auf, um den  Ort  der	Ghostscript-Dokumentation  auf
       Ihrem System zu ermitteln, wo Sie weitere Informationen finden.

VERSION
       Dieses  Document  wurde	zuletzt  für  Ghostscript Version 7.21 überar‐
       beitet.

AUTOR
       Artifex Software, Inc. sind die Hauptautoren von Ghostscript.



7.21				  8.Juli 2002			      PS2PS(1)
