<errno.h>(P)		   POSIX Programmer's Manual		  <errno.h>(P)



NAME
       errno.h - system error numbers

SYNOPSIS
       #include <errno.h>

DESCRIPTION
       Some  of the functionality described on this reference page extends the
       ISO C standard. Any conflict between the  requirements  described  here
       and   the   ISO C   standard   is   unintentional.    This   volume  of
       IEEE Std 1003.1-2001 defers to the ISO C standard.

       The ISO C standard only requires  the  symbols  [EDOM],	[EILSEQ],  and
       [ERANGE] to be defined.

       The  <errno.h>  header  shall  provide a declaration for errno and give
       positive values for the	following  symbolic  constants.  Their	values
       shall be unique except as noted below.

       E2BIG  Argument list too long.

       EACCES Permission denied.

       EADDRINUSE
	      Address in use.

       EADDRNOTAVAIL
	      Address not available.

       EAFNOSUPPORT
	      Address family not supported.

       EAGAIN Resource	unavailable,  try  again  (may	be  the  same value as
	      [EWOULDBLOCK]).

       EALREADY
	      Connection already in progress.

       EBADF  Bad file descriptor.

       EBADMSG
	      Bad message.

       EBUSY  Device or resource busy.

       ECANCELED
	      Operation canceled.

       ECHILD No child processes.

       ECONNABORTED
	      Connection aborted.

       ECONNREFUSED
	      Connection refused.

       ECONNRESET
	      Connection reset.

       EDEADLK
	      Resource deadlock would occur.

       EDESTADDRREQ
	      Destination address required.

       EDOM   Mathematics argument out of domain of function.

       EDQUOT Reserved.

       EEXIST File exists.

       EFAULT Bad address.

       EFBIG  File too large.

       EHOSTUNREACH
	      Host is unreachable.

       EIDRM  Identifier removed.

       EILSEQ Illegal byte sequence.

       EINPROGRESS
	      Operation in progress.

       EINTR  Interrupted function.

       EINVAL Invalid argument.

       EIO    I/O error.

       EISCONN
	      Socket is connected.

       EISDIR Is a directory.

       ELOOP  Too many levels of symbolic links.

       EMFILE Too many open files.

       EMLINK Too many links.

       EMSGSIZE
	      Message too large.

       EMULTIHOP
	      Reserved.

       ENAMETOOLONG
	      Filename too long.

       ENETDOWN
	      Network is down.

       ENETRESET
	      Connection aborted by network.

       ENETUNREACH
	      Network unreachable.

       ENFILE Too many files open in system.

       ENOBUFS
	      No buffer space available.

       ENODATA
	      No message is available on the STREAM head read queue.

       ENODEV No such device.

       ENOENT No such file or directory.

       ENOEXEC
	      Executable file format error.

       ENOLCK No locks available.

       ENOLINK
	      Reserved.

       ENOMEM Not enough space.

       ENOMSG No message of the desired type.

       ENOPROTOOPT
	      Protocol not available.

       ENOSPC No space left on device.

       ENOSR  No STREAM resources.

       ENOSTR Not a STREAM.

       ENOSYS Function not supported.

       ENOTCONN
	      The socket is not connected.

       ENOTDIR
	      Not a directory.

       ENOTEMPTY
	      Directory not empty.

       ENOTSOCK
	      Not a socket.

       ENOTSUP
	      Not supported.

       ENOTTY Inappropriate I/O control operation.

       ENXIO  No such device or address.

       EOPNOTSUPP
	      Operation not supported on socket.

       EOVERFLOW
	      Value too large to be stored in data type.

       EPERM  Operation not permitted.

       EPIPE  Broken pipe.

       EPROTO Protocol error.

       EPROTONOSUPPORT

	      Protocol not supported.

       EPROTOTYPE
	      Protocol wrong type for socket.

       ERANGE Result too large.

       EROFS  Read-only file system.

       ESPIPE Invalid seek.

       ESRCH  No such process.

       ESTALE Reserved.

       ETIME  Stream ioctl() timeout.

       ETIMEDOUT
	      Connection timed out.

       ETXTBSY
	      Text file busy.

       EWOULDBLOCK
	      Operation would block (may be the same value as [EAGAIN]).

       EXDEV  Cross-device link.


       The following sections are informative.

APPLICATION USAGE
       Additional error numbers may be defined on conforming systems; see  the
       System Interfaces volume of IEEE Std 1003.1-2001.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       The  System  Interfaces	volume	of  IEEE Std 1003.1-2001, Section 2.3,
       Error Numbers

COPYRIGHT
       Portions of this text are reprinted and reproduced in  electronic  form
       from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology
       -- Portable Operating System Interface (POSIX),	The  Open  Group  Base
       Specifications  Issue  6,  Copyright  (C) 2001-2003 by the Institute of
       Electrical and Electronics Engineers, Inc and The Open  Group.  In  the
       event of any discrepancy between this version and the original IEEE and
       The Open Group Standard, the original IEEE and The Open Group  Standard
       is  the	referee document. The original Standard can be obtained online
       at http://www.opengroup.org/unix/online.html .



IEEE/The Open Group		     2003			  <errno.h>(P)
