EUIDACCESS(3)		   Linux Programmer's Manual		 EUIDACCESS(3)



NAME
       euidaccess, eaccess - check effective user's permissions for a file

SYNOPSIS
       #define _GNU_SOURCE
       #include <unistd.h>

       int euidaccess(const char *pathname, int mode);
       int eaccess(const char *pathname, int mode);

DESCRIPTION
       Like  access(2),  euidaccess()  checks permissions and existence of the
       file identified by its argument pathname.  However, whereas  access(2),
       performs  checks  using	the  real  user  and  group identifiers of the
       process, euidaccess() uses the effective identifiers.

       mode is a mask consisting of one or more of R_OK, W_OK, X_OK and  F_OK,
       with the same meanings as for access(2).

       eaccess()  is  a  synonym  for euidaccess(), provided for compatibility
       with some other systems.

RETURN VALUE
       On success (all requested permissions granted), zero is	returned.   On
       error  (at least one bit in mode asked for a permission that is denied,
       or some other error occurred), -1 is returned, and errno is set	appro‐
       priately.

ERRORS
       As for access(2).

VERSIONS
       The eaccess() function was added to glibc in version 2.4.

CONFORMING TO
       These functions are non-standard.  Some other systems have an eaccess()
       function.

SEE ALSO
       access(2),  chmod(2),  chown(2),  faccessat(2),	 open(2),   setgid(2),
       setuid(2), stat(2), credentials(7), path_resolution(7)

COLOPHON
       This  page  is  part of release 3.23 of the Linux man-pages project.  A
       description of the project, and information about reporting  bugs,  can
       be found at http://www.kernel.org/doc/man-pages/.



				  2007-07-26			 EUIDACCESS(3)
