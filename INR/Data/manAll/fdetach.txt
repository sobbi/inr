FDETACH(P)		   POSIX Programmer's Manual		    FDETACH(P)



NAME
       fdetach - detach a name from a STREAMS-based file descriptor (STREAMS)

SYNOPSIS
       #include <stropts.h>

       int fdetach(const char *path);


DESCRIPTION
       The  fdetach() function shall detach a STREAMS-based file from the file
       to which it was attached by a previous call  to	fattach().   The  path
       argument  points  to  the  pathname  of	the attached STREAMS file. The
       process shall have appropriate privileges or be the owner of the  file.
       A successful call to fdetach() shall cause all pathnames that named the
       attached STREAMS file to again name the file to which the STREAMS  file
       was  attached.  All  subsequent operations on path shall operate on the
       underlying file and not on the STREAMS file.

       All open file descriptions  established	while  the  STREAMS  file  was
       attached  to  the  file	referenced  by	path  shall still refer to the
       STREAMS file after the fdetach() has taken effect.

       If there are no open  file  descriptors	or  other  references  to  the
       STREAMS	file,  then a successful call to fdetach() shall be equivalent
       to performing the last close() on the attached file.

RETURN VALUE
       Upon successful completion, fdetach() shall  return  0;	otherwise,  it
       shall return -1 and set errno to indicate the error.

ERRORS
       The fdetach() function shall fail if:

       EACCES Search permission is denied on a component of the path prefix.

       EINVAL The path argument names a file that is not currently attached.

       ELOOP  A loop exists in symbolic links encountered during resolution of
	      the path argument.

       ENAMETOOLONG
	      The size of a pathname exceeds {PATH_MAX} or a  pathname	compo‐
	      nent is longer than {NAME_MAX}.

       ENOENT A component of path does not name an existing file or path is an
	      empty string.

       ENOTDIR
	      A component of the path prefix is not a directory.

       EPERM  The effective user ID is not the owner of path and  the  process
	      does not have appropriate privileges.


       The fdetach() function may fail if:

       ELOOP  More  than  {SYMLOOP_MAX} symbolic links were encountered during
	      resolution of the path argument.

       ENAMETOOLONG
	      Pathname resolution of a symbolic link produced an  intermediate
	      result whose length exceeds {PATH_MAX}.


       The following sections are informative.

EXAMPLES
   Detaching a File
       The following example detaches the STREAMS-based file /tmp/named-STREAM
       from the file to which it was attached by a previous,  successful  call
       to  fattach(). Subsequent calls to open this file refer to the underly‐
       ing file, not to the STREAMS file.


	      #include <stropts.h>
	      ...
		  char *filename = "/tmp/named-STREAM";
		  int ret;


		  ret = fdetach(filename);

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       fattach()  ,  the  Base	Definitions  volume  of  IEEE Std 1003.1-2001,
       <stropts.h>

COPYRIGHT
       Portions  of  this text are reprinted and reproduced in electronic form
       from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology
       --  Portable  Operating	System	Interface (POSIX), The Open Group Base
       Specifications Issue 6, Copyright (C) 2001-2003	by  the  Institute  of
       Electrical  and	Electronics  Engineers, Inc and The Open Group. In the
       event of any discrepancy between this version and the original IEEE and
       The  Open Group Standard, the original IEEE and The Open Group Standard
       is the referee document. The original Standard can be  obtained	online
       at http://www.opengroup.org/unix/online.html .



IEEE/The Open Group		     2003			    FDETACH(P)
