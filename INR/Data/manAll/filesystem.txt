filesystem(7)																      filesystem(7)



NAME
       filesystem - event signalling that filesystems have been mounted

SYNOPSIS
       filesystem [ENV]...

DESCRIPTION
       The  filesystem	event is generated by the mountall(8) daemon after it has mounted all filesystems listed in fstab(5).  mountall(8) emits this event
       as an informational signal, services and tasks started or stopped by this event will do so in parallel with other activity.

EXAMPLE
       A service that wishes to be running once filesystems are mounted might use:

	      start on filesystem

SEE ALSO
       mounting(7) mounted(7) virtual-filesystems(7) local-filesystems(7) remote-filesystems(7) all-swaps(7)



mountall								 2009-12-21							      filesystem(7)
