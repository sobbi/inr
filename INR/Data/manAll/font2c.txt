FONT2C(1)			  Ghostscript			     FONT2C(1)



NAME
       font2c - schreibt PostScript-Type-0- oder Type-1-Schriften als C-Code

SYNTAX
       font2c Schriftname

BESCHREIBUNG
       Das Skript ruft gs(1) mit den folgen Paramtern auf:

			  -q -dNODISPLAY -dWRITESYSTEMDICT

       gefolgt	von den Argumenten der Kommandozeile. Dies schreibt eine Post‐
       Script-Type-0 oder Type-1-Schrift als C-Quelltext, so daß sie  mit  dem
       Interpreter gelinkt werden kann.

SIEHE AUCH
       gs(1)

VERSION
       Dieses  Dokument wurde zuletzt für Ghostscript Version 7.21 durchfgese‐
       hen.

AUTHOR
       Artifex Software, Inc. sind die Hauptautoren  von  Ghostscript.	 Diese
       Manpage ist von George Ferguson.




7.21				  8.Juli 2002			     FONT2C(1)
