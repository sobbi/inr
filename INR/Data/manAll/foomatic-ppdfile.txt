FOOMATIC-PPDFILE(1)															FOOMATIC-PPDFILE(1)



NAME
       foomatic-ppdfile - Generate a PPD file for a given printer/driver combo

SYNOPSIS
       foomatic-ppdfile [-w] [-d <driver>] -p <printerid>
       foomatic-ppdfile [-A] [-P pattern]
       foomatic-ppdfile -h


DESCRIPTION
       The  first  form  of  the  foomatic-ppdfile to compute the spooler-independent Foomatic PPD file for any valid printer/driver combo available in the
       Foomatic database, both for printing with foomatic-rip(1) and for applications/clients being able to access the printer's options.  The PPD file  is
       returned on standard ouput.  If the driver is not specified, the default driver is used.


       The  second  form  of the foomatic-ppdfile program will search the printer database and return either all printer entries or those whose name and/or
       model information match a regular expression.


       The last form prints a help message and exits.


   Options
       -d drivername
	    The (optional) driver name to use.	If the driver name is not supplied, the default or first driver in the printers compatible driver  list  is
	    used.


       -p   printer id The printer id.


       -w   Return a PPD file that conforms to the Microsoft Windows format limitations.


       -A   Return all printer entries in the database.


       -Ppattern
	    Return all printer entries in the database which match the regular expression.




       SEE ALSO
	      foomatic-rip(1)


EXIT STATUS
       foomatic-ppdfile returns 0 on success, 1 otherwise.


AUTHOR
       Manfred Wassmann <manolo@NCC-1701.B.Shuttle.de> for the foomatic project using output from the associated binary.

       Modified and updated by Patrick Powell <papowell at astart.com>.


BUGS
       None so far.

       Please post bug reports on

       http://lists.freestandards.org/mailman/listinfo/printing-foomatic




Foomatic Project							 2001-05-07							FOOMATIC-PPDFILE(1)
