OPENPTY(3)							 Linux Programmer's Manual							 OPENPTY(3)



NAME
       openpty, login_tty, forkpty - tty utility functions

SYNOPSIS
       #include <pty.h>

       int openpty(int *amaster, int *aslave, char *name,
		   struct termios *termp, struct winsize *winp);

       pid_t forkpty(int *amaster, char *name, struct termios *termp,
		     struct winsize *winp);

       #include <utmp.h>

       int login_tty(int fd);

       Link with -lutil.

DESCRIPTION
       The  openpty()  function finds an available pseudo-terminal and returns file descriptors for the master and slave in amaster and aslave.  If name is
       not NULL, the filename of the slave is returned in name.  If termp is not NULL, the terminal parameters of the slave will be set to  the  values  in
       termp.  If winp is not NULL, the window size of the slave will be set to the values in winp.

       The  login_tty()  function  prepares  for  a  login  on the tty fd (which may be a real tty device, or the slave of a pseudo-terminal as returned by
       openpty()) by creating a new session, making fd the controlling terminal for the calling process, setting fd to be the standard input,  output,	and
       error streams of the current process, and closing fd.

       The  forkpty() function combines openpty(), fork(2), and login_tty() to create a new process operating in a pseudo-terminal.  The file descriptor of
       the master side of the pseudo-terminal is returned in amaster, and the filename of the slave in name if it is not NULL.	The termp  and	winp  argu‐
       ments, if not NULL, will determine the terminal attributes and window size of the slave side of the pseudo-terminal.

RETURN VALUE
       If  a  call to openpty(), login_tty(), or forkpty() is not successful, -1 is returned and errno is set to indicate the error.  Otherwise, openpty(),
       login_tty(), and the child process of forkpty() return 0, and the parent process of forkpty() returns the process ID of the child process.

ERRORS
       openpty() will fail if:

       ENOENT There are no available ttys.

       login_tty() will fail if ioctl(2) fails to set fd to the controlling terminal of the calling process.

       forkpty() will fail if either openpty() or fork(2) fails.

CONFORMING TO
       These are BSD functions, present in libc5 and glibc2.

NOTES
       In versions of glibc before 2.0.92, openpty() returns file descriptors for a BSD pseudo-terminal pair; since glibc 2.0.92, it first attempts to open
       a Unix 98 pseudo-terminal pair, and falls back to opening a BSD pseudo-terminal pair if that fails.

BUGS
       Nobody knows how much space should be reserved for name.  So, calling openpty() or forkpty() with non-NULL name may not be secure.

SEE ALSO
       fork(2), ttyname(3), pty(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



GNU									 2003-07-18								 OPENPTY(3)
