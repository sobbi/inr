FPUTWS(3)							 Linux Programmer's Manual							  FPUTWS(3)



NAME
       fputws - write a wide-character string to a FILE stream

SYNOPSIS
       #include <wchar.h>

       int fputws(const wchar_t *ws, FILE *stream);

DESCRIPTION
       The  fputws() function is the wide-character equivalent of the fputs(3) function.  It writes the wide-character string starting at ws, up to but not
       including the terminating L'\0' character, to stream.

       For a non-locking counterpart, see unlocked_stdio(3).

RETURN VALUE
       The fputws() function returns a non-negative integer if the operation was successful, or -1 to indicate an error.

CONFORMING TO
       C99, POSIX.1-2001.

NOTES
       The behavior of fputws() depends on the LC_CTYPE category of the current locale.

       In the absence of additional information passed to the fopen(3) call, it is reasonable to expect that fputws() will  actually  write  the  multibyte
       string corresponding to the wide-character string ws.

SEE ALSO
       fputwc(3), unlocked_stdio(3)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



GNU									 1999-07-25								  FPUTWS(3)
