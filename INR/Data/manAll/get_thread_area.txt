GET_THREAD_AREA(2)						 Linux Programmer's Manual						 GET_THREAD_AREA(2)



NAME
       get_thread_area - Get a Thread Local Storage (TLS) area

SYNOPSIS
       #include <linux/unistd.h>
       #include <asm/ldt.h>

       int get_thread_area(struct user_desc *u_info);

DESCRIPTION
       get_thread_area()  returns  an  entry  in the current thread's Thread Local Storage (TLS) array.  The index of the entry corresponds to the value of
       u_info->entry_number, passed in by the user.  If the value is in bounds, get_thread_area() copies the corresponding TLS entry into the area  pointed
       to by u_info.

RETURN VALUE
       get_thread_area() returns 0 on success.	Otherwise, it returns -1 and sets errno appropriately.

ERRORS
       EFAULT u_info is an invalid pointer.

       EINVAL u_info->entry_number is out of bounds.

VERSIONS
       A version of get_thread_area() first appeared in Linux 2.5.32.

CONFORMING TO
       get_thread_area() is Linux-specific and should not be used in programs that are intended to be portable.

NOTES
       Glibc does not provide a wrapper for this system call; call it using syscall(2).

SEE ALSO
       modify_ldt(2), set_thread_area(2)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2008-11-27							 GET_THREAD_AREA(2)
