GETGID(2)							 Linux Programmer's Manual							  GETGID(2)



NAME
       getgid, getegid - get group identity

SYNOPSIS
       #include <unistd.h>
       #include <sys/types.h>

       gid_t getgid(void);
       gid_t getegid(void);

DESCRIPTION
       getgid() returns the real group ID of the calling process.

       getegid() returns the effective group ID of the calling process.

       When  a	normal	program is executed, the effective and real group ID of the process are set to the group ID of the user executing the file.  When a
       set ID program is executed the real group ID is set to the group of the calling user and the effective user ID corresponds to the set group  ID	bit
       on the file being executed.

ERRORS
       These functions are always successful.

CONFORMING TO
       POSIX.1-2001, 4.3BSD.

SEE ALSO
       getresgid(2), setgid(2), setregid(2), credentials(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 1993-07-23								  GETGID(2)
