GETUID(2)							 Linux Programmer's Manual							  GETUID(2)



NAME
       getuid, geteuid - get user identity

SYNOPSIS
       #include <unistd.h>
       #include <sys/types.h>

       uid_t getuid(void);
       uid_t geteuid(void);

DESCRIPTION
       getuid() returns the real user ID of the calling process.

       geteuid() returns the effective user ID of the calling process.

       When  a	normal	program is executed, the effective and real user ID of the process are set to the ID of the user executing the file.  When a set ID
       program is executed the real user ID is set to the calling user and the effective user ID corresponds to the set ID bit on the file being executed.

ERRORS
       These functions are always successful.

CONFORMING TO
       POSIX.1-2001, 4.3BSD.

NOTES
   History
       In Unix V6 the getuid() call returned (euid << 8) + uid.  Unix V7 introduced separate calls getuid() and geteuid().

SEE ALSO
       getresuid(2), setreuid(2), setuid(2), credentials(7)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 1993-07-23								  GETUID(2)
