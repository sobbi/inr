AGETTY(8)																	  AGETTY(8)



NAME
       getty - alternative Linux getty


SYNOPSIS
       getty [-8ihLmnUw] [-f issue_file] [-l login_program] [-I init] [-t timeout] [-H login_host] port baud_rate,...  [term]
       getty [-8ihLmnw] [-f issue_file] [-l login_program] [-I init] [-t timeout] [-H login_host] baud_rate,...  port [term]


DESCRIPTION
       getty opens a tty port, prompts for a login name and invokes the /bin/login command. It is normally invoked by init(8).

       getty has several non-standard features that are useful for hard-wired and for dial-in lines:

       o      Adapts  the tty settings to parity bits and to erase, kill, end-of-line and uppercase characters when it reads a login name.  The program can
	      handle 7-bit characters with even, odd, none or space parity, and 8-bit characters with no parity. The following special characters are  rec‐
	      ognized: @ and Control-U (kill); #, DEL and back space (erase); carriage return and line feed (end of line).

       o      Optionally deduces the baud rate from the CONNECT messages produced by Hayes(tm)-compatible modems.

       o      Optionally does not hang up when it is given an already opened line (useful for call-back applications).

       o      Optionally does not display the contents of the /etc/issue file.

       o      Optionally displays an alternative issue file instead of /etc/issue.

       o      Optionally does not ask for a login name.

       o      Optionally invokes a non-standard login program instead of /bin/login.

       o      Optionally turns on hard-ware flow control

       o      Optionally forces the line to be local with no need for carrier detect.

       This program does not use the /etc/gettydefs (System V) or /etc/gettytab (SunOS 4) files.

ARGUMENTS
       port   A  path  name relative to the /dev directory. If a "-" is specified, getty assumes that its standard input is already connected to a tty port
	      and that a connection to a remote user has already been established.

	      Under System V, a "-" port argument should be preceded by a "--".

       baud_rate,...
	      A comma-separated list of one or more baud rates. Each time getty receives a BREAK character it advances through the list, which	is  treated
	      as if it were circular.

	      Baud rates should be specified in descending order, so that the null character (Ctrl-@) can also be used for baud rate switching.

       term   The  value  to  be  used	for  the TERM environment variable. This overrides whatever init(8) may have set, and is inherited by login and the
	      shell.

OPTIONS
       -8     Assume that the tty is 8-bit clean, hence disable parity detection.

       -h     Enable hardware (RTS/CTS) flow control. It is left up to the application to disable software (XON/XOFF) flow protocol where appropriate.

       -i     Do not display the contents of /etc/issue (or other) before writing the login prompt. Terminals or communications hardware  may  become  con‐
	      fused when receiving lots of text at the wrong baud rate; dial-up scripts may fail if the login prompt is preceded by too much text.

       -f issue_file
	      Display the contents of issue_file instead of /etc/issue.  This allows custom messages to be displayed on different terminals.  The -i option
	      will override this option.

       -I initstring
	      Set an initial string to be sent to the tty or modem before sending anything else. This may be used to initialize  a  modem.   Non  printable
	      characters  may  be  sent  by writing their octal code preceded by a backslash (\). For example to send a linefeed character (ASCII 10, octal
	      012) write \012.

       -l login_program
	      Invoke the specified login_program instead of /bin/login.  This allows the use of a non-standard login program (for example,  one  that  asks
	      for a dial-up password or that uses a different password file).

       -H login_host
	      Write the specified login_host into the utmp file. (Normally, no login host is given, since getty is used for local hardwired connections and
	      consoles. However, this option can be useful for identifying terminal concentrators and the like.

       -m     Try to extract the baud rate the CONNECT status message produced by Hayes(tm)-compatible modems. These  status  messages	are  of  the  form:
	      "<junk><speed><junk>".  getty assumes that the modem emits its status message at the same speed as specified with (the first) baud_rate value
	      on the command line.

	      Since the -m feature may fail on heavily-loaded systems, you still should enable BREAK processing by enumerating all expected baud  rates  on
	      the command line.

       -n     Do  not  prompt the user for a login name. This can be used in connection with -l option to invoke a non-standard login process such as a BBS
	      system. Note that with the -n option, getty gets no input from user who logs in and therefore won't be able to figure out  parity,  character
	      size,  and  newline  processing  of  the	connection. It defaults to space parity, 7 bit characters, and ASCII CR (13) end-of-line character.
	      Beware that the program that getty starts (usually /bin/login) is run as root.

       -t timeout
	      Terminate if no user name could be read within timeout seconds. This option should probably not be used with hard-wired lines.

       -L     Force the line to be a local line with no need for carrier detect. This can be useful when you have a locally  attached  terminal  where	the
	      serial line does not set the carrier detect signal.

       -U     Turn  on	support  for detecting an uppercase only terminal.  This setting will detect a login name containing only capitals as indicating an
	      uppercase only terminal and turn on some upper to lower case conversions.  Note that this has no support for any unicode characters.

       -w     Wait for the user or the modem to send a carriage-return or a linefeed character before sending the /etc/issue (or other) file and the  login
	      prompt. Very useful in connection with the -I option.

EXAMPLES
       This  section  shows  examples  for the process field of an entry in the /etc/inittab file.  You'll have to prepend appropriate values for the other
       fields.	See inittab(5) for more details.

       For a hard-wired line or a console tty:
	    /sbin/getty 9600 ttyS1

       For a directly connected terminal without proper carriage detect wiring: (try this if your terminal just sleeps instead of giving  you  a  password:
       prompt.)
	    /sbin/getty -L 9600 ttyS1 vt100

       For a old style dial-in line with a 9600/2400/1200 baud modem:
	    /sbin/getty -mt60 ttyS1 9600,2400,1200

       For a Hayes modem with a fixed 115200 bps interface to the machine: (the example init string turns off modem echo and result codes, makes modem/com‐
       puter DCD track modem/modem DCD, makes a DTR drop cause a dis-connection and turn on auto-answer after 1 ring.)
	    /sbin/getty -w -I 'ATE0Q1&D2&C1S0=1\015' 115200 ttyS1


ISSUE ESCAPES
       The issue-file (/etc/issue or the file set with the -f option) may contain certain escape codes to display the system name, date and time  etc.	All
       escape codes consist of a backslash (\) immediately followed by one of the letters explained below.


       b      Insert the baudrate of the current line.

       d      Insert the current date.

       s      Insert the system name, the name of the operating system.

       l      Insert the name of the current tty line.

       m      Insert the architecture identifier of the machine, eg. i486

       n      Insert the nodename of the machine, also known as the hostname.

       o      Insert the NIS domainname of the machine.

       O      Insert the DNS domainname of the machine.

       r      Insert the release number of the OS, eg. 1.1.9.

       t      Insert the current time.

       u      Insert the number of current users logged in.

       U      Insert the string "1 user" or "<n> users" where <n> is the number of current users logged in.

       v      Insert the version of the OS, eg. the build-date etc.

       Example: On my system, the following /etc/issue file:

	      This is \n.\o (\s \m \r) \t

       displays as

	      This is thingol.orcan.dk (Linux i386 1.1.9) 18:29:30



FILES
       /var/run/utmp, the system status file.
       /etc/issue, printed before the login prompt.
       /dev/console, problem reports (if syslog(3) is not used).
       /etc/inittab, init(8) configuration file.

BUGS
       The  baud-rate  detection feature (the -m option) requires that getty be scheduled soon enough after completion of a dial-in call (within 30 ms with
       modems that talk at 2400 baud). For robustness, always use the -m option in combination with a multiple baud rate  command-line	argument,  so  that
       BREAK processing is enabled.

       The text in the /etc/issue file (or other) and the login prompt are always output with 7-bit characters and space parity.

       The baud-rate detection feature (the -m option) requires that the modem emits its status message after raising the DCD line.

DIAGNOSTICS
       Depending  on  how the program was configured, all diagnostics are written to the console device or reported via the syslog(3) facility.  Error mes‐
       sages are produced if the port argument does not specify a terminal device; if there is no utmp entry for the current process (System V	only);	and
       so on.

AUTHOR(S)
       W.Z. Venema <wietse@wzv.win.tue.nl>
       Eindhoven University of Technology
       Department of Mathematics and Computer Science
       Den Dolech 2, P.O. Box 513, 5600 MB Eindhoven, The Netherlands

       Peter Orbaek <poe@daimi.aau.dk>
       Linux port and more options. Still maintains the code.

       Eric Rasmussen <ear@usfirst.org>
       Added -f option to display custom login messages on different terminals.


AVAILABILITY
       The getty command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



																		  AGETTY(8)
