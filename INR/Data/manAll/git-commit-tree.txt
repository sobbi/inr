GIT-COMMIT-TREE(1)							 Git Manual							 GIT-COMMIT-TREE(1)



NAME
       git-commit-tree - Create a new commit object

SYNOPSIS
       git commit-tree <tree> [-p <parent commit>]\* < changelog

DESCRIPTION
       This is usually not what an end user wants to run directly. See git-commit(1) instead.

       Creates a new commit object based on the provided tree object and emits the new commit object id on stdout.

       A commit object may have any number of parents. With exactly one parent, it is an ordinary commit. Having more than one parent makes the commit a
       merge between several lines of history. Initial (root) commits have no parents.

       While a tree represents a particular directory state of a working directory, a commit represents that state in "time", and explains how to get
       there.

       Normally a commit would identify a new "HEAD" state, and while git doesn’t care where you save the note about that state, in practice we tend to
       just write the result to the file that is pointed at by .git/HEAD, so that we can always see what the last committed state was.

OPTIONS
       <tree>
	   An existing tree object

       -p <parent commit>
	   Each -p indicates the id of a parent commit object.

COMMIT INFORMATION
       A commit encapsulates:

       ·   all parent object ids

       ·   author name, email and date

       ·   committer name and email and the commit time.

       While parent object ids are provided on the command line, author and committer information is taken from the following environment variables, if
       set:

	   GIT_AUTHOR_NAME
	   GIT_AUTHOR_EMAIL
	   GIT_AUTHOR_DATE
	   GIT_COMMITTER_NAME
	   GIT_COMMITTER_EMAIL
	   GIT_COMMITTER_DATE
	   EMAIL

       (nb "<", ">" and "\n"s are stripped)

       In case (some of) these environment variables are not set, the information is taken from the configuration items user.name and user.email, or, if
       not present, system user name and fully qualified hostname.

       A commit comment is read from stdin. If a changelog entry is not provided via "<" redirection, git commit-tree will just wait for one to be entered
       and terminated with ^D.

DATE FORMATS
       The GIT_AUTHOR_DATE, GIT_COMMITTER_DATE environment variables support the following date formats:

       Git internal format
	   It is <unix timestamp> <timezone offset>, where <unix timestamp> is the number of seconds since the UNIX epoch.  <timezone offset> is a positive
	   or negative offset from UTC. For example CET (which is 2 hours ahead UTC) is +0200.

       RFC 2822
	   The standard email format as described by RFC 2822, for example Thu, 07 Apr 2005 22:13:13 +0200.

       ISO 8601
	   Time and date specified by the ISO 8601 standard, for example 2005-04-07T22:13:13. The parser accepts a space instead of the T character as
	   well.

	       Note
	       In addition, the date part is accepted in the following formats: YYYY.MM.DD, MM/DD/YYYY and DD.MM.YYYY.

DIAGNOSTICS
       You don’t exist. Go away!
	   The passwd(5) gecos field couldn’t be read

       Your parents must have hated you!
	   The passwd(5) gecos field is longer than a giant static buffer.

       Your sysadmin must hate you!
	   The passwd(5) name field is longer than a giant static buffer.

DISCUSSION
       At the core level, git is character encoding agnostic.

       ·   The pathnames recorded in the index and in the tree objects are treated as uninterpreted sequences of non-NUL bytes. What readdir(2) returns are
	   what are recorded and compared with the data git keeps track of, which in turn are expected to be what lstat(2) and creat(2) accepts. There is
	   no such thing as pathname encoding translation.

       ·   The contents of the blob objects are uninterpreted sequences of bytes. There is no encoding translation at the core level.

       ·   The commit log messages are uninterpreted sequences of non-NUL bytes.

       Although we encourage that the commit log messages are encoded in UTF-8, both the core and git Porcelain are designed not to force UTF-8 on
       projects. If all participants of a particular project find it more convenient to use legacy encodings, git does not forbid it. However, there are a
       few things to keep in mind.

	1.  git commit and git commit-tree issues a warning if the commit log message given to it does not look like a valid UTF-8 string, unless you
	   explicitly say your project uses a legacy encoding. The way to say this is to have i18n.commitencoding in .git/config file, like this:

	       [i18n]
		       commitencoding = ISO-8859-1

	   Commit objects created with the above setting record the value of i18n.commitencoding in its encoding header. This is to help other people who
	   look at them later. Lack of this header implies that the commit log message is encoded in UTF-8.

	2.  git log, git show, git blame and friends look at the encoding header of a commit object, and try to re-code the log message into UTF-8 unless
	   otherwise specified. You can specify the desired output encoding with i18n.logoutputencoding in .git/config file, like this:

	       [i18n]
		       logoutputencoding = ISO-8859-1

	   If you do not have this configuration variable, the value of i18n.commitencoding is used instead.

       Note that we deliberately chose not to re-code the commit log message when a commit is made to force UTF-8 at the commit object level, because
       re-coding to UTF-8 is not necessarily a reversible operation.

SEE ALSO
       git-write-tree(1)

AUTHOR
       Written by Linus Torvalds <torvalds@osdl.org[1]>

DOCUMENTATION
       Documentation by David Greaves, Junio C Hamano and the git-list <git@vger.kernel.org[2]>.

GIT
       Part of the git(1) suite

NOTES
	1. torvalds@osdl.org
	   mailto:torvalds@osdl.org

	2. git@vger.kernel.org
	   mailto:git@vger.kernel.org



Git 1.7.0.4								 12/18/2010							 GIT-COMMIT-TREE(1)
