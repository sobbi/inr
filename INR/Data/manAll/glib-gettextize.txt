GLIB-GETTEXTIZE(1)															 GLIB-GETTEXTIZE(1)



NAME
       glib-gettextize - gettext internationalization utility

SYNOPSIS
       glib-gettextize [option...] [directory]

DESCRIPTION
       glib-gettextize helps to prepare a source package for being internationalized through gettext. It is a variant of the gettextize that ships with
       gettext.

       glib-gettextize differs from gettextize in that it doesn't create an intl/ subdirectory and doesn't modify po/ChangeLog (note that newer versions of
       gettextize behave like this when called with the --no-changelog option).

   Options
       --help print help and exit

       --version
	      print version information and exit

       -c, --copy
	      copy files instead of making symlinks

       -f, --force
	      force writing of new files even if old ones exist

SEE ALSO
	gettextize(1)



									 08/15/2005							 GLIB-GETTEXTIZE(1)
