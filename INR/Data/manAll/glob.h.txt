<glob.h>(P)							 POSIX Programmer's Manual							<glob.h>(P)



NAME
       glob.h - pathname pattern-matching types

SYNOPSIS
       #include <glob.h>

DESCRIPTION
       The <glob.h> header shall define the structures and symbolic constants used by the glob() function.

       The structure type glob_t shall contain at least the following members:


	      size_t   gl_pathc Count of paths matched by pattern.
	      char   **gl_pathv Pointer to a list of matched pathnames.
	      size_t   gl_offs	Slots to reserve at the beginning of gl_pathv.

       The following constants shall be provided as values for the flags argument:

       GLOB_APPEND
	      Append generated pathnames to those previously obtained.

       GLOB_DOOFFS
	      Specify how many null pointers to add to the beginning of gl_pathv.

       GLOB_ERR
	      Cause glob() to return on error.

       GLOB_MARK
	      Each pathname that is a directory that matches pattern has a slash appended.

       GLOB_NOCHECK
	      If pattern does not match any pathname, then return a list consisting of only pattern.

       GLOB_NOESCAPE
	      Disable backslash escaping.

       GLOB_NOSORT
	      Do not sort the pathnames returned.


       The following constants shall be defined as error return values:

       GLOB_ABORTED
	      The scan was stopped because GLOB_ERR was set or (*errfunc)() returned non-zero.

       GLOB_NOMATCH
	      The pattern does not match any existing pathname, and GLOB_NOCHECK was not set in flags.

       GLOB_NOSPACE
	      An attempt to allocate memory failed.

       GLOB_NOSYS
	      Reserved.


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int  glob(const char *restrict, int, int (*)(const char *, int),
		       glob_t *restrict);
	      void globfree (glob_t *);

       The implementation may define additional macros or constants using names beginning with GLOB_.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       The System Interfaces volume of IEEE Std 1003.1-2001, glob(), the Shell and Utilities volume of IEEE Std 1003.1-2001

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								<glob.h>(P)
