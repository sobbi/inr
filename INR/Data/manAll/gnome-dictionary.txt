gnome-dictionary(1)															gnome-dictionary(1)



NAME
       gnome-dictionary - Look up words on dictionaries

SYNOPSIS
       gnome-dictionary
       gnome-dictionary [options]

       or select Dictionary from the Accessories submenu of the Applications menu.

DESCRIPTION
       GNOME Dictionary provides dictionary definitions of words, using a dictionary source.

       For full documentation see the GNOME Dictionary online help.


OPTIONS
       --look-up word
	      Looks up the specified word using the pre-defined dictionary source.

       -s source or --source source
	      Uses the specified source for looking up words.  This does not affect the global settings.

       -l or --list-sources
	      Lists all the sources available.

       -n or --no-window
	      Using this switch with the --look-up will print the definitions found on the console without launching the GUI.

       --help Display help information.

CONFIGURATION
       All the configuration is handled using GConf.

AUTHOR
       GNOME  Dictionary  was  originally  written  by	Spiros Papadimitriou (<spapadim+@cs.cmu.edu>), Mike Hughes (<mfh@psilord.com>) and Bradford Hovinen
       (<hovinen@udel.edu>).

       Emmanuele Bassi (<ebassi@gmail.com>) rewrote it from scratch.

       This manual page was originally written by Jochen Voss <voss@mathematik.uni-kl.de>.


SEE ALSO
       dict(1), dictd(8), http://www.dict.org/, RFC 2229



gnome-utils 2.13.4							 Jan 2 2005							gnome-dictionary(1)
