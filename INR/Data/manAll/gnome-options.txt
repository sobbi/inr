gnome-options(7)						    Gnome User's Manual 						   gnome-options(7)



NAME
       gnome-options - Standard Command Line Options for GNOME 2 Programs

SYNOPSIS
       program [standard options] [specific options] arguments

DESCRIPTION
       This manual page describes the command line options, which are common to all Gnome applications.

OPTIONS
   Help options
       -?, --help
	      Show the application's help message.

       --usage
	      Display a brief usage message.

       --version
	      Show the application's version information.

   Bonobo Activation support
       --oaf-ior-fd=FD
	      File descriptor to print IOR on

       --oaf-activate-iid=IID
	      IID to activate

       --oaf-private
	      Prevent registering of server with OAF

   GNOME library options
       --disable-sound
	      Disable sound server usage

       --enable-sound
	      Enable sound server usage

       --espeaker=HOSTNAME:PORT
	      Host:port on which the esd sound server to use is running.  This is useful if you simultaneously run programs on several machines.

   GNOME GUI options
       --disable-crash-dialog
	      Disable the crash dialog, which is normally shown when the application segfaults.

   Session management options
       --sm-client-id=ID
	      Specify session management ID.  Only used by the session manager itself while restarting the application.

       --sm-config-prefix=PREFIX
	      Specify prefix of saved configuration.

       --sm-disable
	      Disable connection to session manager.

   Other options
       --load-modules=MODULE1,MODULE2,...
	      Dynamic modules to load

SEE ALSO
       gtk-options(7)

       For  most  GNOME programs there will be additional command line options, which are specific to the program.  These will be explained in the Applica‐
       tion options section of the --help output and in the application's online help.

AUTHOR
       This manual page was written by Jochen Voss <voss@debian.org>.  A first version was generated with the help of the help2man program.  The  program's
       output was manually edited later.



GNOME 2.2								 2003-10-20							   gnome-options(7)
