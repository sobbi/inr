gnomine(6)																	 gnomine(6)



NAME
       Mines - The popular logic puzzle minesweeper

SYNOPSIS
       gnomine [OPTION...]

DESCRIPTION
       gnomine is a puzzle game where you locate mines floating in an ocean using only your brain and a little bit of luck.

OPTIONS
       -x, --width=X
	      Width of grid

       -y, --height=Y
	      Height of grid

       -n, --mines=NUMBER
	      Number of mines

       -f, --size
	      Size of the board (0-2 = small-large, 3=custom)

       -a, --a=X
	      X location of window

       -b, --b=Y
	      Y location of window

       This program also accepts the standard GNOME and GTK options.

AUTHORS
       gnomine was written by Szekeres Istvan and others.

       This manual page was written by Sven Arvidsson <sa@whiz.se>, for the Debian project (but may be used by others).

SEE ALSO
       gtk-options(7), gnome-options(7)

       The online documentation available through the program's Help menu.



GNOME									 2007-06-09								 gnomine(6)
