GOBJECT-QUERY(1)						      [FIXME: manual]							   GOBJECT-QUERY(1)



NAME
       gobject-query - display a tree of types

SYNOPSIS
       gobject-query froots [options...]

       gobject-query tree [options...]

DESCRIPTION
       gobject-query is a small utility that draws a tree of types.

INVOCATION
       gobject-query takes a mandatory argument that specifies whether it should iterate over the fundamental types or print a type tree.

   Options
       froots
	   iterate over fundamental roots

       tree
	   print type tree

       -r type
	   specify the root type

       -n
	   don´t descend type tree

       -b string
	   specify indent string

       -i string
	   specify incremental indent string

       -s number
	   specify line spacing

       -h, --help
	   Print brief help and exit.

       -v, --version
	   Print version and exit.



[FIXME: source] 							 03/13/2009							   GOBJECT-QUERY(1)
