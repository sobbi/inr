GRUB(1) 							       User Commands								    GRUB(1)



NAME
       GRUB - manual page for GRUB version 1.98-1ubuntu7

SYNOPSIS
       grub-mkisofs [options] file...

OPTIONS
       -a, -all-files
	      Process all files (don't skip backup files)

       -abstract FILE
	      Set Abstract filename

       -A ID, -appid ID
	      Set Application ID

       -biblio FILE
	      Set Bibliographic filename

       -copyright FILE
	      Set Copyright filename

       -G FILE, --embedded-boot FILE
	      Set embedded boot image name

       --protective-msdos-label
	      Patch a protective DOS-style label in the image

       -b FILE, -eltorito-boot FILE
	      Set El Torito boot image name

       -c FILE, -eltorito-catalog FILE
	      Set El Torito boot catalog name

       -boot-info-table
	      Patch Boot Info Table in El Torito boot image

       -no-emul-boot
	      Dummy option for backward compatibility

       --eltorito-emul-floppy
	      Enable floppy drive emulation for El Torito

       -C PARAMS, -cdwrite-params PARAMS
	      Magic parameters from cdrecord

       -d, -omit-period
	      Omit trailing periods from filenames

       -D, -disable-deep-relocation
	      Disable deep directory relocation

       -f, -follow-links
	      Follow symbolic links

       -help  Print option help

       --help Print option help

       --version
	      Print version information and exit

       -hide GLOBFILE
	      Hide ISO9660/RR file

       -hide-joliet GLOBFILE
	      Hide Joliet file

       -i ADD_FILES
	      No longer supported

       -J, -joliet
	      Generate Joliet directory information

       -l, -full-iso9660-filenames Allow full 32 character filenames for iso9660 names

       -L, -allow-leading-dots
	      Allow iso9660 filenames to start with '.'

       -log-file LOG_FILE
	      Re-direct messages to LOG_FILE

       -m GLOBFILE, -exclude GLOBFILE
	      Exclude file name

       -M FILE, -prev-session FILE Set path to previous session to merge

       -N, -omit-version-number
	      Omit version number from iso9660 filename

       -no-split-symlink-components
	      Inhibit splitting symlink components

       -no-split-symlink-fields
	      Inhibit splitting symlink fields

       -o FILE, -output FILE
	      Set output file name

       -p PREP, -preparer PREP
	      Set Volume preparer

       -print-size
	      Print estimated filesystem size and exit

       -P PUB, -publisher PUB
	      Set Volume publisher

       -quiet Run quietly

       -r, -rational-rock
	      Generate rationalized Rock Ridge directory information

       -R, -rock
	      Generate Rock Ridge directory information

       -split-output
	      Split output into files of approx. 1GB size

       -sysid ID
	      Set System ID

       -T, -translation-table
	      Generate translation tables for systems that don't understand long filenames

       -v, -verbose
	      Verbose

       -V ID, -volid ID
	      Set Volume ID

       -volset ID
	      Set Volume set ID

       -volset-size #
	      Set Volume set size

       -volset-seqno #
	      Set Volume set sequence number

       -x FILE, -old-exclude FILE
	      Exclude file name (deprecated)

       --creation-date
	      Override creation date

       --modification-date
	      Override modification date

       --expiration-date
	      Override expiration date

       --effective-date
	      Override effective date



FSF									 July 2010								    GRUB(1)
