GRUB-MKRELPATH(1)						       User Commands							  GRUB-MKRELPATH(1)



NAME
       grub-mkrelpath - manual page for grub-mkrelpath (GRUB) 1.98-1ubuntu7

SYNOPSIS
       grub-mkrelpath [OPTIONS] PATH

DESCRIPTION
       Make a system path relative to it's root.

OPTIONS
       -h, --help
	      display this message and exit

       -V, --version
	      print version information and exit

REPORTING BUGS
       Report bugs to <bug-grub@gnu.org>.



FSF									 July 2010							  GRUB-MKRELPATH(1)
