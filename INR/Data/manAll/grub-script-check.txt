GRUB-SCRIPT-CHECK(1)						       User Commands						       GRUB-SCRIPT-CHECK(1)



NAME
       grub-script-check - manual page for grub-script-check (GRUB) 1.98-1ubuntu7

SYNOPSIS
       grub-script-check [PATH]

DESCRIPTION
       Checks GRUB script configuration file for syntax errors.

       -h, --help
	      display this message and exit

       -V, --version
	      print version information and exit

       -v, --verbose
	      print script being processed

REPORTING BUGS
       Report bugs to <bug-grub@gnu.org>.



FSF									 July 2010						       GRUB-SCRIPT-CHECK(1)
