rpc.gssd(8)																	rpc.gssd(8)



NAME
       rpc.gssd - rpcsec_gss daemon

SYNOPSIS
       rpc.gssd [-f] [-n] [-k keytab] [-p pipefsdir] [-v] [-r] [-d ccachedir]

DESCRIPTION
       The  rpcsec_gss	protocol  gives a means of using the gss-api generic security api to provide security for protocols using rpc (in particular, nfs).
       Before exchanging any rpc requests using rpcsec_gss, the rpc client must first establish a security context.  The linux kernel's  implementation  of
       rpcsec_gss  depends on the userspace daemon rpc.gssd to establish security contexts.  The rpc.gssd daemon uses files in the rpc_pipefs filesystem to
       communicate with the kernel.


OPTIONS
       -f     Runs rpc.gssd in the foreground and sends output to stderr (as opposed to syslogd)

       -n     By default, rpc.gssd treats accesses by the user with UID 0 specially, and uses "machine credentials" for all accesses  by  that	user  which
	      require  Kerberos  authentication.   With  the -n option, "machine credentials" will not be used for accesses by UID 0.  Instead, credentials
	      must be obtained manually like all other users.  Use of this option means that  "root"  must  manually  obtain  Kerberos	credentials  before
	      attempting to mount an nfs filesystem requiring Kerberos authentication.

       -k keytab
	      Tells rpc.gssd to use the keys found in keytab to obtain "machine credentials".  The default value is "/etc/krb5.keytab".

	      Previous	versions of rpc.gssd used only "nfs/*" keys found within the keytab.  To be more consistent with other implementations, we now look
	      for specific keytab entries.  The search order for keytabs to be used for "machine credentials" is now:
		root/<hostname>@<REALM>
		nfs/<hostname>@<REALM>
		host/<hostname>@<REALM>
		root/<anyname>@<REALM>
		nfs/<anyname>@<REALM>
		host/<anyname>@<REALM>

       -p path
	      Tells rpc.gssd where to look for the rpc_pipefs filesystem.  The default value is "/var/lib/nfs/rpc_pipefs".

       -d directory
	      Tells rpc.gssd where to look for Kerberos credential files.  The default value is "/tmp".  This can also be a colon separated list of  direc‐
	      tories to be searched for Kerberos credential files.  Note that if machine credentials are being stored in files, then the first directory on
	      this list is where the machine credentials are stored.

       -v     Increases the verbosity of the output (can be specified multiple times).

       -r     If the rpcsec_gss library supports setting debug level, increases the verbosity of the output (can be specified multiple times).

       -R realm
	      Kerberos tickets from this realm will be preferred when scanning available credentials cache files to  be  used  to  create  a  context.	 By
	      default, the default realm, as configured in the Kerberos configuration file, is preferred.

       -t timeout
	      Timeout,	in  seconds,  for  kernel gss contexts. This option allows you to force new kernel contexts to be negotiated after timeout seconds,
	      which allows changing Kerberos tickets and identities frequently.  The default is no explicit timeout, which means the  kernel  context  will
	      live the lifetime of the Kerberos service ticket used in its creation.

SEE ALSO
       rpc.svcgssd(8)

AUTHORS
       Dug Song <dugsong@umich.edu>
       Andy Adamson <andros@umich.edu>
       Marius Aamodt Eriksen <marius@umich.edu>
       J. Bruce Fields <bfields@umich.edu>



									14 Mar 2007								rpc.gssd(8)
