GStreamer(1)																       GStreamer(1)



NAME
       gst-visualise - Run a GStreamer pipeline to display an audio visualisation

SYNOPSIS
       gst-visualise [visualiser]

DESCRIPTION
       gst-visualise is a tool that is used to run a basic GStreamer pipeline, to display a graphical visualisation of an audio stream.

       By  default,  the audio stream is read from ESD (the Enlightened Sound Daemon), but this can be changed by setting the AUDIOSRC parameter in ~/.gst.
       For example, you might set "AUDIOSRC=osssrc" to display a visualisation of the sound input to your soundcard.

       You can select a visualiser by providing a parameter naming the visualiser.  For example:

	gst-visualise synaesthesia

       will use the synaesthesia plugin.  If no visualiser is named, the VISUALIZER property in ~/.gst will be used.  If this is not specified either,	the
       goom visualiser will be used.

       The videosink to use to display the visualisation will be read from the VIDEOSINK parameter in ~/.gst, defaulting to sdlvideosink.


SEE ALSO
       gst-launch-ext(1), gst-inspect(1), gst-launch(1),

AUTHOR
       The GStreamer team at http://gstreamer.net/



								       February 2002							       GStreamer(1)
