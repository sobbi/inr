GWIBBER-POSTER(1)		 User Manuals		     GWIBBER-POSTER(1)



NAME
       Gwibber - Microblogging client for GNOME

SYNOPSIS
       gwibber-poster [ OPTIONS ]

DESCRIPTION
       Gwibber is an open source microblogging client for GNOME developed with
       Python and  GTK.  It  supports  Twitter,  Jaiku,  Identi.ca,  Facebook,
       Flickr, Digg, and RSS.

OPTIONS
       -h, --help
	      Show help message and exit

       -p, --persist
	      Don't exit after posting

       -w, --window
	      Don't exit when the window loses focus, also implies -d

       --decorate
	      Display window decorations

       -m, --message
	      Message to post

       -d, --debug
	      Log debug messages

BUGS
       Please  report all bugs to the Gwibber bug tracker: http://bugs.launch‐
       pad.net/gwibber

AUTHOR
       Ken VanDine <ken.vandine@canonical.com>

       Copyright 2010.	Licensed under a Creative  Commons  Attribution-Share‐
       Alike License <http://creativecommons.org/licenses/by-sa/3.0/>



Linux				 JANUARY 2010		     GWIBBER-POSTER(1)
