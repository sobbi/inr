HAL-FIND-BY-PROPERTY(1) 													    HAL-FIND-BY-PROPERTY(1)



NAME
       hal-find-by-property - find device objects by property matching

SYNOPSIS
       hal-find-by-property [options]


DESCRIPTION
       hal-get-property finds device object in the HAL device database by looking at device properties. For more information about both the big picture and
       specific HAL properties, refer to the HAL spec which can be found in /usr/share/doc/hal-doc/spec/hal-spec.html depending on the distribution.


OPTIONS
       The following options are supported:

       --key  The name of the property.

       --string
	      String value of the property

       --verbose
	      Verbose output.

       --help Print out usage.

       --version
	      Print the version.


RETURN VALUE
       If devices matching the given property are found each UDI (Unique Device Identifier) is printed on stdout and the program exits with exit code 0. If
       no devices are found or an error occured, the program exits with a non-zero exit code.


BUGS
       Please  send  bug  reports  to either the distribution or the HAL mailing list, see http://lists.freedesktop.org/mailman/listinfo/hal on how to sub‐
       scribe.


SEE ALSO
       hald(8), lshal(1), hal-set-property(1), hal-get-property(1), hal-find-by-capability(1), dbus-send(1)


AUTHOR
       Written by David Zeuthen <david@fubar.dk> with a lot of help from many others.




																    HAL-FIND-BY-PROPERTY(1)
