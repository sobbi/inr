d2i_X509_CRL(3SSL)							  OpenSSL							 d2i_X509_CRL(3SSL)



NAME
       d2i_X509_CRL, i2d_X509_CRL, d2i_X509_CRL_bio, d2i_509_CRL_fp, i2d_X509_CRL_bio, i2d_X509_CRL_fp - PKCS#10 certificate request functions.

SYNOPSIS
	#include <openssl/x509.h>

	X509_CRL *d2i_X509_CRL(X509_CRL **a, const unsigned char **pp, long length);
	int i2d_X509_CRL(X509_CRL *a, unsigned char **pp);

	X509_CRL *d2i_X509_CRL_bio(BIO *bp, X509_CRL **x);
	X509_CRL *d2i_X509_CRL_fp(FILE *fp, X509_CRL **x);

	int i2d_X509_CRL_bio(X509_CRL *x, BIO *bp);
	int i2d_X509_CRL_fp(X509_CRL *x, FILE *fp);

DESCRIPTION
       These functions decode and encode an X509 CRL (certificate revocation list).

       Othewise the functions behave in a similar way to d2i_X509() and i2d_X509() described in the d2i_X509(3) manual page.

SEE ALSO
       d2i_X509(3)

HISTORY
       TBA



0.9.8k									 2005-07-13							 d2i_X509_CRL(3SSL)
