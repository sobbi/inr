IBUS(1) 																	    IBUS(1)



NAME
       ibus - Intelligent Input Bus for Linux / Unix OS


SYNOPSIS
       ibus


DESCRIPTION
       IBus is an Intelligent Input Bus. It is a new input framework for Linux OS. It provides full featured and user friendly input method user interface.
       It also may help developers to develop input method easily.


       ibus will launch ibus-daemon, ibus-gconf, ibus-ui-gtk and ibus-x11.


       Homepage: http://code.google.com/p/ibus/


SEE ALSO
       scim(1), fcitx(1)



									 2008-11-08								    IBUS(1)
