IBUS-SETUP(1)																      IBUS-SETUP(1)



NAME
       ibus-setup - configuration program for ibus


SYNOPSIS
       ibus-setup


DESCRIPTION
       IBus is an Intelligent Input Bus. It is a new input framework for Linux OS. It provides full featured and user friendly input method user interface.
       It also may help developers to develop input method easily.


       ibus-setup is the configuration program for IBus.


       Homepage: http://code.google.com/p/ibus/


SEE ALSO
       ibus(1)



									 2008-11-08							      IBUS-SETUP(1)
