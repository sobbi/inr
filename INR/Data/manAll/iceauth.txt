ICEAUTH(1)																	 ICEAUTH(1)



NAME
       iceauth - ICE authority file utility

SYNOPSIS
       iceauth [ -f authfile ] [ -vqib ] [ command arg ... ]

DESCRIPTION
       The  iceauth program is used to edit and display the authorization information used in connecting with ICE.  This program is usually used to extract
       authorization records from one machine and merge them in on another (as is the case when using remote logins or granting  access  to  other  users).
       Commands (described below) may be entered interactively, on the iceauth command line, or in scripts.

AUTHOR
       Ralph Mor, X Consortium



X Version 11							       iceauth 1.0.3								 ICEAUTH(1)
