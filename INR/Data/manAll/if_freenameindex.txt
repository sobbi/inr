IF_FREENAMEINDEX(P)						 POSIX Programmer's Manual						IF_FREENAMEINDEX(P)



NAME
       if_freenameindex - free memory allocated by if_nameindex

SYNOPSIS
       #include <net/if.h>

       void if_freenameindex(struct if_nameindex *ptr);


DESCRIPTION
       The  if_freenameindex()	function  shall  free  the  memory  allocated  by  if_nameindex(). The ptr argument shall be a pointer that was returned by
       if_nameindex(). After if_freenameindex() has been called, the application shall not use the array of which ptr is the address.

RETURN VALUE
       None.

ERRORS
       No errors are defined.

       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       getsockopt() , if_indextoname() , if_nameindex() , if_nametoindex() , setsockopt() , the Base Definitions volume of IEEE Std 1003.1-2001, <net/if.h>

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							IF_FREENAMEINDEX(P)
