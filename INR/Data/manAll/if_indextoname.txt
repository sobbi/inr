IF_INDEXTONAME(P)						 POSIX Programmer's Manual						  IF_INDEXTONAME(P)



NAME
       if_indextoname - map a network interface index to its corresponding name

SYNOPSIS
       #include <net/if.h>

       char *if_indextoname(unsigned ifindex, char *ifname);


DESCRIPTION
       The if_indextoname() function shall map an interface index to its corresponding name.

       When  this  function  is called, ifname shall point to a buffer of at least {IF_NAMESIZE} bytes. The function shall place in this buffer the name of
       the interface with index ifindex.

RETURN VALUE
       If ifindex is an interface index, then the function shall return the value supplied in ifname, which points to a buffer now containing the interface
       name. Otherwise, the function shall return a NULL pointer and set errno to indicate the error.

ERRORS
       The if_indextoname() function shall fail if:

       ENXIO  The interface does not exist.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       getsockopt()  ,	if_freenameindex()  ,  if_nameindex()  ,  if_nametoindex()  ,  setsockopt()  , the Base Definitions volume of IEEE Std 1003.1-2001,
       <net/if.h>

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							  IF_INDEXTONAME(P)
