ifup(8) 																	    ifup(8)



NAME
       ifup - bring a network interface up

       ifdown - take a network interface down

SYNOPSIS
       ifup [-nv] [--no-act] [--verbose] [-i FILE|--interfaces=FILE] [--allow CLASS] -a|IFACE...
       ifup -h|--help
       ifup -V|--version

       ifdown [-nv] [--no-act] [--verbose] [-i FILE|--interfaces=FILE] [--allow CLASS] -a|IFACE...

DESCRIPTION
       The  ifup and ifdown commands may be used to configure (or, respectively, deconfigure) network interfaces based on interface definitions in the file
       /etc/network/interfaces.

OPTIONS
       A summary of options is included below.

       -a, --all
	      If given to ifup, affect all interfaces marked auto.  Interfaces are brought up in the order in which they are defined in /etc/network/inter‐
	      faces.   If  given  to ifdown, affect all defined interfaces.  Interfaces are brought down in the order in which they are currently listed in
	      the state file. Only interfaces defined in /etc/network/interfaces will be brought down.

       --force
	      Force configuration or deconfiguration of the interface.

       -h, --help
	      Show summary of options.

       --allow=CLASS
	      Only allow interfaces listed in an allow-CLASS line in /etc/network/interfaces to be acted upon.

       -i FILE, --interfaces=FILE
	      Read interface definitions from FILE instead of from /etc/network/interfaces.

       -n, --no-act
	      Don't configure any interfaces or run any "up" or "down" commands.

       --no-mappings
	      Don't run any mappings.  See interfaces(5) for more information about the mapping feature.

       -V, --version
	      Show copyright and version information.

       -v, --verbose
	      Show commands as they are executed.

EXAMPLES
       ifup -a
	      Bring up all the interfaces defined with auto in /etc/network/interfaces

       ifup eth0
	      Bring up interface eth0

       ifup eth0=home
	      Bring up interface eth0 as logical interface home

       ifdown -a
	      Bring down all interfaces that are currently up.

NOTES
       ifup and ifdown are actually the same program called by different names.

       The program does not configure network interfaces directly; it runs low level utilities such as ifconfig and route to do its dirty work.

FILES
       /etc/network/interfaces
	      definitions of network interfaces See interfaces(5) for more information.

       /var/run/network/ifstate
	      current state of network interfaces

KNOWN BUGS/LIMITATIONS
       The program keeps records of whether network interfaces are up or down.	Under exceptional circumstances these records can become inconsistent  with
       the  real  states  of  the interfaces.  For example, an interface that was brought up using ifup and later deconfigured using ifconfig will still be
       recorded as up.	To fix this you can use the --force option to force ifup or ifdown to run configuration or deconfiguration commands despite what it
       considers the current state of the interface to be.

       The  file  /var/run/network/ifstate must be writable for ifup or ifdown to work properly.  On Ubuntu the /var/run location is a temporary filesystem
       which is always writable and thrown away on shutdown.  You can also use the --force option to run configuration or deconfiguration commands  without
       updating the file.

       Note  that  the	program does not run automatically: ifup alone does not bring up interfaces that appear as a result of hardware being installed and
       ifdown alone does not bring down interfaces that disappear as a result of hardware being removed.  To automate the configuration of  network  inter‐
       faces you need to install other packages such as hotplug(8) or ifplugd(8).

AUTHOR
       The ifupdown suite was written by Anthony Towns <aj@azure.humbug.org.au>.

SEE ALSO
       interfaces(5), ifconfig(8).



IFUPDOWN								22 May 2004								    ifup(8)
