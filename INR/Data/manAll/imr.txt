IMR(1)									IMR(1)



NAME
       imr - MICO implementation repository tool

SYNOPSIS
       imr list
       imr info ...
       imr create ...
       imr delete ...
       imr activate ...

DESCRIPTION
       imr  is	used  to list all entries in the implementation repository, to
       show detailed information for one entry, to create a new entry, and  to
       delete an entry.

OPERATION MODES
   Listing All Entries
       imr list
	      will  print  a listing of the names of all entries in the imple‐
	      mentation repository.

   Details For One Entry
       imr info <name>
	      will show you detailed information for the entry named <name>.

   Creating New Entries
       imr create <name> <mode> <command> <repoid1> ...
	      will create a new entry with name <name>.  <mode> is one of per‐
	      sistent, shared, unshared, permethod, library.  <command> is the
	      shell command that should be used to start  the  server  or  the
	      file  name  of  a  loadable  module for library activation mode.
	      Note that all paths have to be absolute  since  micod's  current
	      directory  is  probably  different  from your current directory.
	      Furthermore you have to make sure that the server is located  on
	      the  same  machine  as  micod,  otherwise  you  have to use rsh.
	      <repoid1>, <repoid2> and so on are the repository  ids  for  the
	      IDL interfaces implemented by the server.

   Deleting Entries
       imr delete <name>
	      will delete the entry named <name>.

   Activation implementation
       imr activate <name> [<micod-address>]
	      will force activation of the implementation <name>.

FILES
       ~/.micorc

SEE ALSO
       MICO  Reference	Manual,  rsh(1),  micod(8), imr(1), ird(8), micorc(5),
       idl(1)

COPYRIGHT
       Copyright (C) 1997, Kay Roemer & Arno Puder

AUTHOR
       Kay Roemer & Arno Puder




				 April 8 1997				IMR(1)
