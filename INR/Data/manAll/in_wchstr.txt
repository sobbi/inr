in_wchstr(3NCURSES)															in_wchstr(3NCURSES)



NAME
       in_wchstr, in_wchnstr, win_wchstr, win_wchnstr, mvin_wchstr, mvin_wchnstr, mvwin_wchstr, mvwin_wchnstr - get an array of complex characters and
       renditions from a curses window

SYNOPSIS
       #include <curses.h>

       int in_wchstr(cchar_t *wchstr);
       int in_wchnstr(cchar_t *wchstr, int n);
       int win_wchstr(WINDOW *win, cchar_t *wchstr);
       int win_wchnstr(WINDOW *win, cchar_t *wchstr, int n);
       int mvin_wchstr(int y, int x, cchar_t *wchstr);
       int mvin_wchnstr(int y, int x, cchar_t *wchstr, int n);
       int mvwin_wchstr(WINDOW *win, int y, int x, cchar_t *wchstr);
       int mvwin_wchnstr(WINDOW *win, int y, int x, cchar_t *wchstr, int n);

DESCRIPTION
       These functions return an array of complex characters in wchstr, starting at the current cursor position in the named  window.	Attributes  (rendi‐
       tion) are stored with the characters.

       The in_wchnstr, mvin_wchnstr, mvwin_wchnstr and win_wchnstr fill the array with at most n cchar_t elements.

NOTES
       Note that all routines except win_wchnstr may be macros.

       Reading	a  line  that  overflows  the  array pointed to by wchstr with in_wchstr, mvin_wchstr, mvwin_wchstr or win_wchstr causes undefined results.
       Therefore, the use of in_wchnstr, mvin_wchnstr, mvwin_wchnstr, or win_wchnstr is recommended.

RETURN VALUES
       Upon successful completion, these functions return OK.  Otherwise, they return ERR.

PORTABILITY
       The XSI Curses defines no error conditions.  This implementation checks for null pointers, returning ERR in that case.

SEE ALSO
       Functions: ncurses(3NCURSES), in_wch(3NCURSES), instr(3NCURSES), inwstr(3NCURSES) inchstr(3NCURSES)



																	in_wchstr(3NCURSES)
