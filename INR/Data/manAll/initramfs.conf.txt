INITRAMFS.CONF(5)						   initramfs.conf manual						  INITRAMFS.CONF(5)



NAME
       initramfs.conf - configuration file for mkinitramfs


DESCRIPTION
       The behaviour of mkinitramfs can be modified by its configuration file.

       Each line in the file can be a configuration variable, a blank line, or a comment. The value of an variable is assigned by an statement of the form:
       name=[value]

       Configuration options can be broken out into configuration snippets and placed in individual files in the /etc/mkinitramfs/conf.d directory.   Files
       in  this  directory  are  always read after the main configuration file, so you can override the settings in the main config file without editing it
       directly.


GENERAL VARIABLES
	MODULES
	      Specifies the modules for the initramfs image.  The default setting is most.

	      most adds all the framebuffer, acpi, file system, ide, sata, scsi and usb drivers.

	      dep tries to guess which modules are necessary for the running box.

	      netboot adds the base modules, network modules, but skips block devices.

	      list includes only modules from the additional modules list to load them early.


	BUSYBOX
	      Include busybox utilities for the boot scripts.  If set to 'n' mkinitramfs will build an initramfs without busybox.  Beware  that  many  boot
	      scripts need busybox utilities.


	KEYMAP
	      If  set to 'y', the console keymap will be loaded during the initramfs stage.  The keymap will anyway be loaded by the initscripts later, and
	      the packages that might need input will normally set this variable automatically, so there should normally be no need to set this.


	COMPCACHE_SIZE

	      Amount of RAM to use for RAM-based compressed swap space.  The default is not to use compcache.

	      An empty value - compcache isn't used, or added to the initramfs at all.

	      An integer and K (e.g. 65536 K) - use a number of kilobytes.

	      An integer and M (e.g. 256 M) - use a number of megabytes.

	      An integer and G (e.g. 1 G) - use a number of gigabytes.

	      An integer and % (e.g. 50 %) - use a percentage of the amount of RAM.

	      You can optionally install the compcache package to configure this setting via debconf and have userspace scripts to load  and  unload  comp‐
	      cache.


NFS VARIABLES
	BOOT  Allows to use an nfs drive as the root of the drive.  The default is to boot from local media (hard drive, USB stick).  Set to nfs for an NFS
	      root share.


	DEVICE
	      Specifies the network interface, like eth0.


	ROOT  Allows optional root bootarg hardcoding, when no root bootarg can be passed.  A root bootarg overrides that special setting.


	NFSROOT
	      Defaults to auto in order to pick up value from DHCP server.  Otherwise you need to specify HOST:MOUNT.



AUTHOR
       The initramfs-tools are written by Maximilian Attems <maks@debian.org>, Jeff Bailey  <jbailey@raspberryginger.com>  and	numerous  others.   Loosely
       based on mkinitrd.conf by Herbert Xu.


SEE ALSO
	initramfs-tools(8), mkinitramfs(8), update-initramfs(8).



								    $Date: 2007/01/01 $ 						  INITRAMFS.CONF(5)
