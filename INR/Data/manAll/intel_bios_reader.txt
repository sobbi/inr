intel_bios_reader(1)														       intel_bios_reader(1)



NAME
       intel_bios_reader - Parses an Intel BIOS and displays many of its tables

SYNOPSIS
       intel_bios_reader filename

DESCRIPTION
       intel_bios_reader  is  a  tool  to  parse the contents of an Intel video BIOS file.  The file can come from intel_bios_dumper.  This can be used for
       quick debugging of video bios table handling, which is harder when done inside of the kernel graphics driver.

SEE ALSO
       intel_bios_dumper(1)



								   intel_bios_dumper 1.0					       intel_bios_reader(1)
