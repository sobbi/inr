IO_DESTROY(2)							 Linux Programmer's Manual						      IO_DESTROY(2)



NAME
       io_destroy - destroy an asynchronous I/O context

SYNOPSIS
       #include <libaio.h>

       int io_destroy(aio_context_t ctx);

       Link with -laio.

DESCRIPTION
       io_destroy()  removes the asynchronous I/O context from the list of I/O contexts and then destroys it.  io_destroy() can also cancel any outstanding
       asynchronous I/O actions on ctx and block on completion.

RETURN VALUE
       On success, io_destroy() returns 0.  For the failure return, see NOTES.

ERRORS
       EFAULT The context pointed to is invalid.

       EINVAL The AIO context specified by ctx is invalid.

       ENOSYS io_destroy() is not implemented on this architecture.

VERSIONS
       The asynchronous I/O system calls first appeared in Linux 2.5, August 2002.

CONFORMING TO
       io_destroy() is Linux-specific and should not be used in programs that are intended to be portable.

NOTES
       Glibc does not provide a wrapper function for this system call.

       The wrapper provided in libaio for io_destroy() does not follow the usual C library conventions for indicating error: on error it returns a  negated
       error  number (the negative of one of the values listed in ERRORS).  If the system call is invoked via syscall(2), then the return value follows the
       usual conventions for indicating an error: -1, with errno set to a (positive) value that indicates the error.

SEE ALSO
       io_cancel(2), io_getevents(2), io_setup(2), io_submit(2)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2008-06-18							      IO_DESTROY(2)
