ISO_8859-11(7)							 Linux Programmer's Manual						     ISO_8859-11(7)



NAME
       iso_8859-11 - the ISO 8859-11 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO 8859 standard includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-11 encodes the characters
       used in the Thai language.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-11 Characters
       The following table displays the characters in ISO 8859-11, which are printable and unlisted in the ascii(7) manual page.  The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-11.

       Oct   Dec   Hex	 Char	Description
       ─────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       241   161   a1	  ก	THAI CHARACTER KO KAI
       242   162   a2	  ข	THAI CHARACTER KHO KHAI
       243   163   a3	  ฃ	THAI CHARACTER KHO KHUAT
       244   164   a4	  ค	THAI CHARACTER KHO KHWAI
       245   165   a5	  ฅ	THAI CHARACTER KHO KHON
       246   166   a6	  ฆ	THAI CHARACTER KHO RAKHANG
       247   167   a7	  ง	THAI CHARACTER NGO NGU
       250   168   a8	  จ	THAI CHARACTER CHO CHAN
       251   169   a9	  ฉ	THAI CHARACTER CHO CHING
       252   170   aa	  ช	THAI CHARACTER CHO CHANG
       253   171   ab	  ซ	THAI CHARACTER SO SO
       254   172   ac	  ฌ	THAI CHARACTER CHO CHOE
       255   173   ad	  ญ	THAI CHARACTER YO YING
       256   174   ae	  ฎ	THAI CHARACTER DO CHADA
       257   175   af	  ฏ	THAI CHARACTER TO PATAK
       260   176   b0	  ฐ	THAI CHARACTER THO THAN
       261   177   b1	  ฑ	THAI CHARACTER THO NANGMONTHO
       262   178   b2	  ฒ	THAI CHARACTER THO PHUTHAO
       263   179   b3	  ณ	THAI CHARACTER NO NEN
       264   180   b4	  ด	THAI CHARACTER DO DEK
       265   181   b5	  ต	THAI CHARACTER TO TAO
       266   182   b6	  ถ	THAI CHARACTER THO THUNG
       267   183   b7	  ท	THAI CHARACTER THO THAHAN
       270   184   b8	  ธ	THAI CHARACTER THO THONG
       271   185   b9	  น	THAI CHARACTER NO NU
       272   186   ba	  บ	THAI CHARACTER BO BAIMAI
       273   187   bb	  ป	THAI CHARACTER PO PLA
       274   188   bc	  ผ	THAI CHARACTER PHO PHUNG

       275   189   bd	  ฝ	THAI CHARACTER FO FA
       276   190   be	  พ	THAI CHARACTER PHO PHAN
       277   191   bf	  ฟ	THAI CHARACTER FO FAN
       300   192   c0	  ภ	THAI CHARACTER PHO SAMPHAO
       301   193   c1	  ม	THAI CHARACTER MO MA
       302   194   c2	  ย	THAI CHARACTER YO YAK
       303   195   c3	  ร	THAI CHARACTER RO RUA
       304   196   c4	  ฤ	THAI CHARACTER RU
       305   197   c5	  ล	THAI CHARACTER LO LING
       306   198   c6	  ฦ	THAI CHARACTER LU
       307   199   c7	  ว	THAI CHARACTER WO WAEN
       310   200   c8	  ศ	THAI CHARACTER SO SALA
       311   201   c9	  ษ	THAI CHARACTER SO RUSI
       312   202   ca	  ส	THAI CHARACTER SO SUA
       313   203   cb	  ห	THAI CHARACTER HO HIP
       314   204   cc	  ฬ	THAI CHARACTER LO CHULA
       315   205   cd	  อ	THAI CHARACTER O ANG
       316   206   ce	  ฮ	THAI CHARACTER HO NOKHUK
       317   207   cf	  ฯ	THAI CHARACTER PAIYANNOI
       320   208   d0	  ะ	THAI CHARACTER SARA A
       321   209   d1	  ั     THAI CHARACTER MAI HAN-AKAT
       322   210   d2	  า	THAI CHARACTER SARA AA
       323   211   d3	  ำ	THAI CHARACTER SARA AM
       324   212   d4	  ิ     THAI CHARACTER SARA I
       325   213   d5	  ี     THAI CHARACTER SARA II
       326   214   d6	  ึ     THAI CHARACTER SARA UE
       327   215   d7	  ื     THAI CHARACTER SARA UEE
       330   216   d8	  ุ     THAI CHARACTER SARA U
       331   217   d9	  ู     THAI CHARACTER SARA UU
       332   218   da	  ฺ     THAI CHARACTER PHINTHU
       337   222   df	  ฿	THAI CURRENCY SYMBOL BAHT
       340   223   e0	  เ	THAI CHARACTER SARA E
       341   224   e1	  แ	THAI CHARACTER SARA AE
       342   225   e2	  โ	THAI CHARACTER SARA O
       343   226   e3	  ใ	THAI CHARACTER SARA AI MAIMUAN
       344   227   e4	  ไ	THAI CHARACTER SARA AI MAIMALAI
       345   228   e5	  ๅ	THAI CHARACTER LAKKHANGYAO
       346   229   e6	  ๆ	THAI CHARACTER MAIYAMOK
       347   230   e7	  ็     THAI CHARACTER MAITAIKHU
       350   231   e8	  ่     THAI CHARACTER MAI EK
       351   232   e9	  ้     THAI CHARACTER MAI THO
       352   233   ea	  ๊     THAI CHARACTER MAI TRI
       353   234   eb	  ๋     THAI CHARACTER MAI CHATTAWA
       354   235   ec	  ์     THAI CHARACTER THANTHAKHAT
       355   236   ed	  ํ     THAI CHARACTER NIKHAHIT
       356   237   ee	  ๎     THAI CHARACTER YAMAKKAN
       357   238   ef	  ๏	THAI CHARACTER FONGMAN
       360   239   f0	  ๐	THAI DIGIT ZERO
       361   240   f1	  ๑	THAI DIGIT ONE
       362   241   f2	  ๒	THAI DIGIT TWO
       363   242   f3	  ๓	THAI DIGIT THREE
       364   243   f4	  ๔	THAI DIGIT FOUR
       365   244   f5	  ๕	THAI DIGIT FIVE
       366   245   f6	  ๖	THAI DIGIT SIX
       367   246   f7	  ๗	THAI DIGIT SEVEN
       370   247   f8	  ๘	THAI DIGIT EIGHT
       371   248   f9	  ๙	THAI DIGIT NINE
       372   249   fa	  ๚	THAI CHARACTER ANGKHANKHU
       373   250   fb	  ๛	THAI CHARACTER KHOMUT

NOTES
       ISO  8859-11 is the same as TIS (Thai Industrial Standard) 620-2253, commonly known as TIS-620, except for the character in position a0: ISO 8859-11
       defines this as "non-breaking space", while TIS 620 leaves it undefined.

SEE ALSO
       ascii(7)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-28							     ISO_8859-11(7)
