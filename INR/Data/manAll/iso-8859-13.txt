ISO_8859-13(7)							 Linux Programmer's Manual						     ISO_8859-13(7)



NAME
       iso_8859-13 - the ISO 8859-13 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO 8859 standard includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-13 encodes the characters
       used in Baltic Rim languages.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-13 Characters
       The following table displays the characters in ISO 8859-13, which are printable and unlisted in the ascii(7) manual page.  The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-13.

       Oct   Dec   Hex	 Char	Description
       ────────────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       241   161   a1	  ‘	RIGHT DOUBLE QUOTATION MARK
       242   162   a2	  ’	CENT SIGN
       243   163   a3	  £	POUND SIGN
       244   164   a4	  €	CURRENCY SIGN
       245   165   a5	  ₯	DOUBLE LOW-9 QUOTATION MARK
       246   166   a6	  ¦	BROKEN BAR
       247   167   a7	  §	SECTION SIGN
       250   168   a8	  ¨	LATIN CAPITAL LETTER O WITH STROKE
       251   169   a9	  ©	COPYRIGHT SIGN
       252   170   aa	  ͺ	LATIN CAPITAL LETTER R WITH CEDILLA
       253   171   ab	  «	LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
       254   172   ac	  ¬	NOT SIGN
       255   173   ad		SOFT HYPHEN
       256   174   ae		REGISTERED SIGN
       257   175   af	  ―	LATIN CAPITAL LETTER AE
       260   176   b0	  °	DEGREE SIGN
       261   177   b1	  ±	PLUS-MINUS SIGN
       262   178   b2	  ²	SUPERSCRIPT TWO
       263   179   b3	  ³	SUPERSCRIPT THREE
       264   180   b4	  ΄	LEFT DOUBLE QUOTATION MARK
       265   181   b5	  ΅	MICRO SIGN
       266   182   b6	  Ά	PILCROW SIGN
       267   183   b7	  ·	MIDDLE DOT
       270   184   b8	  Έ	LATIN SMALL LETTER O WITH STROKE
       271   185   b9	  Ή	SUPERSCRIPT ONE
       272   186   ba	  Ί	LATIN SMALL LETTER R WITH CEDILLA
       273   187   bb	  »	RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
       274   188   bc	  Ό	VULGAR FRACTION ONE QUARTER

       275   189   bd	  ½	VULGAR FRACTION ONE HALF
       276   190   be	  Ύ	VULGAR FRACTION THREE QUARTERS
       277   191   bf	  Ώ	LATIN SMALL LETTER AE
       300   192   c0	  ΐ	LATIN CAPITAL LETTER A WITH OGONEK
       301   193   c1	  Α	LATIN CAPITAL LETTER I WITH OGONEK
       302   194   c2	  Β	LATIN CAPITAL LETTER A WITH MACRON
       303   195   c3	  Γ	LATIN CAPITAL LETTER C WITH ACUTE
       304   196   c4	  Δ	LATIN CAPITAL LETTER A WITH DIAERESIS
       305   197   c5	  Ε	LATIN CAPITAL LETTER A WITH RING ABOVE
       306   198   c6	  Ζ	LATIN CAPITAL LETTER E WITH OGONEK
       307   199   c7	  Η	LATIN CAPITAL LETTER E WITH MACRON
       310   200   c8	  Θ	LATIN CAPITAL LETTER C WITH CARON
       311   201   c9	  Ι	LATIN CAPITAL LETTER E WITH ACUTE
       312   202   ca	  Κ	LATIN CAPITAL LETTER Z WITH ACUTE
       313   203   cb	  Λ	LATIN CAPITAL LETTER E WITH DOT ABOVE
       314   204   cc	  Μ	LATIN CAPITAL LETTER G WITH CEDILLA
       315   205   cd	  Ν	LATIN CAPITAL LETTER K WITH CEDILLA
       316   206   ce	  Ξ	LATIN CAPITAL LETTER I WITH MACRON
       317   207   cf	  Ο	LATIN CAPITAL LETTER L WITH CEDILLA
       320   208   d0	  Π	LATIN CAPITAL LETTER S WITH CARON
       321   209   d1	  Ρ	LATIN CAPITAL LETTER N WITH ACUTE
       322   210   d2		LATIN CAPITAL LETTER N WITH CEDILLA
       323   211   d3	  Σ	LATIN CAPITAL LETTER O WITH ACUTE
       324   212   d4	  Τ	LATIN CAPITAL LETTER O WITH MACRON
       325   213   d5	  Υ	LATIN CAPITAL LETTER O WITH TILDE
       326   214   d6	  Φ	LATIN CAPITAL LETTER O WITH DIAERESIS
       327   215   d7	  Χ	MULTIPLICATION SIGN
       330   216   d8	  Ψ	LATIN CAPITAL LETTER U WITH OGONEK
       331   217   d9	  Ω	LATIN CAPITAL LETTER L WITH STROKE
       332   218   da	  Ϊ	LATIN CAPITAL LETTER S WITH ACUTE
       333   219   db	  Ϋ	LATIN CAPITAL LETTER U WITH MACRON
       334   219   dc	  ά	LATIN CAPITAL LETTER U WITH DIAERESIS
       335   220   dd	  έ	LATIN CAPITAL LETTER Z WITH DOT ABOVE
       336   221   de	  ή	LATIN CAPITAL LETTER Z WITH CARON
       337   222   df	  ί	LATIN SMALL LETTER SHARP S
       340   223   e0	  ΰ	LATIN SMALL LETTER A WITH OGONEK
       341   224   e1	  α	LATIN SMALL LETTER I WITH OGONEK
       342   225   e2	  β	LATIN SMALL LETTER A WITH MACRON
       343   226   e3	  γ	LATIN SMALL LETTER C WITH ACUTE
       344   227   e4	  δ	LATIN SMALL LETTER A WITH DIAERESIS
       345   228   e5	  ε	LATIN SMALL LETTER A WITH RING ABOVE
       346   229   e6	  ζ	LATIN SMALL LETTER E WITH OGONEK
       347   230   e7	  η	LATIN SMALL LETTER E WITH MACRON
       350   231   e8	  θ	LATIN SMALL LETTER C WITH CARON
       351   232   e9	  ι	LATIN SMALL LETTER E WITH ACUTE
       352   233   ea	  κ	LATIN SMALL LETTER Z WITH ACUTE
       353   234   eb	  λ	LATIN SMALL LETTER E WITH DOT ABOVE
       354   235   ec	  μ	LATIN SMALL LETTER G WITH CEDILLA
       355   236   ed	  ν	LATIN SMALL LETTER K WITH CEDILLA
       356   237   ee	  ξ	LATIN SMALL LETTER I WITH MACRON
       357   238   ef	  ο	LATIN SMALL LETTER L WITH CEDILLA
       360   239   f0	  π	LATIN SMALL LETTER S WITH CARON
       361   240   f1	  ρ	LATIN SMALL LETTER N WITH ACUTE
       362   241   f2	  ς	LATIN SMALL LETTER N WITH CEDILLA
       363   242   f3	  σ	LATIN SMALL LETTER O WITH ACUTE
       364   243   f4	  τ	LATIN SMALL LETTER O WITH MACRON
       365   244   f5	  υ	LATIN SMALL LETTER O WITH TILDE
       366   245   f6	  ϕ	LATIN SMALL LETTER O WITH DIAERESIS
       367   246   f7	  χ	DIVISION SIGN
       370   247   f8	  ψ	LATIN SMALL LETTER U WITH OGONEK
       371   248   f9	  ω	LATIN SMALL LETTER L WITH STROKE
       372   249   fa	  ϊ	LATIN SMALL LETTER S WITH ACUTE
       373   250   fb	  ϋ	LATIN SMALL LETTER U WITH MACRON
       374   251   fc	  ό	LATIN SMALL LETTER U WITH DIAERESIS
       375   252   fd	  ύ	LATIN SMALL LETTER Z WITH DOT ABOVE

       376   253   fe	  ώ	LATIN SMALL LETTER Z WITH CARON
       377   254   ff		RIGHT SINGLE QUOTATION MARK

NOTES
       ISO 8859-13 is also known as Latin-7.

SEE ALSO
       ascii(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-15							     ISO_8859-13(7)
