ISO_8859-14(7)							 Linux Programmer's Manual						     ISO_8859-14(7)



NAME
       iso_8859-14 - the ISO 8859-14 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO 8859 standard includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-14 encodes the characters
       used in Celtic languages.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-14 Characters
       The following table displays the characters in ISO 8859-14, which are printable and unlisted in the ascii(7) manual page.  The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-14.

       Oct   Dec   Hex	 Char	Description
       ────────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       241   161   a1	  Ḃ	LATIN CAPITAL LETTER B WITH DOT ABOVE
       242   162   a2	  ḃ	LATIN SMALL LETTER B WITH DOT ABOVE
       243   163   a3	  £	POUND SIGN
       244   164   a4	  Ċ	LATIN CAPITAL LETTER C WITH DOT ABOVE
       245   165   a5	  ċ	LATIN SMALL LETTER C WITH DOT ABOVE
       246   166   a6	  Ḋ	LATIN CAPITAL LETTER D WITH DOT ABOVE
       247   167   a7	  §	SECTION SIGN
       250   168   a8	  Ẁ	LATIN CAPITAL LETTER W WITH GRAVE
       251   169   a9	  ©	COPYRIGHT SIGN
       252   170   aa	  Ẃ	LATIN CAPITAL LETTER W WITH ACUTE
       253   171   ab	  ḋ	LATIN SMALL LETTER D WITH DOT ABOVE
       254   172   ac	  Ỳ	LATIN CAPITAL LETTER Y WITH GRAVE
       255   173   ad		SOFT HYPHEN
       256   174   ae	  ®	REGISTERED SIGN
       257   175   af	  Ÿ	LATIN CAPITAL LETTER Y WITH DIAERESIS
       260   176   b0	  Ḟ	LATIN CAPITAL LETTER F WITH DOT ABOVE
       261   177   b1	  ḟ	LATIN SMALL LETTER F WITH DOT ABOVE
       262   178   b2	  Ġ	LATIN CAPITAL LETTER G WITH DOT ABOVE
       263   179   b3	  ġ	LATIN SMALL LETTER G WITH DOT ABOVE
       264   180   b4	  Ṁ	LATIN CAPITAL LETTER M WITH DOT ABOVE
       265   181   b5	  ṁ	LATIN SMALL LETTER M WITH DOT ABOVE
       266   182   b6	  ¶	PILCROW SIGN
       267   183   b7	  Ṗ	LATIN CAPITAL LETTER P WITH DOT ABOVE
       270   184   b8	  ẁ	LATIN SMALL LETTER W WITH GRAVE
       271   185   b9	  ṗ	LATIN SMALL LETTER P WITH DOT ABOVE
       272   186   ba	  ẃ	LATIN SMALL LETTER W WITH ACUTE
       273   187   bb	  Ṡ	LATIN CAPITAL LETTER S WITH DOT ABOVE
       274   188   bc	  ỳ	LATIN SMALL LETTER Y WITH GRAVE

       275   189   bd	  Ẅ	LATIN CAPITAL LETTER W WITH DIAERESIS
       276   190   be	  ẅ	LATIN SMALL LETTER W WITH DIAERESIS
       277   191   bf	  ṡ	LATIN SMALL LETTER S WITH DOT ABOVE
       300   192   c0	  À	LATIN CAPITAL LETTER A WITH GRAVE
       301   193   c1	  Á	LATIN CAPITAL LETTER A WITH ACUTE
       302   194   c2	  Â	LATIN CAPITAL LETTER A WITH CIRCUMFLEX
       303   195   c3	  Ã	LATIN CAPITAL LETTER A WITH TILDE
       304   196   c4	  Ä	LATIN CAPITAL LETTER A WITH DIAERESIS
       305   197   c5	  Å	LATIN CAPITAL LETTER A WITH RING ABOVE
       306   198   c6	  Æ	LATIN CAPITAL LETTER AE
       307   199   c7	  Ç	LATIN CAPITAL LETTER C WITH CEDILLA
       310   200   c8	  È	LATIN CAPITAL LETTER E WITH GRAVE
       311   201   c9	  É	LATIN CAPITAL LETTER E WITH ACUTE
       312   202   ca	  Ê	LATIN CAPITAL LETTER E WITH CIRCUMFLEX
       313   203   cb	  Ë	LATIN CAPITAL LETTER E WITH DIAERESIS
       314   204   cc	  Ì	LATIN CAPITAL LETTER I WITH GRAVE
       315   205   cd	  Í	LATIN CAPITAL LETTER I WITH ACUTE
       316   206   ce	  Î	LATIN CAPITAL LETTER I WITH CIRCUMFLEX
       317   207   cf	  Ï	LATIN CAPITAL LETTER I WITH DIAERESIS
       320   208   d0	  Ŵ	LATIN CAPITAL LETTER W WITH CIRCUMFLEX
       321   209   d1	  Ñ	LATIN CAPITAL LETTER N WITH TILDE
       322   210   d2	  Ò	LATIN CAPITAL LETTER O WITH GRAVE
       323   211   d3	  Ó	LATIN CAPITAL LETTER O WITH ACUTE
       324   212   d4	  Ô	LATIN CAPITAL LETTER O WITH CIRCUMFLEX
       325   213   d5	  Õ	LATIN CAPITAL LETTER O WITH TILDE
       326   214   d6	  Ö	LATIN CAPITAL LETTER O WITH DIAERESIS
       327   215   d7	  Ṫ	LATIN CAPITAL LETTER T WITH DOT ABOVE
       330   216   d8	  Ø	LATIN CAPITAL LETTER O WITH STROKE
       331   217   d9	  Ù	LATIN CAPITAL LETTER U WITH GRAVE
       332   218   da	  Ú	LATIN CAPITAL LETTER U WITH ACUTE
       333   219   db	  Û	LATIN CAPITAL LETTER U WITH CIRCUMFLEX
       334   219   dc	  Ü	LATIN CAPITAL LETTER U WITH DIAERESIS
       335   220   dd	  Ý	LATIN CAPITAL LETTER Y WITH ACUTE
       336   221   de	  Ŷ	LATIN CAPITAL LETTER Y WITH CIRCUMFLEX
       337   222   df	  ß	LATIN SMALL LETTER SHARP S
       340   223   e0	  à	LATIN SMALL LETTER A WITH GRAVE
       341   224   e1	  á	LATIN SMALL LETTER A WITH ACUTE
       342   225   e2	  â	LATIN SMALL LETTER A WITH CIRCUMFLEX
       343   226   e3	  ã	LATIN SMALL LETTER A WITH TILDE
       344   227   e4	  ä	LATIN SMALL LETTER A WITH DIAERESIS
       345   228   e5	  å	LATIN SMALL LETTER A WITH RING ABOVE
       346   229   e6	  æ	LATIN SMALL LETTER AE
       347   230   e7	  ç	LATIN SMALL LETTER C WITH CEDILLA
       350   231   e8	  è	LATIN SMALL LETTER E WITH GRAVE
       351   232   e9	  é	LATIN SMALL LETTER E WITH ACUTE
       352   233   ea	  ê	LATIN SMALL LETTER E WITH CIRCUMFLEX
       353   234   eb	  ë	LATIN SMALL LETTER E WITH DIAERESIS
       354   235   ec	  ì	LATIN SMALL LETTER I WITH GRAVE
       355   236   ed	  í	LATIN SMALL LETTER I WITH ACUTE
       356   237   ee	  î	LATIN SMALL LETTER I WITH CIRCUMFLEX
       357   238   ef	  ï	LATIN SMALL LETTER I WITH DIAERESIS
       360   239   f0	  ŵ	LATIN SMALL LETTER W WITH CIRCUMFLEX
       361   240   f1	  ñ	LATIN SMALL LETTER N WITH TILDE
       362   241   f2	  ò	LATIN SMALL LETTER O WITH GRAVE
       363   242   f3	  ó	LATIN SMALL LETTER O WITH ACUTE
       364   243   f4	  ô	LATIN SMALL LETTER O WITH CIRCUMFLEX
       365   244   f5	  õ	LATIN SMALL LETTER O WITH TILDE
       366   245   f6	  ö	LATIN SMALL LETTER O WITH DIAERESIS
       367   246   f7	  ṫ	LATIN SMALL LETTER T WITH DOT ABOVE
       370   247   f8	  ø	LATIN SMALL LETTER O WITH STROKE
       371   248   f9	  ù	LATIN SMALL LETTER U WITH GRAVE
       372   249   fa	  ú	LATIN SMALL LETTER U WITH ACUTE
       373   250   fb	  û	LATIN SMALL LETTER U WITH CIRCUMFLEX
       374   251   fc	  ü	LATIN SMALL LETTER U WITH DIAERESIS
       375   252   fd	  ý	LATIN SMALL LETTER Y WITH ACUTE

       376   253   fe	  ŷ	LATIN SMALL LETTER Y WITH CIRCUMFLEX
       377   254   ff	  ÿ	LATIN SMALL LETTER Y WITH DIAERESIS

NOTES
       ISO 8859-14 is also known as Latin-8.

SEE ALSO
       ascii(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-15							     ISO_8859-14(7)
