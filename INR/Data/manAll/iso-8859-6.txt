ISO_8859-6(7)							 Linux Programmer's Manual						      ISO_8859-6(7)



NAME
       iso_8859-6 - the ISO 8859-6 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO  8859 standard includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-6 encodes the characters
       used in the Arabic language.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-6 Characters
       The following table displays the characters in ISO 8859-6, which are printable and unlisted in the ascii(7) manual page.   The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-6.

       Oct   Dec   Hex	 Char	Description
       ─────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       244   164   a4	  ¤	CURRENCY SIGN
       254   172   ac	  ،	ARABIC COMMA
       255   173   ad		SOFT HYPHEN
       273   187   bb	  ؛	ARABIC SEMICOLON
       277   191   bf	  ؟	ARABIC QUESTION MARK
       301   193   c1	  ء	ARABIC LETTER HAMZA
       302   194   c2	  آ	ARABIC LETTER ALEF WITH MADDA ABOVE
       303   195   c3	  أ	ARABIC LETTER ALEF WITH HAMZA ABOVE
       304   196   c4	  ؤ	ARABIC LETTER WAW WITH HAMZA ABOVE
       305   197   c5	  إ	ARABIC LETTER ALEF WITH HAMZA BELOW
       306   198   c6	  ئ	ARABIC LETTER YEH WITH HAMZA ABOVE
       307   199   c7	  ا	ARABIC LETTER ALEF
       310   200   c8	  ب	ARABIC LETTER BEH
       311   201   c9	  ة	ARABIC LETTER TEH MARBUTA
       312   202   ca	  ت	ARABIC LETTER TEH
       313   203   cb	  ث	ARABIC LETTER THEH
       314   204   cc	  ج	ARABIC LETTER JEEM
       315   205   cd	  ح	ARABIC LETTER HAH
       316   206   ce	  خ	ARABIC LETTER KHAH
       317   207   cf	  د	ARABIC LETTER DAL
       320   208   d0	  ذ	ARABIC LETTER THAL
       321   209   d1	  ر	ARABIC LETTER REH
       322   210   d2	  ز	ARABIC LETTER ZAIN
       323   211   d3	  س	ARABIC LETTER SEEN
       324   212   d4	  ش	ARABIC LETTER SHEEN
       325   213   d5	  ص	ARABIC LETTER SAD
       326   214   d6	  ض	ARABIC LETTER DAD
       327   215   d7	  ط	ARABIC LETTER TAH

       330   216   d8	  ظ	ARABIC LETTER ZAH
       331   217   d9	  ع	ARABIC LETTER AIN
       332   218   da	  غ	ARABIC LETTER GHAIN
       340   223   e0	  ـ	ARABIC TATWEEL
       341   224   e1	  ف	ARABIC LETTER FEH
       342   225   e2	  ق	ARABIC LETTER QAF
       343   226   e3	  ك	ARABIC LETTER KAF
       344   227   e4	  ل	ARABIC LETTER LAM
       345   228   e5	  م	ARABIC LETTER MEEM
       346   229   e6	  ن	ARABIC LETTER NOON
       347   230   e7	  ه	ARABIC LETTER HEH
       350   231   e8	  و	ARABIC LETTER WAW
       351   232   e9	  ى	ARABIC LETTER ALEF MAKSURA
       352   233   ea	  ي	ARABIC LETTER YEH
       353   234   eb	  ً     ARABIC FATHATAN
       354   235   ec	  ٌ     ARABIC DAMMATAN
       355   236   ed	  ٍ     ARABIC KASRATAN
       356   237   ee	  َ     ARABIC FATHA
       357   238   ef	  ُ     ARABIC DAMMA
       360   239   f0	  ِ     ARABIC KASRA
       361   240   f1	  ّ     ARABIC SHADDA
       362   241   f2	  ْ     ARABIC SUKUN

NOTES
       ISO 8859-6 lacks the glyphs required for many related languages, such as Urdu and Persian (Farsi).

SEE ALSO
       ascii(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-27							      ISO_8859-6(7)
