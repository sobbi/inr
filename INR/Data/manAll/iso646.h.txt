<iso646.h>(P)							 POSIX Programmer's Manual						      <iso646.h>(P)



NAME
       iso646.h - alternative spellings

SYNOPSIS
       #include <iso646.h>

DESCRIPTION
       The <iso646.h> header shall define the following eleven macros (on the left) that expand to the corresponding tokens (on the right):

       and    &&

       and_eq &=

       bitand &

       bitor  |

       compl  ~

       not    !

       not_eq !=

       or     ||

       or_eq  |=

       xor    ^

       xor_eq ^=


       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       None.

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							      <iso646.h>(P)
