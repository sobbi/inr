ISO_8859-10(7)							 Linux Programmer's Manual						     ISO_8859-10(7)



NAME
       iso_8859-10 - the ISO 8859-10 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO 8859 standard includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-10 encodes the characters
       used in Nordic languages.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-10 Characters
       The following table displays the characters in ISO 8859-10, which are printable and unlisted in the ascii(7) manual page.  The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-10.

       Oct   Dec   Hex	 Char	Description
       ────────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       241   161   a1	  Ą	LATIN CAPITAL LETTER A WITH OGONEK
       242   162   a2	  Ē	LATIN CAPITAL LETTER E WITH MACRON
       243   163   a3	  Ģ	LATIN CAPITAL LETTER G WITH CEDILLA
       244   164   a4	  Ī	LATIN CAPITAL LETTER I WITH MACRON
       245   165   a5	  Ĩ	LATIN CAPITAL LETTER I WITH TILDE
       246   166   a6	  Ķ	LATIN CAPITAL LETTER K WITH CEDILLA
       247   167   a7	  §	SECTION SIGN
       250   168   a8	  Ļ	LATIN CAPITAL LETTER L WITH CEDILLA
       251   169   a9	  Đ	LATIN CAPITAL LETTER D WITH STROKE
       252   170   aa	  Š	LATIN CAPITAL LETTER S WITH CARON
       253   171   ab	  Ŧ	LATIN CAPITAL LETTER T WITH STROKE
       254   172   ac	  Ž	LATIN CAPITAL LETTER Z WITH CARON
       255   173   ad		SOFT HYPHEN
       256   174   ae	  Ū	LATIN CAPITAL LETTER U WITH MACRON
       257   175   af	  Ŋ	LATIN CAPITAL LETTER ENG (Sami)
       260   176   b0	  °	DEGREE SIGN
       261   177   b1	  ą	LATIN SMALL LETTER A WITH OGONEK
       262   178   b2	  ē	LATIN SMALL LETTER E WITH MACRON
       263   179   b3	  ģ	LATIN SMALL LETTER G WITH CEDILLA
       264   180   b4	  ī	LATIN SMALL LETTER I WITH MACRON
       265   181   b5	  ĩ	LATIN SMALL LETTER I WITH TILDE
       266   182   b6	  ķ	LATIN SMALL LETTER K WITH CEDILLA
       267   183   b7	  ·	MIDDLE DOT
       270   184   b8	  ļ	LATIN SMALL LETTER L WITH CEDILLA
       271   185   b9	  đ	LATIN SMALL LETTER D WITH STROKE
       272   186   ba	  š	LATIN SMALL LETTER S WITH CARON
       273   187   bb	  ŧ	LATIN SMALL LETTER T WITH STROKE
       274   188   bc	  ž	LATIN SMALL LETTER Z WITH CARON

       275   189   bd	  ―	HORIZONTAL BAR
       276   190   be	  ū	LATIN SMALL LETTER U WITH MACRON
       277   191   bf	  ŋ	LATIN SMALL LETTER ENG (Sami)
       300   192   c0	  Ā	LATIN CAPITAL LETTER A WITH MACRON
       301   193   c1	  Á	LATIN CAPITAL LETTER A WITH ACUTE
       302   194   c2	  Â	LATIN CAPITAL LETTER A WITH CIRCUMFLEX
       303   195   c3	  Ã	LATIN CAPITAL LETTER A WITH TILDE
       304   196   c4	  Ä	LATIN CAPITAL LETTER A WITH DIAERESIS
       305   197   c5	  Å	LATIN CAPITAL LETTER A WITH RING ABOVE
       306   198   c6	  Æ	LATIN CAPITAL LETTER AE
       307   199   c7	  Į	LATIN CAPITAL LETTER I WITH OGONEK
       310   200   c8	  Č	LATIN CAPITAL LETTER C WITH CARON
       311   201   c9	  É	LATIN CAPITAL LETTER E WITH ACUTE
       312   202   ca	  Ę	LATIN CAPITAL LETTER E WITH OGONEK
       312   202   ca	  Ë	LATIN CAPITAL LETTER E WITH DIAERESIS
       314   204   cc	  Ė	LATIN CAPITAL LETTER E WITH DOT ABOVE
       315   205   cd	  Í	LATIN CAPITAL LETTER I WITH ACUTE
       316   206   ce	  Î	LATIN CAPITAL LETTER I WITH CIRCUMFLEX
       317   207   cf	  Ï	LATIN CAPITAL LETTER I WITH DIAERESIS
       320   208   d0	  Ð	LATIN CAPITAL LETTER ETH (Icelandic)
       321   209   d1	  Ņ	LATIN CAPITAL LETTER N WITH CEDILLA
       322   210   d2	  Ō	LATIN CAPITAL LETTER O WITH MACRON
       323   211   d3	  Ó	LATIN CAPITAL LETTER O WITH ACUTE
       324   212   d4	  Ô	LATIN CAPITAL LETTER O WITH CIRCUMFLEX
       325   213   d5	  Õ	LATIN CAPITAL LETTER O WITH TILDE
       326   214   d6	  Ö	LATIN CAPITAL LETTER O WITH DIAERESIS
       327   215   d7	  Ũ	LATIN CAPITAL LETTER U WITH TILDE
       330   216   d8	  Ø	LATIN CAPITAL LETTER O WITH STROKE
       331   217   d9	  Ų	LATIN CAPITAL LETTER U WITH OGONEK
       332   218   da	  Ú	LATIN CAPITAL LETTER U WITH ACUTE
       333   219   db	  Û	LATIN CAPITAL LETTER U WITH CIRCUMFLEX
       334   219   dc	  Ü	LATIN CAPITAL LETTER U WITH DIAERESIS
       335   220   dd	  Ý	LATIN CAPITAL LETTER Y WITH ACUTE
       336   221   de	  Þ	LATIN CAPITAL LETTER THORN (Icelandic)
       337   222   df	  ß	LATIN SMALL LETTER SHARP S (German)
       340   223   e0	  ā	LATIN SMALL LETTER A WITH MACRON
       341   224   e1	  á	LATIN SMALL LETTER A WITH ACUTE
       342   225   e2	  â	LATIN SMALL LETTER A WITH CIRCUMFLEX
       343   226   e3	  ã	LATIN SMALL LETTER A WITH TILDE
       344   227   e4	  ä	LATIN SMALL LETTER A WITH DIAERESIS
       345   228   e5	  å	LATIN SMALL LETTER A WITH RING ABOVE
       346   229   e6	  æ	LATIN SMALL LETTER AE
       347   230   e7	  į	LATIN SMALL LETTER I WITH OGONEK
       350   231   e8	  č	LATIN SMALL LETTER C WITH CARON
       351   232   e9	  é	LATIN SMALL LETTER E WITH ACUTE
       352   233   ea	  ę	LATIN SMALL LETTER E WITH OGONEK
       353   234   eb	  ë	LATIN SMALL LETTER E WITH DIAERESIS
       354   235   ec	  ė	LATIN SMALL LETTER E WITH DOT ABOVE
       355   236   ed	  í	LATIN SMALL LETTER I WITH ACUTE
       356   237   ee	  î	LATIN SMALL LETTER I WITH CIRCUMFLEX
       357   238   ef	  ï	LATIN SMALL LETTER I WITH DIAERESIS
       360   239   f0	  ð	LATIN SMALL LETTER ETH (Icelandic)
       361   240   f1	  ņ	LATIN SMALL LETTER N WITH CEDILLA
       362   241   f2	  ō	LATIN SMALL LETTER O WITH MACRON
       363   242   f3	  ó	LATIN SMALL LETTER O WITH ACUTE
       364   243   f4	  ô	LATIN SMALL LETTER O WITH CIRCUMFLEX
       365   244   f5	  õ	LATIN SMALL LETTER O WITH TILDE
       366   245   f6	  ö	LATIN SMALL LETTER O WITH DIAERESIS
       367   246   f7	  ũ	LATIN SMALL LETTER U WITH TILDE
       370   247   f8	  ø	LATIN SMALL LETTER O WITH STROKE
       371   248   f9	  ų	LATIN SMALL LETTER U WITH OGONEK
       372   249   fa	  ú	LATIN SMALL LETTER U WITH ACUTE
       373   250   fb	  û	LATIN SMALL LETTER U WITH CIRCUMFLEX
       374   251   fc	  ü	LATIN SMALL LETTER U WITH DIAERESIS
       375   252   fd	  ý	LATIN SMALL LETTER Y WITH ACUTE

       376   253   fe	  þ	LATIN SMALL LETTER THORN (Icelandic)
       377   254   ff	  ĸ	LATIN SMALL LETTER KRA (Greenlandic)

NOTES
       ISO 8859-10 is also known as Latin-6.

SEE ALSO
       ascii(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-15							     ISO_8859-10(7)
