ISO_8859-5(7)							 Linux Programmer's Manual						      ISO_8859-5(7)



NAME
       iso_8859-5 - the ISO 8859-5 character set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       The  ISO  8859  standard  includes several 8-bit extensions to the ASCII character set (also known as ISO 646-IRV).  ISO 8859-5 encodes the Cyrillic
       alphabet as used in Russian and Macedonian.

   ISO 8859 Alphabets
       The full set of ISO 8859 alphabets includes:

       ISO 8859-1    West European languages (Latin-1)
       ISO 8859-2    Central and East European languages (Latin-2)
       ISO 8859-3    Southeast European and miscellaneous languages (Latin-3)
       ISO 8859-4    Scandinavian/Baltic languages (Latin-4)
       ISO 8859-5    Latin/Cyrillic
       ISO 8859-6    Latin/Arabic
       ISO 8859-7    Latin/Greek
       ISO 8859-8    Latin/Hebrew
       ISO 8859-9    Latin-1 modification for Turkish (Latin-5)
       ISO 8859-10   Lappish/Nordic/Eskimo languages (Latin-6)
       ISO 8859-11   Latin/Thai
       ISO 8859-13   Baltic Rim languages (Latin-7)
       ISO 8859-14   Celtic (Latin-8)
       ISO 8859-15   West European languages (Latin-9)
       ISO 8859-16   Romanian (Latin-10)

   ISO 8859-5 Characters
       The following table displays the characters in ISO 8859-5, which are printable and unlisted in the ascii(7) manual page.   The  fourth  column  will
       only show the proper glyphs in an environment configured for ISO 8859-5.

       Oct   Dec   Hex	 Char	Description
       ──────────────────────────────────────────────────────────────────────────
       240   160   a0	   	NO-BREAK SPACE
       241   161   a1	  Ё	CYRILLIC CAPITAL LETTER IO
       242   162   a2	  Ђ	CYRILLIC CAPITAL LETTER DJE (Serbocroatian)
       243   163   a3	  Ѓ	CYRILLIC CAPITAL LETTER GJE
       244   164   a4	  Є	CYRILLIC CAPITAL LETTER UKRAINIAN IE
       245   165   a5	  Ѕ	CYRILLIC CAPITAL LETTER DZE
       246   166   a6	  І	CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
       247   167   a7	  Ї	CYRILLIC CAPITAL LETTER YI (Ukrainian)
       250   168   a8	  Ј	CYRILLIC CAPITAL LETTER JE
       251   169   a9	  Љ	CYRILLIC CAPITAL LETTER LJE
       252   170   aa	  Њ	CYRILLIC CAPITAL LETTER NJE
       253   171   ab	  Ћ	CYRILLIC CAPITAL LETTER TSHE (Serbocroatian)
       254   172   ac	  Ќ	CYRILLIC CAPITAL LETTER KJE
       255   173   ad		SOFT HYPHEN
       256   174   ae	  Ў	CYRILLIC CAPITAL LETTER SHORT U (Byelorussian)
       257   175   af	  Џ	CYRILLIC CAPITAL LETTER DZHE
       260   176   b0	  А	CYRILLIC CAPITAL LETTER A
       261   177   b1	  Б	CYRILLIC CAPITAL LETTER BE
       262   178   b2	  В	CYRILLIC CAPITAL LETTER VE
       263   179   b3	  Г	CYRILLIC CAPITAL LETTER GHE
       264   180   b4	  Д	CYRILLIC CAPITAL LETTER DE
       265   181   b5	  Е	CYRILLIC CAPITAL LETTER IE
       266   182   b6	  Ж	CYRILLIC CAPITAL LETTER ZHE
       267   183   b7	  З	CYRILLIC CAPITAL LETTER ZE
       270   184   b8	  И	CYRILLIC CAPITAL LETTER I
       271   185   b9	  Й	CYRILLIC CAPITAL LETTER SHORT I
       272   186   ba	  К	CYRILLIC CAPITAL LETTER KA
       273   187   bb	  Л	CYRILLIC CAPITAL LETTER EL
       274   188   bc	  М	CYRILLIC CAPITAL LETTER EM

       275   189   bd	  Н	CYRILLIC CAPITAL LETTER EN
       276   190   be	  О	CYRILLIC CAPITAL LETTER O
       277   191   bf	  П	CYRILLIC CAPITAL LETTER PE
       300   192   c0	  Р	CYRILLIC CAPITAL LETTER ER
       301   193   c1	  С	CYRILLIC CAPITAL LETTER ES
       302   194   c2	  Т	CYRILLIC CAPITAL LETTER TE
       303   195   c3	  У	CYRILLIC CAPITAL LETTER U
       304   196   c4	  Ф	CYRILLIC CAPITAL LETTER EF
       305   197   c5	  Х	CYRILLIC CAPITAL LETTER HA
       306   198   c6	  Ц	CYRILLIC CAPITAL LETTER TSE
       307   199   c7	  Ч	CYRILLIC CAPITAL LETTER CHE
       310   200   c8	  Ш	CYRILLIC CAPITAL LETTER SHA
       311   201   c9	  Щ	CYRILLIC CAPITAL LETTER SHCHA
       312   202   ca	  Ъ	CYRILLIC CAPITAL LETTER HARD SIGN
       313   203   cb	  Ы	CYRILLIC CAPITAL LETTER YERU
       314   204   cc	  Ь	CYRILLIC CAPITAL LETTER SOFT SIGN
       315   205   cd	  Э	CYRILLIC CAPITAL LETTER E
       316   206   ce	  Ю	CYRILLIC CAPITAL LETTER YU
       317   207   cf	  Я	CYRILLIC CAPITAL LETTER YA
       320   208   d0	  а	CYRILLIC SMALL LETTER A
       321   209   d1	  б	CYRILLIC SMALL LETTER BE
       322   210   d2	  в	CYRILLIC SMALL LETTER VE
       323   211   d3	  г	CYRILLIC SMALL LETTER GHE
       324   212   d4	  д	CYRILLIC SMALL LETTER DE
       325   213   d5	  е	CYRILLIC SMALL LETTER IE
       326   214   d6	  ж	CYRILLIC SMALL LETTER ZHE
       327   215   d7	  з	CYRILLIC SMALL LETTER ZE
       330   216   d8	  и	CYRILLIC SMALL LETTER I
       331   217   d9	  й	CYRILLIC SMALL LETTER SHORT I
       332   218   da	  к	CYRILLIC SMALL LETTER KA
       333   219   db	  л	CYRILLIC SMALL LETTER EL
       334   219   dc	  м	CYRILLIC SMALL LETTER EM
       335   220   dd	  н	CYRILLIC SMALL LETTER EN
       336   221   de	  о	CYRILLIC SMALL LETTER O
       337   222   df	  п	CYRILLIC SMALL LETTER PE
       340   223   e0	  р	CYRILLIC SMALL LETTER ER
       341   224   e1	  с	CYRILLIC SMALL LETTER ES
       342   225   e2	  т	CYRILLIC SMALL LETTER TE
       343   226   e3	  у	CYRILLIC SMALL LETTER U
       344   227   e4	  ф	CYRILLIC SMALL LETTER EF
       345   228   e5	  х	CYRILLIC SMALL LETTER HA
       346   229   e6	  ц	CYRILLIC SMALL LETTER TSE
       347   230   e7	  ч	CYRILLIC SMALL LETTER CHE
       350   231   e8	  ш	CYRILLIC SMALL LETTER SHA
       351   232   e9	  щ	CYRILLIC SMALL LETTER SHCHA
       352   233   ea	  ъ	CYRILLIC SMALL LETTER HARD SIGN
       353   234   eb	  ы	CYRILLIC SMALL LETTER YERU
       354   235   ec	  ь	CYRILLIC SMALL LETTER SOFT SIGN
       355   236   ed	  э	CYRILLIC SMALL LETTER E
       356   237   ee	  ю	CYRILLIC SMALL LETTER YU
       357   238   ef	  я	CYRILLIC SMALL LETTER YA
       360   239   f0	  №	NUMERO SIGN
       361   240   f1	  ё	CYRILLIC SMALL LETTER IO
       362   241   f2	  ђ	CYRILLIC SMALL LETTER DJE (Serbocroatian)
       363   242   f3	  ѓ	CYRILLIC SMALL LETTER GJE
       364   243   f4	  є	CYRILLIC SMALL LETTER UKRAINIAN IE
       365   244   f5	  ѕ	CYRILLIC SMALL LETTER DZE
       366   245   f6	  і	CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
       367   246   f7	  ї	CYRILLIC SMALL LETTER YI (Ukrainian)
       370   247   f8	  ј	CYRILLIC SMALL LETTER JE
       371   248   f9	  љ	CYRILLIC SMALL LETTER LJE
       372   249   fa	  њ	CYRILLIC SMALL LETTER NJE
       373   250   fb	  ј	CYRILLIC SMALL LETTER TSHE (Serbocroatian)
       374   251   fc	  ќ	CYRILLIC SMALL LETTER KJE
       375   252   fd	  §	SECTION SIGN

       376   253   fe	  ў	CYRILLIC SMALL LETTER SHORT U (Byelorussian)
       377   254   ff	  џ	CYRILLIC SMALL LETTER DZHE

SEE ALSO
       ascii(7), koi8-r(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-15							      ISO_8859-5(7)
