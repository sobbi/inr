KOI8-U(7)							 Linux Programmer's Manual							  KOI8-U(7)



NAME
       koi8-u - Ukrainian Net Character Set encoded in octal, decimal, and hexadecimal

DESCRIPTION
       KOI8-U  (KOI8  Ukrainian,  described  in  RFC 2310)  is the de-facto standard character set for encoding Ukrainian texts.  KOI8-U is compatible with
       KOI8-R (RFC 1489) for all Russian letters, and extends KOI8-R with four Ukrainian letters (in both upper and  lower  case)  in  locations  that	are
       compliant with ISO-IR-111.

   KOI8-U Characters
       The  following  table  displays the characters in KOI8-U, which are printable and unlisted in the ascii(7) manual page.	The fourth column will only
       show the proper glyphs in an environment configured for koi8-u.

       Oct   Dec   Hex	 Char	Description
       ────────────────────────────────────────────────────────────────────────────
       200   128   80	  ─	BOX DRAWINGS LIGHT HORIZONTAL
       201   129   81	  │	BOX DRAWINGS LIGHT VERTICAL
       202   130   82	  ┌	BOX DRAWINGS LIGHT DOWN AND RIGHT
       203   131   83	  ┐	BOX DRAWINGS LIGHT DOWN AND LEFT
       204   132   84	  └	BOX DRAWINGS LIGHT UP AND RIGHT
       205   133   85	  ┘	BOX DRAWINGS LIGHT UP AND LEFT
       206   134   86	  ├	BOX DRAWINGS LIGHT VERTICAL AND RIGHT
       207   135   87	  ┤	BOX DRAWINGS LIGHT VERTICAL AND LEFT
       210   136   88	  ┬	BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
       211   137   89	  ┴	BOX DRAWINGS LIGHT UP AND HORIZONTAL
       212   138   8a	  ┼	BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
       213   139   8b	  ▀	UPPER HALF BLOCK
       214   140   8c	  ▄	LOWER HALF BLOCK
       215   141   8d	  █	FULL BLOCK
       216   142   8e	  ▌	LEFT HALF BLOCK
       217   143   8f	  ▐	RIGHT HALF BLOCK
       220   144   90	  ░	LIGHT SHADE
       221   145   91	  ▒	MEDIUM SHADE
       222   146   92	  ▓	DARK SHADE
       223   147   93	  ⌠	TOP HALF INTEGRAL
       224   148   94	  ■	BLACK SQUARE
       225   149   95	  ∙	BULLET OPERATOR
       226   150   96	  √	SQUARE ROOT
       227   151   97	  ≈	ALMOST EQUAL TO
       230   152   98	  ≤	LESS-THAN OR EQUAL TO
       231   153   99	  ≥	GREATER-THAN OR EQUAL TO
       232   154   9a	   	NO-BREAK SPACE
       233   155   9b	  ⌡	BOTTOM HALF INTEGRAL
       234   156   9c	  °	DEGREE SIGN
       235   157   9d	  ²	SUPERSCRIPT TWO
       236   158   9e	  ·	MIDDLE DOT
       237   159   9f	  ÷	DIVISION SIGN
       240   160   a0	  ═	BOX DRAWINGS DOUBLE HORIZONTAL
       241   161   a1	  ║	BOX DRAWINGS DOUBLE VERTICAL
       242   162   a2	  ╒	BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE
       243   163   a3	  ё	CYRILLIC SMALL LETTER IO
       244   164   a4	  є	CYRILLIC SMALL LETTER UKRAINIAN IE
       245   165   a5	  ╔	BOX DRAWINGS DOUBLE DOWN AND RIGHT
       246   166   a6	  і	CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
       247   167   a7	  ї	CYRILLIC SMALL LETTER YI (Ukrainian)
       250   168   a8	  ╗	BOX DRAWINGS DOUBLE DOWN AND LEFT
       251   169   a9	  ╘	BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE
       252   170   aa	  ╙	BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE
       253   171   ab	  ╚	BOX DRAWINGS DOUBLE UP AND RIGHT
       254   172   ac	  ╛	BOX DRAWINGS UP SINGLE AND LEFT DOUBLE
       255   173   ad	  ґ	CYRILLIC SMALL LETTER GHE WITH UPTURN
       256   174   ae	  ╝	BOX DRAWINGS DOUBLE UP AND LEFT

       257   175   af	  ╞	BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE
       260   176   b0	  ╟	BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE
       261   177   b1	  ╠	BOX DRAWINGS DOUBLE VERTICAL AND RIGHT
       262   178   b2	  ╡	BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE
       263   179   b3	  Ё	CYRILLIC CAPITAL LETTER IO
       264   180   b4	  Є	CYRILLIC CAPITAL LETTER UKRAINIAN IE
       265   181   b5	  ╣	BOX DRAWINGS DOUBLE VERTICAL AND LEFT
       266   182   b6	  І	CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
       267   183   b7	  Ї	CYRILLIC CAPITAL LETTER YI (Ukrainian)
       270   184   b8	  ╦	BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL
       271   185   b9	  ╧	BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE
       272   186   ba	  ╨	BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE
       273   187   bb	  ╩	BOX DRAWINGS DOUBLE UP AND HORIZONTAL
       274   188   bc	  ╪	BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE
       275   189   bd	  Ґ	CYRILLIC CAPITAL LETTER GHE WITH UPTURN
       276   190   be	  ╬	BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL
       277   191   bf	  ©	COPYRIGHT SIGN
       300   192   c0	  ю	CYRILLIC SMALL LETTER YU
       301   193   c1	  а	CYRILLIC SMALL LETTER A
       302   194   c2	  б	CYRILLIC SMALL LETTER BE
       303   195   c3	  ц	CYRILLIC SMALL LETTER TSE
       304   196   c4	  д	CYRILLIC SMALL LETTER DE
       305   197   c5	  е	CYRILLIC SMALL LETTER IE
       306   198   c6	  ф	CYRILLIC SMALL LETTER EF
       307   199   c7	  г	CYRILLIC SMALL LETTER GHE
       310   200   c8	  х	CYRILLIC SMALL LETTER HA
       311   201   c9	  и	CYRILLIC SMALL LETTER I
       312   202   ca	  й	CYRILLIC SMALL LETTER SHORT I
       313   203   cb	  к	CYRILLIC SMALL LETTER KA
       314   204   cc	  л	CYRILLIC SMALL LETTER EL
       315   205   cd	  м	CYRILLIC SMALL LETTER EM
       316   206   ce	  н	CYRILLIC SMALL LETTER EN
       317   207   cf	  о	CYRILLIC SMALL LETTER O
       320   208   d0	  п	CYRILLIC SMALL LETTER PE
       321   209   d1	  я	CYRILLIC SMALL LETTER YA
       322   210   d2	  р	CYRILLIC SMALL LETTER ER
       323   211   d3	  с	CYRILLIC SMALL LETTER ES
       324   212   d4	  т	CYRILLIC SMALL LETTER TE
       325   213   d5	  у	CYRILLIC SMALL LETTER U
       326   214   d6	  ж	CYRILLIC SMALL LETTER ZHE
       327   215   d7	  в	CYRILLIC SMALL LETTER VE
       330   216   d8	  ь	CYRILLIC SMALL LETTER SOFT SIGN
       331   217   d9	  ы	CYRILLIC SMALL LETTER YERU
       332   218   da	  з	CYRILLIC SMALL LETTER ZE
       333   219   db	  ш	CYRILLIC SMALL LETTER SHA
       334   219   dc	  э	CYRILLIC SMALL LETTER E
       335   220   dd	  щ	CYRILLIC SMALL LETTER SHCHA
       336   221   de	  ч	CYRILLIC SMALL LETTER CHE
       337   222   df	  ъ	CYRILLIC SMALL LETTER HARD SIGN
       340   223   e0	  Ю	CYRILLIC CAPITAL LETTER YU
       341   224   e1	  А	CYRILLIC CAPITAL LETTER A
       342   225   e2	  Б	CYRILLIC CAPITAL LETTER BE
       343   226   e3	  Ц	CYRILLIC CAPITAL LETTER TSE
       344   227   e4	  Д	CYRILLIC CAPITAL LETTER DE
       345   228   e5	  Е	CYRILLIC CAPITAL LETTER IE
       346   229   e6	  Ф	CYRILLIC CAPITAL LETTER EF
       347   230   e7	  Г	CYRILLIC CAPITAL LETTER GHE
       350   231   e8	  Х	CYRILLIC CAPITAL LETTER HA
       351   232   e9	  И	CYRILLIC CAPITAL LETTER I
       352   233   ea	  Й	CYRILLIC CAPITAL LETTER SHORT I
       353   234   eb	  К	CYRILLIC CAPITAL LETTER KA
       354   235   ec	  Л	CYRILLIC CAPITAL LETTER EL
       355   236   ed	  М	CYRILLIC CAPITAL LETTER EM
       356   237   ee	  Н	CYRILLIC CAPITAL LETTER EN
       357   238   ef	  О	CYRILLIC CAPITAL LETTER O

       360   239   f0	  П	CYRILLIC CAPITAL LETTER PE
       361   240   f1	  Я	CYRILLIC CAPITAL LETTER YA
       362   241   f2	  Р	CYRILLIC CAPITAL LETTER ER
       363   242   f3	  С	CYRILLIC CAPITAL LETTER ES
       364   243   f4	  Т	CYRILLIC CAPITAL LETTER TE
       365   244   f5	  У	CYRILLIC CAPITAL LETTER U
       366   245   f6	  Ж	CYRILLIC CAPITAL LETTER ZHE
       367   246   f7	  В	CYRILLIC CAPITAL LETTER VE
       370   247   f8	  Ь	CYRILLIC CAPITAL LETTER SOFT SIGN
       371   248   f9	  Ы	CYRILLIC CAPITAL LETTER YERU
       372   249   fa	  З	CYRILLIC CAPITAL LETTER ZE
       373   250   fb	  Ш	CYRILLIC CAPITAL LETTER SHA
       374   251   fc	  Э	CYRILLIC CAPITAL LETTER E
       375   252   fd	  Щ	CYRILLIC CAPITAL LETTER SHCHA
       376   253   fe	  Ч	CYRILLIC CAPITAL LETTER CHE
       377   254   ff	  Ъ	CYRILLIC CAPITAL LETTER HARD SIGN

NOTES
       The differences from KOI8-R are in the hex positions a4, a6, a7, ad, b4, b6, b7, and bd.

SEE ALSO
       ascii(7), koi8-r(7)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2009-01-15								  KOI8-U(7)
