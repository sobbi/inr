KPSEACCESS(1)																      KPSEACCESS(1)



NAME
       kpseaccess - determine whether a file can be accessed

SYNOPSIS
       kpseaccess [-rwx] file

DESCRIPTION
       Exit  successfully  if  file  can  be  accessed	with the specified mode.  The mode is one or more letters of rwx, where r is for readable, w is for
       writable, and x is for executable.

       The difference between kpseaccess and test is that the latter looks at the permission bits, while the former checks using the access(2) system call.
       This makes a difference when file systems have been mounted read-only.

OPTIONS
       kpseaccess accepts the following additional options:

       --help Print help message and exit.

       --version
	      Print version information and exit.

SEE ALSO
       access(2)



Kpathsea 5.0.0							       4 January 1998							      KPSEACCESS(1)
