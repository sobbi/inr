KPSESTAT(1)																	KPSESTAT(1)



NAME
       kpsestat - compute octal mode from mode of existing file

SYNOPSIS
       kpsestat mode file

DESCRIPTION
       kpsestat  prints  the  octal  permission of file modified according to mode on standard output.	The mode parameter accepts a subset of the symbolic
       permissions accepted by chmod(1).  Use = as the mode to obtain the unchanged permissions.

OPTIONS
       kpsestat accepts the following additional options:

       --help Print help message and exit.

       --version
	      Print version information and exit.

SEE ALSO
       chmod(1).



Kpathsea 5.0.0							       4 January 1998								KPSESTAT(1)
