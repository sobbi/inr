LIBGRAPHVIZ4-CONFIG-UPDATE(1)				     GRAPHVIZ CONFIGURATION MANAGEMENT				      LIBGRAPHVIZ4-CONFIG-UPDATE(1)



NAME
       libgraphviz4-config-update - maintain libgraphviz's configuration file

SYNOPSIS
       libgraphviz4-config-update -c

DESCRIPTION
       Graphviz tools are using a configuration file (/usr/lib/graphviz/config4 at the moment) to get some plugin options. This command is used in
       libgraphviz4 and libgraphviz4-*-plugin packages' maintainer scripts to update this configuration file when plugins are installed, updated, or
       removed.

NOTE
       This tool is intended to be used only in maintainer scripts and it shouldn't be needed to run it manually.

AUTHOR
       This manual page was written by Cyril Brulebois <cyril.brulebois@enst-bretagne.fr>.

       It can be distributed under the same terms as the graphviz package.



2.20.2									 2010-03-02					      LIBGRAPHVIZ4-CONFIG-UPDATE(1)
