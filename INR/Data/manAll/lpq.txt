lpq(1)				  Apple Inc.				lpq(1)



NAME
       lpq - zeigt den Status von Druckerwarteschlangen

SYNOPSIS
       lpq [ -E ] [ -U Benutzername ] [ -h Server[:Port] ] [ -P Ziel[/Instanz]
       ] [ -a ] [ -l ] [ +Intervall ]

BESCHREIBUNG
       lpq zeigt den aktuellen Druckerwarteschlangenstatus auf	dem  benannten
       Drucker.  Aufträge, die auf dem Standardziel auf den Druck warten, wer‐
       den angezeigt, falls kein Drucker oder keine Druckerklasse auf der Kom‐
       mandozeile angegeben ist.

       Die  Option +Intervall erlaubt es Ihnen, kontinuierlich die Aufträge in
       der Warteschlange zu berichten, bis  diese  leer  ist;  die  Liste  der
       Aufträge wird einmal alle Intervall Sekunden angezeigt.

OPTIONEN
       lpq unterstützt die folgenden Optionen:

       -E
	    Erzwingt Verschlüsselung bei Verbindungen zum Server.

       -P Ziel[/Instanz]
	    Spezifiziert einen alternativen Drucker- oder Klassennamen.

       -U Benutzername
	    Spezifiziert einen alternativen Benutzernamen.

       -a
	    Berichtet Aufträge auf allen Druckern.

       -h Server[:Port]
	    Spezifiziert einen alternativen Server.

       -l
	    Erbittet ein ausführlicheres (langes) Berichtsformat.

SIEHE AUCH
       cancel(1), lp(1), lpr(1), lprm(1), lpstat(1),
       http://localhost:631/help

COPYRIGHT
       Copyright 2007-2009 by Apple Inc.



16. Juni 2008			     CUPS				lpq(1)
