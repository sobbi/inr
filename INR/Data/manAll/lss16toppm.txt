LSS16TOPPM(1)																      LSS16TOPPM(1)



NAME
       lss16toppm — Convert an LSS-16 image to PPM

SYNOPSIS
       lss16toppm [-map]  [< file.lss]	[> file.ppm]

DESCRIPTION
       This manual page documents briefly the lss16toppm       command.


       The lss16toppm utility converts an LSS-16 image to a PPM image.


OPTIONS
       A summary of options is included below.

       -map	 Output the color map to standard error.

SEE ALSO
       ppmtolss16(1)


AUTHOR
       This manual page was compiled by dann frazier <dannf@debian.org> for the Debian GNU/Linux system (but may be used by others).



																	      LSS16TOPPM(1)
