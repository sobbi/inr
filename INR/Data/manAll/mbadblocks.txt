mbadblocks(1)																      mbadblocks(1)



Name
       mbadblocks - tests a floppy disk, and marks the bad blocks in the FAT



Note of warning
       This  manpage  has  been  automatically generated from mtools's texinfo documentation, and may not be entirely accurate or complete.  See the end of
       this man page for details.

Description
       The mbadblocks command is used to scan an MS-DOS floppy and mark its unused bad blocks as bad. It uses the following syntax:

       mbadblocks drive:

       Mbadblocks scans an MS-DOS floppy for bad blocks. All unused bad blocks are marked as such in the FAT. This is intended to be used right after mfor‐
       mat.  It is not intended to salvage bad disks.

Bugs
       Mbadblocks should (but doesn't yet :-( ) also try to salvage bad blocks which are in use by reading them repeatedly, and then mark them bad.

See Also
       Mtools' texinfo doc

Viewing the texi doc
       This manpage has been automatically generated from mtools's texinfo documentation. However, this process is only approximative, and some items, such
       as crossreferences, footnotes and indices are lost in this translation process.	Indeed, these items have no appropriate representation in the  man‐
       page  format.   Moreover,  not all information has been translated into the manpage version.  Thus I strongly advise you to use the original texinfo
       doc.  See the end of this manpage for instructions how to view the texinfo doc.

       *      To generate a printable copy from the texinfo doc, run the following commands:

		     ./configure; make dvi; dvips mtools.dvi



       *      To generate a html copy,	run:

		     ./configure; make html

	      A premade html can be found at `http://www.gnu.org/software/mtools/manual/mtools.html'

       *      To generate an info copy (browsable using emacs' info mode), run:

		     ./configure; make info



       The texinfo doc looks most pretty when printed or as html.  Indeed, in the info version certain examples are difficult to read due  to  the  quoting
       conventions used in info.

mtools-4.0.10								  10Mar09							      mbadblocks(1)
