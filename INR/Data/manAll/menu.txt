menu(3MENU)																	menu(3MENU)



NAME
       menu - curses extension for programming menus

SYNOPSIS
       #include <menu.h>

DESCRIPTION
       The  menu  library provides terminal-independent facilities for composing menu systems on character-cell terminals.  The library includes: item rou‐
       tines, which create and modify menu items; and menu routines, which group items into menus, display menus on the screen, and handle interaction with
       the user.

       The  menu library uses the curses libraries, and a curses initialization routine such as initscr must be called before using any of these functions.
       To use the menu library, link with the options -lmenu -lcurses.

   Current Default Values for Item Attributes
       The menu library maintains a default value for item attributes.	You can get or set this default by calling the appropriate  get_  or  set_  routine
       with  a	NULL  item  pointer.   Changing this default with a set_ function affects future item creations, but does not change the rendering of items
       already created.

   Routine Name Index
       The following table lists each menu routine and the name of the manual page on which it is described.

       curses Routine Name    Manual Page Name
       ───────────────────────────────────────────
       current_item	      menu_current(3MENU)
       free_item	      menu_new(3MENU)
       free_menu	      new(3MENU)
       item_count	      items(3MENU)
       item_description       menu_name(3MENU)
       item_index	      menu_current(3MENU)
       item_init	      hook(3MENU)
       item_name	      menu_name(3MENU)
       item_opts	      menu_opts(3MENU)
       item_opts_off	      menu_opts(3MENU)
       item_opts_on	      menu_opts(3MENU)
       item_term	      hook(3MENU)
       item_userptr	      menu_userptr(3MENU)
       item_value	      menu_value(3MENU)
       item_visible	      menu_visible(3MENU)
       menu_back	      attributes(3MENU)
       menu_driver	      driver(3MENU)
       menu_fore	      attributes(3MENU)
       menu_format	      format(3MENU)
       menu_grey	      attributes(3MENU)
       menu_init	      hook(3MENU)
       menu_items	      items(3MENU)
       menu_mark	      mark(3MENU)
       menu_opts	      opts(3MENU)
       menu_opts_off	      opts(3MENU)
       menu_opts_on	      opts(3MENU)
       menu_pad 	      attributes(3MENU)
       menu_pattern	      pattern(3MENU)
       menu_request_by_name   requestname(3MENU)
       menu_request_name      requestname(3MENU)
       menu_spacing	      spacing(3MENU)
       menu_sub 	      win(3MENU)
       menu_term	      hook(3MENU)
       menu_userptr	      userptr(3MENU)
       menu_win 	      win(3MENU)
       new_item 	      menu_new(3MENU)
       new_menu 	      new(3MENU)

       pos_menu_cursor	      cursor(3MENU)
       post_menu	      post(3MENU)
       scale_menu	      win(3MENU)
       set_current_item       menu_current(3MENU)
       set_item_init	      hook(3MENU)
       set_item_opts	      menu_opts(3MENU)
       set_item_term	      hook(3MENU)
       set_item_userptr       menu_userptr(3MENU)
       set_item_value	      menu_value(3MENU)
       set_menu_back	      attributes(3MENU)
       set_menu_fore	      attributes(3MENU)
       set_menu_format	      format(3MENU)
       set_menu_grey	      attributes(3MENU)
       set_menu_init	      hook(3MENU)
       set_menu_items	      items(3MENU)
       set_menu_mark	      mark(3MENU)
       set_menu_opts	      menu_opts(3MENU)
       set_menu_pad	      attributes(3MENU)
       set_menu_pattern       pattern(3MENU)
       set_menu_spacing       spacing(3MENU)
       set_menu_sub	      win(3MENU)
       set_menu_term	      hook(3MENU)
       set_menu_userptr       userptr(3MENU)
       set_menu_win	      win(3MENU)
       set_top_row	      menu_current(3MENU)
       top_row		      menu_current(3MENU)
       unpost_menu	      post(3MENU)

RETURN VALUE
       Routines that return pointers return NULL on error.  Routines that return an integer return one of the following error codes:

       E_OK The routine succeeded.

       E_BAD_ARGUMENT
	    Routine detected an incorrect or out-of-range argument.

       E_BAD_STATE
	    Routine was called from an initialization or termination function.

       E_NO_MATCH
	    Character failed to match.

       E_NO_ROOM
	    Menu is too large for its window.

       E_NOT_CONNECTED
	    No items are connected to the menu.

       E_NOT_POSTED
	    The menu has not been posted.

       E_NOT_SELECTABLE
	    The designated item cannot be selected.

       E_POSTED
	    The menu is already posted.

       E_REQUEST_DENIED
	    The menu driver could not process the request.

       E_SYSTEM_ERROR
	    System error occurred (see errno).

       E_UNKNOWN_COMMAND
	    The menu driver code saw an unknown request code.

SEE ALSO
       ncurses(3NCURSES) and related pages whose names begin "menu_" for detailed descriptions of the entry points.

NOTES
       The header file <menu.h> automatically includes the header files <curses.h> and <eti.h>.

       In your library list, libmenu.a should be before libncurses.a; that is, you want to say `-lmenu -lncurses', not the other way  around  (which  would
       usually give a link-error).

PORTABILITY
       These routines emulate the System V menu library.  They were not supported on Version 7 or BSD versions.

AUTHORS
       Juergen Pfeifer.  Manual pages and adaptation for ncurses by Eric S. Raymond.

SEE ALSO
       This describes ncurses version 5.7 (patch 20090803).



																		menu(3MENU)
