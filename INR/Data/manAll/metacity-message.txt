METACITY-MESSAGE(1)					   METACITY-MESSAGE(1)



NAME
       METACITY-MESSAGE - a command to send a message to Metacity

SYNOPSIS
       METACITY-MESSAGE  [restart|reload-theme|enable-keybindings|disable-key‐
       bindings]

DESCRIPTION
       This manual page documents briefly the metacity-message.   This	manual
       page  was written for the Debian distribution because the original pro‐
       gram does not have a manual page.

       metacity-message send a specified message to metacity(1).

OPTIONS
       restart
	      Restart metacity(1) which is running.

       reload-theme
	      Reload a theme which is specified on gconf database.

       enable-keybindings
	      Enable all of keybindings which is specified on gconf database.

       disable-keybindings
	      Disable all of keybindings which is specified on gconf database.

SEE ALSO
       metacity(1)

AUTHOR
       This manual page was written by Akira TAGOH <tagoh@debian.org>, for the
       Debian GNU/Linux system (but may be used by others).



				28 August 2002		   METACITY-MESSAGE(1)
