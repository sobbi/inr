mga(4)																		     mga(4)



NAME
       mga - Matrox video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "mga"
	 ...
       EndSection

DESCRIPTION
       mga  is	an  Xorg driver for Matrox video cards.  The driver is fully accelerated, and provides support for the following framebuffer depths: 8, 15,
       16, 24, and an 8+24 overlay mode.  All visual types are supported for depth 8, and both TrueColor and DirectColor  visuals  are	supported  for	the
       other depths except 8+24 mode which supports PseudoColor, GrayScale and TrueColor.  Multi-card configurations are supported.  XVideo is supported on
       G200 and newer systems, with either TexturedVideo or video overlay.  The second head of dual-head cards is supported for the G450 and G550.  Support
       for  the  second head on G400 cards requires a binary-only "mga_hal" module that is available from Matrox <http://www.matrox.com>, and may be on the
       CD supplied with the card.  That module also provides various other enhancements, and may be necessary to use the DVI (digital) output on  the  G550
       (and other cards).

SUPPORTED HARDWARE
       The  mga driver supports PCI and AGP video cards based on the following Matrox chips.  They are listed in approximate chronological order of produc‐
       tion (with the most recent chipsets listed last), so consult this list when you are unsure whether your card is meant when references  are  made  to
       ‘G200 and later’ chips, for example.

       MGA2064W

       MGA1064SG
	      Mystique

       MGA2164W
	      Millennium II

       G100   Productiva G100

       G200   Millennium G200 and Mystique G200

       G400   Millennium G400, Millennium G400 MAX, Millennium G450, and Marvel G450 eTV

       G550   Millennium G550 and Millennium G550 Dual DVI

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration details.	This section only covers configuration details specific to this driver.

       The  driver auto-detects the chipset type, but the following ChipSet names may optionally be specified in the config file "Device" section, and will
       override the auto-detection:

	   "mga2064w", "mga1064sg", "mga2164w", "mga2164w agp", "mgag100", "mgag200", "mgag200 pci", "mgag400", "mgag550".

       The G450 is Chipset "mgag400" with ChipRev 0x80.

       The driver will auto-detect the amount of video memory present for all chips except the Millennium II.  In the Millennium II  case  it  defaults  to
       4096 kBytes.   When  using  a Millennium II, the actual amount of video memory should be specified with a VideoRam entry in the config file "Device"
       section.

       The following driver Options are supported:

       Option "ColorKey" "integer"
	      Set the colormap index used for the transparency key for the depth 8 plane when operating in 8+24 overlay mode.  The value  must	be  in	the
	      range 2-255.  Default: 255.

       Option "HWCursor" "boolean"
	      Enable or disable the HW cursor.	Default: on.

       Option "MGASDRAM" "boolean"
	      Specify  whether	G100,  G200  or G400 cards have SDRAM.	The driver attempts to auto-detect this based on the card's PCI subsystem ID.  This
	      option may be used to override that auto-detection.  The mga driver is not able to auto-detect the presence of of SDRAM on secondary heads in
	      multihead configurations so this option will often need to be specified in multihead configurations.  Default: auto-detected.

       Option "NoAccel" "boolean"
	      Disable or enable acceleration.  Default: acceleration is enabled.

       Option "AccelMethod" "string"
	      Chooses  between	available acceleration architectures.  Valid options are XAA and EXA.  XAA is the traditional acceleration architecture and
	      support for it is very stable.  EXA is a newer acceleration architecture with better performance for the Render and Composite extensions, but
	      the rendering code for it is newer and possibly unstable.  The default is XAA.

       Option "NoHal" "boolean"
	      Disable or enable loading the "mga_hal" module.  Default: the module is loaded when available and when using hardware that it supports.

       Option "OverclockMem"
	      Set clocks to values used by some commercial X Servers (G100, G200 and G400 only).  Default: off.

       Option "PciRetry" "boolean"
	      Enable or disable PCI retries.  Default: off.

       Option "Rotate" "CW"

       Option "Rotate" "CCW"
	      Rotate the display clockwise or counterclockwise.  This mode is unaccelerated.  Default: no rotation.

       Option "ShadowFB" "boolean"
	      Enable or disable use of the shadow framebuffer layer.  Default: off.

       Option "SyncOnGreen" "boolean"
	      Enable or disable combining the sync signals with the green signal.  Default: off.

       Option "UseFBDev" "boolean"
	      Enable  or  disable  use of on OS-specific fb interface (and is not supported on all OSs).  See fbdevhw(4) for further information.  Default:
	      off.

       Option "VideoKey" "integer"
	      This sets the default pixel value for the YUV video overlay key.	Default: undefined.

       Option "TexturedVideo" "boolean"
	      This has XvImage support use the texture engine rather than the video overlay.  This option is only supported by G200 and  later	chips,	and
	      only at 16 and 32 bits per pixel.  Default: off.

       Option "OldDmaInit" "boolean"
	      This  forces  the  driver  to use the old DMA initialization path for DRI.  Use this option only to support a older version of the DRI driver
	      with a newer DRM (version 3.2 or later).	This option also disables the use of direct rendering on PCI cards.  Default: off.

       Option "ForcePciDma" "boolean"
	      This forces the use of PCI DMA even if AGP DMA could be used.  This option is primarily intended for testing purposes, but it could  also  be
	      used on systems with a buggy or poorly function AGP implementation.  Default: off.

SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

AUTHORS
       Authors	include: Radoslaw Kapitan, Mark Vojkovich, and also David Dawes, Guy Desbief, Dirk Hohndel, Doug Merritt, Andrew E. Mileski, Andrew van der
       Stock, Leonard N. Zubkoff, Andrew C. Aitchison.



X Version 11							   xf86-video-mga 1.4.11							     mga(4)
