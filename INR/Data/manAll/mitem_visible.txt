menu_visible(3MENU)															menu_visible(3MENU)



NAME
       menu_visible - check visibility of a menu item

SYNOPSIS
       #include <menu.h>
       bool item_visible(const ITEM *item);

DESCRIPTION
       A  menu	item  is  visible when it is in the portion of a posted menu that is mapped onto the screen (if the menu is scrollable, in particular, this
       portion will be smaller than the whole menu).

SEE ALSO
       ncurses(3NCURSES), menu(3MENU).

NOTES
       The header file <menu.h> automatically includes the header file <curses.h>.

PORTABILITY
       These routines emulate the System V menu library.  They were not supported on Version 7 or BSD versions.

AUTHORS
       Juergen Pfeifer.  Manual pages and adaptation for new curses by Eric S. Raymond.



																	menu_visible(3MENU)
