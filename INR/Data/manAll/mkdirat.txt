MKDIRAT(2)							 Linux Programmer's Manual							 MKDIRAT(2)



NAME
       mkdirat - create a directory relative to a directory file descriptor

SYNOPSIS
       #define _ATFILE_SOURCE
       #include <fcntl.h> /* Definition of AT_* constants */
       #include <sys/stat.h>

       int mkdirat(int dirfd, const char *pathname, mode_t mode);

DESCRIPTION
       The mkdirat() system call operates in exactly the same way as mkdir(2), except for the differences described in this manual page.

       If  the	pathname  given  in pathname is relative, then it is interpreted relative to the directory referred to by the file descriptor dirfd (rather
       than relative to the current working directory of the calling process, as is done by mkdir(2) for a relative pathname).

       If pathname is relative and dirfd is the special value AT_FDCWD, then pathname is interpreted relative to the current working directory of the call‐
       ing process (like mkdir(2)).

       If pathname is absolute, then dirfd is ignored.

RETURN VALUE
       On success, mkdirat() returns 0.  On error, -1 is returned and errno is set to indicate the error.

ERRORS
       The same errors that occur for mkdir(2) can also occur for mkdirat().  The following additional errors can occur for mkdirat():

       EBADF  dirfd is not a valid file descriptor.

       ENOTDIR
	      pathname is relative and dirfd is a file descriptor referring to a file other than a directory.

VERSIONS
       mkdirat() was added to Linux in kernel 2.6.16.

CONFORMING TO
       POSIX.1-2008.

NOTES
       See openat(2) for an explanation of the need for mkdirat().

SEE ALSO
       mkdir(2), openat(2), path_resolution(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2008-08-21								 MKDIRAT(2)
