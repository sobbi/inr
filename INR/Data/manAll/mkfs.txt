MKFS(8) 																	    MKFS(8)



NAME
       mkfs - build a Linux file system

SYNOPSIS
       mkfs [-V] [-t fstype] [fs-options] filesys [blocks]

DESCRIPTION
       mkfs  is  used  to  build  a  Linux  file  system  on  a device, usually a hard disk partition.	filesys is either the device name (e.g.  /dev/hda1,
       /dev/sdb2), or a regular file that shall contain the file system.  blocks is the number of blocks to be used for the file system.

       The exit code returned by mkfs is 0 on success and 1 on failure.

       In actuality, mkfs is simply a front-end for the various file system builders (mkfs.fstype) available under Linux.  The file system-specific builder
       is  searched for in a number of directories like perhaps /sbin, /sbin/fs, /sbin/fs.d, /etc/fs, /etc (the precise list is defined at compile time but
       at least contains /sbin and /sbin/fs), and finally in the directories listed in the PATH environment variable.  Please see the file  system-specific
       builder manual pages for further details.

OPTIONS
       -V     Produce verbose output, including all file system-specific commands that are executed.  Specifying this option more than once inhibits execu‐
	      tion of any file system-specific commands.  This is really only useful for testing.

       -t fstype
	      Specifies the type of file system to be built.  If not specified, the default file system type (currently ext2) is used.

       fs-options
	      File system-specific options to be passed to the real file system builder.  Although not guaranteed, the following options are  supported  by
	      most file system builders.

       -c     Check the device for bad blocks before building the file system.

       -l filename
	      Read the bad blocks list from filename

       -v     Produce verbose output.

BUGS
       All  generic  options  must precede and not be combined with file system-specific options.  Some file system-specific programs do not support the -v
       (verbose) option, nor return meaningful exit codes.  Also, some file system-specific programs do  not  automatically  detect  the  device  size	and
       require the blocks parameter to be specified.

AUTHORS
       David Engel (david@ods.com)
       Fred N. van Kempen (waltje@uwalt.nl.mugnet.org)
       Ron Sommeling (sommel@sci.kun.nl)
       The manual page was shamelessly adapted from Remy Card's version for the ext2 file system.

SEE ALSO
       fs(5),	badblocks(8),  fsck(8),  mkdosfs(8),  mke2fs(8),  mkfs.bfs(8),	mkfs.ext2(8),  mkfs.ext3(8),  mkfs.minix(8),  mkfs.msdos(8),  mkfs.vfat(8),
       mkfs.xfs(8), mkfs.xiafs(8)

AVAILABILITY
       The mkfs command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



Version 1.9								  Jun 1995								    MKFS(8)
