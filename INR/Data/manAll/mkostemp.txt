MKSTEMP(3)							 Linux Programmer's Manual							 MKSTEMP(3)



NAME
       mkstemp, mkostemp - create a unique temporary file

SYNOPSIS
       #include <stdlib.h>

       int mkstemp(char *template);

       int mkostemp (char *template, int flags);

   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):

       mkstemp(): _BSD_SOURCE || _SVID_SOURCE || _XOPEN_SOURCE >= 500
       mkostemp(): _GNU_SOURCE

DESCRIPTION
       The  mkstemp() function generates a unique temporary filename from template, creates and opens the file, and returns an open file descriptor for the
       file.

       The last six characters of template must be "XXXXXX" and these are replaced with a string that makes the filename unique.  Since it  will  be  modi‐
       fied, template must not be a string constant, but should be declared as a character array.

       The  file  is created with permissions 0600, that is, read plus write for owner only.  (In glibc versions 2.06 and earlier, the file is created with
       permissions 0666, that is, read and write for all users.)  The returned file descriptor provides both read and write access to the file.   The  file
       is opened with the open(2) O_EXCL flag, guaranteeing that the caller is the process that creates the file.

       mkostemp() is like mkstemp(), with the difference that flags as for open(2) may be specified in flags (e.g., O_APPEND, O_SYNC).

RETURN VALUE
       On success, these functions return the file descriptor of the temporary file.  On error, -1 is returned, and errno is set appropriately.

ERRORS
       EEXIST Could not create a unique temporary filename.  Now the contents of template are undefined.

       EINVAL The last six characters of template were not XXXXXX.  Now template is unchanged.

       These functions may also fail with any of the errors described for open(2).

VERSIONS
       mkostemp() is available since glibc 2.7.

CONFORMING TO
       mkstemp(): 4.3BSD, POSIX.1-2001.  mkostemp(): is a glibc extension.

NOTES
       The old behavior of creating a file with mode 0666 may be a security risk, especially since other Unix flavors use 0600, and somebody might overlook
       this detail when porting programs.

       More generally, the POSIX specification of mkstemp() does not say anything about file modes, so the application should make sure its file mode  cre‐
       ation mask (see umask(2)) is set appropriately before calling mkstemp() (and mkostemp()).

       The prototype for mktemp() is in <unistd.h> for libc4, libc5, glibc1; glibc2 follows POSIX.1 and has the prototype in <stdlib.h>.

SEE ALSO
       mkdtemp(3), mktemp(3), tempnam(3), tmpfile(3), tmpnam(3)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



GNU									 2008-06-19								 MKSTEMP(3)
