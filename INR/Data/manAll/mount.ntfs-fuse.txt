NTFSMOUNT(8)																       NTFSMOUNT(8)



NAME
       ntfsmount - Read/Write userspace NTFS driver.

SYNOPSIS
       ntfsmount device mount_point [-o options]
       mount -t fuse.ntfs device mount_point [-o options]

       /etc/ftsab entry:
       device mount_point fuse.ntfs options 0 0

DESCRIPTION
       ntfsmount is a read/write userspace NTFS filesystem driver. Technically it connects FUSE with libntfs.

       ntfsmount features:
	      · Create/Delete/Move files and directories.
	      · Hard link files.
	      · Read and write to normal and sparse files.
	      · Read compressed and encrypted files.
	      · Access to special Interix files (symlinks, devices, FIFOs).
	      · List/Read/Write/Add/Remove named data streams.
	      · Supports Linux, FreeBSD, NetBSD and Mac OS X.

OPTIONS
       ntfsmount supports most of options that mount and FUSE accepts (see "man 8 mount" and FUSE documentation for them). Additionally ntfsmount have some
       unique to it options, below is a summary of them.

       silent, nosilent
	      silent option makes ntfsmount to do not return "Operation is not supported" error on chmod  and  chown  operations  (this  option  is  on  by
	      default).  nosilent cancels this.

       locale=value
	      You  can	set  locale  with  this  option. It's useful if locale environment variables are not set before partitions from /etc/fstab had been
	      mounted. Try submitting this option if you are experience problems with displaying national characters in filenames.

       uid=value, gid=value
	      Set the owner and the group of files and directories. The values are numerical.  The defaults are the uid and gid of the current process.

       umask=value, dmask=value, fmask=value
	      Set the bitmask of the file and directory permissions that are not present.  The value is given in octal. Instead of specifying  umask  which
	      applies both to files and directories, fmask applies only to files and dmask only to directories.

       case_insensitive
	      Make ntfsmount treat filenames in POSIX names as case insensitive.  See FILENAME NAMESPACES section for details.

       no_def_opts
	      By  default  ntfsmount acts as some useful options were passed to it (you can get list of this options by running ntfsmount without any argu‐
	      ments). Submitting this option will cancel such behaviour.

       noblkdev
	      By default ntfsmount tries to mount block devices with blkdev FUSE option if it have enough privileges. Submit this option  if  blkdev  mount
	      does not work for you for some reasons.

       force  Force mount even if errors occurred. Use this option only if you know what are you doing and don't cry about data loss.

       relatime, norelatime
	      Update  inode  access  times relative to modify or change time.  Access time is only updated if the previous access time was earlier than the
	      current modify or change time. (Similar to noatime, but doesn't break mutt or other applications that need to know if a file  has  been  read
	      since the last time it was modified.)

       streams_interface=value
	      This  option  controls how the user can access named data streams.  It can be set to, one of none, windows or xattr. See DATA STREAMS section
	      for details.

       debug  Makes ntfsmount to not detach from terminal and print a lot of debug output from libntfs and FUSE.

       no_detach
	      Same as above but with less debug output.

FILENAME NAMESPACES
       There are exist several namespaces for filenames in NTFS: DOS, Win32 and POSIX.	Names in DOS and Win32 namespaces are case insensitive,  but  names
       in  POSIX  namespace are case sensitive. By default windows creates filenames in DOS and Win32 namespaces (with exception for hard links), but ntfs‐
       mount always creates files in POSIX namespace. Note: you can create several files that differs only in case in one  directory  with  ntfsmount,	but
       windows applications may be confused by this.

DATA STREAMS
       All  data on NTFS is stored in streams.	Every file has exactly one unnamed data stream and can have many named data streams.  The size of a file is
       the size of its unnamed data stream.  Windows applications don't, consistently, allow you to read named data streams, so you are recommended to	use
       tools like FAR, or utilities from Cygwin.

       By default or if "streams_interface=none" option was passed, ntfsmount will only read the unnamed data stream.

       By  using  the  option  "streams_interface=windows", you will be able to read any named data streams, simply by specifying the stream's name after a
       colon.  Named data streams act like normals files, so you can read from them, write to them and even delete them (using rm). You can  list  all	the
       named data streams a file has by getting the "ntfs.streams.list" extended attribute. Some examples:

	      cat some.mp3:artist
	      rm some.mp3:album
	      echo Sympho Black Metal > some.mp3:genre
	      getfattr -n ntfs.streams.list some.mp3

       If  streams_interface  option is set to xattr, then the named data streams are mapped to xattrs and user can manipulate them using getfattr and set‐
       fattr utilities. Eg.:

	      setfattr -n user.artist -v "Some Artist" some.mp3
	      getfattr -d some.mp3

ALLOWED CHARACTERS
       Win32 does not allow characters like '<', '>', '*', '?' and so on in the filenames, but NTFS supports any characters except  '\0'  (NULL)  and  '/'.
       You  can  create filenames with any allowed by NTFS characters using ntfsmount, but aware, you will not be able to access files with denied by Win32
       characters from windows.

ACCESS HANDLING AND SECURITY
       By default, files and directories are owned by the user and group of the mounting process and everybody has full read, write, execution	and  direc‐
       tory  browsing  permissions.   If  you want to use permissions handling then use the uid and/or the gid options together with the umask or fmask and
       dmask options.

       Windows users have full access to the files created by ntfsmount.

EXAMPLES
       Mount /dev/hda1 to /mnt/ntfs using ntfsmount submiting locale option:

	      ntfsmount /dev/hda1 /mnt/ntfs -o locale=be_BY.UTF-8

       /etc/fstab entry for above:

	      /dev/hda1 /mnt/ntfs fuse.ntfs locale=be_BY.UTF-8 0 0

       Umount /mnt/ntfs:

	      fusermount -u /mnt/ntfs

BUGS
       If you find a bug please send an email describing the problem to the development team:
       linux-ntfs-dev@lists.sourceforge.net

AUTHORS
       ntfsmount was written by Yura Pakhuchiy, with contributions from Yuval Fledel and Szabolcs Szakacsits.

DEDICATION
       With love to Marina Sapego.

THANKS
       Many thanks to Miklos Szeredi for advice and answers about FUSE.

AVAILABILITY
       ntfsmount is part of the ntfsprogs package and is available from:
       http://www.linux-ntfs.org/content/view/19/37

       The manual pages are available online at:
       http://man.linux-ntfs.org/

       Additional up-to-date information can be found furthermore at:
       http://wiki.linux-ntfs.org/doku.php?id=ntfsmount

SEE ALSO
       Read libntfs(8) for details how to access encrypted files.

       libntfs(8), ntfsprogs(8), attr(5), getfattr(1)



ntfsprogs 2.0.0 						       September 2007							       NTFSMOUNT(8)
