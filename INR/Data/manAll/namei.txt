NAMEI(1)																	   NAMEI(1)



NAME
       namei - follow a pathname until a terminal point is found

SYNOPSIS
       namei [options] pathname...

DESCRIPTION
       Namei uses its arguments as pathnames to any type of Unix file (symlinks, files, directories, and so forth).  Namei then follows each pathname until
       a terminal point is found (a file, directory, char device, etc).  If it finds a symbolic link, we show the link, and start following  it,  indenting
       the output to show the context.

       This program is useful for finding a "too many levels of symbolic links" problems.

       For each line output, namei outputs a the following characters to identify the file types found:

	  f: = the pathname we are currently trying to resolve
	   d = directory
	   l = symbolic link (both the link and it's contents are output)
	   s = socket
	   b = block device
	   c = character device
	   p = FIFO (named pipe)
	   - = regular file
	   ? = an error of some kind

       Namei prints an informative message when the maximum number of symbolic links this system can have has been exceeded.

OPTIONS
       -l, --long
	      Use a long listing format (same as -m -o -v).

       -m, --modes
	      Show the mode bits of each file type in the style of ls(1), for example 'rwxr-xr-x'.

       -o, --owners
	      Show owner and group name of each file.

       -n, --nosymlinks
	      Don't follow symlinks.

       -v, --vertical
	      Vertical align of modes and owners.

       -x, --mountpoints
	      Show mount point directories with a 'D', rather than a 'd'.

AUTHOR
       The original namei program was written by Roger Southwick <rogers@amadeus.wr.tek.com>.

       The program was re-written by Karel Zak <kzak@redhat.com>.

BUGS
       To be discovered.

SEE ALSO
       ls(1), stat(1)

AVAILABILITY
       The namei command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



									   Local								   NAMEI(1)
