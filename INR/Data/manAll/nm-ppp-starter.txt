NM-PPP-SERVICE(1)															  NM-PPP-SERVICE(1)



NAME
       nm-ppp-service - PPP connection helper for NetworkManager

SYNOPSIS
       nm-ppp-starter [options]...

DESCRIPTION
       nm-ppp-service  is  a  helper for the NetworkManager(1) program.  NetworkManager will launch this service automatically as necessary.  For debugging
       purposes, you can launch the helper yourself, and watch its output.

OPTIONS
       None.

SEE ALSO
       NetworkManager(1),

AUTHOR
       nm-ppp-service is part of the PPP plugin for NetworkManager, written by Anthony Mee <eemynotna at gmail.com>.

       This manual page was written by Craig Box <ubuntu at dubculture.co.nz>, for Ubuntu (but may be used freely by anyone).



								      August  6, 2006							  NM-PPP-SERVICE(1)
