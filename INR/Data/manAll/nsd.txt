NSD(8)									NSD(8)



NAME
       nsd - MICO name service server

SYNOPSIS
       nsd

DESCRIPTION
       nsd  implements the COS compliant naming service. To use it you have to
       run micod, create an entry for the naming service in the implementation
       repository  using  the imr tool and pass the clients the address of the
       naming service using the -ORBNamingAddr option.

OPTIONS
       Only standard ORB options.

FILES
       ~/.micorc

SEE ALSO
       MICO Reference Manual, rsh(1),  micod(8),  imr(1),  micorc(5),  idl(1),
       nsadmin(1)

COPYRIGHT
       Copyright (C) 1997, Kai-Uwe Sattler

AUTHOR
       Kai-Uwe Sattler



				 April 8 1997				NSD(8)
