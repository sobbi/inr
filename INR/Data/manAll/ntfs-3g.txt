NTFS-3G(8)																	 NTFS-3G(8)



NAME
       ntfs-3g - Third Generation Read/Write NTFS Driver

SYNOPSIS
       ntfs-3g volume mount_point [-o option[,...]]
       mount -t ntfs-3g volume mount_point [-o option[,...]]

DESCRIPTION
       ntfs-3g	is an NTFS driver, which can create, remove, rename, move files, directories, hard links, and streams; it can read and write files, includ‐
       ing streams and sparse files; it can handle special files like symbolic links, devices, and FIFOs; moreover it can also read  and  create  transpar‐
       ently compressed files.

       The volume to be mounted can be either a block device or an image file.

   Access Handling and Security
       By  default,  files  and directories are owned by the effective user and group of the mounting process and everybody has full read, write, execution
       and directory browsing permissions.  You can also assign permissions to a single user by using the uid and/or the  gid  options	together  with	the
       umask, or fmask and dmask options.

       Doing so, Windows users have full access to the files created by ntfs-3g.

       But,  by defining a Windows-to-Linux user mapping in the file .NTFS-3G/UserMapping, you can benefit from the full ownership and permissions features
       as defined by Posix and those ownership and permissions will be applied to Windows users and conversely.

       If ntfs-3g is set setuid-root then non-root users will be also able to mount volumes.

   Windows Filename Compatibility
       NTFS supports several filename namespaces: DOS, Win32 and POSIX. While the ntfs-3g driver handles all of them, it always creates new  files  in	the
       POSIX  namespace  for maximum portability and interoperability reasons.	This means that filenames are case sensitive and all characters are allowed
       except '/' and '\0'. This is perfectly legal on Windows, though some application may get confused. If you find so  then	please	report	it  to	the
       developer of the relevant Windows software.

   Alternate Data Streams (ADS)
       NTFS  stores  all  data	in streams. Every file has exactly one unnamed data stream and can have many named data streams.  The size of a file is the
       size of its unnamed data stream.  By default, ntfs-3g will only read the unnamed data stream.

       By using the options "streams_interface=windows", you will be able to read any named data streams, simply by specifying the stream's  name  after  a
       colon.  For example:

	      cat some.mp3:artist

       Named data streams act like normal files, so you can read from them, write to them and even delete them (using rm).  You can list all the named data
       streams a file has by getting the "ntfs.streams.list" extended attribute.

OPTIONS
       Below is a summary of the options that ntfs-3g accepts.

       uid=value and gid=value
	      Set the owner and the group of files and directories. The values are numerical.  The defaults are the uid and gid of the current process.

       umask=value
	      Set the  bitmask of the file and directory permissions that are not present. The value is given in octal. The default value is 0 which  means
	      full access to everybody.

       fmask=value
	      Set  the	 bitmask of the file permissions that are not present.	The value is given in octal. The default value is 0 which means full access
	      to everybody.

       dmask=value
	      Set the  bitmask of the directory permissions that are not present. The value is given in octal. The default value  is  0  which	means  full
	      access to everybody.

       usermapping=file-name
	      Use  file  file-name as the user mapping file instead of the default .NTFS-3G/UserMapping. If file-name defines a full path, the file must be
	      located on a partition previously mounted. If it defines a relative path, it is interpreted relative to the  root  of  NTFS  partition  being
	      mounted.

	      When a user mapping file is defined, the options uid=, gid=, umask=, fmask=, dmask= and dsilent= are ignored.

       default_permissions
	      Use  standard  access  control.  This  option requires either a user mapping file to be present, or the options uid= and gid= of a user to be
	      defined. This option is set by default when a user mapping file or an ownership related option is present.

       inherit
	      When creating a new file, set its initial ownership and protections according to inheritance rules defined in parent directory.  These  rules
	      deviate  from  Posix  specifications,  but  yield a better Windows compatibility. A valid user mapping file is required for this option to be
	      effective.

       ro     Mount filesystem read-only. Useful if Windows is hibernated or the NTFS journal file is unclean.

       locale=value
	      This option can be useful when wanting a language specific locale environment.  It is however discouraged as it leads to files with  untrans‐
	      latable chars to not be visible. Please see more information about this topic at http://ntfs-3g.org/support.html#locale

       force  Force  the  mounting  even  if the NTFS logfile is unclean. The logfile will be unconditionally cleared. Use this option with caution and for
	      your own responsibility.

       remove_hiberfile
	      Unlike in case of read-only mount, the read-write mount is denied if the NTFS volume is hibernated. One needs either to  resume  Windows	and
	      shutdown	it properly, or use this option which will remove the Windows hibernation file. Please note, this means that the saved Windows ses‐
	      sion will be completely lost. Use this option for your own responsibility.

       atime, noatime, relatime
	      The atime option updates inode access time for each access.

	      The noatime option disables inode access time updates which can speed up file operations and prevent sleeping (notebook)	disks  spinning  up
	      too often thus saving energy and disk lifetime.

	      The  relatime  option is very similar to noatime.  It  updates inode access times relative to modify or change time.  The access time is only
	      updated if the previous access time was earlier than the current modify or change time. Unlike noatime this option doesn't break applications
	      that need to know if a file has been read since the last time it was modified.  This is the default behaviour.

       show_sys_files
	      Show  the  system files in directory listings.  Otherwise the default behaviour is to hide the system files.  Please note that even when this
	      option is specified, "$MFT" may not be visible due to a glibc bug.  Furthermore, irrespectively of show_sys_files, all files  are  accessible
	      by name, for example you can always do "ls -l '$UpCase'".

       allow_other
	      This  option overrides the security measure restricting file access to the user mounting the filesystem. This option is only allowed to root,
	      but this restriction can be overridden by the 'user_allow_other' option in the /etc/fuse.conf file.

       max_read=value
	      With this option the maximum size of read operations can be set.	The default is infinite.  Note that the size of read  requests	is  limited
	      anyway to 32 pages (which is 128kbyte on i386).

       silent Do  nothing  on  chmod  and chown operations, but do not return error when the user mapping file required by these operations is not defined.
	      This option is on by default.

       no_def_opts
	      By default ntfs-3g acts as "silent" was passed to it, this option cancel this behaviour.

       streams_interface=value
	      This option controls how the user can access Alternate Data Streams (ADS) or in other words, named data streams. It can be  set  to,  one  of
	      none,  windows  or xattr. If the option is set to none, the user will have no access to the named data streams.  If it's set to windows, then
	      the user can access them just like in Windows (eg. cat file:stream). If it's set to xattr, then the named data streams are mapped  to  xattrs
	      and user can manipulate them using {get,set}fattr utilities. The default is xattr.

       user_xattr
	      Same as streams_interface=xattr.

       efs_raw
	      This  option  should  only  be used in backup or restore situation.  It changes the apparent size of files and the behavior of read and write
	      operation so that encrypted files can be saved and restored without being decrypted. The user.ntfs.efsinfo extended attribute has also to  be
	      saved and restored for the file to be decrypted.

       debug  Makes ntfs-3g to not detach from terminal and print a lot of debug output from libntfs-3g and FUSE.

       no_detach
	      Same as above but with less debug output.

USER MAPPING
       NTFS  uses specific ids to record the ownership of files instead of the uid and gid used by Linux. As a consequence a mapping between the ids has to
       be defined for ownerships to be recorded into NTFS and recognized.

       By default this mapping is fetched from the file .NTFS-3G/UserMapping located in the NTFS partition. The option usermapping= may be used  to  define
       another location.

       Each  line  in  the  user mapping file defines a mapping. It is organized in three fields separated by colons. The first field identifies a uid, the
       second field identifies a gid and the third one identifies the corresponding NTFS id, known as a SID. The uid and the gid are optional and  defining
       both of them for the same SID is not recommended.

       If no interoperation with Windows is needed, a single default mapping with no uid and gid can be used. Just copy the example below and replace the 9
       and 10-digit numbers by any number not greater than 4294967295.

	      ::S-1-5-21-3141592653-589793238-462643383-10000

       If interoperation with Windows is needed, the mapping has to be defined for each user and group known in both system, and the SIDs used	by  Windows
       has to be collected. This will lead to a user mapping file like :

	      john::S-1-5-21-3141592653-589793238-462643383-1008					 mary::S-1-5-21-3141592653-589793238-462643383-1009
	      :smith:S-1-5-21-3141592653-589793238-462643383-513 ::S-1-5-21-3141592653-589793238-462643383-10000


       The utility ntfs-3g.usermap may be used to create the user mapping file.

EXAMPLES
       Mount /dev/sda1 to /mnt/windows:

	      ntfs-3g /dev/sda1 /mnt/windows

       or

	      mount -t ntfs-3g /dev/sda1 /mnt/windows

       Read-only mount /dev/sda5 to /home/user/mnt and make user with uid 1000 to be the owner of all files:

	      ntfs-3g /dev/sda5 /home/user/mnt -o ro,uid=1000

       /etc/fstab entry for the above:

	      /dev/sda5 /home/user/mnt ntfs-3g ro,uid=1000 0 0

       Unmount /mnt/windows:

	      umount /mnt/windows


EXIT CODES
       To facilitate the use of the ntfs-3g driver in scripts, an exit code is returned to give an indication of the mountability status of a volume. Value
       0 means success, and all other ones mean an error. The unique error codes are documented in the ntfs-3g.probe(8) manual page.

KNOWN ISSUES
       Please see

	      http://www.tuxera.com/support/

       for common questions and known issues.  If you would find a new one in the latest release of the software then please send an email describing it in
       detail. You can contact the development team on the ntfs-3g-devel@lists.sf.net address.

AUTHORS
       ntfs-3g was based on and a major improvement to ntfsmount and libntfs which were written by Yura Pakhuchiy and the Linux-NTFS team. The improvements
       were made, the ntfs-3g project was initiated and currently led by long time Linux-NTFS team developer Szabolcs Szakacsits (szaka@tuxera.com).

THANKS
       Several	people	made heroic efforts, often over five or more years which resulted the ntfs-3g driver. Most importantly they are Anton Altaparmakov,
       Jean-Pierre André, Richard Russon, Szabolcs Szakacsits, Yura Pakhuchiy, Yuval Fledel, and the author of the groundbreaking FUSE filesystem  develop‐
       ment framework, Miklos Szeredi.

SEE ALSO
       ntfs-3g.probe(8), ntfsprogs(8), attr(5), getfattr(1)



ntfs-3g 2010.3.6						       February 2010								 NTFS-3G(8)
