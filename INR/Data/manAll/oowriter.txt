ooffice(1)							       User Commands								 ooffice(1)



Name
       ooffice - OpenOffice.org office suite

SYNOPSIS
       ooffice	[-accept=accept-string]  [-base]  [-calc]  [-display  display]	[-draw] [-global] [-headless] [-help|-h|-?] [-impress] [-invisible] [-math]
       [-minimized] [-n filename] [-nodefault] [-nolockcheck] [-nologo] [-norestore] [-o filename] [-p filename...]  [-pt printername filename...]   [-show
       presentation] [-unaccept=accept-string] [-terminate_after_init] [-view filename] [-web] [-writer]  [filename...]
       oobase
       oocalc
       oodraw
       oofromtemplate
       ooimpress
       oomath
       ooweb
       oowriter


DESCRIPTION
       OpenOffice.org is a fully featured office suite for the daily use with all features you would normally expect in a office suite.

       This script runs OpenOffice with the arguments <args> given on the command line.

       There are also some wrapper scripts in /usr/bin which run the wanted OpenOffice.org module.


OPTIONS
       filename...   Files  to operate on. Opening them is the default behavior, which can be influenced by command line arguments. -p will print the files
       instead.

       -accept=accept-string
	      Notifies the OpenOffice.org software that upon the creation of "UNO Acceptor Threads", a "UNO Accept String" will be used.

       -base  Starts with the database wizard.

       -calc  Starts with an empty Calc document.

       -display display
	      This option specifies the X server to contact; see X(7)

       -draw  Starts with an empty Draw document.

       -global
	      Starts with an empty Global document.

       -headless
	      Starts in "headless mode", which allows using the application without user interface.

	      This special mode can be used when the application is controlled by external clients via the API.

       -help|-h|-?
	      Lists the available OOo-native command line parameters to the console.

       -impress
	      Starts with an empty Impress document.

       -invisible
	      Starts in invisible mode.

	      Neither the start-up logo nor the initial program window will be visible.  However, the OpenOffice.org software can be controlled  and  docu‐
	      ments and dialogs opened via the API.

	      When  the OpenOffice.org software has been started with this parameter, it can only be ended using the taskmanager (Windows) or the kill com‐
	      mand (UNIX based systems).

	      It cannot be used in conjunction with -quickstart.

       -math  Starts with an empty Math document.

       -minimized
	      Keep the splash screen minimized.

       -n filename
	      Creates a new document using filename as a template.

       -nodefault
	      Starts the application without any window opened for the first time.  A window appears if you start the office next time.

	      It is used together with the option -nologo by quickstarters. Note that the option -quickstart has not been longer  supported  since  OpenOf‐
	      fice.org 2.0.0.

       -nolockcheck
	      Disables the check for remote instances using the installation.

       -nologo
	      Disables the splash screen at program start.

       -norestore
	      Disables	restart and file recovery after a system crash. It is possible OOo will try to restore a file it keeps crashing on, if that happens
	      -norestore is the only way to start OOo.

       -o filename
	      Opens filename for editing, even if it is a template.

       -p filename...
	      Prints the given files to the default printer and ends. The splash screen does not appear.

	      If the file name contains spaces, then it must be enclosed in quotation marks.

       -pt printername filename...
	      Prints the given files to the printer printername and ends. The splash screen does not appear.

	      If the file name contains spaces, then it must be enclosed in quotation marks.

       -show presentation
	      Starts with the given Impress file and starts the presentation. Enters edit mode after the presentation.

       -unaccept=accept-string
	      Closes an acceptor that was created with -accept option.

	      Use -unaccept=all to close all open acceptors.

       -terminate_after_init
	      The office terminates after it registers some UNO services. The office doesn't show the intro bitmap during startup.

       -view filename
	      Creates a temporary copy of the given file and opens it read-only.

       -web   Starts with an empty HTML document.

       -writer
	      Starts with an empty Writer document.


TROUBLESHOOTING PROBLEMS
       See http://en.opensuse.org/Bugs:OOo for more details about how to report bugs in OpenOffice.org.

SEE ALSO
       http://go-oo.org/

AUTHOR
       This manual page was created by Rene Engelhard <rene@debian.org> for the Debian GNU/Linux Distribution, because the original package does  not  have
       one. It was updated for Novell by Petr Mladek <petr.mladek@novell.com>.



OpenOffice.org								 2008-11-19								 ooffice(1)
