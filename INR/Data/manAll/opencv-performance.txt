OPENCV-PERFORMANCE(1)						       User Commands						      OPENCV-PERFORMANCE(1)



NAME
       opencv-performance - evaluate the performance of the classifier



SYNOPSIS
       opencv-performance [options]



DESCRIPTION
       opencv-performance  evaluates  the performance of the classifier. It takes a collection of marked up test images, applies the classifier and outputs
       the performance, i.e. number of found objects, number of missed objects, number of false alarms and other information.

       When there is no such collection available test samples may be created from single object image by the opencv-createsamples(1) utility.	The  scheme
       of test samples creation in this case is similar to training samples

       In the output, the table should be read:

       'Hits' shows the number of correctly found objects

       'Missed'
	      shows the number of missed objects (must exist but are not found, also known as false negatives)

       'False'
	      shows the number of false alarms (must not exist but are found, also known as false positives)



OPTIONS
       opencv-performance supports the following options:


       -data classifier_directory_name
	      The directory, in which the classifier can be found.


       -info collection_file_name
	      File with test samples description.


       -maxSizeDiff max_size_difference
	      Determine the size criterion of reference and detected coincidence.  The default is 1.500000.


       -maxPosDiff max_position_difference
	      Determine the position criterion of reference and detected coincidence.  The default is 0.300000.


       -sf scale_factor
	      Scale the detection window in each iteration. The default is 1.200000.


       -ni    Don't save detection result to an image. This could be useful, if collection_file_name contains paths.


       -nos number_of_stages
	      Number of stages to use. The default is -1 (all stages are used).


       -rs roc_size
	      The default is 40.


       -h sample_height
	      The sample height (must have the same value as used during creation).  The default is 24.


       -w sample_width
	      The sample width (must have the same value as used during creation).  The default is 24.


       The same information is shown, if opencv-performance is called without any arguments/options.



EXAMPLES
       To create training samples from one image applying distortions and show the results:

	      opencv-performance -data trainout -info tests.dat



SEE ALSO
       opencv-createsamples(1), opencv-haartraing(1)

       More information and examples can be found in the OpenCV documentation.



AUTHORS
       This manual page was written by Daniel Leidert <daniel.leidert@wgdd.de> for the Debian project (but may be used by others).



OpenCV									  May 2008						      OPENCV-PERFORMANCE(1)
