orca(1) 																	    orca(1)



NAME
       orca - is a scriptable screen reader and magnifier

SYNOPSIS
       orca [option...]

DESCRIPTION
       orca  is  a  screen reader for people with visual impairments, it provides alternative access to the desktop by using speech synthesis, braille, and
       magnification.

       In addition, orca only provides access to applications/toolkits that support the assistive technology  service  provide	interface  (AT-SPI),  which
       include GTK, Mozilla, Firefox, Evolution, OpenOffice, StarOffice, Java/Swing, etc.

       WARNING: suspending orca, e.g. by pressing Control-Z, from an AT-SPI enabled shell (such as gnome-terminal), can also suspend the desktop until Orca
       is killed.

OPTIONS
       -s, --setup, --gui-setup
	      When starting orca, bring up the GUI configuration dialog.

       -t, --text-setup
	      When starting orca, initiate the text-based configuration.

       -n, --no-setup
	      When starting orca, force the application to be started without configuration, even though it might have	needed	it.  This  is  useful  when
	      starting orca via something like gdm.

       -u, --user-prefs-dir=dirname
	      When starting orca, use dirname as an alternate directory for the user preferences.

       -e, --enable=speech|braille|braille-monitor|magnifier|main-window
	      When starting orca, force the enabling of the supplied options.

       -d, --disable=speech|braille|braille-monitor|magnifier|main-window
	      When starting orca, force the disabling of the supplied options.

       -l, --list-apps
	      Prints  the  names  of  all the currently running applications.  This is used primarily for debugging purposes to see if orca can talk to the
	      accessibility infrastructure.  Note that if orca is already running, this will not kill the other orca process.  It will just list  the  cur‐
	      rently running applications, and you will see orca listed twice: once for the existing orca and once for this instance.

       --debug
	      Enables  debug  output  for  orca and sends all debug output to a file with a name of the form 'debug-YYYY-MM-DD-HH:MM:SS.out' in the current
	      directory.  The YYYY-MM-DD-HH:MM:SS portion will be replaced with the current date and time.

       --debug-file=filename
	      Enables debug output for orca and sends all debug output to the given filename.

       -v, --version
	      outputs orca version number and exits.

       -?, -h, --help
	      displays orca help and exits.

       --replace
	      Replace a currently running orca process.  By default, if orca detects an existing orca process for the same session, it will not start a new
	      orca process.  This option will kill and cleanup after any existing orca process and then start a new orca in its place.

       -q, --quit
	      Kill and cleanup after any existing Orca process.


KEYBOARD SETTINGS
       Orca  provides  two  keyboard modes, Desktop and Laptop keyboard layout. The Orca_Modifier key is Insert in desktop keyboard layout and Caps_Lock in
       laptop keyboard layout.

       Orca uses default GNOME keyboard shortcuts to navigate the desktop and interact with various applications.  The	flat  review  commands	provide  an
       alternative  method  of interaction in certain inaccessible applications. It should not be confused with flat review functionality provided by other
       screen readers.


Desktop Mode
       Flat review commands

       Numpad-7 move the flat review cursor to the previous line and read it.

       Numpad-8 read the current line.

       Numpad-9 move the flat review cursor to the next line and read it.

       Numpad-4 move the flat review cursor to the previous word and read it.

       Numpad-5 read the current word.

       Numpad-6 move the flat review cursor to the next word and read it.

       Numpad-1 move the flat review cursor to the previous character and read it.

       Numpad-2 read the current character.

       Numpad-3 move the flat review cursor to the next character and read it.

       Numpad-slash perform a left mouse click at the location of the flat review cursor.

       Numpad-star perform a right mouse click at the location of the flat review cursor.

       Bookmark commands

       Alt+Insert+[1-6] assign a bookmark to a numbered slot. If a bookmark already exists in the slot it will be replaced with the new one.

       Insert+[1-6] go to the position pointed to by the bookmark bound to this numbered slot.

       Alt+Shift+[1-6]

       Insert+B and Insert+Shift+B move between the given bookmarks for the given application or page.

       Alt+Insert+B save the defined bookmarks for the current application or page.


       Miscellaneous functions

       Numpad+Plus
	'say all' command; reads from the current position of the caret to the end of the document.

       Numpad+Enter
	'Where am I' command; speaks information such as the title of the current application window, as well as the name of the control that currently has
       focus.

       Insert+H enter into orca's 'learn mode'; press Escape to exit.

       Insert+Shift+Backslash toggle live regions monitoring on and off.

       Insert+F speak font and attribute information for the current character.

       Insert+Space Launch the orca Configuration dialog.

       Insert+Ctrl+Space  reload user settings and reinitialize services as necessary. Also launches the orca Configuration dialog for the current applica‐
       tion.

       Insert+S toggle speech on and off.

       Insert+F11 toggle the reading of tables, either by single cell or whole row.

       Insert+Q quit orca.

       Debugging

       Ctrl+Alt+Insert+Home report information on the currently active script.

       Ctrl+Alt+Insert+End print a debug listing of all known applications to the console where orca is running.

       Ctrl+Alt+Insert+Page_Up print debug information about the ancestry of the object with focus.

       Ctrl+Alt+Insert+Page_Down print debug information about the hierarchy of the application with focus.


Laptop Mode
       Flat review commands

       Caps_Lock+U move the flat review cursor to the previous line and read it. Double-click to move flat review to the top of the current window.

       Caps_Lock+I read the current line. Double-click to read the current line along with formatting and capitalization details.

       Caps_Lock+O move the flat review cursor to the next line and read it. Double- click to move flat review to the bottom of the current window.

       Caps_Lock+J move the flat review cursor to the previous word and read it. Double-click to move flat review to the word above the current word.

       Caps_Lock+K read the current word. Double-click to spell the word. Triple-click to hear the word spelled phonetically.

       Caps_Lock+L move the flat review cursor to the next word and read it. Double- click to move flat review to the word below the current word.

       Caps_Lock+M move the flat review cursor to the previous character and read it. Double-click to move flat review to the end of the current line.

       Caps_Lock+Comma read the current character. Double-click to pronounce the character phonetically if it is a letter.

       Caps_Lock+Period move the flat review cursor to the next character and read it.

       Caps_Lock+7 perform a left mouse click at the location of the flat review cursor.

       Caps_Lock+8 perform a right mouse click at the location of the flat review cursor.

       Caps_Lock+8 perform a right mouse click at the location of the flat review cursor.

       Bookmark commands

       Alt+Caps_Lock+[1-6]

       add a bookmark to the numbered slot. If a bookmark already exists for the slot it will be replaced with the new one.

       Caps_Lock+[1-6] go to the position pointed to by the bookmark bound to this numbered slot.

       Alt+Shift+[1-6]

       Caps_Lock+Band Caps_Lock+Shift+B move between the given bookmarks for the given application or page.

       Alt+Caps_Lock+B save the defined bookmarks for the current application or page.

       Miscellaneous functions

       Caps_Lock+Semicolon Caps_Lock+Enter

       Caps_Lock+H enter learn mode (press Escape to exit).

       Caps_Lock+Shift+Backslash toggle live regions monitoring on and off.

       Caps_Lock+F speak font and attribute information for the current character.

       Caps_Lock+Space launch the orca Configuration dialog.

       Caps_Lock+Ctrl+Space reload user settings and reinitialize services as necessary; also launches the orca Configuration dialog for the current appli‐
       cation.

       Caps_Lock+S toggle speech on and off.

       Caps_Lock+F11 toggle the reading of tables, either by single cell or whole row.

       Caps_Lock+Q quit orca.

       Debugging

       Caps_Lock+Alt+Ctrl+Home report information on the currently active script.

       Caps_Lock+Alt+Ctrl+End prints a debug listing of all known applications to the console where orca is running.

       Caps_Lock+Alt+Ctrl+Page_Up prints debug information about the ancestry of the object with focus.

       Caps_Lock+Alt+Ctrl+Page_Down prints debug information about the object hierarchy of the application with focus.


FILES
       ~/.orca
	      Orca user preferences directory

       ~/.orca/user-settings.py
	      Orca user preferences configuration file.

       ~/.orca/orca-customizations.py
	      Orca user customizations configuration file


       ~/.orca/orca-scripts
	      Orca user orca scripts directory

       ~/.orca/bookmarks
	      Orca user bookmarks directory

       ~/.orca/app-settings
	      Orca user application specific settings directory



AUTHOR
       Orca  development  is  a  community effort led by the Sun Microsystems Inc.  Accessibility Program Office and with contributions from many community
       members.

SEE ALSO
       For more information please visit orca wiki at <http://live.gnome.org/Orca> ⟨http://live.gnome.org/Orca⟩

       The orca mailing list <http://mail.gnome.org/mailman/listinfo/orca-list> To post a message to all orca list, send a email to orca-list@gnome.org



GNOME									10 Nov 2008								    orca(1)
