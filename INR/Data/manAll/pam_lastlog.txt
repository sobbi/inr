PAM_LASTLOG(8)							      Linux-PAM Manual							     PAM_LASTLOG(8)



NAME
       pam_lastlog - PAM module to display date of last login

SYNOPSIS
       pam_lastlog.so [debug] [silent] [never] [nodate] [nohost] [noterm] [nowtmp] [noupdate] [showfailed]

DESCRIPTION
       pam_lastlog is a PAM module to display a line of information about the last login of the user. In addition, the module maintains the
       /var/log/lastlog file.

       Some applications may perform this function themselves. In such cases, this module is not necessary.

OPTIONS
       debug
	   Print debug information.

       silent
	   Don´t inform the user about any previous login, just update the /var/log/lastlog file.

       never
	   If the /var/log/lastlog file does not contain any old entries for the user, indicate that the user has never previously logged in with a welcome
	   message.

       nodate
	   Don´t display the date of the last login.

       noterm
	   Don´t display the terminal name on which the last login was attempted.

       nohost
	   Don´t indicate from which host the last login was attempted.

       nowtmp
	   Don´t update the wtmp entry.

       noupdate
	   Don´t update any file.

       showfailed
	   Display number of failed login attempts and the date of the last failed attempt from btmp. The date is not displayed when nodate is specified.

MODULE TYPES PROVIDED
       Only the session module type is provided.

RETURN VALUES
       PAM_SUCCESS
	   Everything was successful.

       PAM_SERVICE_ERR
	   Internal service module error.

       PAM_USER_UNKNOWN
	   User not known.

EXAMPLES
       Add the following line to /etc/pam.d/login to display the last login time of an user:

	       session	required  pam_lastlog.so nowtmp


FILES
       /var/log/lastlog
	   Lastlog logging file

SEE ALSO
       pam.conf(5), pam.d(5), pam(7)

AUTHOR
       pam_lastlog was written by Andrew G. Morgan <morgan@kernel.org>.



Linux-PAM Manual							 08/24/2009							     PAM_LASTLOG(8)
