PDF2PS(1)		       Ghostscript Tools		     PDF2PS(1)



NAME
       pdf2ps - Ghostscript PDF nach PostScript Konverter

SYNTAX
       pdf2ps [ Optionen ] Eingabe.pdf [Ausgabe.ps]

DESCRIPTION
       pdf2ps  benutzt	gs(1)  um die Datei "Eingabe.pdf" im Portable Document
       Format (PDF) in die Datei "Ausgabe.ps" im PostScript(tm)-Format zu kon‐
       vertieren.   Normalerweise  darf  die  Ausgabe  PostScript-Level-2-Kon‐
       strukte (nicht jedoch PostScript LanguageLevel 3) benutzen; die	-dLan‐
       guageLevel=1  Option  erzwingt  Level 1, während -dLanguageLevel=3 Lan‐
       guageLevel-3-Konstrukte für die Ausgabe erlaubt.

DATEIEN
       Starten Sie "gs -h" um den Ort der Ghostscript-Dokumentation auf  Ihrem
       System zu ermitteln, wo Sie weitere Datails finden.

VERSION
       Dieses  Dokument  wurde zuletzt für Ghostscript Version 7.21 durchgese‐
       hen.

AUTOR
       Artifex Software, Inc. sind die Hauptautoren of Ghostscript.



7.21				  8.Juli 2002			     PDF2PS(1)
