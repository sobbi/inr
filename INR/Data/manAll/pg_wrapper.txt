PG_WRAPPER(1)						      Debian PostgreSQL infrastructure						      PG_WRAPPER(1)



NAME
       pg_wrapper - wrapper for PostgreSQL client commands

SYNOPSIS
       client-program [--cluster version/cluster] [...]

       (client-program: psql, createdb, dropuser, and all other client programs installed in /usr/lib/postgresql/version/bin).

DESCRIPTION
       This program is run only as a link to names which correspond to PostgreSQL programs in /usr/lib/postgresql/version/bin. It determines the configured
       cluster and database for the user and calls the appropriate version of the desired program to connect to that cluster and database, supplying any
       specifed options to that command.

       The target cluster is selected by the following means, in descending order of precedence:

       1.  explicit specification with the --cluster option

       2.  explicit specification with the PGCLUSTER environment variable

       3.  matching entry in ~/.postgresqlrc (see postgresqlrc(5)), if that file exists

       4.  matching entry in /etc/postgresql-common/user_clusters (see user_clusters(5)), if that file exists

       5.  If only one local cluster exists, that one will be selected.

       6.  If several local clusters exist, the one listening on the default port 5432 will be selected.

       If none of these rules match, pg_wrapper aborts with an error.

OPTIONS
       --cluster version/cluster
	   cluster is either the name of a local cluster, or takes the form host:port for a remote cluster. If port is left empty (i. e. you just specify
	   host:), it defaults to 5432.

ENVIRONMENT
       PGCLUSTER
	   If $PGCLUSTER is set, its value (of the form version/cluster) specifies the desired cluster, similar to the --cluster option. However, if
	   --cluster is specified, it overrides the value of $PGCLUSTER.

       PG_CLUSTER_CONF_ROOT
	   This specifies an alternative base directory for cluster configurations. This is usually /etc/postgresql/, but for testing/development purposes
	   you can change this to point to e. g. your home directory, so that you can use the postgresql-common tools without root privileges.

FILES
       /etc/postgresql-common/user_clusters
	   stores the default cluster and database for users and groups as set by the administrators.

       $HOME/.postgresqlrc
	   stores defaults set by the user himself.

SEE ALSO
       user_clusters(5), postgresqlrc(5)

AUTHOR
       Martin Pitt <mpitt@debian.org>



Debian									 2010-01-19							      PG_WRAPPER(1)
