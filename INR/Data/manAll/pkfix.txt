PKFIX(1)							       User Commands								   PKFIX(1)



NAME
       pkfix - replace pk fonts in postscript files with type1 versions

SYNOPSIS
       pkfix [options] <inputfile.ps> <outputfile.ps>

DESCRIPTION
       This program tries to replace pk fonts in <inputfile.ps> by the type 1 versions. The result is written in <outputfile.ps>.

       Options: (defaults in parenthesis)

       --help print usage

       --(no)quiet
	      suppress messages 			   (false)

       --(no)verbose
	      verbose printing				   (false)

       --(no)debug
	      debug informations			   (false)

       --(no)clean
	      clear temp files				   (true)

       --(no)usetex
	      use TeX for generating the DVI file	   (false)

       --tex texcmd
	      tex command name (plain format)		   (tex)

       --dvips dvipscmd
	      dvips command name			   (dvips)

       --options opt
	      dvips options				   (-Ppdf -G0)

AUTHORS
       pkfix  has  been  written by Heiko Oberdiek.  This manual page has been written by Norbert Preining for the Debian/GNU Linux distribution and may be
       freely used, modified and/or distributed by anyone.



PKFIX 1.3, 2005/02/25							  May 2006								   PKFIX(1)
