POSIX_OPENPT(3) 						 Linux Programmer's Manual						    POSIX_OPENPT(3)



NAME
       posix_openpt - open a pseudo-terminal device

SYNOPSIS
       #include <stdlib.h>
       #include <fcntl.h>

       int posix_openpt(int flags);

   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):

       posix_openpt(): _POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600

DESCRIPTION
       The posix_openpt() function opens an unused pseudo-terminal master device, returning a file descriptor that can be used to refer to that device.

       The flags argument is a bit mask that ORs together zero or more of the following flags:

       O_RDWR Open the device for both reading and writing.  It is usual to specify this flag.

       O_NOCTTY
	      Do not make this device the controlling terminal for the process.

RETURN VALUE
       On  success,  posix_openpt() returns a non-negative file descriptor which is the lowest numbered unused descriptor.  On failure, -1 is returned, and
       errno is set to indicate the error.

ERRORS
       See open(2).

VERSIONS
       Glibc support for posix_openpt() has been provided since version 2.2.1.

CONFORMING TO
       posix_openpt() is part of the Unix98 pseudo-terminal support (see pts(4)).  This function is specified in POSIX.1-2001.

NOTES
       This function is a recent invention in POSIX.  Some Unix implementations that support System V (aka Unix 98) pseudo-terminals don't have this  func‐
       tion, but it is easy to implement:

	   int
	   posix_openpt(int flags)
	   {
	       return open("/dev/ptmx", flags);
	   }

SEE ALSO
       open(2), getpt(3), grantpt(3), ptsname(3), unlockpt(3), pts(4), pty(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



									 2007-07-26							    POSIX_OPENPT(3)
