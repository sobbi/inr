POSIX_TRACE_ATTR_DESTROY(P)					 POSIX Programmer's Manual					POSIX_TRACE_ATTR_DESTROY(P)



NAME
       posix_trace_attr_destroy, posix_trace_attr_init - destroy and initialize the trace stream attributes object (TRACING)

SYNOPSIS
       #include <trace.h>

       int posix_trace_attr_destroy(trace_attr_t *attr);
       int posix_trace_attr_init(trace_attr_t *attr);


DESCRIPTION
       The  posix_trace_attr_destroy()	function shall destroy an initialized trace attributes object. A destroyed attr attributes object can be reinitial‐
       ized using posix_trace_attr_init(); the results of otherwise referencing the object after it has been destroyed are undefined.

       The posix_trace_attr_init() function shall initialize a trace attributes object attr with the default value for all  of	the  individual  attributes
       used  by  a  given implementation. The read-only generation-version and clock-resolution attributes of the newly initialized trace attributes object
       shall be set to their appropriate values (see Trace Stream Attributes ).

       Results are undefined if posix_trace_attr_init() is called specifying an already initialized attr attributes object.

       Implementations may add extensions to the trace attributes object structure as permitted in the Base  Definitions  volume  of  IEEE Std 1003.1-2001,
       Chapter 2, Conformance.

       The  resulting  attributes  object  (possibly  modified	by  setting  individual  attributes values), when used by posix_trace_create(), defines the
       attributes of the trace stream created. A single attributes object can be used in multiple calls to posix_trace_create().  After one or	more  trace
       streams	have  been  created  using an attributes object, any function affecting that attributes object, including destruction, shall not affect any
       trace stream previously created. An initialized attributes object also serves to receive the attributes of an existing trace  stream  or  trace	log
       when calling the posix_trace_get_attr() function.

RETURN VALUE
       Upon successful completion, these functions shall return a value of zero. Otherwise, they shall return the corresponding error number.

ERRORS
       The posix_trace_attr_destroy() function may fail if:

       EINVAL The value of attr is invalid.


       The posix_trace_attr_init() function shall fail if:

       ENOMEM Insufficient memory exists to initialize the trace attributes object.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       posix_trace_create() , posix_trace_get_attr() , uname() , the Base Definitions volume of IEEE Std 1003.1-2001, <trace.h>

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003						POSIX_TRACE_ATTR_DESTROY(P)
