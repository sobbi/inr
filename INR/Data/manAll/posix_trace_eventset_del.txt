POSIX_TRACE_EVENTSET_ADD(P)					 POSIX Programmer's Manual					POSIX_TRACE_EVENTSET_ADD(P)



NAME
       posix_trace_eventset_add,  posix_trace_eventset_del,  posix_trace_eventset_empty, posix_trace_eventset_fill, posix_trace_eventset_ismember - manipu‐
       late trace event type sets (TRACING)

SYNOPSIS
       #include <trace.h>

       int posix_trace_eventset_add(trace_event_id_t event_id,
	      trace_event_set_t *set);
       int posix_trace_eventset_del(trace_event_id_t event_id,
	      trace_event_set_t *set);
       int posix_trace_eventset_empty(trace_event_set_t *set);
       int posix_trace_eventset_fill(trace_event_set_t *set, int what);
       int posix_trace_eventset_ismember(trace_event_id_t event_id,
	      const trace_event_set_t *restrict set, int *restrict ismember);


DESCRIPTION
       These primitives manipulate sets of trace event types. They operate on data objects addressable by the application, not on the current  trace  event
       filter of any trace stream.

       The posix_trace_eventset_add() and posix_trace_eventset_del() functions, respectively, shall add or delete the individual trace event type specified
       by the value of the argument event_id to or from the trace event type set pointed to by the argument set. Adding a trace event type already  in	the
       set or deleting a trace event type not in the set shall not be considered an error.

       The  posix_trace_eventset_empty()  function shall initialize the trace event type set pointed to by the set argument such that all trace event types
       defined, both system and user, shall be excluded from the set.

       The posix_trace_eventset_fill() function shall initialize the trace event type set pointed to by the argument set, such that the set of trace  event
       types  defined  by  the	argument what shall be included in the set. The value of the argument what shall consist of one of the following values, as
       defined in the <trace.h> header:

       POSIX_TRACE_WOPID_EVENTS

	      All the process-independent implementation-defined system trace event types are included in the set.

       POSIX_TRACE_SYSTEM_EVENTS

	      All the implementation-defined system trace event types are included in the set, as are those defined in IEEE Std 1003.1-2001.

       POSIX_TRACE_ALL_EVENTS

	      All trace event types defined, both system and user, are included in the set.


       Applications shall call either posix_trace_eventset_empty() or posix_trace_eventset_fill() at least once for each object of  type  trace_event_set_t
       prior  to  any  other use of that object. If such an object is not initialized in this way, but is nonetheless supplied as an argument to any of the
       posix_trace_eventset_add(), posix_trace_eventset_del(), or posix_trace_eventset_ismember() functions, the results are undefined.

       The posix_trace_eventset_ismember() function shall test whether the trace event type specified by the value of the argument event_id is a member  of
       the  set pointed to by the argument set. The value returned in the object pointed to by ismember argument is zero if the trace event type identifier
       is not a member of the set and a value different from zero if it is a member of the set.

RETURN VALUE
       Upon successful completion, these functions shall return a value of zero. Otherwise, they shall return the corresponding error number.

ERRORS
       These functions may fail if:

       EINVAL The value of one of the arguments is invalid.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       posix_trace_set_filter() , posix_trace_trid_eventid_open() , the Base Definitions volume of IEEE Std 1003.1-2001, <trace.h>

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003						POSIX_TRACE_EVENTSET_ADD(P)
