POSIX_TRACE_GET_ATTR(P) 					 POSIX Programmer's Manual					    POSIX_TRACE_GET_ATTR(P)



NAME
       posix_trace_get_attr, posix_trace_get_status - retrieve the trace attributes or trace status (TRACING)

SYNOPSIS
       #include <trace.h>

       int posix_trace_get_attr(trace_id_t trid, trace_attr_t *attr);
       int posix_trace_get_status(trace_id_t trid,
	      struct posix_trace_status_info *statusinfo);


DESCRIPTION
       The  posix_trace_get_attr()  function shall copy the attributes of the active trace stream identified by trid into the object pointed to by the attr
       argument.   If the Trace Log option is supported, trid may represent a pre-recorded trace log.

       The posix_trace_get_status() function shall return, in the structure pointed to by the statusinfo argument, the current trace status for  the  trace
       stream identified by the trid argument. These status values returned in the structure pointed to by statusinfo shall have been appropriately read to
       ensure that the returned values are consistent.	  If the Trace Log option is supported and the trid argument refers to a pre-recorded trace stream,
       the status shall be the status of the completed trace stream.

       Each time the posix_trace_get_status() function is used, the overrun status of the trace stream shall be reset to POSIX_TRACE_NO_OVERRUN immediately
       after the call completes.    If the Trace Log option is supported, the posix_trace_get_status() function shall behave the same as when the option is
       not supported except for the following differences:

	* If  the  trid  argument refers to a trace stream with log, each time the posix_trace_get_status() function is used, the log overrun status of the
	  trace stream shall be reset to POSIX_TRACE_NO_OVERRUN and the flush_error status shall be reset to zero immediately after the call completes.

	* If the trid argument refers to a pre-recorded trace stream, the status returned shall be the status of the completed trace stream and the  status
	  values of the trace stream shall not be reset.

RETURN VALUE
       Upon successful completion, these functions shall return a value of zero. Otherwise, they shall return the corresponding error number.

       The posix_trace_get_attr() function stores the trace attributes in the object pointed to by attr, if successful.

       The posix_trace_get_status() function stores the trace status in the object pointed to by statusinfo, if successful.

ERRORS
       These functions shall fail if:

       EINVAL The trace stream argument trid does not correspond to a valid active trace stream or a valid trace log.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       posix_trace_attr_destroy()   ,	posix_trace_attr_init()   ,   posix_trace_create()   ,	 posix_trace_open()   ,  the  Base  Definitions  volume  of
       IEEE Std 1003.1-2001, <trace.h>

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003						    POSIX_TRACE_GET_ATTR(P)
