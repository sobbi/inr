POSIX_TRACE_GETNEXT_EVENT(P)					 POSIX Programmer's Manual				       POSIX_TRACE_GETNEXT_EVENT(P)



NAME
       posix_trace_getnext_event, posix_trace_timedgetnext_event, posix_trace_trygetnext_event - retrieve a trace event (TRACING)

SYNOPSIS
       #include <sys/types.h>
       #include <trace.h>

       int posix_trace_getnext_event(trace_id_t trid,
	      struct posix_trace_event_info *restrict event,
	      void *restrict data, size_t num_bytes,
	      size_t *restrict data_len, int *restrict unavailable);


       int posix_trace_timedgetnext_event(trace_id_t trid,
	      struct posix_trace_event_info *restrict event,
	      void *restrict data, size_t num_bytes,
	      size_t *restrict data_len, int *restrict unavailable,
	      const struct timespec *restrict abs_timeout);


       int posix_trace_trygetnext_event(trace_id_t trid,
	      struct posix_trace_event_info *restrict event,
	      void *restrict data, size_t num_bytes,
	      size_t *restrict data_len, int *restrict unavailable);


DESCRIPTION
       The posix_trace_getnext_event() function shall report a recorded trace event either from an active trace stream without log  or a pre-recorded trace
       stream identified by the trid argument.	The posix_trace_trygetnext_event() function shall report a recorded trace event from an active trace stream
       without log identified by the trid argument.

       The  trace  event information associated with the recorded trace event shall be copied by the function into the structure pointed to by the argument
       event and the data associated with the trace event shall be copied into the buffer pointed to by the data argument.

       The posix_trace_getnext_event() function shall block if the trid argument identifies an active trace stream and there is currently  no  trace  event
       ready  to  be retrieved. When returning, if a recorded trace event was reported, the variable pointed to by the unavailable argument shall be set to
       zero.  Otherwise, the variable pointed to by the unavailable argument shall be set to a value different from zero.

	The posix_trace_timedgetnext_event() function shall attempt to get another trace event	from  an  active  trace  stream  without  log,	as  in	the
       posix_trace_getnext_event()  function.  However, if no trace event is available from the trace stream, the implied wait shall be terminated when the
       timeout specified by the argument abs_timeout expires, and the function shall return the error [ETIMEDOUT].

       The timeout shall expire when the absolute time specified by abs_timeout passes, as measured by the clock upon which timeouts are  based  (that	is,
       when  the value of that clock equals or exceeds abs_timeout), or if the absolute time specified by abs_timeout has already passed at the time of the
       call.

	If the Timers option is supported, the timeout shall be based on the CLOCK_REALTIME clock; if the Timers option is not supported, the timeout shall
       be  based  on the system clock as returned by the time() function. The resolution of the timeout shall be the resolution of the clock on which it is
       based. The timespec data type is defined in the <time.h> header.

       Under no circumstance shall the function fail with a timeout if a trace event is immediately available from the trace stream. The  validity  of	the
       abs_timeout argument need not be checked if a trace event is immediately available from the trace stream.

       The behavior of this function for a pre-recorded trace stream is unspecified.

       The  posix_trace_trygetnext_event()  function  shall not block.	 This function shall return an error if the trid argument identifies a pre-recorded
       trace stream.  If a recorded trace event was reported, the variable pointed to by the unavailable argument shall be set to zero.  Otherwise,  if  no
       trace event was reported, the variable pointed to by the unavailable argument shall be set to a value different from zero.

       The  argument num_bytes shall be the size of the buffer pointed to by the data argument. The argument data_len reports to the application the length
       in bytes of the data record just transferred. If num_bytes is greater than or equal to the size of the data associated with the trace event  pointed
       to  by the event argument, all the recorded data shall be transferred. In this case, the truncation-status member of the trace event structure shall
       be either POSIX_TRACE_NOT_TRUNCATED, if the trace event data was recorded without truncation while tracing, or POSIX_TRACE_TRUNCATED_RECORD, if	the
       trace event data was truncated when it was recorded. If the num_bytes argument is less than the length of recorded trace event data, the data trans‐
       ferred shall be truncated to a length of num_bytes, the value stored in the variable pointed to by data_len shall be equal  to  num_bytes,  and	the
       truncation-status  member  of  the  event  structure  argument  shall be set to POSIX_TRACE_TRUNCATED_READ (see the posix_trace_event_info structure
       defined in <trace.h>).

       The report of a trace event shall be sequential starting from the oldest recorded trace event. Trace events shall be reported in the order in  which
       they  were generated, up to an implementation-defined time resolution that causes the ordering of trace events occurring very close to each other to
       be unknown. Once reported, a trace event cannot be reported again from an active trace stream. Once a trace event is reported from an  active  trace
       stream without log, the trace stream shall make the resources associated with that trace event available to record future generated trace events.

RETURN VALUE
       Upon successful completion, these functions shall return a value of zero. Otherwise, they shall return the corresponding error number.

       If successful, these functions store:

	* The recorded trace event in the object pointed to by event

	* The trace event information associated with the recorded trace event in the object pointed to by data

	* The length of this trace event information in the object pointed to by data_len

	* The value of zero in the object pointed to by unavailable

ERRORS
       These functions shall fail if:

       EINVAL The trace stream identifier argument trid is invalid.


       The posix_trace_getnext_event() and posix_trace_timedgetnext_event() functions shall fail if:

       EINTR  The operation was interrupted by a signal, and so the call had no effect.


       The posix_trace_trygetnext_event() function shall fail if:

       EINVAL The trace stream identifier argument trid does not correspond to an active trace stream.


       The posix_trace_timedgetnext_event() function shall fail if:

       EINVAL There is no trace event immediately available from the trace stream, and the timeout argument is invalid.

       ETIMEDOUT
	      No trace event was available from the trace stream before the specified timeout timeout expired.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       posix_trace_create() , posix_trace_open() , the Base Definitions volume of IEEE Std 1003.1-2001, <sys/types.h>, <trace.h>

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003					       POSIX_TRACE_GETNEXT_EVENT(P)
