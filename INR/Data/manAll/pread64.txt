PREAD(2)							 Linux Programmer's Manual							   PREAD(2)



NAME
       pread, pwrite - read from or write to a file descriptor at a given offset

SYNOPSIS
       #define _XOPEN_SOURCE 500

       #include <unistd.h>

       ssize_t pread(int fd, void *buf, size_t count, off_t offset);

       ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset);

DESCRIPTION
       pread()	reads  up  to  count bytes from file descriptor fd at offset offset (from the start of the file) into the buffer starting at buf.  The file
       offset is not changed.

       pwrite() writes up to count bytes from the buffer starting at buf to the file descriptor fd at offset offset.  The file offset is not changed.

       The file referenced by fd must be capable of seeking.

RETURN VALUE
       On success, the number of bytes read or written is returned (zero indicates that nothing was written, in the case of pwrite(), or end  of  file,  in
       the case of pread(), or -1 on error, in which case errno is set to indicate the error.

ERRORS
       pread()	can fail and set errno to any error specified for read(2) or lseek(2).	pwrite() can fail and set errno to any error specified for write(2)
       or lseek(2).

VERSIONS
       The pread() and pwrite() system calls were added to Linux in version 2.1.60; the entries in the i386 system call table  were  added  in	2.1.69.   C
       library support (including emulation using lseek(2) on older kernels without the system calls) was added in glibc 2.1.

CONFORMING TO
       POSIX.1-2001.

SEE ALSO
       lseek(2), read(2), write(2), feature_test_macros(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2008-12-03								   PREAD(2)
