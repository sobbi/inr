PRINTAFM(1)			  Ghostscript			   PRINTAFM(1)



NAME
       printafm  - Gibt die Metrik einer Postscript-Schrift im AFM-Format mit‐
       tels ghostscript aus

SYNTAX
       printafm Schriftname

BESCHREIBUNG
       Dieses Skript ruft gs(1) auf, um die Metrik einer Schrift im AFM-Format
       zur Standardausgabe (stdout) zu schreiben.

SIEHE AUCH
       gs(1)

VERSION
       Das Dokument wurde zuletzt für Ghostscript Version 7.21 durchgesehen.

AUTOR
       Artifex	Software,  Inc.  sind die Hauptautoren von Ghostscript.  Diese
       Manpage ist von George Ferguson.




7.21				  8.Juli 2002			   PRINTAFM(1)
