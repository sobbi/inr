PS2PDF(1)			  Ghostscript			     PS2PDF(1)



NAME
       ps2pdf - konvertiert PostScript nach PDF mittels ghostscript
       ps2pdf12 - konvertiert PostScript nach PDF 1.2 (kompatibel zu Acrobat 3
       und später) mittels ghostscript
       ps2pdf13 - konvertiert PostScript nach PDF 1.3 (kompatibel zu Acrobat 4
       und später) mittels ghostscript

SYNTAX
       ps2pdf  [Optionen...] {Eingabe.[e]ps|-} [Ausgabe.pdf|-]
       ps2pdf12  [Optionen...] {Eingabe.[e]ps|-} [Ausgabe.pdf|-]
       ps2pdf13  [Optionen...] {Eingabe.[e]ps|-} [Ausgabe.pdf|-]

BESCHREIBUNG
       Außer  in  der  Benutzerschnittstelle sind die ps2pdf-Skripte nahezu in
       allen Funktionen  äquivalent  zu  Adobe	Acrobat  Distiller:  Sie  kon‐
       vertieren PostScript-Dateien in das Portable Document Format (PDF).


       Die drei Skripte unterscheiden sich wie folgt:

       -      ps2pdf12 produziert immer PDF-1.2-Dateien (kompatibel zu Acrobat
	      3 und später).

       -      ps2pdf13 produziert immer PDF-1.3-Dateien (kompatibel zu Acrobat
	      4 und später).

       -      ps2pdf selbst produziert standardgemäß PDF-1.2-Dateien (kompati‐
	      bel zu Acrobat 3 und später); In späteren  Versionen  kann  sich
	      das   jedoch   ändern.  Falls  Sie  eine	bestimmte  PDF-Version
	      benötigen, sollten Sie ps2pdf12, ps2pdf13 oder den -dCompatibil‐
	      ity=1.x Schalter in der Kommandozeile benutzen.

       Die  Konvertierung  mittels  ps2pdf  unterliegt einigen Beschränkungen.
       Ausführlichere Informationen dazu stehen in der HTML-Dokumentation.

OPTIONEN
       Für ps2pdf gelten die gleichen Optionen wie für gs(1).

SIEHE AUCH
       gs(1), ps2pdfwr(1),
       Ps2pdf.htm in der Ghostscript-Dokumentation

VERSION
       Dieses Dokument wurde zuletzt für Ghostscript Version  7.21  durchgese‐
       hen.

AUTOR
       Artifex	Software,  Inc.  sind die Hauptautoren von Ghostscript.  Diese
       Manpage ist von George Ferguson.



7.21				  8.Juli 2002			     PS2PDF(1)
