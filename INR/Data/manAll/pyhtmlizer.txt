pyhtmlizer(1)																      pyhtmlizer(1)



NAME
       pyhtmlizer - pretty-print Python source as HTML


SYNTAX
       pyhtmlizer [-s|--stylesheet <url>] <filename>

DESCRIPTION
       This generates a HTML document with Python source marked up with span elements.	To colorize, provide a stylesheet.

OPTIONS
       --stylesheet, -s <url>
	      Links to the stylesheet at <url>.

       --help Output help information and exit.

       -v, --version
	      Output version information and exit.



Twisted Matrix Laboratories														      pyhtmlizer(1)
