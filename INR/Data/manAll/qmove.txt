QMOVE(P)							 POSIX Programmer's Manual							   QMOVE(P)



NAME
       qmove - move batch jobs

SYNOPSIS
       qmove destination job_identifier ...

DESCRIPTION
       To  move  a  batch  job	is to remove the batch job from the batch queue in which it resides and instantiate the batch job in another batch queue. A
       batch job is moved by a request to the batch server that manages the batch job. The qmove utility is a user-accessible batch  client  that  requests
       the movement of one or more batch jobs.

       The qmove utility shall move those batch jobs, and only those batch jobs, for which a batch job_identifier is presented to the utility.

       The qmove utility shall move batch jobs in the order in which the corresponding batch job_identifiers are presented to the utility.

       If the qmove utility fails to process a batch job_identifier successfully, the utility shall proceed to process the remaining batch job_identifiers,
       if any.

       The qmove utility shall move batch jobs by sending a Move Job Request to the batch server that manages each batch job. The qmove utility  shall	not
       exit before the batch jobs corresponding to all successfully processed batch job_identifiers have been moved.

OPTIONS
       None.

OPERANDS
       The qmove utility shall accept one operand that conforms to the syntax for a destination (see Destination ).

       The qmove utility shall accept one or more operands that conform to the syntax for a batch job_identifier (see Batch Job Identifier ).

STDIN
       Not used.

INPUT FILES
       None.

ENVIRONMENT VARIABLES
       The following environment variables shall affect the execution of qmove:

       LANG   Provide	a  default  value  for	the  internationalization  variables  that  are  unset	or  null.  (See  the  Base  Definitions  volume  of
	      IEEE Std 1003.1-2001, Section 8.2, Internationalization Variables for the precedence of internationalization variables used to determine	the
	      values of locale categories.)

       LC_ALL If set to a non-empty string value, override the values of all the other internationalization variables.

       LC_CTYPE
	      Determine  the  locale for the interpretation of sequences of bytes of text data as characters (for example, single-byte as opposed to multi-
	      byte characters in arguments).

       LC_MESSAGES
	      Determine the locale that should be used to affect the format and contents of diagnostic messages written to standard error.

       LOGNAME
	      Determine the login name of the user.


ASYNCHRONOUS EVENTS
       Default.

STDOUT
       None.

STDERR
       The standard error shall be used only for diagnostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       The following exit values shall be returned:

	0     Successful completion.

       >0     An error occurred.


CONSEQUENCES OF ERRORS
       In addition to the default behavior, the qmove utility shall not be required to write a diagnostic message to standard error when  the  error  reply
       received  from a batch server indicates that the batch job_identifier does not exist on the server. Whether or not the qmove utility waits to output
       the diagnostic message while attempting to locate the job on other servers is implementation-defined.

       The following sections are informative.

APPLICATION USAGE
       None.

EXAMPLES
       None.

RATIONALE
       The qmove utility allows users to move jobs between queues.

       The alternative to using the qmove utility-deleting the batch job and requeuing it-entails considerably more typing.

       Since the means of selecting jobs based on attributes has been encapsulated in the qselect utility, the only option of the  qmove  utility  concerns
       authorization.  The  -u option provides the user with the convenience of changing the user identifier under which the batch job will execute.  Mini‐
       malism and consistency have taken precedence over convenience; the -u option has been deleted because the equivalent capability exists with  the  -u
       option of the qalter utility.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Batch Environment Services , qalter , qselect

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								   QMOVE(P)
