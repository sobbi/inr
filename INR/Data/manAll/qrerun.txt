QRERUN(P)							 POSIX Programmer's Manual							  QRERUN(P)



NAME
       qrerun - rerun batch jobs

SYNOPSIS
       qrerun job_identifier ...

DESCRIPTION
       To  rerun  a  batch job is to terminate the session leader of the batch job, delete any associated checkpoint files, and return the batch job to the
       batch queued state. A batch job is rerun by a request to the batch server that manages the batch job. The qrerun utility is a user-accessible  batch
       client that requests the rerunning of one or more batch jobs.

       The qrerun utility shall rerun those batch jobs for which a batch job_identifier is presented to the utility.

       The qrerun utility shall rerun batch jobs in the order in which their batch job_identifiers are presented to the utility.

       If  the	qrerun utility fails to process any batch job_identifier successfully, the utility shall proceed to process the remaining batch job_identi‐
       fiers, if any.

       The qrerun utility shall rerun batch jobs by sending a Rerun Job Request to the batch server that manages each batch job.

       For each successfully processed batch job_identifier, the qrerun utility shall have rerun the corresponding batch job at the time the utility exits.

OPTIONS
       None.

OPERANDS
       The qrerun utility shall accept one or more operands that conform to the syntax for a batch job_identifier (see Batch Job Identifier ).

STDIN
       Not used.

INPUT FILES
       None.

ENVIRONMENT VARIABLES
       The following environment variables shall affect the execution of qrerun:

       LANG   Provide  a  default  value  for  the  internationalization  variables  that  are	unset  or  null.  (See	the  Base  Definitions	volume	 of
	      IEEE Std 1003.1-2001,  Section 8.2, Internationalization Variables for the precedence of internationalization variables used to determine the
	      values of locale categories.)

       LC_ALL If set to a non-empty string value, override the values of all the other internationalization variables.

       LC_CTYPE
	      Determine the locale for the interpretation of sequences of bytes of text data as characters (for example, single-byte as opposed  to  multi-
	      byte characters in arguments).

       LC_MESSAGES
	      Determine the locale that should be used to affect the format and contents of diagnostic messages written to standard error.

       LOGNAME
	      Determine the login name of the user.


ASYNCHRONOUS EVENTS
       Default.

STDOUT
       None.

STDERR
       The standard error shall be used only for diagnostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       The following exit values shall be returned:

	0     Successful completion.

       >0     An error occurred.


CONSEQUENCES OF ERRORS
       In  addition  to the default behavior, the qrerun utility shall not be required to write a diagnostic message to standard error when the error reply
       received from a batch server indicates that the batch job_identifier does not exist on the server. Whether or not the qrerun utility waits to output
       the diagnostic message while attempting to locate the job on other servers is implementation-defined.

       The following sections are informative.

APPLICATION USAGE
       None.

EXAMPLES
       None.

RATIONALE
       The qrerun utility allows users to cause jobs in the running state to exit and rerun.

       The  qrerun utility is a new utility, vis-a-vis existing practice, that has been defined in this volume of IEEE Std 1003.1-2001 to correct user-per‐
       ceived deficiencies in the existing practice.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Batch Environment Services

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								  QRERUN(P)
