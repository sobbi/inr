Quadrapassel(6) 					       Quadrapassel(6)



NAME
       Quadrapassel - Tetris-like game for GNOME

SYNOPSIS
       quadrapassel [OPTION...]

DESCRIPTION
       Quadrapassel  comes  from  the  classic falling-block game, Tetris. The
       goal of the game is to create  complete	horizontal  lines  of  blocks,
       which will disappear.

OPTIONS
       -l, --level=LEVEL
	      Set starting level (1 or greater)

       This program also accepts the standard GNOME and GTK options.

AUTHORS
       quadrapassel	 was	  written     by     J.     Marcin     Gorycki
       <janusz.gorycki@intel.com>.

       This manual page was written by Sven Arvidsson  <sa@whiz.se>,  for  the
       Debian project (but may be used by others).

SEE ALSO
       gtk-options(7), gnome-options(7)

       The online documentation available through the program's Help menu.



GNOME				  2007-06-09		       Quadrapassel(6)
