RADEON(4)																	  RADEON(4)



NAME
       radeon - ATI RADEON video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "radeon"
	 ...
       EndSection

DESCRIPTION
       radeon is an Xorg driver for ATI RADEON based video cards with the following features:

       · Full support for 8, 15, 16 and 24 bit pixel depths;
       · RandR 1.2 and RandR 1.3 support;
       · TV-out support (only on R/RV/RS1xx, R/RV/RS2xx, R/RV/RS3xx. Experimental support on R/RV5xx, R/RV6xx, and R/RV7xx through the ATOMTvOut option);
       · Full EXA 2D acceleration;
       · Full  XAA  2D	acceleration  (only  on  R/RV/RS1xx,  R/RV/RS2xx, R/RV/RS3xx, R/RV/RS4xx, R/RV5xx, RS6xx. XAA Render acceleration supported only on
	 R/RV100, R/RV/RS2xx and RS3xx);
       · Textured XVideo acceleration including anti-tearing support (Bicubic filtering only available on R/RV3xx, R/RV/RS4xx, R/RV5xx, and RS6xx/RS740);
       · Overlay XVideo acceleration (only on R/RV/RS1xx, R/RV/RS2xx, R/RV/RS3xx, R/RV/RS4xx);
       · 3D acceleration;

SUPPORTED HARDWARE
       The radeon driver supports PCI, AGP, and PCIE video cards based on the following ATI chips:

       R100	   Radeon 7200
       RV100	   Radeon 7000(VE), M6, RN50/ES1000
       RS100	   Radeon IGP320(M)
       RV200	   Radeon 7500, M7, FireGL 7800
       RS200	   Radeon IGP330(M)/IGP340(M)
       RS250	   Radeon Mobility 7000 IGP
       R200	   Radeon 8500, 9100, FireGL 8800/8700
       RV250	   Radeon 9000PRO/9000, M9
       RV280	   Radeon 9200PRO/9200/9200SE/9250, M9+
       RS300	   Radeon 9100 IGP
       RS350	   Radeon 9200 IGP
       RS400/RS480 Radeon XPRESS 200(M)/1100 IGP
       R300	   Radeon 9700PRO/9700/9500PRO/9500/9600TX, FireGL X1/Z1
       R350	   Radeon 9800PRO/9800SE/9800, FireGL X2
       R360	   Radeon 9800XT
       RV350	   Radeon 9600PRO/9600SE/9600/9550, M10/M11, FireGL T2
       RV360	   Radeon 9600XT
       RV370	   Radeon X300, M22
       RV380	   Radeon X600, M24
       RV410	   Radeon X700, M26 PCIE
       R420	   Radeon X800 AGP
       R423/R430   Radeon X800, M28 PCIE
       R480/R481   Radeon X850 PCIE/AGP
       RV505/RV515/RV516/RV550
		   Radeon X1300/X1400/X1500/X2300
       R520	   Radeon X1800
       RV530/RV560 Radeon X1600/X1650/X1700
       RV570/R580  Radeon X1900/X1950
       RS600/RS690/RS740
		   Radeon X1200/X1250/X2100
       R600	   Radeon HD 2900
       RV610/RV630 Radeon HD 2400/2600
       RV620/RV635 Radeon HD 3450/3470
       RV670	   Radeon HD 3850/3870
       RS780	   Radeon HD 3100/3200/3300
       RV710	   Radeon HD 4350/4550
       RV730	   Radeon HD 4650/4670
       RV770	   Radeon HD 4850/4870

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration details.	This section only covers configuration details specific to this driver.

       The driver auto-detects all device information necessary to initialize the card.  However, if you have problems with auto-detection, you  can  spec‐
       ify:

	   VideoRam - in kilobytes
	   MemBase  - physical address of the linear framebuffer
	   IOBase   - physical address of the MMIO registers
	   ChipID   - PCI DEVICE ID

       In addition, the following driver Options are supported:

       Option "SWcursor" "boolean"
	      Selects software cursor.	The default is off.

       Option "NoAccel" "boolean"
	      Enables or disables all hardware acceleration.
	      The default is to enable hardware acceleration.

       Option "Dac6Bit" "boolean"
	      Enables or disables the use of 6 bits per color component when in 8 bpp mode (emulates VGA mode).  By default, all 8 bits per color component
	      are used.
	      The default is off.

       Option "VideoKey" "integer"
	      This overrides the default pixel value for the YUV video overlay key.
	      The default value is 0x1E.

       Option "ScalerWidth" "integer"
	      This sets the overlay scaler buffer width. Accepted values range from 1024 to 2048, divisible by 64, values other than 1536 and 1920 may	not
	      make  sense  though.  Should be set automatically, but noone has a clue what the limit is for which chip. If you think quality is not optimal
	      when playing back HD video (with horizontal resolution larger than this setting), increase this value, if you get an empty area at the  right
	      (usually pink), decrease it. Note this only affects the "true" overlay via xv, it won't affect things like textured video.
	      The default value is either 1536 (for most chips) or 1920.

       Option "AGPMode" "integer"
	      Set AGP data transfer rate.  (used only when DRI is enabled)
	      1      -- 1x (before AGP v3 only)
	      2      -- 2x (before AGP v3 only)
	      4      -- 4x
	      8      -- 8x (AGP v3 only)
	      others -- invalid
	      The default is to leave it unchanged.

       Option "AGPFastWrite" "boolean"
	      Enable  AGP  fast writes.  Enabling this is frequently the cause of instability. Used only when the DRI is enabled. If you enable this option
	      you will get *NO* support from developers.
	      The default is off.

       Option "BusType" "string"
	      Used to replace previous ForcePCIMode option.  Should only be used when driver's bus detection is incorrect or you want to force a  AGP  card
	      to PCI mode. Should NEVER force a PCI card to AGP bus.
	      PCI    -- PCI bus
	      AGP    -- AGP bus
	      PCIE   -- PCI Express bus
	      (used only when DRI is enabled)
	      The default is auto detect.

       Option "DisplayPriority" "string"
	      Used to prevent flickering or tearing problem caused by display buffer underflow.
	      AUTO   -- Driver calculated (default).
	      BIOS   -- Remain unchanged from BIOS setting.
			Use this if the calculation is not correct
			for your card.
	      HIGH   -- Force to the highest priority.
			Use this if you have problem with above options.
			This may affect performance slightly.
	      The default value is AUTO.

       Option "ColorTiling" "boolean"
	      Frame  buffer  can be addressed either in linear or tiled mode. Tiled mode can provide significant performance benefits with 3D applications,
	      for 2D it shouldn't matter much. Tiling will be disabled if the virtual x resolution exceeds 2048 (3968 for R300 and above),  or	if  DRI  is
	      enabled the drm module is too old.
	      If this option is enabled, a new dri driver is required for direct rendering too.
	      Color tiling will be automatically disabled in interlaced or doublescan screen modes.
	      The default value is on.

       Option "IgnoreEDID" "boolean"
	      Do not use EDID data for mode validation, but DDC is still used for monitor detection. This is different from NoDDC option.
	      The default value is off.

       Option "CustomEDID" "string"
	      Forces the X driver to use the EDID data specified in a file rather than the display's EDID. Also overrides DDC monitor detection.
	      You  may	specify  a semicolon separated list of output name and filename pairs with an optional flag, "digital" or "analog", to override the
	      digital bit in the edid which is used by the driver to determine whether to use the analog or digital encoder associated with a  DVI-I  port.
	      The output name is the randr output name, e.g., "VGA-0" or "DVI-0"; consult the Xorg log for the supported output names of any given system.
	      The file must contain a raw 128-byte EDID block, as captured by get-edid.
	      For  example:  Option  "CustomEDID" "VGA-0:/tmp/edid1.bin; DVI-0:/tmp/edid2.bin:digital" will assign the EDID from the file /tmp/edid1.bin to
	      the output device VGA-0, and the EDID from the file /tmp/edid2.bin to the output device DVI-0 and force the  DVI	port  to  use  the  digital
	      encoder.
	      Note that a output name must always be specified, even if only one EDID is specified.
	      Caution:	Specifying  an	EDID  that  doesn't exactly match your display may damage your hardware, as it allows the driver to specify timings
	      beyond the capabilities of your display. Use with care.

       Option "PanelSize" "string"
	      Should only be used when driver cannot detect the correct panel size.  Apply to both desktop (TMDS) and laptop (LVDS) digital panels.  When a
	      valid panel size is specified, the timings collected from DDC and BIOS will not be used. If you have a panel with timings different from that
	      of a standard VESA mode, you have to provide this information through the Modeline.
	      For example, Option "PanelSize" "1400x1050"
	      The default value is none.

       Option "EnablePageFlip" "boolean"
	      Enable page flipping for 3D acceleration. This will increase performance but not work correctly in some rare cases, hence the default is off.
	      It is currently only supported on R/RV/RS4xx and older hardware.

       Option "ForceMinDotClock" "frequency"
	      Override	minimum  dot clock. Some Radeon BIOSes report a minimum dot clock unsuitable (too high) for use with television sets even when they
	      actually can produce lower dot clocks. If this is the case you can override the value here.  Note that using  this  option  may  damage  your
	      hardware.   You  have  been  warned.  The  frequency parameter may be specified as a float value with standard suffixes like "k", "kHz", "M",
	      "MHz".

       Option "RenderAccel" "boolean"
	      Enables or disables hardware Render acceleration.  It is supported on all Radeon cards when using EXA acceleration and on Radeon	R/RV/RS1xx,
	      R/RV/RS2xx and RS3xx when usig XAA.  The default is to enable Render acceleration.

       Option "AccelMethod" "string"
	      Chooses  between	available acceleration architectures.  Valid options are XAA and EXA.  XAA is the traditional acceleration architecture and
	      support for it is very stable.  EXA is a newer acceleration architecture with better performance for the	Render	and  Composite	extensions.
	      The default is EXA.

       Option "AccelDFS" "boolean"
	      Use  or don't use accelerated EXA DownloadFromScreen hook when possible (only when Direct Rendering is enabled, e.g.).  Default: off with AGP
	      due to issues with GPU->host transfers with some AGP bridges, on otherwise.

       Option "FBTexPercent" "integer"
	      Amount of video RAM to reserve for OpenGL textures, in percent. With EXA, the remainder of video RAM is reserved for  EXA  offscreen  manage‐
	      ment.  Specifying  0 results in all offscreen video RAM being reserved for EXA and only GART memory being available for OpenGL textures. This
	      may improve EXA performance, but beware that it may cause problems with OpenGL drivers from Mesa versions older than 6.4. With XAA,  specify‐
	      ing  lower  percentage than what gets reserved without this option has no effect, but the driver tries to increase the video RAM reserved for
	      textures to the amount specified roughly.  Default: 50.

       Option "DepthBits" "integer"
	      Precision in bits per pixel of the shared depth buffer used for 3D acceleration.	Valid values are 16 and 24. When this  is  24,	there  will
	      also  be	a  hardware accelerated stencil buffer, but the combined depth/stencil buffer will take up twice as much video RAM as when it's 16.
	      Default: The same as the screen depth.

       Option "DMAForXv" "boolean"
	      Try or don't try to use DMA for Xv image transfers. This will reduce CPU usage when playing big videos like DVDs, but  may  cause  instabili‐
	      ties.  Default: on.

       Option "SubPixelOrder" "string"
	      Force subpixel order to specified order.	Subpixel order is used for subpixel decimation on flat panels.
	      NONE   -- No subpixel (CRT like displays)
	      RGB    -- in horizontal RGB order (most flat panels)
	      BGR    -- in horizontal BGR order (some flat panels)

	      This option is intended to be used in following cases:
	      1. The default subpixel order is incorrect for your panel.
	      2. Enable subpixel decimation on analog panels.
	      3. Adjust to one display type in dual-head clone mode setup.
	      4. Get better performance with Render acceleration on digital panels (use NONE setting).
	      The default is NONE for CRT, RGB for digital panels

       Option "ClockGating" "boolean"
	      Enable dynamic clock gating.  This can help reduce heat and increase battery life by reducing power usage.  Some users report reduced 3D per‐
	      formance with this enabled.  The default is off. This option is not supported with KMS enabled (you need to either  add  radeon.modeset=0  to
	      your boot stanza, or use a 2.6.34 kernel).

       Option "ForceLowPowerMode" "boolean"
	      Enable  a  static low power mode.  This can help reduce heat and increase battery life by reducing power usage at the expense of performance.
	      The default is off. This option is not supported with KMS enabled (you need to either add radeon.modeset=0 to your  boot	stanza,  or  use  a
	      2.6.34 kernel).

       Option "DynamicPM" "boolean"
	      Enable  dynamic  power  mode  switching.	This can help reduce heat and increase battery life by reducing power usage when the system is idle
	      (DPMS active). The default is off. This option is not supported with KMS enabled (you need  to  either  add  radeon.modeset=0  to  your  boot
	      stanza, or use a 2.6.34 kernel).

       Option "VGAAccess" "boolean"
	      Tell  the  driver  if  it  can  do  legacy  VGA  IOs to the card. This is necessary for properly resuming consoles when in VGA text mode, but
	      shouldn't be if the console is using radeonfb or some other graphic mode driver. Some platforms like PowerPC have issues with those, and they
	      aren't necessary unless you have a real text mode in console. The default is off on PowerPC and SPARC and on on other architectures.

       Option "ReverseDDC" "boolean"
	      When  BIOS  connector informations aren't available, use this option to reverse the mapping of the 2 main DDC ports. Use this if the X server
	      obviously detects the wrong display for each connector. This is typically needed on the Radeon 9600 cards bundled with Apple G5s. The default
	      is off.

       Option "LVDSProbePLL" "boolean"
	      When BIOS panel informations aren't available (like on PowerBooks), it may still be necessary to use the firmware provided PLL values for the
	      panel or flickering will happen. This option will force probing of the current value programmed in the chip when X is launched in that  case.
	      This is only useful for LVDS panels (laptop internal panels).  The default is on.

       Option "TVDACLoadDetect" "boolean"
	      Enable  load detection on the TV DAC.  The TV DAC is used to drive both TV-out and analog monitors. Load detection is often unreliable in the
	      TV DAC so it is disabled by default.  The default is off.

       Option "DefaultTMDSPLL" "boolean"
	      Use the default driver provided TMDS PLL values rather than the ones provided by the bios. This option has no effect on  Mac  cards.   Enable
	      this option if you are having problems with a DVI monitor using the internal TMDS controller.  The default is off.

       Option "DefaultTVDACAdj" "boolean"
	      Use  the	default driver provided TVDAC Adj values rather than the ones provided by the bios. This option has no effect on Mac cards.  Enable
	      this option if you are having problems with a washed out display on the secondary DAC.  The default is off.

       Option "DRI" "boolean"
	      Enable DRI support.  This option allows you to enable to disable the DRI.  The default is off for RN50/ES1000 and on for others.

       Option "DefaultConnectorTable" "boolean"
	      Enable this option to skip the BIOS connector table parsing and use the driver defaults for each chip.  The default is off

       Option "MacModel" "string"
	      Used to specify Mac models for connector tables and quirks.  If you have a powerbook or mini with DVI that does not work	properly,  try	the
	      alternate  options as Apple does not seem to provide a good way of knowing whether they use internal or external TMDS for DVI.  Only valid on
	      PowerPC.	On Linux, the driver will attempt to detect the MacModel automatically.
	      ibook		   -- ibooks
	      powerbook-external   -- Powerbooks with external DVI
	      powerbook-internal   -- Powerbooks with integrated DVI
	      powerbook-vga	   -- Powerbooks with VGA rather than DVI
	      mini-external	   -- Mac Mini with external DVI
	      mini-internal	   -- Mac Mini with integrated DVI
	      imac-g5-isight	   -- iMac G5 iSight
	      emac		   -- eMac G4
	      The default value is undefined.

       Option "TVStandard" "string"
	      Used to specify the default TV standard if you want to use something other than the bios default. Valid options are:
	      ntsc
	      pal
	      pal-m
	      pal-60
	      ntsc-j
	      scart-pal
	      The default value is undefined.

       Option "ForceTVOut" "boolean"
	      Enable this option to force TV-out to always be detected as attached.  The default is off

       Option "IgnoreLidStatus" "boolean"
	      Enable this option to ignore lid status on laptops and always detect LVDS as attached.  The default is on.

       Option "Int10" "boolean"
	      This option allows you to disable int10 initialization.  Set this to False if you are experiencing a hang when initializing a secondary card.
	      The default is on.

       Option "EXAVSync" "boolean"
	      This  option  attempts  to  avoid  tearing by stalling the engine until the display controller has passed the destination region.  It reduces
	      tearing at the cost of performance and has been know to cause instability on some chips.	The default is off.

       Option "ATOMTvOut" "boolean"
	      This option enables experimental TV-out support for R/RV5xx, R/RV6xx, and R/RV7xx atombios chips. TV-out is experimental and may not function
	      on these chips as well as hoped for.  The default is off.

       Option "R4xxATOM" "boolean"
	      This option enables modesetting on R/RV4xx chips using atombios.	The default is off.

       Option "EXAPixmaps" "boolean"
	      (KMS  Only)  Under kernel modesetting to avoid thrashing pixmaps in/out of VRAM on low memory cards, we use a heuristic based on VRAM amount,
	      to determine whether to allow EXA to use VRAM for non-essential pixmaps.	This option allows us to override the heurisitc.  The default is on
	      with > 32MB VRAM, off with < 32MB.

       Option "ZaphodHeads" "string"
	      Specify  the  randr  output(s) to use with zaphod mode for a particular driver instance.	If you use this option you most use this option for
	      all instances of the driver.
	      For example: Option "ZaphodHeads" "LVDS,VGA-0" will assign xrandr outputs LVDS and VGA-0 to this instance of the driver.


TEXTURED VIDEO ATTRIBUTES
       The driver supports the following X11 Xv attributes for Textured Video.	You can use the "xvattr" tool to query/set those attributes at runtime.


       XV_VSYNC
	      XV_VSYNC is used to control whether textured adapter synchronizes the screen update to the monitor vertical refresh to eliminate tearing.  It
	      has two values: 'off'(0) and 'on'(1). The default is 'on'(1).


       XV_CRTC
	      XV_CRTC is used to control which display controller (crtc) the textured adapter synchronizes the screen update with when XV_VSYNC is enabled.
	      The default, 'auto'(-1), will sync to the display controller that more of the video is on.  This attribute is useful for	things	like  clone
	      mode where the user can best decide which display should be synced.  The default is 'auto'(-1).


       XV_BICUBIC
	      XV_BICUBIC  is  used  to	control whether textured adapter should apply a bicubic filter to smooth the output. It has three values: 'off'(0),
	      'on'(1) and 'auto'(2). 'off' means never apply the filter, 'on' means always apply the filter and 'auto' means apply the filter only if the X
	      and  Y  sizes are scaled to more than double to avoid blurred output.  Bicubic filtering is not currently compatible with other Xv attributes
	      like hue, contrast, and brightness, and must be disabled to use those attributes.  The default is 'auto'(2).


SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

	1. Wiki page:
	   http://www.x.org/wiki/radeon

	2. Overview about radeon development code:
	   http://cgit.freedesktop.org/xorg/driver/xf86-video-ati/

	3. Mailing list:
	   http://lists.x.org/mailman/listinfo/xorg-driver-ati

	4. IRC channel:
	   #radeon on irc.freenode.net

	5. Query the bugtracker for radeon bugs:
	   https://bugs.freedesktop.org/query.cgi?product=xorg&component=Driver/Radeon

	6. Submit bugs & patches:
	   https://bugs.freedesktop.org/enter_bug.cgi?product=xorg&component=Driver/Radeon


AUTHORS
       Authors include:
       Rickard E. (Rik) Faith	faith@precisioninsight.com
       Kevin E. Martin		kem@freedesktop.org
       Alan Hourihane		alanh@fairlite.demon.co.uk
       Marc Aurele La France	tsi@xfree86.org
       Benjamin Herrenschmidt	benh@kernel.crashing.org
       Michel Dänzer		michel@tungstengraphics.com
       Alex Deucher		alexdeucher@gmail.com
       Bogdan D.		bogdand@users.sourceforge.net
       Eric Anholt		eric@anholt.net



X Version 11							   xf86-video-ati 6.13.0							  RADEON(4)
