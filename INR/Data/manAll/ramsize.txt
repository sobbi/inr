RDEV(8) 							 Linux Programmer's Manual							    RDEV(8)



NAME
       rdev - query/set image root device, RAM disk size, or video mode

SYNOPSIS
       rdev [-Rrvh] [-o offset] [image [value [offset]]]
       rdev [-o offset] [image [root_device [offset]]]
       ramsize [-o offset] [image [size [offset]]]
       vidmode [-o offset] [image [mode [offset]]]
       rootflags [-o offset] [image [flags [offset]]]

DESCRIPTION
       With  no arguments, rdev outputs an /etc/mtab line for the current root file system.  With no arguments, ramsize, vidmode, and rootflags print usage
       information.

       In a bootable image for the Linux kernel on i386, there are several pairs of bytes which specify the root device, the video mode, and  the  size  of
       the RAM disk.  These pairs of bytes, by default, begin at offset 504 (decimal) in the kernel image:

	       498 Root flags
	      (500 and 502 Reserved)
	       504 RAM Disk Size
	       506 VGA Mode
	       508 Root Device
	      (510 Boot Signature)

       rdev will change these values.

       Typical values for the image parameter, which is a bootable Linux kernel image, might be:

	      /vmlinux
	      /vmunix
	      /boot/bzImage-2.4.0
	      /dev/fd0
	      /dev/fd1

       When using the rdev command, the root_device parameter might be something like:

	      /dev/hda1
	      /dev/hdf13
	      /dev/sda2
	      /dev/sdc4
	      /dev/ida/c0d0p1

       One may also specify the device by a comma-separated pair of decimal integers major,minor.

       For  the  ramsize  command,  the  size  parameter  specifies the size of the RAM disk in kilobytes. 2.0.x kernels and newer dynamically allocate the
       ramdisk and do not need this setting.

       For the rootflags command, the flags parameter contains extra information used when mounting root.  Currently the only effect of these flags  is  to
       force the kernel to mount the root filesystem in readonly mode if flags is non-zero.

       For the vidmode command, the mode parameter specifies the video mode:

	      -3 = Prompt
	      -2 = Extended VGA
	      -1 = Normal VGA
	       0 = as if "0" was pressed at the prompt
	       1 = as if "1" was pressed at the prompt
	       2 = as if "2" was pressed at the prompt
	       n = as if "n" was pressed at the prompt

       If the value is not specified, the image will be examined to determine the current settings.

OPTIONS
       -r     Causes rdev to act like ramsize (Not relevant for 2.0.x and newer kernels).

       -R     Causes rdev to act like rootflags.

       -v     Causes rdev to act like vidmode.

       -h     Provides help.

BUGS
       The  rdev  utility,  when  used other than to find a name for the current root device, is an ancient hack that works by patching a kernel image at a
       magic offset with magic numbers. It does not work on architectures other than i386.  Its use is strongly discouraged. Use a boot  loader  like  Sys‐
       Linux or LILO instead.

HISTORY
       At  offset  502	there  used  to be the device number of the swap device (in Linux 0.12), and "rdev -s" or "swapdev" would set this.  However, since
       Linux 0.95 this constant is not used any longer, and the swap device is specified using the swapon(2) system call.

       At offset 504 there used to be the size of the ramdisk in kilobytes.  One would specify a size, and this much was grabbed off the top of memory.  In
       Linux 1.1.39 it became also possible to set this value on the kernel command line.  In Linux 1.3.48 the ramdisk setup was changed. Ramdisk memory is
       now taken from the buffer cache, so that the ramdisk can grow dynamically.  The interpretation of the ramdisk word was changed to a  word  of  which
       the  high  order bit is a prompt flag (1: prompt for ramdisk: "VFS: Insert ramdisk floppy and press ENTER" - this is needed with a two-floppy boot),
       the next bit a load flag (1: load ramdisk), and the low order 11 bits give the starting block number of the root filesystem image (so that  one	can
       have a single floppy boot).  See also linux/Documentation/ramdisk.txt.

AUTHORS
       Originally by Werner Almesberger (almesber@nessie.cs.id.ethz.ch)
       Modified by Peter MacDonald (pmacdona@sanjuan.UVic.CA)
       rootflags support added by Stephen Tweedie (sct@dcs.ed.ac.uk)

AVAILABILITY
       The rdev command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



Linux 0.99							      20 November 1993								    RDEV(8)
