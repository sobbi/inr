REASSIGN OWNED(7)							SQL Commands							  REASSIGN OWNED(7)



NAME
       REASSIGN OWNED - change the ownership of database objects owned by a database role


SYNOPSIS
       REASSIGN OWNED BY old_role [, ...] TO new_role


DESCRIPTION
       REASSIGN OWNED instructs the system to change the ownership of the database objects owned by one of the old_roles, to new_role.

PARAMETERS
       old_role
	      The name of a role. The ownership of all the objects in the current database owned by this role will be reassigned to new_role.

       new_role
	      The name of the role that will be made the new owner of the affected objects.

NOTES
       REASSIGN  OWNED	is often used to prepare for the removal of one or more roles. Because REASSIGN OWNED only affects the objects in the current data‐
       base, it is usually necessary to execute this command in each database that contains objects owned by a role that is to be removed.

       The DROP OWNED [drop_owned(7)] command is an alternative that drops all the database objects owned by one or more roles.

       The REASSIGN OWNED command does not affect the privileges granted to the old_roles in objects that are not owned by them. Use DROP OWNED  to  revoke
       those privileges.

COMPATIBILITY
       The REASSIGN OWNED statement is a PostgreSQL extension.

SEE ALSO
       DROP OWNED [drop_owned(7)], DROP ROLE [drop_role(7)]



SQL - Language Statements						 2010-05-19							  REASSIGN OWNED(7)
