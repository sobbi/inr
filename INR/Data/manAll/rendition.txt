RENDITION(4)																       RENDITION(4)



NAME
       rendition - Rendition video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "rendition"
	 ...
       EndSection

DESCRIPTION
       rendition is an Xorg driver for Rendition/Micron based video cards.  The driver supports following framebuffer depths: 8, 15 (Verite V1000 only), 16
       and 24. Acceleration and multi-head configurations are not supported yet, but are work in progress.

SUPPORTED HARDWARE
       The rendition driver supports PCI and AGP video cards based on the following Rendition/Micron chips:

       V1000	   Verite V1000 based cards.

       V2100	   Verite V2100 based cards. Diamond Stealth II S220 is the only known such card.

       V2200	   Verite V2200 based cards.

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration details.	This section only covers configuration details specific to this driver.

       The driver auto-detects the chipset type, but the following ChipSet names may optionally be specified in the config file "Device" section, and  will
       override the auto-detection:

	   "v1000", "v2x00".

       The  driver will auto-detect the amount of video memory present for all chips. If the amount of memory is detected incorrectly, the actual amount of
       video memory should be specified with a VideoRam entry in the config file "Device" section.

       The following driver Options are supported:

       Option "SWCursor" "boolean"
	      Disables use of the hardware cursor. Default: use HW-cursor.

       Option "OverclockMem" "boolean"
	      Increases the Mem/Sys clock to 125MHz/60MHz from standard 110MHz/50MHz.  Default: Not overclocked.

       Option "DacSpeed" "MHz"
	      Run the memory at a higher clock. Useful on some cards with display glitches at higher resolutions. But adds the risk to damage the hardware.
	      Use with caution.

       Option "FramebufferWC" "boolean"
	      If  writecombine is disabled in BIOS, and you add this option in configuration file, then the driver will try to request writecombined access
	      to the framebuffer. This can drastically increase the performance on unaccelerated server. Requires that "MTRR"-support is compiled into	the
	      OS-kernel.  Default: Disabled for V1000, enabled for V2100/V2200.

       Option "NoDDC" "boolean"
	      Disable probing of DDC-information from your monitor. This information is not used yet and is only there for informational purposes.  Safe to
	      disable if you experience problems during startup of X-server.  Default: Probe DDC.

       Option "ShadowFB" "boolean"
	      If this option is enabled, the driver will cause the CPU to do each drawing operation first into a shadow frame buffer in system virtual mem‐
	      ory  and	then  copy the result into video memory. If this option is not active, the CPU will draw directly into video memory.  Enabling this
	      option is beneficial for those systems where reading from video memory is, on average, slower than the corresponding read/modify/write opera‐
	      tion in system virtual memory.  This is normally the case for PCI or AGP adapters, and, so, this option is enabled by default unless acceler‐
	      ation is enabled.  Default: Enabled unless acceleration is used.

       Option "Rotate" "CW"

       Option "Rotate" "CCW"
	      Rotate the display clockwise or counterclockwise.  This mode is unaccelerated.  Default: no rotation.


       Notes  For the moment the driver defaults to not request write-combine for any chipset as there has been indications of problems with it. Use Option
	      "MTRR" to let the driver request write-combining of memory access on the video board.

SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

AUTHORS
       Authors include: Marc Langenbach, Dejan Ilic



X Version 11							 xf86-video-rendition 4.2.3						       RENDITION(4)
