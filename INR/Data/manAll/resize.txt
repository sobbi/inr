RESIZE(1)																	  RESIZE(1)



NAME
       resize - set TERMCAP and terminal settings to current xterm window size

SYNOPSIS
       resize [ -u | -c ] [ -s [ row col ] ]

DESCRIPTION
       Resize  prints  a  shell  command for setting the TERM and TERMCAP environment variables to indicate the current size of xterm window from which the
       command is run.	For this output to take effect, resize must either be evaluated as part of the command line (usually done with	a  shell  alias  or
       function)  or  else  redirected	to  a  file  which can then be read in.  From the C shell (usually known as /bin/csh), the following alias could be
       defined in the user's .cshrc:

	       %  alias rs 'set noglob; eval `resize`'

       After resizing the window, the user would type:

	       %  rs

       Users of versions of the Bourne shell (usually known as /bin/sh) that don't have command functions will need to send the output to a temporary  file
       and then read it back in with the “.” command:

	       $  resize > /tmp/out
	       $  . /tmp/out

OPTIONS
       The following options may be used with resize:

       -u      This option indicates that Bourne shell commands should be generated even if the user's current shell isn't /bin/sh.

       -c      This option indicates that C shell commands should be generated even if the user's current shell isn't /bin/csh.

       -s [rows columns]
	       This option indicates that Sun console escape sequences will be used instead of the VT100-style xterm escape codes.  If rows and columns are
	       given, resize will ask the xterm to resize itself.  However, the window manager may choose to disallow the change.

	       Note that the Sun console escape sequences are recognized by XFree86 xterm and by dtterm.  The resize program may be installed  as  sunsize,
	       which causes makes it assume the -s option.

	       The rows and columns arguments must appear last; though they are normally associated with the -s option, they are parsed separately.

FILES
       /etc/termcap   for the base termcap entry to modify.

       ~/.cshrc       user's alias for the command.

SEE ALSO
       csh(1), tset(1), xterm(1)

AUTHORS
       Mark Vandevoorde (MIT-Athena), Edward Moy (Berkeley)
       Copyright (c) 1984, 1985 by X Consortium
       See X(7) for a complete copyright notice.



								      X Window System								  RESIZE(1)
