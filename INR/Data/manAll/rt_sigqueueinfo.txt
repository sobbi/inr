SIGQUEUE(2)							 Linux Programmer's Manual							SIGQUEUE(2)



NAME
       sigqueue, rt_sigqueueinfo - queue a signal and data to a process

SYNOPSIS
       #include <signal.h>

       int sigqueue(pid_t pid, int sig, const union sigval value);

   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):

       sigqueue(): _POSIX_C_SOURCE >= 199309L

DESCRIPTION
       sigqueue()  sends  the  signal specified in sig to the process whose PID is given in pid.  The permissions required to send a signal are the same as
       for kill(2).  As with kill(2), the null signal (0) can be used to check if a process with a given PID exists.

       The value argument is used to specify an accompanying item of data (either an integer or a pointer value) to be sent with the signal,  and  has	the
       following type:

	   union sigval {
	       int   sival_int;
	       void *sival_ptr;
	   };

       If  the	receiving  process  has installed a handler for this signal using the SA_SIGINFO flag to sigaction(2), then it can obtain this data via the
       si_value field of the siginfo_t structure passed as the second argument to the handler.	Furthermore, the si_code field of that	structure  will  be
       set to SI_QUEUE.

RETURN VALUE
       On  success,  sigqueue() returns 0, indicating that the signal was successfully queued to the receiving process.  Otherwise -1 is returned and errno
       is set to indicate the error.

ERRORS
       EAGAIN The limit of signals which may be queued has been reached.  (See signal(7) for further information.)

       EINVAL sig was invalid.

       EPERM  The process does not have permission to send the signal to the receiving process.  For the required permissions, see kill(2).

       ESRCH  No process has a PID matching pid.

VERSIONS
       This system call first appeared in Linux 2.2.

CONFORMING TO
       POSIX.1-2001.

NOTES
       If this function results in the sending of a signal to the process that invoked it, and that signal was not blocked by the calling  thread,  and  no
       other  threads  were willing to handle this signal (either by having it unblocked, or by waiting for it using sigwait(3)), then at least some signal
       must be delivered to this thread before this function returns.

       On Linux, the underlying system call is actually named rt_sigqueueinfo(), and differs in its third argument, which is the siginfo_t  structure  that
       will be supplied to the receiving process's signal handler or returned by the receiving process's sigtimedwait(2) call.	Inside the glibc sigqueue()
       wrapper, this argument, info, is initialized as follows:

	   info.si_signo = sig;      /* argument supplied to sigqueue() */
	   info.si_code = SI_QUEUE;
	   info.si_pid = getpid();   /* Process ID of sender */
	   info.si_uid = getuid();   /* Real UID of sender */
	   info.si_value = val;      /* argument supplied to sigqueue() */

SEE ALSO
       kill(2), sigaction(2), signal(2), sigwait(3), signal(7)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2007-07-26								SIGQUEUE(2)
