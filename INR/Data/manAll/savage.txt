SAVAGE(4)																	  SAVAGE(4)



NAME
       savage - S3 Savage video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "savage"
	 ...
       EndSection

DESCRIPTION
       savage  is  an  Xorg driver for the S3 Savage family video accelerator chips.  2D, 3D, and Xv acceleration is supported on all chips except the Sav‐
       age2000 (2D only).  Dualhead operation is supported on MX, IX, and SuperSavage chips.  The savage driver supports PCI and AGP boards with  the  fol‐
       lowing chips:

       Savage3D        (8a20 and 8a21) (2D, 3D)

       Savage4	       (8a22) (2D, 3D)

       Savage2000      (9102) (2D only)

       Savage/MX       (8c10 and 8c11) (2D, 3D, Dualhead)

       Savage/IX       (8c12 and 8c13) (2D, 3D, Dualhead)

       SuperSavage/MX  (8c22, 8c24, and 8c26) (2D, 3D, Dualhead)

       SuperSavage/IX  (8c2a, 8c2b, 8c2c, 8c2d, 8c2e, and 8c2f) (2D, 3D, Dualhead)

       ProSavage PM133 (8a25) (2D, 3D)

       ProSavage KM133 (8a26) (2D, 3D)

       Twister (ProSavage PN133)
		       (8d01) (2D, 3D)

       TwisterK (ProSavage KN133)
		       (8d02) (2D, 3D)

       ProSavage DDR   (8d03) (2D, 3D)

       ProSavage DDR-K (8d04) (2D, 3D)

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration details.	This section only covers configuration details specific to this driver.

       The following driver Options are supported:

       Option "HWCursor" "boolean"

       Option "SWCursor" "boolean"
	      These  two  options  interact  to specify hardware or software cursor.  If the SWCursor option is specified, any HWCursor setting is ignored.
	      Thus, either "HWCursor off" or "SWCursor on" will force the use of the software cursor.  On Savage/MX and Savage/IX chips which are connected
	      to LCDs, a software cursor will be forced, because the Savage hardware cursor does not correctly track the automatic panel expansion feature.
	      Default: hardware cursor.

       Option "NoAccel" "boolean"
	      Disable or enable acceleration.  Default: acceleration is enabled.

       Option "AccelMethod" "string"
	      Chooses between available acceleration architectures.  Valid options are XAA and EXA.  XAA is the traditional acceleration  architecture	and
	      support for it is very stable.  EXA is a newer acceleration architecture with better performance for the Render and Composite extensions, but
	      the rendering code for it is newer and possibly unstable.  The default is XAA.

       Option "Rotate" "CW"

       Option "Rotate" "CCW"
	      Rotate the desktop 90 degrees clockwise or counterclockwise.  This option forces the ShadowFB option on, and disables  acceleration  and	the
	      RandR extension.	Default: no rotation.

       Option "ShadowFB" "boolean"
	      Enable or disable use of the shadow framebuffer layer.  This option disables acceleration.  Default: off.

       Option "LCDClock" "frequency"
	      Override	the maximum dot clock.	Some LCD panels produce incorrect results if they are driven at too fast of a frequency.  If UseBIOS is on,
	      the BIOS will usually restrict the clock to the correct range.  If not, it might be necessary to override it here.  The  frequency  parameter
	      may be specified as an integer in Hz (135750000), or with standard suffixes like "k", "kHz", "M", or "MHz" (as in 135.75MHz).

       Option "CrtOnly" "boolean"
	      This option disables output to the LCD and enables output to the CRT port only.  It is useful on laptops if you only want to use the CRT port
	      or to force the CRT output only on desktop cards that use mobile chips. Default: auto-detect active outputs

       Option "UseBIOS" "boolean"
	      Enable or disable use of the video BIOS to change modes.	Ordinarily, the savage driver tries to use the video  BIOS  to	do  mode  switches.
	      This  generally  produces  the  best results with the mobile chips (/MX and /IX), since the BIOS knows how to handle the critical but unusual
	      timing requirements of the various LCD panels supported by the chip.  To do this, the driver searches through the BIOS mode list, looking for
	      the mode which most closely matches the xorg.conf mode line.  Some purists find this scheme objectionable.  If you would rather have the sav‐
	      age driver use your mode line timing exactly, turn off the UseBios option.  Note: Use  of  the  BIOS  is	required  for  dualhead  operation.
	      Default: on (use the BIOS).

       Option "IgnoreEDID" "boolean"
	      Do not use EDID data for mode validation, but DDC is still used for monitor detection. This is different from NoDDC option.
	      The default value is off.

       Option "ShadowStatus" "boolean"
	      Enables  the  use  of a shadow status register.  There is a chip bug in the Savage graphics engine that can cause a bus lock when reading the
	      engine status register under heavy load, such as when scrolling text or dragging windows.  The bug affects about 4% of all Savage users with‐
	      out  DRI	and  a large fraction of users with DRI.  If your system hangs regularly while scrolling text or dragging windows, try turning this
	      option on.  This uses an alternate method of reading the engine status which is slightly more expensive, but avoids the problem.	When DRI is
	      enabled then the default is "on" (use shadow status), otherwise the default is "off" (use normal status register).

       Option "DisableCOB" "boolean"
	      Disables	the  COB (Command Overflow Buffer) on savage4 and newer chips.	There is supposedly a HW cache coherency problem on certain savage4
	      and newer chips that renders the COB useless. If you are having problems with 2D acceleration you can disable the COB, however you will  lose
	      some performance.  3D acceleration requires the COB to work.  This option only applies to Savage4 and newer chips.  Default: "off" (use COB).

       Option "BCIforXv" "boolean"
	      Use  the	BCI to copy and reformat Xv pixel data.  Using the BCI for Xv causes graphics artifacts on some chips.	This option only applies to
	      Savage4 and prosavage/twister chips. On some combinations of chipsets and video players, BCI formatting might actually be slower	than  soft‐
	      ware  formatting	("AGPforXv"  might  help in this case). BCI formatting can only be used on video data with a width that is a multiple of 16
	      pixels (which is the vast majority of videos).  Other widths are handled through software formatting. Default: on for prosavage  and  twister
	      (use BCI for Xv); off for savage4 (do not use the BCI for Xv).

       Option "AGPforXv" "boolean"
	      Instructs  the BCI Xv pixel formatter to use AGP memory as a scratch buffer.  Ordinarily the BCI formatter uses a an area in framebuffer mem‐
	      ory to hold YV12 planar data to be converted for display. This requires a somewhat expensive upload of YV12 data to framebuffer  memory.	The
	      "AGPforXv"  option  causes the BCI formatter to place the YV12 data in AGP memory instead, which can be uploaded faster than the framebuffer.
	      Use of this option cuts upload overhead by 25% according to benchmarks. This option also smooths out most of the shearing present when  using
	      BCI for pixel conversion. Currently this option is experimental and is disabled by default. Video width restrictions that apply to "BCIforXv"
	      also apply here. Only valid when "DRI" and "BCIforXv" are both active, and only on AGP chipsets. Default: "off".
	      If "AccelMethod" is set to "EXA" and "AGPforXv" is enabled, then the driver will also attempt to reuse the AGP scratch buffer  for  UploadTo‐
	      Screen acceleration.

       Option "AGPMode" "integer"
	      Set AGP data transfer rate.  (used only when DRI is enabled)
	      1      -- x1 (default)
	      2      -- x2
	      4      -- x4
	      others -- invalid

       Option "AGPSize" "integer"
	      The amount of AGP memory that will allocated for DMA and textures in MB. Valid sizes are 4, 8, 16, 32, 64, 128 and 256. The default is 16MB.

       Option "DmaMode" "string"
	      This option influences in which way DMA (direct memory access) is used by the kernel and 3D drivers.
	      Any      -- Try command DMA first, then vertex DMA (default)
	      Command  -- Only use command DMA or don't use DMA at all
	      Vertex   -- Only use vertex DMA or don't use DMA at all
	      None     -- Disable DMA
	      Command  and  vertex DMA cannot be enabled at the same time. Which DMA mode is actually used in the end also depends on the DRM version (only
	      >= 2.4.0 supports command DMA) and the hardware (Savage3D/MX/IX doesn't support command DMA).

       Option "DmaType" "string"
	      The type of memory that will be used by the 3D driver for DMA (direct memory access).
	      PCI    -- PCI memory (default on PCI cards)
	      AGP    -- AGP memory (default on AGP cards)
	      "AGP" only works if you have an AGP card.

       Option "BusType" "string"
	      The bus type that will be used to access the graphics card.
	      PCI    -- PCI bus (default)
	      AGP    -- AGP bus
	      "AGP" only works if you have an AGP card. If you choose "PCI" on an AGP card the AGP bus speed is not set and no AGP aperture  is  allocated.
	      This implies DmaType "PCI".

       Option "DRI" "boolean"
	      Enable DRI support.  This option allows you to enable or disable the DRI.  Default: "on" (enable DRI).

FILES
       savage_drv.o

SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

AUTHORS
       Authors	include  Tim Roberts (timr@probo.com) and Ani Joshi (ajoshi@unixbox.com) for this version, and Tim Roberts and S. Marineau for the original
       driver from which this was derived.



X Version 11							  xf86-video-savage 2.3.1							  SAVAGE(4)
