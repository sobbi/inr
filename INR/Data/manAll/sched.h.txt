<sched.h>(P)							 POSIX Programmer's Manual						       <sched.h>(P)



NAME
       sched.h - execution scheduling (REALTIME)

SYNOPSIS
       #include <sched.h>

DESCRIPTION
       The  <sched.h> header shall define the sched_param structure, which contains the scheduling parameters required for implementation of each supported
       scheduling policy. This structure shall contain at least the following member:


	      int	sched_priority	      Process execution scheduling priority.

       In addition, if _POSIX_SPORADIC_SERVER or _POSIX_THREAD_SPORADIC_SERVER is defined, the sched_param structure defined in <sched.h> shall contain the
       following members in addition to those specified above:


	      int	      sched_ss_low_priority Low scheduling priority for
						    sporadic server.
	      struct timespec sched_ss_repl_period  Replenishment period for
						    sporadic server.
	      struct timespec sched_ss_init_budget  Initial budget for sporadic server.
	      int	      sched_ss_max_repl     Maximum pending replenishments for
						    sporadic server.

       Each process is controlled by an associated scheduling policy and priority. Associated with each policy is a priority range.  Each policy definition
       specifies the minimum priority range for that policy. The priority ranges for each policy may overlap the priority ranges of other policies.

       Four scheduling policies are defined; others may be defined by the implementation. The four standard policies are indicated by  the  values  of	the
       following symbolic constants:

       SCHED_FIFO
	      First in-first out (FIFO) scheduling policy.

       SCHED_RR
	      Round robin scheduling policy.

       SCHED_SPORADIC
	      Sporadic server scheduling policy.

       SCHED_OTHER
	      Another scheduling policy.


       The values of these constants are distinct.

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int    sched_get_priority_max(int);
	      int    sched_get_priority_min(int);

	      int    sched_getparam(pid_t, struct sched_param *);
	      int    sched_getscheduler(pid_t);

	      int    sched_rr_get_interval(pid_t, struct timespec *);

	      int    sched_setparam(pid_t, const struct sched_param *);
	      int    sched_setscheduler(pid_t, int, const struct sched_param *);

	      int    sched_yield(void);



       Inclusion of the <sched.h> header may make visible all symbols from the <time.h> header.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <time.h>

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							       <sched.h>(P)
