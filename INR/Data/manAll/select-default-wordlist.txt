SELECT-DEFAULT-WORDLIST(8)													 SELECT-DEFAULT-WORDLIST(8)



NAME
	   select-default-wordlist - select default wordlist

SYNOPSIS
	select-default-wordlist [--rebuild]

DESCRIPTION
       This program will make debconf always ask the shared question about the default wordlist to be used in your system according to the installed ones,
       and will do the appropriate settings.

       Calls internally update-default-wordlist.

OPTIONS
	 --rebuild    Rebuild emacsen and jed stuff

SEE ALSO
       The dictionaries-common policy document

AUTHORS
       Rafael Laboissiere



1.4.0ubuntu2								 2010-1-20						 SELECT-DEFAULT-WORDLIST(8)
