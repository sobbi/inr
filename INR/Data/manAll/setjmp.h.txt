<setjmp.h>(P)							 POSIX Programmer's Manual						      <setjmp.h>(P)



NAME
       setjmp.h - stack environment declarations

SYNOPSIS
       #include <setjmp.h>

DESCRIPTION
       Some  of the functionality described on this reference page extends the ISO C standard. Applications shall define the appropriate feature test macro
       (see the System Interfaces volume of IEEE Std 1003.1-2001, Section 2.2, The Compilation Environment) to enable the visibility of  these	symbols  in
       this header.

       The <setjmp.h> header shall define the array types jmp_buf and	sigjmp_buf.

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      void   longjmp(jmp_buf, int);

	      void   siglongjmp(sigjmp_buf, int);


	      void  _longjmp(jmp_buf, int);


       The following may be declared as a function, or defined as a macro, or both. Function prototypes shall be provided.


	      int    setjmp(jmp_buf);

	      int    sigsetjmp(sigjmp_buf, int);


	      int   _setjmp(jmp_buf);


       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       The System Interfaces volume of IEEE Std 1003.1-2001, longjmp(), _longjmp(), setjmp(), siglongjmp(), sigsetjmp()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							      <setjmp.h>(P)
