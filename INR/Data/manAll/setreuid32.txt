SETREUID(2)							 Linux Programmer's Manual							SETREUID(2)



NAME
       setreuid, setregid - set real and/or effective user or group ID

SYNOPSIS
       #include <sys/types.h>
       #include <unistd.h>

       int setreuid(uid_t ruid, uid_t euid);
       int setregid(gid_t rgid, gid_t egid);

   Feature Test Macro Requirements for glibc (see feature_test_macros(7)):

       setreuid(), setregid(): _BSD_SOURCE || _XOPEN_SOURCE >= 500

DESCRIPTION
       setreuid() sets real and effective user IDs of the calling process.

       Supplying a value of -1 for either the real or effective user ID forces the system to leave that ID unchanged.

       Unprivileged processes may only set the effective user ID to the real user ID, the effective user ID or the saved set-user-ID.

       POSIX:  It  is unspecified whether unprivileged processes may set the real user ID to the real user ID, the effective user ID or the saved set-user-
       ID.

       Linux: Unprivileged users may only set the real user ID to the real user ID or the effective user ID.

       Linux: If the real user ID is set or the effective user ID is set to a value not equal to the previous real user ID, the saved set-user-ID  will  be
       set to the new effective user ID.

       Completely  analogously,  setregid()  sets  real and effective group ID's of the calling process, and all of the above holds with "group" instead of
       "user".

RETURN VALUE
       On success, zero is returned.  On error, -1 is returned, and errno is set appropriately.

ERRORS
       EPERM  The calling process is not privileged (Linux: does not have the CAP_SETUID capability in the case of setreuid(), or the CAP_SETGID capability
	      in  the case of setregid()) and a change other than (i) swapping the effective user (group) ID with the real user (group) ID, or (ii) setting
	      one to the value of the other or (iii) setting the effective user (group) ID to the value of the saved set-user-ID (saved  set-group-ID)	was
	      specified.

CONFORMING TO
       POSIX.1-2001, 4.3BSD (the setreuid() and setregid() function calls first appeared in 4.2BSD).

NOTES
       Setting the effective user (group) ID to the saved set-user-ID (saved set-group-ID) is possible since Linux 1.1.37 (1.1.38).

SEE ALSO
       getgid(2), getuid(2), seteuid(2), setgid(2), setresuid(2), setuid(2), capabilities(7)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2007-07-26								SETREUID(2)
