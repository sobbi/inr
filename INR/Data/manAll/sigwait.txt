SIGWAIT(P)							 POSIX Programmer's Manual							 SIGWAIT(P)



NAME
       sigwait - wait for queued signals

SYNOPSIS
       #include <signal.h>

       int sigwait(const sigset_t *restrict set, int *restrict sig);


DESCRIPTION
       The  sigwait() function shall select a pending signal from set, atomically clear it from the system's set of pending signals, and return that signal
       number in the location referenced by sig. If prior to the call to sigwait() there are multiple pending instances of a single signal  number,  it  is
       implementation-defined  whether upon successful return there are any remaining pending signals for that signal number.	 If the implementation sup‐
       ports queued signals and there are multiple signals queued for the signal number selected, the first such queued signal shall cause  a  return  from
       sigwait() and the remainder shall remain queued.  If no signal in set is pending at the time of the call, the thread shall be suspended until one or
       more becomes pending. The signals defined by set shall have been blocked at the time of the call to sigwait(); otherwise, the behavior is undefined.
       The effect of sigwait() on the signal actions for the signals in set is unspecified.

       If  more than one thread is using sigwait() to wait for the same signal, no more than one of these threads shall return from sigwait() with the sig‐
       nal number. Which thread returns from sigwait() if more than a single thread is waiting is unspecified.

       Should any of the multiple pending signals in the range SIGRTMIN to SIGRTMAX be selected, it shall be the lowest numbered one. The  selection  order
       between realtime and non-realtime signals, or between multiple pending non-realtime signals, is unspecified.

RETURN VALUE
       Upon successful completion, sigwait() shall store the signal number of the received signal at the location referenced by sig and return zero. Other‐
       wise, an error number shall be returned to indicate the error.

ERRORS
       The sigwait() function may fail if:

       EINVAL The set argument contains an invalid or unsupported signal number.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       To provide a convenient way for a thread to wait for a signal, this volume of IEEE Std 1003.1-2001 provides the sigwait() function. For	most  cases
       where a thread has to wait for a signal, the sigwait() function should be quite convenient, efficient, and adequate.

       However,  requests were made for a lower-level primitive than sigwait() and for semaphores that could be used by threads.  After some consideration,
       threads were allowed to use semaphores and sem_post() was defined to be async-signal and async-cancel-safe.

       In summary, when it is necessary for code run in response to an asynchronous signal to notify a thread, sigwait() should be used to handle the  sig‐
       nal.  Alternatively,  if  the implementation provides semaphores, they also can be used, either following sigwait() or from within a signal handling
       routine previously registered with sigaction().

FUTURE DIRECTIONS
       None.

SEE ALSO
       Signal Concepts , Realtime Signals , pause() , pthread_sigmask() , sigaction() , sigpending() , sigsuspend() , sigwaitinfo() , the Base	Definitions
       volume of IEEE Std 1003.1-2001, <signal.h>, <time.h>

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								 SIGWAIT(P)
