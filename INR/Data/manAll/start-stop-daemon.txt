start-stop-daemon(8)	      dpkg-Hilfsprogramme	  start-stop-daemon(8)



NAME
       start-stop-daemon - startet und stoppt System-Daemon-Programme

ÜBERSICHT
       start-stop-daemon [Optionen] Befehl

BESCHREIBUNG
       start-stop-daemon  wird	zur Steuerung der Erzeugung und Beendigung von
       Prozessen auf Systemebene verwendet. Durch  die	Verwendung  einer  der
       Abgleich-Optionen  kann	start-stop-daemon so konfiguriert werden, dass
       er existierende Instanzen von einem laufenden Prozess finden kann.

       Hinweis:  Falls	 --pidfile   nicht   angegeben	 ist,	verhält   sich
       start-stop-daemon  ähnlich  zu  killall(1).  start-stop-daemon wird die
       Prozesstabelle nach Prozessen durchsuchen, die  auf  den  Prozessnamen,
       uid  und/oder gid (falls angegeben) passen. Jeder passende Prozess wird
       --start daran hindern, den Daemon zu starten. Allen passenden Prozessen
       wird  das  TERM-Signal  (oder das mit --signal oder --retry angegebene)
       geschickt, falls --stop	angegeben  ist.  Für  Daemons  die  langlebige
       Kinder  haben,  die  ein  --stop  überleben  müssen,  müssen  Sie  eine
       PID-Datei angeben.

BEFEHLE
       -S, --start [--] Argumente
	      Prüfe  auf  Existenz  eines  angegebenen	Prozesses.  Falls  ein
	      solcher Prozess existiert führt start-stop-daemon nichts aus und
	      beendet sich mit Fehlerstatus 1  (0,  falls  --oknodo  angegeben
	      wurde).  Falls ein solcher Prozess nicht existiert, dann startet
	      es eine Instanz, entweder unter Verwendung des ausführbaren Pro‐
	      gramms,  das  mittels  --exec  (oder,  falls  angegeben, mittels
	      --startas) spezifiziert wurde. Jedes  weitere  auf  der  Komman‐
	      dozeile  nach  -- angegebene Argument wird unverändert an das zu
	      startende Programm weitergegeben.

       -K, --stop
	      Überprüft auf die Existenz eines speziellen Prozesses. Falls ein
	      solcher  Prozess	existiert,  dann  sendet start-stop-daemon das
	      durch --signal angegebene Signal und beendet sich mit Fehlersta‐
	      tus  0.  Falls kein solcher Prozess existiert, dann beendet sich
	      start-stop-daemon mit Fehlerstatus 1 (0 falls --oknodo angegeben
	      ist).  Falls  --retry  angegeben	wurde,	wird start-stop-daemon
	      überprüfen, ob der/die Prozess(e) beendet wurden.

       -H, --help
	      Zeige Bedienungsinformationen und beendet sich.

       -V, --version
	      Zeige die Programmversion und beendet sich.

ABGLEICH-OPTIONEN
       -p, --pidfile pid-Datei
	      Überprüfe, ob ein Prozess die Datei pid-Datei angelegt hat.

       -x, --exec Programm
	      Überprüfe auf Prozesse die (laut	/proc/pid/exe)	Instanzen  von
	      diesem Programm sind.

       -n, --name Prozessname
	      Überprüfe  (laut	/proc/pid/stat)  auf  Prozesse	mit  dem Namen
	      Prozessname.

       -u, --user Benutzername|uid
	      Überprüfe auf Prozesse die dem mit Benutzername oder uid	spezi‐
	      fizierten Benutzer gehören.

OPTIONEN
       -g, --group Gruppe|gid
	      Wechsle zum Starten des Prozesses zur Gruppe oder gid.

       -s, --signal Signal
	      Mit  --stop  wird  das  an den zu beendenden Prozess zu sendende
	      Signal spezifiziert (standardmäßig TERM).

       -R, --retry Zeitüberschreitung|Plan
	      Mit --stop spezifiziert, dass start-stop-daemon überprüfen soll,
	      ob der Prozess (die Prozesse) sich beenden. Es überprüft wieder‐
	      holt, ob passende Prozesse laufen, bis dies nicht mehr der  Fall
	      ist.  Falls  sich  die Prozesse nicht beenden, werden weitere im
	      »Plan« angegebene Aktionen durchgeführt.

	      Falls Zeitüberschreitung anstelle von  Plan  spezifiziert  wird,
	      dann wird der Plan Signal/Zeitüberschreitung/KILL/Zeitüberschre‐
	      itung verwendet, wobei Signal  das  mit  --signal  spezifizierte
	      Signal ist.

	      Plan  ist eine Liste von mindestens zwei durch Schrägstriche (/)
	      getrennten Punkten; jeder  Punkt	kann  aus  -Signalnummer  oder
	      [-]Signalname   bestehen,   was  bedeutet,  dass	dieses	Signal
	      gesendet werden soll, oder aus Zeitüberschreitung, was bedeutet,
	      dass  soviele  Sekunden  auf  das Beenden von Prozessen gewartet
	      werden soll, oder aus forever was bedeutet, den Rest  des  Plans
	      falls notwendig für immer zu wiederholen.

	      Falls  das Ende des Plans erreicht wird und forever nicht spezi‐
	      fiziert wurde, dann beendet sich der start-stop-daemon  mit  dem
	      Fehlerstatus  2.	Falls  ein  Plan spezifiziert wurde, dann wird
	      jedes mit --signal spezifizierte Signal ignoriert.

       -a, --startas Pfadname
	      Mit --start wird der über Pfadname  spezifizierte  Prozess  ges‐
	      tartet.  Falls  nicht  angegeben,  werden  standardmäßig	die an
	      --exec übergebenen Argumente verwendet.

       -t, --test
	      Gebe die	Aktionen  aus,	die  erledigt  würden  und  setze  die
	      entsprechenden Rückgabewerte, führe aber keine Aktionen durch.

       -o, --oknodo
	      Liefert  den  Rückgabewert 0 anstatt 1 falls keine Aktionen aus‐
	      geführt wurden (würden).

       -q, --quiet
	      Gebe keine informativen Meldungen aus, zeige nur Fehlermeldungen
	      an.

       -c ,  --chuid Benutzername|uid
	      Wechsele vor dem Start des Prozesses zu diesem Benutzername/uid.
	      Sie können durch Anhängen von : auch die	Gruppe	spezifizieren,
	      in   diesem   Fall   wird  die  Gruppe  oder  gid  wie  bei  dem
	      »chown«-Befehl (Benutzer:Gruppe) angegeben. Falls  ein  Benutzer
	      ohne  Gruppe  angegeben  ist,  wird  die	primäre GID für diesen
	      Benutzer verwandt. Wenn Sie diese Option verwenden,  müssen  Sie
	      daran  denken,  dass  die  primäre  und zusätzliche Gruppen auch
	      gesetzt werden, selbst wenn die Option --group  nicht  angegeben
	      wird.  Die  Option  --group  ist	nur  für Gruppen, in denen der
	      Benutzer normalerweise kein Mitglied ist (wie das Hinzufügen von
	      pro-Prozess  Gruppenmitgliedschaften für generische Benutzer wie
	      nobody).

       -r, --chroot Wurzel
	      Chdir und chroot vor dem Start des  Prozesse  zu	Wurzel.  Bitte
	      beachten	 Sie,	dass   die  PID-Datei  auch  nach  dem	chroot
	      geschrieben wird.

       -d, --chdir Pfad
	      Chdir vor dem Starten des Prozesses zu Pfad.  Dies  wird,  falls
	      die   -r|--chroot   Option   gesetzt   ist,   nach   dem	chroot
	      durchgeführt. Falls nicht angegeben, wird start-stop-daemon  vor
	      dem Prozess-Start in das Wurzelverzeichnis wechseln.

       -b, --background
	      Typischerweise   verwendet   für	 Programme,   die  sich  nicht
	      selbständig ablösen. Diese Option zwingt	start-stop-daemon  vor
	      dem  Start  des  Prozesses  einen Fork durchzuführen, und zwingt
	      diesen dann in den Hintergrund. WARNUNG: start-stop-daemon  kann
	      nicht  den Rückgabewert überprüfen, falls der Prozess aus irgen‐
	      deinem Grund nicht startet. Dies ist ein letztes Mittel und  ist
	      nur  für	Programme gedacht, bei denen das selbstständige Forken
	      keinen Sinn ergibt oder wo  es  nicht  sinnvoll  ist,  den  Code
	      hierfür hinzuzufügen.

       -N, --nicelevel Ganzzahl
	      Dies ändert die Priorität des Prozesses bevor er gestartet wird.

       -P, --procsched Strategie:Priorität
	      Dies  ändert die Prozesssteuerstrategie (»process scheduler pol‐
	      icy«) und die Priorität des Prozesses,  bevor  dieser  gestartet
	      wird. Die Priorität kann optional festgelegt werden, indem ein :
	      gefolgt von einem Wert angegeben	wird.  Die  Standard-Priorität
	      beträgt  0.  Die	derzeit  unterstützten Werte für die Strategie
	      lauten other, fifo und rr.

       -I, --iosched Klasse:Priorität
	      Dies ändert die IO-Steuerklasse (»IO scheduler class«)  und  die
	      Priorität  des  Prozesses, bevor dieser gestartet wird. Die Pri‐
	      orität kann optional festgelegt werden, indem ein : gefolgt  von
	      einem  Wert  angegeben  wird.  Die Standard-Priorität beträgt 4,
	      außer Klasse lautet idle, dann beträgt sie immer 7. Die  derzeit
	      unterstützten  Werte für die Klasse lauten idle, best-effort und
	      real-time.

       -k, --umask Maske
	      Dies setzt die umask des Prozesses bevor er gestartet wird.

       -m, --make-pidfile
	      Verwendet, wenn ein Programm gestartet wird,  das  keine	eigene
	      PID-Datei anlegt. Diese Option sorgt dafür, dass start-stop-dae‐
	      mon die mit --pidfile referenzierte Datei  anlegt  und  die  PID
	      kurz  vor  der Ausführung des Prozesse hineinlegt. Beachten Sie,
	      dass die Datei nicht entfernt wird, wenn	das  Programm  beendet
	      wird. HINWEIS: Diese Funktion könnte in nicht allen Fällen funk‐
	      tionieren. Insbesondere wenn das auszuführende Programm sich vom
	      Hauptprozess  forkt.  Deshalb ist diese Option normalerweise nur
	      in Kombination mit der Option --background sinnvoll.

       -v, --verbose
	      Gibt ausführliche informative Meldungen aus.

RÜCKGABEWERT
       start-stop-daemon liefert 0, falls die angeforderte  Aktion  ausgeführt
       wurde  oder --oknodo angegeben und entweder --start angegeben wurde und
       ein passender Prozess lief bereits oder --stop angebeben wurde  und  es
       gab  keinen passenden Prozess. Falls --oknodo nicht angegeben wurde und
       nichts erfolgte, wird  1  zurückgeliefert.  Falls  --stop  und  --retry
       angegeben  wurden,  aber  das  Ende  des  Plans errreicht wurde und der
       Prozess noch läuft, lautet der Fehlerwert 2. Bei allen anderen  Fehlern
       ist der Status 3.

BEISPIEL
       Starte  den  Daemon food falls noch keiner läuft (ein Prozess mit Namen
       food, der als Benutzer food mit PID in food.pid läuft):

	      start-stop-daemon --start --oknodo --user food --name food --pidfile /var/run/food.pid --startas /usr/sbin/food --chuid food -- --daemon

       Schicke SIGTERM an food und warte bis zu fünf Sekunden auf  sein  Been‐
       den:

	      start-stop-daemon --stop --oknodo --user food --name food --pidfile /var/run/food.pid --retry 5

       Vorführung eines angepassten Plans zum Beenden von food:

	      start-stop-daemon --stop --oknodo --user food --name food --pidfile /var/run/food.pid --retry=TERM/30/KILL/5

AUTOREN
       Marek  Michalkiewicz  <marekm@i17linuxb.ists.pwr.wroc.pl> basierend auf
       einer vorherigen Version von Ian Jackson <ian@chiark.greenend.org.uk>.

       Handbuchseite von Klee Dienes <klee@mit.edu>, teilweise von Ian Jackson
       umformatiert.

ÜBERSETZUNG
       Die  deutsche  Übersetzung  wurde  2004, 2006-2009 von Helge Kreutzmann
       <debian@helgefjell.de>, 2007 von Florian Rehnisch  <eixman@gmx.de>  und
       2008  von Sven Joachim <svenjoac@gmx.de> angefertigt. Diese Übersetzung
       ist Freie Dokumentation; lesen Sie die GNU General Public License  Ver‐
       sion 2 oder neuer für die Kopierbedingungen.  Es gibt KEINE HAFTUNG.



Debian-Projekt			  2009-02-26		  start-stop-daemon(8)
