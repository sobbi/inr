<sys/stat.h>(P) 						 POSIX Programmer's Manual						    <sys/stat.h>(P)



NAME
       sys/stat.h - data returned by the stat() function

SYNOPSIS
       #include <sys/stat.h>

DESCRIPTION
       The <sys/stat.h> header shall define the structure of the data returned by the functions fstat(), lstat(), and stat().

       The stat structure shall contain at least the following members:


	      dev_t	st_dev	   Device ID of device containing file.
	      ino_t	st_ino	   File serial number.
	      mode_t	st_mode    Mode of file (see below).
	      nlink_t	st_nlink   Number of hard links to the file.
	      uid_t	st_uid	   User ID of file.
	      gid_t	st_gid	   Group ID of file.

	      dev_t	st_rdev    Device ID (if file is character or block special).

	      off_t	st_size    For regular files, the file size in bytes.
				   For symbolic links, the length in bytes of the
				   pathname contained in the symbolic link.

				   For a shared memory object, the length in bytes.


				   For a typed memory object, the length in bytes.

				   For other file types, the use of this field is
				   unspecified.
	      time_t	st_atime   Time of last access.
	      time_t	st_mtime   Time of last data modification.
	      time_t	st_ctime   Time of last status change.

	      blksize_t st_blksize A file system-specific preferred I/O block size for
				   this object. In some file system types, this may
				   vary from file to file.
	      blkcnt_t	st_blocks  Number of blocks allocated for this object.


       The  st_ino  and  st_dev fields taken together uniquely identify the file within the system. The blkcnt_t, blksize_t, dev_t, ino_t, mode_t, nlink_t,
       uid_t, gid_t, off_t, and time_t types shall be defined as described in <sys/types.h> . Times shall be given in seconds since the Epoch.

       Unless otherwise specified, the structure members st_mode, st_ino, st_dev, st_uid, st_gid, st_atime, st_ctime, and st_mtime  shall  have  meaningful
       values for all file types defined in IEEE Std 1003.1-2001.

       For  symbolic links, the st_mode member shall contain meaningful information, which can be used with the file type macros described below, that take
       a mode argument. The st_size member shall contain the length, in bytes, of the pathname contained in the symbolic link. File mode bits and the  con‐
       tents of the remaining members of the stat structure are unspecified. The value returned in the st_size field shall be the length of the contents of
       the symbolic link, and shall not count a trailing null if one is present.

       The following symbolic names for the values of type mode_t shall also be defined.

       File type:

       S_IFMT Type of file.

       S_IFBLK
	      Block special.

       S_IFCHR
	      Character special.

       S_IFIFO
	      FIFO special.

       S_IFREG
	      Regular.

       S_IFDIR
	      Directory.

       S_IFLNK
	      Symbolic link.

       S_IFSOCK
	      Socket.



       File mode bits:

       S_IRWXU
	      Read, write, execute/search by owner.

       S_IRUSR
	      Read permission, owner.

       S_IWUSR
	      Write permission, owner.

       S_IXUSR
	      Execute/search permission, owner.


       S_IRWXG
	      Read, write, execute/search by group.

       S_IRGRP
	      Read permission, group.

       S_IWGRP
	      Write permission, group.

       S_IXGRP
	      Execute/search permission, group.


       S_IRWXO
	      Read, write, execute/search by others.

       S_IROTH
	      Read permission, others.

       S_IWOTH
	      Write permission, others.

       S_IXOTH
	      Execute/search permission, others.


       S_ISUID
	      Set-user-ID on execution.

       S_ISGID
	      Set-group-ID on execution.

       S_ISVTX
	      On directories, restricted deletion flag.


       The bits defined by S_IRUSR, S_IWUSR, S_IXUSR, S_IRGRP, S_IWGRP, S_IXGRP, S_IROTH, S_IWOTH, S_IXOTH, S_ISUID, S_ISGID,	  and  S_ISVTX	  shall  be
       unique.

       S_IRWXU is the bitwise-inclusive OR of S_IRUSR, S_IWUSR, and S_IXUSR.

       S_IRWXG is the bitwise-inclusive OR of S_IRGRP, S_IWGRP, and S_IXGRP.

       S_IRWXO is the bitwise-inclusive OR of S_IROTH, S_IWOTH, and S_IXOTH.

       Implementations	may  OR  other	implementation-defined	bits  into  S_IRWXU, S_IRWXG, and S_IRWXO, but they shall not overlap any of the other bits
       defined in this volume of IEEE Std 1003.1-2001. The file permission bits are defined to be  those  corresponding  to  the  bitwise-inclusive  OR  of
       S_IRWXU, S_IRWXG, and S_IRWXO.

       The  following  macros shall be provided to test whether a file is of the specified type. The value m supplied to the macros is the value of st_mode
       from a stat structure.  The macro shall evaluate to a non-zero value if the test is true; 0 if the test is false.

       S_ISBLK(m)
	      Test for a block special file.

       S_ISCHR(m)
	      Test for a character special file.

       S_ISDIR(m)
	      Test for a directory.

       S_ISFIFO(m)
	      Test for a pipe or FIFO special file.

       S_ISREG(m)
	      Test for a regular file.

       S_ISLNK(m)
	      Test for a symbolic link.

       S_ISSOCK(m)
	      Test for a socket.


       The implementation may implement message queues, semaphores, or shared memory objects as distinct file types. The following macros shall be provided
       to  test  whether  a file is of the specified type. The value of the buf argument supplied to the macros is a pointer to a stat structure. The macro
       shall evaluate to a non-zero value if the specified object is implemented as a distinct file type and the specified file type is  contained  in	the
       stat structure referenced by buf. Otherwise, the macro shall evaluate to zero.

       S_TYPEISMQ(buf)
	      Test for a message queue.

       S_TYPEISSEM(buf)
	      Test for a semaphore.

       S_TYPEISSHM(buf)
	      Test for a shared memory object.


       The  implementation may implement typed memory objects as distinct file types, and the following macro shall test whether a file is of the specified
       type. The value of the buf argument supplied to the macros is a pointer to a stat structure.  The macro shall evaluate to a non-zero  value  if	the
       specified  object  is  implemented as a distinct file type and the specified file type is contained in the stat structure referenced by buf.  Other‐
       wise, the macro shall evaluate to zero.

       S_TYPEISTMO(buf)
	      Test macro for a typed memory object.


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int    chmod(const char *, mode_t);
	      int    fchmod(int, mode_t);
	      int    fstat(int, struct stat *);
	      int    lstat(const char *restrict, struct stat *restrict);
	      int    mkdir(const char *, mode_t);
	      int    mkfifo(const char *, mode_t);

	      int    mknod(const char *, mode_t, dev_t);

	      int    stat(const char *restrict, struct stat *restrict);
	      mode_t umask(mode_t);

       The following sections are informative.

APPLICATION USAGE
       Use of the macros is recommended for determining the type of a file.

RATIONALE
       A conforming C-language application must include <sys/stat.h> for functions that have arguments or return values of type mode_t,  so  that  symbolic
       values for that type can be used.  An alternative would be to require that these constants are also defined by including <sys/types.h>.

       The S_ISUID and S_ISGID bits may be cleared on any write, not just on open(), as some historical implementations do.

       System  calls  that  update the time entry fields in the stat structure must be documented by the implementors.	POSIX-conforming systems should not
       update the time entry fields for functions listed in the System Interfaces volume of IEEE Std 1003.1-2001 unless the standard requires that they do,
       except in the case of documented extensions to the standard.

       Note that st_dev must be unique within a Local Area Network (LAN) in a ``system'' made up of multiple computers' file systems connected by a LAN.

       Networked  implementations of a POSIX-conforming system must guarantee that all files visible within the file tree (including parts of the tree that
       may be remotely mounted from other machines on the network) on each individual processor are uniquely identified by the combination  of	the  st_ino
       and st_dev fields.

       The  unit for the st_blocks member of the stat structure is not defined within IEEE Std 1003.1-2001. In some implementations it is 512 bytes. It may
       differ on a file system basis. There is no correlation between values of the st_blocks and st_blksize, and the f_bsize (from <sys/statvfs.h>) struc‐
       ture members.

       Traditionally, some implementations defined the multiplier for st_blocks in <sys/param.h> as the symbol DEV_BSIZE.

FUTURE DIRECTIONS
       No  new	S_IFMT symbolic names for the file type values of mode_t will be defined by IEEE Std 1003.1-2001; if new file types are required, they will
       only be testable through S_ISxx() or S_TYPEISxxx() macros instead.

SEE ALSO
       <sys/statvfs.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, chmod(),  fchmod(),  fstat(),  lstat(),	mkdir(),  mkfifo(),
       mknod(), stat(), umask()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							    <sys/stat.h>(P)
