<sys/statvfs.h>(P)						 POSIX Programmer's Manual						 <sys/statvfs.h>(P)



NAME
       sys/statvfs.h - VFS File System information structure

SYNOPSIS
       #include <sys/statvfs.h>

DESCRIPTION
       The <sys/statvfs.h> header shall define the statvfs structure that includes at least the following members:


	      unsigned long f_bsize    File system block size.
	      unsigned long f_frsize   Fundamental file system block size.
	      fsblkcnt_t    f_blocks   Total number of blocks on file system in units of f_frsize.
	      fsblkcnt_t    f_bfree    Total number of free blocks.
	      fsblkcnt_t    f_bavail   Number of free blocks available to
				       non-privileged process.
	      fsfilcnt_t    f_files    Total number of file serial numbers.
	      fsfilcnt_t    f_ffree    Total number of free file serial numbers.
	      fsfilcnt_t    f_favail   Number of file serial numbers available to
				       non-privileged process.
	      unsigned long f_fsid     File system ID.
	      unsigned long f_flag     Bit mask of f_flag values.
	      unsigned long f_namemax  Maximum filename length.

       The fsblkcnt_t and fsfilcnt_t types shall be defined as described in <sys/types.h> .

       The following flags for the f_flag member shall be defined:

       ST_RDONLY
	      Read-only file system.

       ST_NOSUID
	      Does not support the semantics of the ST_ISUID and ST_ISGID file mode bits.


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int statvfs(const char *restrict, struct statvfs *restrict);
	      int fstatvfs(int, struct statvfs *);

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, fstatvfs(), statvfs()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							 <sys/statvfs.h>(P)
