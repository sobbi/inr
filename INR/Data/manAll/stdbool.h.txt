<stdbool.h>(P)							 POSIX Programmer's Manual						     <stdbool.h>(P)



NAME
       stdbool.h - boolean type and values

SYNOPSIS
       #include <stdbool.h>

DESCRIPTION
       The <stdbool.h> header shall define the following macros:

       bool   Expands to _Bool.

       true   Expands to the integer constant 1.

       false  Expands to the integer constant 0.

       __bool_true_false_are_defined

	      Expands to the integer constant 1.


       An application may undefine and then possibly redefine the macros bool, true, and false.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       The ability to undefine and redefine the macros bool, true, and false is an obsolescent feature and may be withdrawn in a future version.

SEE ALSO
       None.

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							     <stdbool.h>(P)
