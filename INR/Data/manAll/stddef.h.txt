<stddef.h>(P)							 POSIX Programmer's Manual						      <stddef.h>(P)



NAME
       stddef.h - standard type definitions

SYNOPSIS
       #include <stddef.h>

DESCRIPTION
       The <stddef.h> header shall define the following macros:

       NULL   Null pointer constant.

       offsetof(type, member-designator)

	      Integer  constant  expression of type size_t, the value of which is the offset in bytes to the structure member (member-designator), from the
	      beginning of its structure (type).


       The <stddef.h> header shall define the following types:

       ptrdiff_t
	      Signed integer type of the result of subtracting two pointers.

       wchar_t
	      Integer type whose range of values can represent distinct wide-character codes for all members of the largest character set  specified  among
	      the  locales  supported by the compilation environment: the null character has the code value 0 and each member of the portable character set
	      has a code value equal to its value when used as the lone character in an integer character constant.

       size_t Unsigned integer type of the result of the sizeof operator.


       The implementation shall support one or more programming environments in which the widths of ptrdiff_t, size_t, and wchar_t are no greater than	the
       width of type long.  The names of these programming environments can be obtained using the confstr() function or the getconf utility.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <wchar.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, confstr(), the Shell and Utilities volume of IEEE Std 1003.1-2001,
       getconf

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							      <stddef.h>(P)
