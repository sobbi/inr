SYNC(1) 							       User Commands								    SYNC(1)



NAME
       sync - flush file system buffers

SYNOPSIS
       sync [OPTION]

DESCRIPTION
       Force changed blocks to disk, update the super block.

       --help display this help and exit

       --version
	      output version information and exit

AUTHOR
       Written by Jim Meyering.

REPORTING BUGS
       Report sync bugs to bug-coreutils@gnu.org
       GNU coreutils home page: <http://www.gnu.org/software/coreutils/>
       General help using GNU software: <http://www.gnu.org/gethelp/>

COPYRIGHT
       Copyright © 2009 Free Software Foundation, Inc.	License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

SEE ALSO
       sync(2)

       The full documentation for sync is maintained as a Texinfo manual.  If the info and sync programs are properly installed at your site, the command

	      info coreutils 'sync invocation'

       should give you access to the complete manual.



GNU coreutils 7.4							 March 2010								    SYNC(1)
