<sys/msg.h>(P)							 POSIX Programmer's Manual						     <sys/msg.h>(P)



NAME
       sys/msg.h - XSI message queue structures

SYNOPSIS
       #include <sys/msg.h>

DESCRIPTION
       The <sys/msg.h> header shall define the following data types through typedef:

       msgqnum_t
	      Used for the number of messages in the message queue.

       msglen_t
	      Used for the number of bytes allowed in a message queue.


       These types shall be unsigned integer types that are able to store values at least as large as a type unsigned short.

       The <sys/msg.h> header shall define the following constant as a message operation flag:

       MSG_NOERROR
	      No error if big message.


       The msqid_ds structure shall contain the following members:


	      struct ipc_perm msg_perm	 Operation permission structure.
	      msgqnum_t       msg_qnum	 Number of messages currently on queue.
	      msglen_t	      msg_qbytes Maximum number of bytes allowed on queue.
	      pid_t	      msg_lspid  Process ID of last msgsnd
	       ().
	      pid_t	      msg_lrpid  Process ID of last msgrcv
	       ().
	      time_t	      msg_stime  Time of last msgsnd
	       ().
	      time_t	      msg_rtime  Time of last msgrcv
	       ().
	      time_t	      msg_ctime  Time of last change.

       The pid_t, time_t, key_t, size_t, and ssize_t types shall be defined as described in <sys/types.h> .

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int	msgctl(int, int, struct msqid_ds *);
	      int	msgget(key_t, int);
	      ssize_t	msgrcv(int, void *, size_t, long, int);
	      int	msgsnd(int, const void *, size_t, int);

       In addition, all of the symbols from <sys/ipc.h> shall be defined when this header is included.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <sys/ipc.h> , <sys/types.h> , msgctl(), msgget(), msgrcv(), msgsnd()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							     <sys/msg.h>(P)
