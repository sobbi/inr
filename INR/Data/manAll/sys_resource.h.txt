<sys/resource.h>(P)						 POSIX Programmer's Manual						<sys/resource.h>(P)



NAME
       sys/resource.h - definitions for XSI resource operations

SYNOPSIS
       #include <sys/resource.h>

DESCRIPTION
       The  <sys/resource.h>  header  shall define the following symbolic constants as possible values of the which argument of getpriority() and setprior‐
       ity():

       PRIO_PROCESS
	      Identifies the who argument as a process ID.

       PRIO_PGRP
	      Identifies the who argument as a process group ID.

       PRIO_USER
	      Identifies the who argument as a user ID.


       The following type shall be defined through typedef:

       rlim_t Unsigned integer type used for limit values.


       The following symbolic constants shall be defined:

       RLIM_INFINITY
	      A value of rlim_t indicating no limit.

       RLIM_SAVED_MAX
	      A value of type rlim_t indicating an unrepresentable saved hard limit.

       RLIM_SAVED_CUR
	      A value of type rlim_t indicating an unrepresentable saved soft limit.


       On implementations where all resource limits are representable in an object of type rlim_t, RLIM_SAVED_MAX and RLIM_SAVED_CUR need not  be  distinct
       from RLIM_INFINITY.

       The following symbolic constants shall be defined as possible values of the who parameter of getrusage():

       RUSAGE_SELF
	      Returns information about the current process.

       RUSAGE_CHILDREN
	      Returns information about children of the current process.


       The <sys/resource.h> header shall define the rlimit structure that includes at least the following members:


	      rlim_t rlim_cur  The current (soft) limit.
	      rlim_t rlim_max  The hard limit.

       The <sys/resource.h> header shall define the rusage structure that includes at least the following members:


	      struct timeval ru_utime  User time used.
	      struct timeval ru_stime  System time used.

       The timeval structure shall be defined as described in <sys/time.h> .

       The following symbolic constants shall be defined as possible values for the resource argument of getrlimit() and setrlimit():

       RLIMIT_CORE
	      Limit on size of core file.

       RLIMIT_CPU
	      Limit on CPU time per process.

       RLIMIT_DATA
	      Limit on data segment size.

       RLIMIT_FSIZE
	      Limit on file size.

       RLIMIT_NOFILE
	      Limit on number of open files.

       RLIMIT_STACK
	      Limit on stack size.

       RLIMIT_AS
	      Limit on address space size.


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int  getpriority(int, id_t);
	      int  getrlimit(int, struct rlimit *);
	      int  getrusage(int, struct rusage *);
	      int  setpriority(int, id_t, int);
	      int  setrlimit(int, const struct rlimit *);

       The id_t type shall be defined through typedef as described in <sys/types.h> .

       Inclusion of the <sys/resource.h> header may also make visible all symbols from <sys/time.h>.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <sys/time.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, getpriority(), getrusage(), getrlimit()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							<sys/resource.h>(P)
