<sys/uio.h>(P)							 POSIX Programmer's Manual						     <sys/uio.h>(P)



NAME
       sys/uio.h - definitions for vector I/O operations

SYNOPSIS
       #include <sys/uio.h>

DESCRIPTION
       The <sys/uio.h> header shall define the iovec structure that includes at least the following members:


	      void   *iov_base	Base address of a memory region for input or output.
	      size_t  iov_len	The size of the memory pointed to by iov_base.

       The <sys/uio.h> header uses the iovec structure for scatter/gather I/O.

       The ssize_t and size_t types shall be defined as described in <sys/types.h>.

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      ssize_t readv(int, const struct iovec *, int);
	      ssize_t writev(int, const struct iovec *, int);

       The following sections are informative.

APPLICATION USAGE
       The  implementation  can  put  a  limit on the number of scatter/gather elements which can be processed in one call. The symbol {IOV_MAX} defined in
       <limits.h> should always be used to learn about the limits instead of assuming a fixed value.

RATIONALE
       Traditionally, the maximum number of scatter/gather elements the system can process in one call were described by the symbolic  value  {UIO_MAXIOV}.
       In IEEE Std 1003.1-2001 this value is replaced by the constant {IOV_MAX} which can be found in <limits.h>.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <limits.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, read(), write()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							     <sys/uio.h>(P)
