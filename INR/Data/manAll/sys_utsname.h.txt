<sys/utsname.h>(P)						 POSIX Programmer's Manual						 <sys/utsname.h>(P)



NAME
       sys/utsname.h - system name structure

SYNOPSIS
       #include <sys/utsname.h>

DESCRIPTION
       The <sys/utsname.h> header shall define the structure utsname which shall include at least the following members:


	      char  sysname[]  Name of this implementation of the operating system.
	      char  nodename[] Name of this node within an implementation-defined
			       communications network.
	      char  release[]  Current release level of this implementation.
	      char  version[]  Current version level of this release.
	      char  machine[]  Name of the hardware type on which the system is running.

       The character arrays are of unspecified size, but the data stored in them shall be terminated by a null byte.

       The following shall be declared as a function and may also be defined as a macro:


	      int uname(struct utsname *);

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       The System Interfaces volume of IEEE Std 1003.1-2001, uname()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							 <sys/utsname.h>(P)
