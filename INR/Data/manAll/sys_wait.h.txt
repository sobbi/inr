<sys/wait.h>(P) 						 POSIX Programmer's Manual						    <sys/wait.h>(P)



NAME
       sys/wait.h - declarations for waiting

SYNOPSIS
       #include <sys/wait.h>

DESCRIPTION
       The <sys/wait.h> header shall define the following symbolic constants for use with waitpid():

       WNOHANG
	      Do not hang if no status is available; return immediately.

       WUNTRACED
	      Report status of stopped child process.


       The <sys/wait.h> header shall define the following macros for analysis of process status values:

       WEXITSTATUS
	      Return exit status.

       WIFCONTINUED
	      True if child has been continued.

       WIFEXITED
	      True if child exited normally.

       WIFSIGNALED
	      True if child exited due to uncaught signal.

       WIFSTOPPED
	      True if child is currently stopped.

       WSTOPSIG
	      Return signal number that caused process to stop.

       WTERMSIG
	      Return signal number that caused process to terminate.


       The following symbolic constants shall be defined as possible values for the options argument to waitid():

       WEXITED
	      Wait for processes that have exited.

       WSTOPPED
	      Status is returned for any child that has stopped upon receipt of a signal.

       WCONTINUED
	      Status is returned for any child that was stopped and has been continued.

       WNOHANG
	      Return immediately if there are no children to wait for.

       WNOWAIT
	      Keep the process whose status is returned in infop in a waitable state.


       The type idtype_t shall be defined as an enumeration type whose possible values shall include at least the following: P_ALL P_PID P_PGID

       The id_t and pid_t types shall be defined as described in <sys/types.h> .

       The siginfo_t type shall be defined as described in <signal.h> .

       The rusage structure shall be defined as described in <sys/resource.h> .

       Inclusion of the <sys/wait.h> header may also make visible all symbols from <signal.h> and <sys/resource.h>.

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      pid_t  wait(int *);

	      int    waitid(idtype_t, id_t, siginfo_t *, int);

	      pid_t  waitpid(pid_t, int *, int);

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <signal.h> , <sys/resource.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, wait(), waitid()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							    <sys/wait.h>(P)
