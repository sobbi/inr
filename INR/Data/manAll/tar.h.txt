<tar.h>(P)							 POSIX Programmer's Manual							 <tar.h>(P)



NAME
       tar.h - extended tar definitions

SYNOPSIS
       #include <tar.h>

DESCRIPTION
       The <tar.h> header shall define header block definitions as follows.

       General definitions:

							  Name	    Description  Value
							  TMAGIC    "ustar"	 ustar plus null byte.
							  TMAGLEN   6		 Length of the above.
							  TVERSION  "00"	 00 without a null byte.
							  TVERSLEN  2		 Length of the above.

       Typeflag field definitions:

							   Name       Description   Value
							   REGTYPE    '0'	    Regular file.
							   AREGTYPE   '\0'	    Regular file.
							   LNKTYPE    '1'	    Link.
							   SYMTYPE    '2'	    Symbolic link.
							   CHRTYPE    '3'	    Character special.
							   BLKTYPE    '4'	    Block special.
							   DIRTYPE    '5'	    Directory.
							   FIFOTYPE   '6'	    FIFO special.
							   CONTTYPE   '7'	    Reserved.

       Mode field bit definitions (octal):

						 Name	 Description Value
						 TSUID	 04000	     Set UID on execution.
						 TSGID	 02000	     Set GID on execution.
						 TSVTX	 01000	     On directories, restricted deletion flag.
						 TUREAD  00400	     Read by owner.
						 TUWRITE 00200	     Write by owner special.
						 TUEXEC  00100	     Execute/search by owner.
						 TGREAD  00040	     Read by group.
						 TGWRITE 00020	     Write by group.
						 TGEXEC  00010	     Execute/search by group.
						 TOREAD  00004	     Read by other.
						 TOWRITE 00002	     Write by other.
						 TOEXEC  00001	     Execute/search by other.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       The Shell and Utilities volume of IEEE Std 1003.1-2001, pax

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								 <tar.h>(P)
