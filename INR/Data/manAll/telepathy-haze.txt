TELEPATHY-HAZE(8)						       D-Bus services							  TELEPATHY-HAZE(8)



NAME
       telepathy-haze - Telepathy connection manager using libpurple

SYNOPSIS
       /usr/lib/telepathy/telepathy-haze

DESCRIPTION
       Haze  implements  the Telepathy D-Bus specification using libpurple, allowing Telepathy clients like empathy(1) to connect to any protocol supported
       by pidgin(1).

       It is a D-Bus service which runs on the session bus, and should usually be started automatically by D-Bus activation. However, it might be useful to
       start it manually for debugging.

OPTIONS
       There are no command-line options.

ENVIRONMENT
       HAZE_DEBUG=type
	      May be set to "all" for full debug output, or various undocumented options (which may change from release to release) to filter the output.

       HAZE_PERSIST=1
	      If set, Haze will not automatically close itself after five seconds without any connections.

       HAZE_LOGFILE=filename
	      If set, all debugging output will be written to filename rather than to the terminal.

SEE ALSO
       http://telepathy.freedesktop.org/, empathy(1), pidgin(1)



Telepathy								October 2007							  TELEPATHY-HAZE(8)
