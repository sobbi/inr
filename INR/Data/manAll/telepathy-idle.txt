TELEPATHY-IDLE(8)						       D-Bus services							  TELEPATHY-IDLE(8)



NAME
       telepathy-idle - Telepathy connection manager for IRC

SYNOPSIS
       /usr/lib/telepathy/telepathy-idle

DESCRIPTION
       Idle implements the Telepathy D-Bus specification for Internet Relay Chat, allowing Telepathy clients like empathy(1) to connect to IRC servers.

       It is a D-Bus service which runs on the session bus, and should usually be started automatically by D-Bus activation. However, it might be useful to
       start it manually for debugging.

OPTIONS
       There are no command-line options.

ENVIRONMENT
       IDLE_LOGFILE=filename
	      If set, debug output will go to the given file rather than to stderr.  If + is prepended	to  the  filename  (e.g.   IDLE_LOGFILE=+idle.log),
	      debug output will be appended to the file; otherwise, any existing file will be replaced.


       IDLE_DEBUG=type
	      May  be  set  to "all" for full debug output, or various undocumented options (which may change from release to release) to filter the
	      output.

SEE ALSO
       http://telepathy.freedesktop.org/, empathy(1)



Telepathy								October 2007							  TELEPATHY-IDLE(8)
