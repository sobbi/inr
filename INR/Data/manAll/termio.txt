TERMIO(7)							 Linux Programmer's Manual							  TERMIO(7)



NAME
       termio - the System V terminal driver interface

DESCRIPTION
       termio  is the name of the old System V terminal driver interface.  This interface defined a termio structure used to store terminal settings, and a
       range of ioctl(2) operations to get and set terminal attributes.

       The termio interface is now obsolete: POSIX.1-1990 standardized a modified version of this interface, under the	name  termios.	 The  POSIX.1  data
       structure  differs  slightly  from  the	System	V version, and POSIX.1 defined a suite of functions to replace the various ioctl(2) operations that
       existed in System V.  (This was done because ioctl(2) was unstandardized, and its variadic third argument does not allow argument type checking.)

       If you're looking for page called "termio", then you can probably find most of the information that you seek in either termios(3) or tty_ioctl(4).

SEE ALSO
       termios(3), tty_ioctl(4)

COLOPHON
       This page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be  found
       at http://www.kernel.org/doc/man-pages/.



Linux									 2006-12-28								  TERMIO(7)
