texi2pdf(1)								   teTeX								texi2pdf(1)



NAME
       texi2pdf - create a PDF file from a Texinfo file

SYNOPSIS
       texi2pdf [ OPTION... ]  FILE...

DESCRIPTION
       texi2pdf sets the TEX environment variable to pdftex(1) and runs texi2dvi(1), passing all options to texi2dvi(1).

OPTIONS
       See texi2dvi(1) for relevant options.

ENVIRONMENT
       TEX    set to pdftex(1) by the script

SEE ALSO
       pdftex(1), texi2dvi(1), texinfo(5).

BUGS
       Problems with this script should be reported to the author or to the teTeX list, <tetex@dbs.uni-hannover.de> (mailing list).

       Problems with texi2dvi(1) should be reported to Karl Berry, the texinfo maintainer, at <bug-texinfo@gnu.org> (mailing list)

AUTHOR
       Written by Thomas Esser <te@dbs.uni-hannover.de>.

       This  manual  page was written by C.M. Connelly <c@eskimo.com>, for the Debian GNU/Linux system.  It may be used by other distributions without con‐
       tacting the author.  Any mistakes or omissions in the manual page are my fault; inquiries about	or  corrections  to  this  manual  page  should  be
       directed to me (and not to the primary author).



teTeX								       September 2000								texi2pdf(1)
