TIMES(P)							 POSIX Programmer's Manual							   TIMES(P)



NAME
       times - write process times

SYNOPSIS
       times

DESCRIPTION
       The  times utility shall write the accumulated user and system times for the shell and for all of its child processes, in the following POSIX locale
       format:


	      "%dm%fs %dm%fs\n%dm%fs %dm%fs\n", <shell user minutes>,
		  <shell user seconds>, <shell system minutes>,
		  <shell system seconds>, <children user minutes>,
		  <children user seconds>, <children system minutes>,
		  <children system seconds>

       The four pairs of times shall correspond  to  the  members  of  the  <sys/times.h>  tms	structure  (defined  in  the  Base  Definitions  volume  of
       IEEE Std 1003.1-2001, Chapter 13, Headers) as returned by times(): tms_utime, tms_stime, tms_cutime, and tms_cstime, respectively.

OPTIONS
       None.

OPERANDS
       None.

STDIN
       Not used.

INPUT FILES
       None.

ENVIRONMENT VARIABLES
       None.

ASYNCHRONOUS EVENTS
       Default.

STDOUT
       See the DESCRIPTION.

STDERR
       The standard error shall be used only for diagnostic messages.

OUTPUT FILES
       None.

EXTENDED DESCRIPTION
       None.

EXIT STATUS
       Zero.

CONSEQUENCES OF ERRORS
       Default.

       The following sections are informative.

APPLICATION USAGE
       None.

EXAMPLES
	      $ times
	      0m0.43s 0m1.11s
	      8m44.18s 1m43.23s

RATIONALE
       The times special built-in from the Single UNIX Specification is now required for all conforming shells.

FUTURE DIRECTIONS
       None.

SEE ALSO
       Special Built-In Utilities

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								   TIMES(P)
