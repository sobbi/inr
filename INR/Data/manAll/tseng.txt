TSENG(4)																	   TSENG(4)



NAME
       tseng - Tseng Labs video driver

SYNOPSIS
       Section "Device"
	 Identifier "devname"
	 Driver "tseng"
	 ...
       EndSection

DESCRIPTION
       tseng is an Xorg driver for Tseng Labs video cards.  THIS MAN PAGE NEEDS TO BE FILLED IN.

SUPPORTED HARDWARE
       The tseng driver supports...

CONFIGURATION DETAILS
       Please refer to xorg.conf(5) for general configuration details.	This section only covers configuration details specific to this driver.

SEE ALSO
       Xorg(1), xorg.conf(5), Xserver(1), X(7)

AUTHORS
       Authors include: ...



X Version 11							   xf86-video-tseng 1.2.3							   TSENG(4)
