TSORT(1)							       User Commands								   TSORT(1)



NAME
       tsort - perform topological sort

SYNOPSIS
       tsort [OPTION] [FILE]

DESCRIPTION
       Write totally ordered list consistent with the partial ordering in FILE.  With no FILE, or when FILE is -, read standard input.

       --help display this help and exit

       --version
	      output version information and exit

AUTHOR
       Written by Mark Kettenis.

REPORTING BUGS
       Report tsort bugs to bug-coreutils@gnu.org
       GNU coreutils home page: <http://www.gnu.org/software/coreutils/>
       General help using GNU software: <http://www.gnu.org/gethelp/>

COPYRIGHT
       Copyright © 2009 Free Software Foundation, Inc.	License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

SEE ALSO
       The full documentation for tsort is maintained as a Texinfo manual.  If the info and tsort programs are properly installed at your site, the command

	      info coreutils 'tsort invocation'

       should give you access to the complete manual.



GNU coreutils 7.4							 March 2010								   TSORT(1)
