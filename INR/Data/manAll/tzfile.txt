TZFILE(5)							 Linux Programmer's Manual							  TZFILE(5)



NAME
       tzfile - time zone information

SYNOPSIS
       #include <tzfile.h>

DESCRIPTION
       This page describes the structure of timezone files as commonly found in /usr/lib/zoneinfo or /usr/share/zoneinfo.

       The time zone information files used by tzset(3) begin with the magic characters "TZif" to identify then as time zone information files, followed by
       sixteen bytes reserved for future use, followed by six four-byte values of type long, written in a "standard" byte order (the high-order byte of the
       value is written first).  These values are, in order:

       tzh_ttisgmtcnt
	      The number of UTC/local indicators stored in the file.

       tzh_ttisstdcnt
	      The number of standard/wall indicators stored in the file.

       tzh_leapcnt
	      The number of leap seconds for which data is stored in the file.

       tzh_timecnt
	      The number of "transition times" for which data is stored in the file.

       tzh_typecnt
	      The number of "local time types" for which data is stored in the file (must not be zero).

       tzh_charcnt
	      The number of characters of "timezone abbreviation strings" stored in the file.

       The  above  header is followed by tzh_timecnt four-byte values of type long, sorted in ascending order.	These values are written in "standard" byte
       order.  Each is used as a transition time (as returned by time(2)) at which the rules for computing local time change.  Next come  tzh_timecnt  one-
       byte  values  of  type unsigned char; each one tells which of the different types of "local time" types described in the file is associated with the
       same-indexed transition time.  These values serve as indices into an array of ttinfo structures that appears next in the file; these structures	are
       defined as follows:

	   struct ttinfo {
	       long	    tt_gmtoff;
	       int	    tt_isdst;
	       unsigned int tt_abbrind;
	   };

       Each structure is written as a four-byte value for tt_gmtoff of type long, in a standard byte order, followed by a one-byte value for tt_isdst and a
       one-byte value for tt_abbrind.  In each structure, tt_gmtoff gives the number of seconds to be added to UTC, tt_isdst tells whether tm_isdst  should
       be  set by localtime(3), and tt_abbrind serves as an index into the array of timezone abbreviation characters that follow the ttinfo structure(s) in
       the file.

       Then there are tzh_leapcnt pairs of four-byte values, written in standard byte order; the first value of each pair gives the time  (as  returned  by
       time(2))  at  which a leap second occurs; the second gives the total number of leap seconds to be applied after the given time.	The pairs of values
       are sorted in ascending order by time.

       Then there are tzh_ttisstdcnt standard/wall indicators, each stored as a one-byte value; they tell whether  the	transition  times  associated  with
       local  time  types  were  specified  as standard time or wall clock time, and are used when a timezone file is used in handling POSIX-style timezone
       environment variables.

       Finally, there are tzh_ttisgmtcnt UTC/local indicators, each stored as a one-byte value; they tell whether  the	transition  times  associated  with
       local  time  types were specified as UTC or local time, and are used when a timezone file is used in handling POSIX-style timezone environment vari‐
       ables.

       Localtime uses the first standard-time ttinfo structure in the file (or simply the first ttinfo structure in the absence of a  standard-time  struc‐
       ture) if either tzh_timecnt is zero or the time argument is less than the first transition time recorded in the file.

NOTES
       This manual page documents <tzfile.h> in the glibc source archive, see timezone/tzfile.h.

       It  seems that timezone uses tzfile internally, but glibc refuses to expose it to userspace.  This is most likely because the standardised functions
       are more useful and portable, and actually documented by glibc.	It may only be in glibc just to  support  the  non-glibc-maintained  timezone  data
       (which is maintained by some other entity).

SEE ALSO
       time(3), gettimeofday(3), tzset(3), ctime(3)

COLOPHON
       This  page is part of release 3.23 of the Linux man-pages project.  A description of the project, and information about reporting bugs, can be found
       at http://www.kernel.org/doc/man-pages/.



									 1996-06-05								  TZFILE(5)
