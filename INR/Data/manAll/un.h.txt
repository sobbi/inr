<sys/un.h>(P)							 POSIX Programmer's Manual						      <sys/un.h>(P)



NAME
       sys/un.h - definitions for UNIX domain sockets

SYNOPSIS
       #include <sys/un.h>

DESCRIPTION
       The <sys/un.h> header shall define the sockaddr_un structure that includes at least the following members:


	      sa_family_t  sun_family  Address family.
	      char	   sun_path[]  Socket pathname.

       The  sockaddr_un  structure is used to store addresses for UNIX domain sockets. Values of this type shall be cast by applications to struct sockaddr
       for use with socket functions.

       The sa_family_t type shall be defined as described in <sys/socket.h> .

       The following sections are informative.

APPLICATION USAGE
       The size of sun_path has intentionally been left undefined.  This is because different implementations use different sizes.  For  example,  4.3	BSD
       uses a size of 108, and 4.4 BSD uses a size of 104. Since most implementations originate from BSD versions, the size is typically in the range 92 to
       108.

       Applications should not assume a particular length for sun_path or assume that it can hold {_POSIX_PATH_MAX} characters (255).

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <sys/socket.h> , the System Interfaces volume of IEEE Std 1003.1-2001, bind(), socket(), socketpair()

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							      <sys/un.h>(P)
