UNSHARE(1)								User Manuals								 UNSHARE(1)



NAME
       unshare - run program with some namespaces unshared from parent

SYNOPSIS
       unshare [options] program [arguments]

DESCRIPTION
       Unshares specified namespaces from parent process and then executes specified program. Unshareable namespaces are:

       mount namespace
	      mounting and unmounting filesystems will not affect rest of the system (CLONE_NEWNS flag),

       UTS namespace
	      setting hostname, domainname will not affect rest of the system (CLONE_NEWUTS flag),

       IPC namespace
	      process will have indpendent namespace for System V message queues, semaphore sets and shared memory segments (CLONE_NEWIPC flag),

       network namespace
	      process  will  have  independent	IPv4  and IPv6 stacks, IP routing tables, firewall rules, the /proc/net and /sys/class/net directory trees,
	      sockets etc. (CLONE_NEWNET flag).

       See the clone(2) for exact semantics of the flags.

OPTIONS
       -h, --help
	      Print a help message,

       -m, --mount
	      Unshare the mount namespace,

       -u, --uts
	      Unshare the UTC namespace,

       -i, --ipc
	      Unshare the IPC namespace,

       -n, --net
	      Unshare the network namespace.

SEE ALSO
       unshare(2), clone(2)

BUGS
       None known so far.

AUTHOR
       Mikhail Gusarov <dottedmag@dottedmag.net>

AVAILABILITY
       The unshare command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.



Linux									OCTOBER 2008								 UNSHARE(1)
