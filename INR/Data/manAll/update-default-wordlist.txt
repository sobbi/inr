UPDATE-DEFAULT-WORDLIST(8)													 UPDATE-DEFAULT-WORDLIST(8)



NAME
       update-default-wordlist - update default wordlist

SYNOPSIS
	update-default-wordlist [--rebuild] [--ignore-symlinks]

DESCRIPTION
       WARNING: Not to be used from the command line unless you know very well what you are doing.

       This program is intended to be called from package postinst (with --rebuild), from select-default-wordlist or from dictionaries-common postinst
       (with --ignore-symlinks).

       Reads the system default from the debconf database and set default links in /etc/dictionaries-common pointing to the appropriate files in
       /usr/share/dict/.  If option --rebuild is given, rebuilds the /var/cache/dictionaries-common/wordlist.db  from the files in
       /var/lib/dictionaries-common/wordlist

OPTIONS
       --rebuild	  Rebuild database, emacsen and jed stuff --ignore-symlinks  Do not set symlinks

SEE ALSO
       The dictionaries-common policy document

AUTHORS
       Rafael Laboissiere



1.4.0ubuntu2								 2010-1-20						 UPDATE-DEFAULT-WORDLIST(8)
