UPDATE-GRUB(8)																     UPDATE-GRUB(8)



NAME
       update-grub - stub for grub-mkconfig

SYNOPSIS
       update-grub

DESCRIPTION
       update-grub is a stub for running grub-mkconfig -o /boot/grub/grub.cfg to generate a grub2 config file.

SEE ALSO
       grub-mkconfig(8)



									 April 2009							     UPDATE-GRUB(8)
