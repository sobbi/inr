UPDATE-INFO-DIR(8)															 UPDATE-INFO-DIR(8)



NAME
       update-info-dir - (re-)create /usr/share/info/dir from all installed info files

SYNOPSIS
       update-info-dir [options]

DESCRIPTION
       This  manual  page  documents  briefly  the update-info-dir command.  This manual page was written for the Debian GNU/Linux distribution because the
       original script was designed for Debian packaging system.

       update-info-dir [info-directory] (re-)creates the index of available documentation in info format (the file /usr/share/info/dir)  which	is  usually
       presented by info browsers on startup.

OPTIONS
       info-directory
	      If an argument is given update the dir file in the directory given, otherwise use /usr/share/info.

AUTHOR
       This manual page was written by Norbert Preining <preining@logic.at>, for the Debian GNU/Linux system (but may be used by others).



																	 UPDATE-INFO-DIR(8)
