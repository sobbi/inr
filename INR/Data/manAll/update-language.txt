update-fontlang(1)						    Debian User's Manual						 update-fontlang(1)



NAME
       update-updmap, update-language, update-fmtutil, update-fontlang - update various TeX-related configuration files


SYNOPSIS
       update-updmap [options]
       update-language [options]
       update-fmtutil [options]


DESCRIPTION
       This manual page explains briefly the usage of the three TeX configuration update programs update-updmap, update-language and update-fmtutil.

       The  update-fontlang  script should not be called directly, but only via the three described links.  For a more in-depth description, please see the
       document TeX on Debian in /usr/share/doc/tex-common/TeX-on-Debian.txt.gz (also available as HTML and PDF).

       The programs update-updmap, update-language and update-fmtutil create or update the configuration files updmap.cfg,  language.dat  and  fmtutil.cnf,
       respectively.  These files define the the outline fonts available for the TeX system (updmap.cfg), the hyphenation patterns to be loaded into LaTeX-
       related TeX formats (language.dat), and the list of formats to be created (fmtutil.cnf).

       These programs can be used either in system-wide mode if called by root, or in a user-specific mode if called by a user	without  super-user  privi‐
       leges.

OPTIONS
       -c DIR, --conf-dir=DIR
	      directory  where the user-specific configuration files are looked for in user-specific mode (default: TEXMFCONFIG/updmap.d for update-updmap,
	      TEXMFCONFIG/language.d for update-language and TEXMFCONFIG/fmt.d for update-fmtutil, where TEXMFCONFIG is usually $HOME/.texmf-config).

       -o FILE, --output-file=FILE
	      file to write the output to. Per default, in system-wide	mode,  update-updmap  writes  to  /var/lib/texmf/web2c/updmap.cfg,  update-language
	      writes to /var/lib/texmf/tex/generic/config/language.dat and update-fmtutil writes to /var/lib/texmf/web2c/fmtutil.cnf.

       --checks
	      perform sanity checks on the generated config file. Don't use this in maintainer scripts.

       --quiet
	      don't write anything to the standard output during normal operation

       --help print a summary of the command-line usage and exit

       --version
	      output version information and exit


USAGE
       In  system-wide	mode,  all  three  programs  merge those files ("configuration snippets") with a specific extension in the respective configuration
       directories to produce the final file. These configuration directories and extensions are language.d and .cnf for update-language, updmap.d and .cfg
       for update-updmap, and fmt.d and .cnf for update-fmtutil.  In system-wide mode, these directories are those under /etc/texmf/. Both TeX add-on pack‐
       ages and local administrators can add files to these directories.

       If a package that provides such snippets is removed but not purged, including the snippet will likely break the system.	To prevent the inclusion in
       these cases, snippets installed by packages have to contain a magic header:

       # -_- DebPkgProvidedMaps -_-

       which  local  administrators  should  not remove.  From the files with a magic header, only those files which are also listed in one of the files in
       /var/lib/tex-common/fontmap-cfg/ for update-updmap, /var/lib/tex-common/language-cnf/ for update-language, and /var/lib/tex-common/fmtutil-cnf/	for
       update-fmtutil, are actually included into the final output file. This way, local changes to the configuration can be preserved while the package is
       in state 'rc' (that is, the package is removed, but its configuration files are still present).	For details about this mechanism, package maintain‐
       ers  should consult the Debian TeX Policy.  As a special case, the files for JadeTeX and xmlTeX are only included if there is already a file for the
       LaTeX format (see TeX on Debian for details).

       The user-specific mode provides a way for a non-admin user to override system-wide  settings.   In  this  mode,	update-language  writes  to  TEXMF‐
       VAR/tex/generic/config/language.dat,  update-updmap  writes  to	TEXMFVAR/web2c/updmap.cfg, and update-fmtutil writes to TEXMFVAR/web2c/fmtutil.cnf,
       where TEXMFVAR is usually $HOME/.texmf-var.  Furthermore, files present within the user-specific configuration directories are included in  addition
       to  the files present in the system-wide configuration directories.  In case the same filename exists in the system-wide configuration directory and
       the user-specific configuration directory, the user-specific file is used instead of the system-wide one. The user-specific  configuration  directo‐
       ries  are  TEXMFCONFIG/updmap.d for update-updmap, TEXMFCONFIG/language.d for update-language and TEXMFCONFIG/fmt.d for update-fmtutil, where TEXMF‐
       CONFIG is usually $HOME/.texmf-config. The system-wide configuration directories have the same names, but are  located  in  /etc/texmf/	instead  of
       TEXMFCONFIG.

       Note that changes introduced by updates of packages are not propagated to the user's configuration files. This has to be done by hand.


FILES
       /var/lib/texmf/tex/generic/config/language.dat
	      This  file  is generated or updated by update-language in system-wide mode and contains a list of the hyphenation patterns loaded into LaTeX-
	      based formats by fmtutil-sys.

       /var/lib/texmf/web2c/updmap.cfg
	      This file is generated or updated by update-updmap in system-wide mode and contains a list of map files to be included into the lists of out‐
	      line fonts generated by updmap-sys.

       /var/lib/texmf/web2c/fmtutil.cnf
	      This file is generated or updated by update-fmtutil in system-wide mode and contains a list of formats to be generated by fmtutil-sys.

       /etc/texmf/language.d/name.cnf
	      Input files for update-language

       /etc/texmf/updmap.d/name.cfg
	      Input files for update-updmap

       /etc/texmf/fmt.d/name.cnf
	      Input files for update-fmtutil

       /var/lib/tex-common/language-cnf/package.list
	      Lists the file(s) installed by package in /etc/texmf/language.d/.

       /var/lib/tex-common/fontmap-cfg/package.list
	      Lists the file(s) installed by package in /etc/texmf/updmap.d/.

       /var/lib/tex-common/fmtutil-cnf/package.list
	      Lists the file(s) installed by package in /etc/texmf/fmt.d/.


SEE ALSO
       updmap(1), updmap-sys(1), fmtutil(1), fmtutil-sys(1)
	      The programs actually using the generated configuration files (updmap.cfg, language.dat and fmtutil.cnf).

       TeX on Debian Documentation
	      to  be  found  in /usr/share/doc/tex-common/TeX-on-Debian.txt.gz (also available as HTML and PDF), describing in more detail how to setup and
	      maintain a TeX system in Debian. It also includes details on user-specific configuration.

       Debian TeX Policy
	      to be found in /usr/share/doc/tex-common/Debian-TeX-Policy.txt.gz (also available as HTML and PDF), describing the internals and the TeX Pol‐
	      icy  established	on  the  Debian TeX mailing-list (debian-tex-maint@lists.debian.org). Intended audience is mainly developers packaging TeX-
	      related resources for Debian.

       dh_installtex(1)
	      a debhelper-like script for managing the installation of files into the system-wide configuration directories; this  script  helps  to  write
	      Debian packages containing TeX-related resources that conform to the Debian TeX Policy.


AUTHOR
       This manual page was written by Norbert Preining <preining@debian.org> for the Debian distribution (and may be used by others). It was later updated
       by Florent Rougon <f.rougon@free.fr>.



Debian									 2006-12-11							 update-fontlang(1)
