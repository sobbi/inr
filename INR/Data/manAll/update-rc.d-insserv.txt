update-bootsystem-insserv(8)												       update-bootsystem-insserv(8)



NAME
       update-rc.d-insserv - obsolete

DESCRIPTION
       update-rc.d-insserv  is an obsolete implementation of the update-rc.d interface for the dependency based boot sequencing system insserv.  The imple‐
       mentation was moved into the sysv-rc update-rc.d implementation.

SEE ALSO
       update-rc.d(8), insserv(8), update-bootsystem-insserv(8)

AUTHOR
       Petter Reinholdtsen, <pere@hungry.com>



Petter Reinholdtsen							28 July 2009					       update-bootsystem-insserv(8)
