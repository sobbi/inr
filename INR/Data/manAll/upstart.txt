init(8) 																	    init(8)



NAME
       init - Upstart process management daemon

SYNOPSIS
       init [OPTION]...

DESCRIPTION
       init  is the parent of all processes on the system, it is executed by the kernel and is responsible for starting all other processes; it is the par‐
       ent of all processes whose natural parents have died and it is responsible for reaping those when they die.

       Processes managed by init are known as jobs and are defined by files in the /etc/init directory.   See  init(5)	for  more  details  on	configuring
       Upstart.

   Events
       init(8)	is  an	event-based init daemon.  This means that jobs will be automatically started and stopped by changes that occur to the system state,
       including as a result of jobs starting and stopping.

       This is different to dependency-based init daemons which start a specified set of goal jobs, and resolve the order in which they should	be  started
       and other jobs required by iterating their dependencies.

       For  more information on starting and stopping jobs, as well as emitting events that will automatically start and stop jobs, see the manual page for
       the initctl(8) tool.

       The primary event is the startup(7) event, emitted when the daemon has finished loading its configuration.  Other useful events are the starting(7),
       started(7), stopping(7) and stopped(7) events emitted as jobs change state.

   System V compatibility
       The Upstart init(8) daemon does not keep track of runlevels itself, instead they are implemented entirely by its userspace tools.  The event emitted
       to signify a change of runlevel is the runlevel(7) event.  For more information see its manual page.

OPTIONS
       Options are passed to init(8) by placing them on the kernel command-line.

       --verbose
	      Outputs verbose messages about job state changes and event emissions to the system console or log, useful for debugging boot.

NOTES
       init is not normally executed by a user process, and expects to have a process id of 1.	 If  this  is  not  the  case,	it  will  actually  execute
       telinit(8) and pass all arguments to that.  See that manual page for further details.

FILES
       /etc/init.conf

       /etc/init/*.conf

AUTHOR
       Written by Scott James Remnant <scott@netsplit.com>

REPORTING BUGS
       Report bugs at <https://launchpad.net/upstart/+bugs>

COPYRIGHT
       Copyright © 2010 Canonical Ltd.
       This  is  free software; see the source for copying conditions.	There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PUR‐
       POSE.

SEE ALSO
       init(5) initctl(8) telinit(8) runlevel(7) startup(7) starting(7) started(7) stopping(7) stopped(7)



Upstart 								 2010-02-04								    init(8)
