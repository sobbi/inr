<utmpx.h>(P)							 POSIX Programmer's Manual						       <utmpx.h>(P)



NAME
       utmpx.h - user accounting database definitions

SYNOPSIS
       #include <utmpx.h>

DESCRIPTION
       The <utmpx.h> header shall define the utmpx structure that shall include at least the following members:


	      char	      ut_user[]  User login name.
	      char	      ut_id[]	 Unspecified initialization process identifier.
	      char	      ut_line[]  Device name.
	      pid_t	      ut_pid	 Process ID.
	      short	      ut_type	 Type of entry.
	      struct timeval  ut_tv	 Time entry was made.

       The pid_t type shall be defined through typedef as described in <sys/types.h> .

       The timeval structure shall be defined as described in <sys/time.h> .

       Inclusion of the <utmpx.h> header may also make visible all symbols from <sys/time.h>.

       The following symbolic constants shall be defined as possible values for the ut_type member of the utmpx structure:

       EMPTY  No valid user accounting information.

       BOOT_TIME
	      Identifies time of system boot.

       OLD_TIME
	      Identifies time when system clock changed.

       NEW_TIME
	      Identifies time after system clock changed.

       USER_PROCESS
	      Identifies a process.

       INIT_PROCESS
	      Identifies a process spawned by the init process.

       LOGIN_PROCESS
	      Identifies the session leader of a logged-in user.

       DEAD_PROCESS
	      Identifies a session leader who has exited.


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      void	    endutxent(void);
	      struct utmpx *getutxent(void);
	      struct utmpx *getutxid(const struct utmpx *);
	      struct utmpx *getutxline(const struct utmpx *);
	      struct utmpx *pututxline(const struct utmpx *);
	      void	    setutxent(void);

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <sys/time.h> , <sys/types.h> , the System Interfaces volume of IEEE Std 1003.1-2001, endutxent()

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							       <utmpx.h>(P)
