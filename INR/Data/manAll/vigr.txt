VIPW(8) 		 Befehle zur Systemverwaltung		       VIPW(8)



NAME
       vipw, vigr - bearbeitet die Passwort-, Gruppen-, Shadow-Passwort- oder
       Shadow-Gruppen-Datei

SYNOPSIS
       vipw [Optionen]

       vigr [Optionen]

BESCHREIBUNG
       Die Befehle vipw und vigr bearbeiten die Dateien /etc/passwd
       beziehungsweise /etc/group. Mit der Option -s bearbeiten sie die
       Shadow-Versionen beider Dateien, /etc/shadow und /etc/gshadow. Die
       Programme werden die notwendige Sperre einrichten, um eine Beschädigung
       der Dateien zu verhindern. Wenn ein Editor benötigt wird, werden zuerst
       die Umgebungsvariable $VISUAL ausgewertet, danach die Umgebungsvariable
       $EDITOR. Zuletzt wird der Standardeditor vi(1) verwendet.

OPTIONEN
       Die Optionen, die von den Befehlen vipw und vigr unterstützt werden,
       sind:

       -g, --group
	   Bearbeitet die Gruppen-Datenbank.

       -h, --help
	   Zeigt die Hilfe an und beendet das Programm.

       -p, --passwd
	   Bearbeitet die Passwd-Datenbank.

       -q, --quiet
	   Stiller Modus.

       -s, --shadow
	   Bearbeitet die Shadow- oder Gshadow-Datenbank.

DATEIEN
       /etc/group
	   Informationen zu den Gruppenkonten.

       /etc/gshadow
	   Verschlüsselte Informationen zu den Gruppenkonten.

       /etc/passwd
	   Informationen zu den Benutzerkonten.

       /etc/shadow
	   Verschlüsselte Informationen zu den Benutzerkonten.

SIEHE AUCH
       vi(1), group(5), gshadow(5)passwd(5), shadow(5).



Befehle zur Systemverwaltung	  26.01.2010			       VIPW(8)
