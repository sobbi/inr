W3M(1)																		     W3M(1)



NAME
       w3m - a text based Web browser and pager

SYNOPSIS
       w3m [options] [URL or filename]

       Use "w3m -h" to display a complete list of current options.

DESCRIPTION
       w3m  is a World Wide Web (WWW) text based client. It has English and Japanese help files and an option menu and can be configured to use either lan‐
       guage. It will display hypertext markup language (HTML) documents containing links to files residing on the local system, as well as files  residing
       on  remote  systems.  It  can display HTML tables and frames.  In addition, it can be used as a "pager" in much the same manner as "more" or "less".
       Current versions of w3m run on Unix (Solaris, SunOS, HP-UX, Linux, FreeBSD, and EWS4800) and on Microsoft Windows 9x/NT.

OPTIONS
       At start up, w3m will load any local file or remote URL specified at the command line.  For help with runtime options, press "H" while running  w3m.
       Command line options are:

       -t tab set tab width

       -r     ignore backspace effect

       -l line
	      # of preserved line (default 10000)

       -B     load bookmark

       -bookmark file
	      specify bookmark file

       -T type
	      specify content-type

       -m     internet message mode

       -v     visual startup mode

       -M     monochrome display

       -F     automatically render frame

       -dump  dump formatted page into stdout

       -cols width
	      specify column width (used with -dump)

       -ppc count
	      specify the number of pixels per character (default 8.0) Larger values will make tables narrower.

       -dump_source
	      dump page source into stdout

       -dump_head
	      dump response of HEAD request into stdout

       -dump_both
	      dump HEAD and source into stdout

       -dump_extra
	      dump HEAD, source, and extra information into stdout

       -post file
	      use POST method with file content

       -header string
	      insert string as a header

       +<num> goto <num> line

       -num   show line number

       -no-proxy
	      don't use proxy

       -no-mouse
	      don't use mouse

       -cookie
	      use cookie.

       -no-cookie
	      don't use cookie

       -pauth user:pass
	      proxy authentication

       -s     squeeze multiple blank lines

       -W     toggle wrap search mode

       -X     don't use termcap init/deinit

       -title [=TERM]
	      set buffer name to terminal title string.  If TERM is specified, use the TERM style title configuration.

       -o opt=value
	      assign value to config option

       -show-option
	      show all available config option

       -config file
	      specify config file

       -help  show usage

       -version
	      show w3m version

       -debug DO NOT USE

EXAMPLES
       To use w3m as a pager:
	      $ ls | w3m

       To use w3m to translate HTML files:
	      $ cat foo.html | w3m -T text/html

       or
	      $ cat foo.html | w3m -dump -T text/html >foo.txt

NOTES
       This is the w3m 0.2.1 Release.

       Additional information about w3m may be found on its Japanese language Web site located at:
	 http://w3m.sourceforge.net/index.ja.html
       or on its English version of the site at:
	 http://w3m.sourceforge.net/index.en.html

ACKNOWLEDGMENTS
       w3m  has  incorporated  code  from  several  sources.   Hans J. Boehm, Alan J. Demers, Xerox Corp. and Silicon Graphics have the copyright of the GC
       library comes with w3m package.	Users have contributed patches and suggestions over time.

AUTHOR
       Akinori ITO <aito@fw.ipsj.or.jp>



4th Berkeley Distribution						   Local								     W3M(1)
