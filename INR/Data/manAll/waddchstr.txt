addchstr(3NCURSES)															 addchstr(3NCURSES)



NAME
       addchstr, addchnstr, waddchstr, waddchnstr, mvaddchstr, mvaddchnstr, mvwaddchstr, mvwaddchnstr - add a string of characters (and attributes) to a
       curses window

SYNOPSIS
       #include <curses.h>

       int addchstr(const chtype *chstr);
       int addchnstr(const chtype *chstr, int n);
       int waddchstr(WINDOW *win, const chtype *chstr);
       int waddchnstr(WINDOW *win, const chtype *chstr, int n);
       int mvaddchstr(int y, int x, const chtype *chstr);
       int mvaddchnstr(int y, int x, const chtype *chstr, int n);
       int mvwaddchstr(WINDOW *win, int y, int x, const chtype *chstr);
       int mvwaddchnstr(WINDOW *win, int y, int x, const chtype *chstr, int n);

DESCRIPTION
       These routines copy chstr into the window image structure at and after the current cursor position.  The four routines with n as the  last  argument
       copy  at  most n elements, but no more than will fit on the line.  If n=-1 then the whole string is copied, to the maximum number of characters that
       will fit on the line.

       The window cursor is not advanced, and these routines work faster than waddnstr.  On the other hand, they do not perform any kind of checking  (such
       as  for	the  newline,  backspace, or carriage return characters), they do not advance the current cursor position, they do not expand other control
       characters to ^-escapes, and they truncate the string if it crosses the right margin, rather than wrapping it around to the new line.

RETURN VALUES
       All routines return the integer ERR upon failure and OK on success (the SVr4 manuals specify only "an integer value other than ERR") upon successful
       completion, unless otherwise noted in the preceding routine descriptions.

       X/Open does not define any error conditions.  This implementation returns an error if the window pointer is null.

NOTES
       Note that all routines except waddchnstr may be macros.

PORTABILITY
       These entry points are described in the XSI Curses standard, Issue 4.

SEE ALSO
       ncurses(3NCURSES).

       Comparable functions in the wide-character (ncursesw) library are described in add_wchstr(3NCURSES).



																	 addchstr(3NCURSES)
