bkgd(3NCURSES)																     bkgd(3NCURSES)



NAME
       bkgdset, wbkgdset, bkgd, wbkgd, getbkgd - curses window background manipulation routines

SYNOPSIS
       #include <curses.h>

       void bkgdset(chtype ch);
       void wbkgdset(WINDOW *win, chtype ch);
       int bkgd(chtype ch);
       int wbkgd(WINDOW *win, chtype ch);
       chtype getbkgd(WINDOW *win);

DESCRIPTION
       The bkgdset and wbkgdset routines manipulate the background of the named window.  The window background is a chtype consisting of any combination of
       attributes (i.e., rendition) and a character.  The attribute part of the background is combined (OR'ed) with all non-blank characters that are writ‐
       ten  into  the window with waddch.  Both the character and attribute parts of the background are combined with the blank characters.  The background
       becomes a property of the character and moves with the character through any scrolling and insert/delete line/character operations.

       To the extent possible on a particular terminal, the attribute part of the background is displayed as the graphic rendition of the character put  on
       the screen.

       The  bkgd and wbkgd functions set the background property of the current or specified window and then apply this setting to every character position
       in that window:

	      The rendition of every character on the screen is changed to the new background rendition.

	      Wherever the former background character appears, it is changed to the new background character.

       The getbkgd function returns the given window's current background character/attribute pair.

RETURN VALUE
       The routines bkgd and wbkgd return the integer OK.  The SVr4.0 manual says "or a non-negative integer if immedok is set", but this appears to be  an
       error.

NOTES
       Note that bkgdset and bkgd may be macros.

PORTABILITY
       These  functions  are  described in the XSI Curses standard, Issue 4.  It specifies that bkgd and wbkgd return ERR on failure.  but gives no failure
       conditions.

SEE ALSO
       ncurses(3NCURSES), addch(3NCURSES), attr(3NCURSES), outopts(3NCURSES)



																	     bkgd(3NCURSES)
