<wchar.h>(P)							 POSIX Programmer's Manual						       <wchar.h>(P)



NAME
       wchar.h - wide-character handling

SYNOPSIS
       #include <wchar.h>

DESCRIPTION
       Some  of the functionality described on this reference page extends the ISO C standard. Applications shall define the appropriate feature test macro
       (see the System Interfaces volume of IEEE Std 1003.1-2001, Section 2.2, The Compilation Environment) to enable the visibility of  these	symbols  in
       this header.

       The <wchar.h> header shall define the following types:

       wchar_t
	      As described in <stddef.h> .

       wint_t An integer type capable of storing any valid value of wchar_t or WEOF.

       wctype_t
	      A scalar type of a data object that can hold values which represent locale-specific character classification.

       mbstate_t
	      An  object  type	other than an array type that can hold the conversion state information necessary to convert between sequences of (possibly
	      multi-byte) characters and wide characters.    If a codeset is being used such that an mbstate_t needs to preserve  more	than  2  levels  of
	      reserved state, the results are unspecified.

       FILE   As described in <stdio.h> .

       size_t As described in <stddef.h> .

       va_list
	      As described in <stdarg.h> .


       The  implementation  shall  support one or more programming environments in which the width of wint_t is no greater than the width of type long. The
       names of these programming environments can be obtained using the confstr() function or the getconf utility.

       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      wint_t	    btowc(int);
	      wint_t	    fgetwc(FILE *);
	      wchar_t	   *fgetws(wchar_t *restrict, int, FILE *restrict);
	      wint_t	    fputwc(wchar_t, FILE *);
	      int	    fputws(const wchar_t *restrict, FILE *restrict);
	      int	    fwide(FILE *, int);
	      int	    fwprintf(FILE *restrict, const wchar_t *restrict, ...);
	      int	    fwscanf(FILE *restrict, const wchar_t *restrict, ...);
	      wint_t	    getwc(FILE *);
	      wint_t	    getwchar(void);

	      int	    iswalnum(wint_t);
	      int	    iswalpha(wint_t);
	      int	    iswcntrl(wint_t);
	      int	    iswctype(wint_t, wctype_t);
	      int	    iswdigit(wint_t);
	      int	    iswgraph(wint_t);
	      int	    iswlower(wint_t);
	      int	    iswprint(wint_t);
	      int	    iswpunct(wint_t);
	      int	    iswspace(wint_t);
	      int	    iswupper(wint_t);
	      int	    iswxdigit(wint_t);

	      size_t	    mbrlen(const char *restrict, size_t, mbstate_t *restrict);
	      size_t	    mbrtowc(wchar_t *restrict, const char *restrict, size_t,
				mbstate_t *restrict);
	      int	    mbsinit(const mbstate_t *);
	      size_t	    mbsrtowcs(wchar_t *restrict, const char **restrict, size_t,
				mbstate_t *restrict);
	      wint_t	    putwc(wchar_t, FILE *);
	      wint_t	    putwchar(wchar_t);
	      int	    swprintf(wchar_t *restrict, size_t,
				const wchar_t *restrict, ...);
	      int	    swscanf(const wchar_t *restrict,
				const wchar_t *restrict, ...);

	      wint_t	    towlower(wint_t);
	      wint_t	    towupper(wint_t);

	      wint_t	    ungetwc(wint_t, FILE *);
	      int	    vfwprintf(FILE *restrict, const wchar_t *restrict, va_list);
	      int	    vfwscanf(FILE *restrict, const wchar_t *restrict, va_list);
	      int	    vwprintf(const wchar_t *restrict, va_list);
	      int	    vswprintf(wchar_t *restrict, size_t,
				const wchar_t *restrict, va_list);
	      int	    vswscanf(const wchar_t *restrict, const wchar_t *restrict,
				va_list);
	      int	    vwscanf(const wchar_t *restrict, va_list);
	      size_t	    wcrtomb(char *restrict, wchar_t, mbstate_t *restrict);
	      wchar_t	   *wcscat(wchar_t *restrict, const wchar_t *restrict);
	      wchar_t	   *wcschr(const wchar_t *, wchar_t);
	      int	    wcscmp(const wchar_t *, const wchar_t *);
	      int	    wcscoll(const wchar_t *, const wchar_t *);
	      wchar_t	   *wcscpy(wchar_t *restrict, const wchar_t *restrict);
	      size_t	    wcscspn(const wchar_t *, const wchar_t *);
	      size_t	    wcsftime(wchar_t *restrict, size_t,
				const wchar_t *restrict, const struct tm *restrict);
	      size_t	    wcslen(const wchar_t *);
	      wchar_t	   *wcsncat(wchar_t *restrict, const wchar_t *restrict, size_t);
	      int	    wcsncmp(const wchar_t *, const wchar_t *, size_t);
	      wchar_t	   *wcsncpy(wchar_t *restrict, const wchar_t *restrict, size_t);
	      wchar_t	   *wcspbrk(const wchar_t *, const wchar_t *);
	      wchar_t	   *wcsrchr(const wchar_t *, wchar_t);
	      size_t	    wcsrtombs(char *restrict, const wchar_t **restrict,
				size_t, mbstate_t *restrict);
	      size_t	    wcsspn(const wchar_t *, const wchar_t *);
	      wchar_t	   *wcsstr(const wchar_t *restrict, const wchar_t *restrict);
	      double	    wcstod(const wchar_t *restrict, wchar_t **restrict);
	      float	    wcstof(const wchar_t *restrict, wchar_t **restrict);
	      wchar_t	   *wcstok(wchar_t *restrict, const wchar_t *restrict,
				wchar_t **restrict);
	      long	    wcstol(const wchar_t *restrict, wchar_t **restrict, int);
	      long double   wcstold(const wchar_t *restrict, wchar_t **restrict);
	      long long     wcstoll(const wchar_t *restrict, wchar_t **restrict, int);
	      unsigned long wcstoul(const wchar_t *restrict, wchar_t **restrict, int);
	      unsigned long long
			    wcstoull(const wchar_t *restrict, wchar_t **restrict, int);

	      wchar_t	   *wcswcs(const wchar_t *, const wchar_t *);
	      int	    wcswidth(const wchar_t *, size_t);

	      size_t	    wcsxfrm(wchar_t *restrict, const wchar_t *restrict, size_t);
	      int	    wctob(wint_t);

	      wctype_t	    wctype(const char *);
	      int	    wcwidth(wchar_t);

	      wchar_t	   *wmemchr(const wchar_t *, wchar_t, size_t);
	      int	    wmemcmp(const wchar_t *, const wchar_t *, size_t);
	      wchar_t	   *wmemcpy(wchar_t *restrict, const wchar_t *restrict, size_t);
	      wchar_t	   *wmemmove(wchar_t *, const wchar_t *, size_t);
	      wchar_t	   *wmemset(wchar_t *, wchar_t, size_t);
	      int	    wprintf(const wchar_t *restrict, ...);
	      int	    wscanf(const wchar_t *restrict, ...);

       The <wchar.h> header shall define the following macros:

       WCHAR_MAX
	      The maximum value representable by an object of type wchar_t.

       WCHAR_MIN
	      The minimum value representable by an object of type wchar_t.

       WEOF   Constant expression of type wint_t that is returned by several WP functions to indicate end-of-file.

       NULL   As described in <stddef.h> .


       The tag tm shall be declared as naming an incomplete structure type, the contents of which are described in the header <time.h> .

       Inclusion of the <wchar.h> header may make  visible  all  symbols  from	the  headers  <ctype.h>,  <string.h>,  <stdarg.h>,  <stddef.h>,  <stdio.h>,
       <stdlib.h>, and <time.h>.

       The following sections are informative.

APPLICATION USAGE
       The  iswblank()	function was a late addition to the ISO C standard and was introduced at the same time as the ISO C standard introduced <wctype.h>,
       which contains all of the isw*() functions. The Open Group Base Specifications had previously aligned with the MSE working draft and had  introduced
       the  rest of the isw*() functions into <wchar.h>. For backwards-compatibility, the original set of isw*() functions, without iswblank(), are permit‐
       ted (as an XSI extension) in <wchar.h>. For maximum portability, applications should include <wctype.h> in order  to  obtain  declarations  for	the
       isw*() functions.

RATIONALE
       In the ISO C standard, the symbols referenced as XSI extensions are in <wctype.h>. Their presence here is thus an extension.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <ctype.h>  ,  <stdarg.h>  ,  <stddef.h>	,  <stdio.h>  ,  <stdlib.h>  ,	<string.h>  ,  <time.h>  ,  <wctype.h>	,  the	System Interfaces volume of
       IEEE Std 1003.1-2001, btowc(), confstr(), fgetwc(), fgetws(), fputwc(), fputws(), fwide(), fwprintf(), fwscanf(), getwc(),  getwchar(),	iswalnum(),
       iswalpha(),  iswcntrl(),  iswctype(),  iswdigit(),  iswgraph(), iswlower(), iswprint(), iswpunct(), iswspace(), iswupper(), iswxdigit(), iswctype(),
       mbsinit(), mbrlen(), mbrtowc(), mbsrtowcs(), putwc(), putwchar(), swprintf(), swscanf(), towlower(), towupper(), ungetwc(), vfwprintf(), vfwscanf(),
       vswprintf(),  vswscanf(),  vwscanf(),  wcrtomb(),  wcsrtombs(),	wcscat(), wcschr(), wcscmp(), wcscoll(), wcscpy(), wcscspn(), wcsftime(), wcslen(),
       wcsncat(), wcsncmp(), wcsncpy(), wcspbrk(), wcsrchr(), wcsspn(), wcsstr(), wcstod(), wcstof(), wcstok(), wcstol(), wcstold(), wcstoll(),  wcstoul(),
       wcstoull(),  wcswcs(),  wcswidth(),  wcsxfrm(),	wctob(),  wctype(),  wcwidth(),  wmemchr(), wmemcmp(), wmemcpy(), wmemmove(), wmemset(), wprintf(),
       wscanf(), the Shell and Utilities volume of IEEE Std 1003.1-2001, getconf

COPYRIGHT
       Portions of this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard  for	Information  Technology  --
       Portable  Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical and
       Electronics Engineers, Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open  Group  Stan‐
       dard,  the  original  IEEE  and	The  Open  Group Standard is the referee document. The original Standard can be obtained online at http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							       <wchar.h>(P)
