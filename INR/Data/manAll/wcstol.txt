WCSTOL(P)							 POSIX Programmer's Manual							  WCSTOL(P)



NAME
       wcstol, wcstoll - convert a wide-character string to a long integer

SYNOPSIS
       #include <wchar.h>

       long wcstol(const wchar_t *restrict nptr, wchar_t **restrict endptr,
	      int base);
       long long wcstoll(const wchar_t *restrict nptr,
	      wchar_t **restrict endptr, int base);


DESCRIPTION
       These  functions  shall	convert the initial portion of the wide-character string pointed to by nptr to long, long long, unsigned long, and unsigned
       long long representation, respectively. First, they shall decompose the input string into three parts:

	1. An initial, possibly empty, sequence of white-space wide-character codes (as specified by iswspace())

	2. A subject sequence interpreted as an integer represented in some radix determined by the value of base

	3. A final wide-character string of one or more unrecognized wide-character codes, including the terminating null wide-character code of the  input
	   wide-character string

       Then they shall attempt to convert the subject sequence to an integer, and return the result.

       If  base is 0, the expected form of the subject sequence is that of a decimal constant, octal constant, or hexadecimal constant, any of which may be
       preceded by a '+' or '-' sign. A decimal constant begins with a non-zero digit, and consists of a sequence of decimal digits. An octal constant con‐
       sists of the prefix '0' optionally followed by a sequence of the digits '0' to '7' only. A hexadecimal constant consists of the prefix 0x or 0X fol‐
       lowed by a sequence of the decimal digits and letters 'a' (or 'A' ) to 'f' (or 'F' ) with values 10 to 15 respectively.

       If the value of base is between 2 and 36, the expected form of the subject sequence is a sequence of letters and digits representing an integer with
       the  radix  specified  by base, optionally preceded by a '+' or '-' sign, but not including an integer suffix. The letters from 'a' (or 'A' ) to 'z'
       (or 'Z' ) inclusive are ascribed the values 10 to 35; only letters whose ascribed values are less than that of base shall be permitted. If the value
       of  base is 16, the wide-character code representations of 0x or 0X may optionally precede the sequence of letters and digits, following the sign if
       present.

       The subject sequence is defined as the longest initial subsequence of the input wide-character string, starting with the first non-white-space wide-
       character  code	that is of the expected form.  The subject sequence contains no wide-character codes if the input wide-character string is empty or
       consists entirely of white-space wide-character code, or if the first non-white-space wide-character code is other than a sign or a permissible let‐
       ter or digit.

       If the subject sequence has the expected form and base is 0, the sequence of wide-character codes starting with the first digit shall be interpreted
       as an integer constant. If the subject sequence has the expected form and the value of base is between 2 and 36, it shall be used as  the  base	for
       conversion,  ascribing  to each letter its value as given above. If the subject sequence begins with a minus sign, the value resulting from the con‐
       version shall be negated. A pointer to the final wide-character string shall be stored in the object pointed to by endptr, provided that  endptr  is
       not a null pointer.

       In other than the C    or POSIX	locales, other implementation-defined subject sequences may be accepted.

       If  the	subject  sequence  is  empty or does not have the expected form, no conversion shall be performed; the value of nptr shall be stored in the
       object pointed to by endptr, provided that endptr is not a null pointer.

       These functions shall not change the setting of errno if successful.

       Since 0, {LONG_MIN} or {LLONG_MIN} and {LONG_MAX} or {LLONG_MAX} are returned on error and are also valid returns on success, an application wishing
       to check for error situations should set errno to 0, then call wcstol() or wcstoll(), then check errno.

RETURN VALUE
       Upon  successful  completion,  these  functions	shall  return the converted value, if any. If no conversion could be performed, 0 shall be returned
	and errno may be set to indicate the error.  If  the  correct  value  is  outside  the	range  of  representable  values,  {LONG_MIN},	{LONG_MAX},
       {LLONG_MIN}, or {LLONG_MAX} shall be returned (according to the sign of the value), and errno set to [ERANGE].

ERRORS
       These functions shall fail if:

       EINVAL The value of base is not supported.

       ERANGE The value to be returned is not representable.


       These functions may fail if:

       EINVAL No conversion could be performed.


       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       iswalpha() , scanf() , wcstod() , the Base Definitions volume of IEEE Std 1003.1-2001, <wchar.h>

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003								  WCSTOL(P)
