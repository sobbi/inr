WIPEFS(8)							    MAINTENANCE COMMANDS							  WIPEFS(8)



NAME
       wipefs - wipe a filesystem signature from a device

SYNOPSIS
       wipefs [-ahnp] [-o offset] device

DESCRIPTION
       wipefs allows to erase filesystem or raid signatures (magic strings) from the device to make the filesystem invisible for libblkid.  wipefs does not
       erase the whole filesystem or any other data from the device.  When used without options -a or -o, it lists all visible filesystems and	offsets  of
       their signatures.

OPTIONS
       -a, --all
	      Erase all available signatures.

       -h, --help
	      Print help and exit.

       -n, --no-act
	      Causes everything to be done except for the write() call.

       -o, --offset offset
	      Specifies  location (in bytes) of the signature which should be erased from the device. The offset number may include a "0x" prefix, and then
	      the number will be read as a hex value. It is possible to specify multiple -o options.

       -p, --parsable
	      Print out in parsable instead of printable format. Encode all potentially unsafe characters of a string to the corresponding hex	value  pre‐
	      fixed by '\x'.

AUTHOR
       Karel Zak <kzak@redhat.com>.

AVAILABILITY
       The wipefs command is part of the util-linux-ng package and is available from ftp://ftp.kernel.org/pub/linux/utils/util-linux-ng/.

SEE ALSO
       blkid(8) findfs(8)




Linux									October 2009								  WIPEFS(8)
