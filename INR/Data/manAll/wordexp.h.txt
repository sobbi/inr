<wordexp.h>(P)							 POSIX Programmer's Manual						     <wordexp.h>(P)



NAME
       wordexp.h - word-expansion types

SYNOPSIS
       #include <wordexp.h>

DESCRIPTION
       The <wordexp.h> header shall define the structures and symbolic constants used by the wordexp() and wordfree() functions.

       The structure type wordexp_t shall contain at least the following members:


	      size_t   we_wordc  Count of words matched by words.
	      char   **we_wordv  Pointer to list of expanded words.
	      size_t   we_offs	 Slots to reserve at the beginning of we_wordv.

       The flags argument to the wordexp() function shall be the bitwise-inclusive OR of the following flags:

       WRDE_APPEND
	      Append words to those previously generated.

       WRDE_DOOFFS
	      Number of null pointers to prepend to we_wordv.

       WRDE_NOCMD
	      Fail if command substitution is requested.

       WRDE_REUSE
	      The pwordexp argument was passed to a previous successful call to wordexp(), and has not been passed to wordfree(). The result is the same as
	      if the application had called wordfree() and then called wordexp() without WRDE_REUSE.

       WRDE_SHOWERR
	      Do not redirect stderr to /dev/null.

       WRDE_UNDEF
	      Report error on an attempt to expand an undefined shell variable.


       The following constants shall be defined as error return values:

       WRDE_BADCHAR
	      One of the unquoted characters- <newline>, '|' , '&' , ';' , '<' , '>' , '(' , ')' , '{' , '}' - appears in words in  an	inappropriate  con‐
	      text.

       WRDE_BADVAL
	      Reference to undefined shell variable when WRDE_UNDEF is set in flags.

       WRDE_CMDSUB
	      Command substitution requested when WRDE_NOCMD was set in flags.

       WRDE_NOSPACE
	      Attempt to allocate memory failed.

       WRDE_NOSYS
	      Reserved.

       WRDE_SYNTAX
	      Shell syntax error, such as unbalanced parentheses or unterminated string.


       The <wordexp.h> header shall define the following type:

       size_t As described in <stddef.h> .


       The following shall be declared as functions and may also be defined as macros. Function prototypes shall be provided.


	      int  wordexp(const char *restrict, wordexp_t *restrict, int);
	      void wordfree(wordexp_t *);

       The implementation may define additional macros or constants using names beginning with WRDE_.

       The following sections are informative.

APPLICATION USAGE
       None.

RATIONALE
       None.

FUTURE DIRECTIONS
       None.

SEE ALSO
       <stddef.h> , the System Interfaces volume of IEEE Std 1003.1-2001, wordexp(), the Shell and Utilities volume of IEEE Std 1003.1-2001

COPYRIGHT
       Portions  of  this  text  are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2003 Edition, Standard for Information Technology --
       Portable Operating System Interface (POSIX), The Open Group Base Specifications Issue 6, Copyright (C) 2001-2003 by the Institute of Electrical	and
       Electronics  Engineers,	Inc and The Open Group. In the event of any discrepancy between this version and the original IEEE and The Open Group Stan‐
       dard, the original IEEE and The Open Group Standard is the referee document. The original  Standard  can  be  obtained  online  at  http://www.open‐
       group.org/unix/online.html .



IEEE/The Open Group							    2003							     <wordexp.h>(P)
