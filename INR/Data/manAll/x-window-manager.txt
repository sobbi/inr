METACITY(1)							   METACITY(1)



NAME
       METACITY - minimal GTK2 Window Manager

SYNOPSIS
       metacity [--display=DISPLAY] [--replace] [--sm-client-id=ID] [--sm-dis‐
       able] [--sm-save-file=FILENAME] [--version] [--help]

DESCRIPTION
       This manual page documents briefly metacity.

       metacity is a minimal X window manager aimed at nontechnical users  and
       is  designed  to integrate well with the GNOME desktop.	metacity lacks
       some features that may be expected by traditional UNIX or other techni‐
       cal  users;  these users may want to investigate other available window
       managers for use with GNOME or standalone.

OPTIONS
       --display=DISPLAY
	      Connect to X display DISPLAY.

       --replace
	      a window manager which  is  running  is  replaced  by  metacity.
	      Users  are encouraged to change the GNOME window manager by run‐
	      ning the new WM with the --replace or -replace option, and  sub‐
	      sequently saving the session.

       --sm-client-id=ID
	      Specify a session management ID.

       --sm-disable
	      Disable the session management.

       --sm-save-file=FILENAME
	      Load a session from FILENAME.

       --version
	      Print the version number.

       -?, --help
	      Show summary of options.

CONFIGURATION
       metacity  configuration	can  be  found	under Preferences->Windows and
       Preferences->Keyboard Shortcuts on the menu-panel. Advanced  configura‐
       tion  can  be  achieved directly through gconf editing (gconf-editor or
       gconftool-2).

SEE ALSO
       metacity-message(1)

AUTHOR
       The original manual page was written by Thom May <thom@debian.org>.  It
       was  updated by Akira TAGOH <tagoh@debian.org> for the Debian GNU/Linux
       system (with permission to use by others), and  then  updated  by  Luke
       Morton and Philip O'Brien for inclusion in metacity.



			       11 February 2006 		   METACITY(1)
