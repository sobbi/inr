xjc(1)																		     xjc(1)



									   NAME
							   xjc - Java(TM) Architecture for XML Binding
							   Binding Compiler Specification Version: 2.0
							  Reference Implementation (RI) Version: 2.0 ea3


Launching xjc
       The binding compiler can be launched using the appropriate xjc shell script in the bin directory for your platform. We also provide an Ant task to
       run the binding complier - see the instructions for using xjc with Ant.

	  For Solaris/Linux

	     % /path/to/jaxb/bin/xjc.sh -help

	  For WindowsNT/2000/XP

	     > c:\path\to\jaxb\bin\xjc.bat -help

	  Output

	     Usage: xjc [-options ...] <schema_file/URL/dir> ... [-b <bindinfo>] ...
	     Options:
	      -nv		   : do not perform strict validation of the input schema(s)
	      -extension	   : allow vendor extensions - do not strictly follow the
				     Compatibility Rules and App E.2 from the JAXB Spec
	      -b <file> 	   : specify external bindings files (each <file> must have its own -b)
	      -d <dir>		   : generated files will go into this directory
	      -p <pkg>		   : specifies the target package
	      -httpproxy <proxy>   : set HTTP/HTTPS proxy. Format is [user[:password]@]proxyHost:proxyPort
	      -httpproxyfile <file>: set the proxy string (same format as above).
	      -classpath <arg>	   : specify where to find user class files
	      -catalog <file>	   : specify catalog files to resolve external entity references
				     support TR9401, XCatalog, and OASIS XML Catalog format.
	      -readOnly 	   : generated files will be in read-only mode
	      -npa		   : suppress generation of package level annotations (**/package-info.java)
	      -xmlschema	   : treat input as W3C XML Schema (default)
	      -relaxng		   : treat input as RELAX NG (experimental,unsupported)
	      -relaxng-compact	   : treat input as RELAX NG compact syntax (experimental,unsupported)
	      -dtd		   : treat input as XML DTD (experimental,unsupported)
	      -wsdl		   : treat input as WSDL and compile schemas inside it (experimental,unsupported)
	      -verbose		   : be extra verbose
	      -quiet		   : suppress compiler output
	      -help		   : display this help message
	      -version		   : display version information


	  Execute the jaxb-xjc.jar JAR File

	     If all else fails, you should be able to execute the jaxb-xjc.jar file:

		   For Solaris/Linux:
		      % java -jar $JAXB_HOME/lib/jaxb-xjc.jar -help

		   For Windows:
		      > java -jar %JAXB_HOME%\lib\jaxb-xjc.jar -help


	     This is equivalent of running "xjc.sh" or "xjc.bat", and it allows you to set the JVM parameters.

OPTIONS
	  -nv
	     By default, the XJC binding compiler performs strict validation of the source schema before processing it. Use this option to disable strict
	     schema validation. This does not mean that the binding compiler will not perform any validation, it simply means that it will perform
	     less-strict validation.

	  -extension
	     By default, the XJC binding compiler strictly enforces the rules outlined in the Compatibility chapter of the JAXB Specification. Appendix E.2
	     defines a set of W3C XML Schema features that are not completely supported by JAXB v1.0. In some cases, you may be allowed to use them in the
	     "-extension" mode enabled by this switch. In the default (strict) mode, you are also limited to using only the binding customizations defined
	     in the specification. By using the "-extension" switch, you will be allowed to use the JAXB Vendor Extensions.

	  -b <file>
	     Specify one or more external binding files to process. (Each binding file must have it's own "-b" switch.) The syntax of the external binding
	     files is extremely flexible. You may have a single binding file that contains customizations for multiple schemas or you can break the cus‐
	     tomizations into multiple bindings files:


	     xjc schema1.xsd schema2.xsd schema3.xsd -b bindings123.xjb
	     xjc schema1.xsd schema2.xsd schema3.xsd -b bindings1.xjb -b bindings2.xjb -b bindings3.xjb
	  In addition, the ordering of the schema files and binding files on the command line does not matter.

	  -d <dir>
	     By default, the XJC binding compiler will generate the Java content classes in the current directory. Use this option to specify an alternate
	     output directory. The output directory must already exist, the XJC binding compiler will not create it for you.

	  -p <pkg>
	     Specifying a target package via this command-line option overrides any binding customization for package name and the default package name
	     algorithm defined in the specification.

	  -httpproxy <proxy>
	     Specify the HTTP/HTTPS proxy. The format is [user[:password]@]proxyHost[:proxyPort]. The old -host and -port are still supported by the RI for
	     backwards compatibility, but they have been deprecated. Note that the password specified with this option is an argument that is visible to
	     other users who use the top command, for example. For greater security, use -httpproxyfile, below.

	  -httpproxyfile <file>
	     Specify the HTTP/HTTPS proxy using a file. Same format as above, but the password specified in the file is not visible to other users.

	  -classpath <arg>
	     Specify where to find client application class files used by the <jxb:javaType> and <xjc:superClass> customizations.

	  -catalog <file>
	     Specify catalog files to resolve external entity references. Supports TR9401, XCatalog, and OASIS XML Catalog format. Please read the XML
	     Entity and URI Resolvers document or the catalog-resolver sample application.

	  -readOnly
	     By default, the XJC binding compiler does not write-protect the Java source files it generates. Use this option to force the XJC binding com‐
	     piler to mark the generated Java sources read-only.

	  -npa
	     Supress the generation of package level annotations into **/package-info.java. Using this switch causes the generated code to internalize
	     those annotations into the other generated classes.

	  -xmlschema
	     treat input schemas as W3C XML Schema (default). If you do not specify this switch, your input schemas will be treated as W3C XML Schema.

	  -relaxng
	     Treat input schemas as RELAX NG (experimental, unsupported). Support for RELAX NG schemas is provided as a JAXB Vendor Extension.

	  -relaxng-compact
	     Treat input schemas as RELAX NG compact syntax(experimental, unsupported). Support for RELAX NG schemas is provided as a JAXB Vendor Exten‐
	     sion.

	  -dtd
	     Treat input schemas as XML DTD (experimental, unsupported). Support for RELAX NG schemas is provided as a JAXB Vendor Extension.

	  -wsdl
	     Treat input as WSDL and compile schemas inside it (experimental,unsupported).

	  -quiet
	     Suppress compiler output, such as progress information and warnings..

	  -verbose
	     Be extra verbose, such as printing informational messages or displaying stack traces upon some errors..

	  -help
	     Display a brief summary of the compiler switches.

	  -version
	     Display the compiler version information.

	  <schema file/URL/dir>
	     Specify one or more schema files to compile. If you specify a directory, then xjc will scan it for all schema files and compile them.

   Summary of Deprecated and Removed Command Line Options
	     -host & -port
		These options have been deprecated and replaced with the -httpproxy option. For backwards compatibility, we will continue to support these
		options, but they will no longer be documented and may be removed from future releases.

	     -use-runtime
		Since the JAXB 2.0 specification has defined a portable runtime, it is no longer necessary for the JAXB RI to generate **/impl/runtime
		packages. Therefore, this switch is obsolete and has been removed.

	     -source
		The -source compatibility switch was introduced in the first JAXB 2.0 Early Access release. We have decided to remove this switch from
		future releases of JAXB 2.0. If you need to generate 1.0.x code, please use an installation of the 1.0.x codebase.

	     -Xlocator & -Xsync-methods
		These switches have been disabled for now. We plan on releasing this functionality as a separate download in the future.


   Compiler Restrictions
       In general, it is safest to compile all related schemas as a single unit with the same binding compiler switches.

       Please keep the following list of restrictions in mind when running xjc. Most of these issues only apply when compiling multiple schemas with multi‐
       ple invocations of xjc.

	  o To compile multiple schemas at the same time, keep the following precedence rules for the target Java package name in mind:

	     1. The "-p" command line option takes the highest precedence.

	     2. <jaxb:package> customization

	     3. If targetNamespace is declared, apply targetNamespace -> Java package name algorithm defined in the specification.

	     4. If no targetNamespace is declared, use a hardcoded package named "generated".

	  o It is not legal to have more than one <jaxb:schemaBindings> per namespace, so it is impossible to have two schemas in the same target namespace
	    compiled into different Java packages.

	  o All schemas being compiled into the same Java package must be submitted to the XJC binding compiler at the same time - they cannot be compiled
	    independently and work as expected.

	  o Element substitution groups spread across multiple schema files must be compiled at the same time.


See Also
	  o JAXB 2.0:

	     * Release Notes @
	       http://java.sun.com/webservices/docs/2.0/jaxb/ReleaseNotes.html

	     * XJC @
	       http://java.sun.com/webservices/docs/2.0/jaxb/xjc.html

	     * XJCTask @
	       http://java.sun.com/webservices/docs/2.0/jaxb/xjcTask.html

	     * SchemaGen @
	       http://java.sun.com/webservices/docs/2.0/jaxb/schemagen.html

	     * schemagenTask @
	       http://java.sun.com/webservices/docs/2.0/jaxb/schemagenTask.html

	     * Sample Apps @
	       http://java.sun.com/webservices/docs/2.0/jaxb/samples.html

	     * Changelog @
	       http://java.sun.com/webservices/docs/2.0/jaxb/changelog2.html

	  o JAXB 1.0.x:

	     * Release Notes @
	       http://java.sun.com/webservices/docs/2.0/jaxb/jaxb-1_0.html

	     * Changelog @
	       http://java.sun.com/webservices/docs/2.0/jaxb/changelog.html

	  o JAXB RI Extensions:

	     * Runtime Properties @
	       http://java.sun.com/webservices/docs/2.0/jaxb/vendorProperties.html

	     * XJC Customizations @
	       http://java.sun.com/webservices/docs/2.0/jaxb/vendorCustomizations.html

	     * Develop Plugins @
	       http://java.sun.com/webservices/docs/2.0/jaxb/developPlugins.html

	  o JAXB RI Schema Languages:

	     * W3C XML Schema @
	       http://java.sun.com/webservices/docs/2.0/jaxb/vendorSchemaLangs.html#xschema

	     * RELAX NG @
	       http://java.sun.com/webservices/docs/2.0/jaxb/vendorSchemaLangs.html#relaxng

	     * DTD @
	       http://java.sun.com/webservices/docs/2.0/jaxb/vendorSchemaLangs.html#dtd

	  o JAXB Community:

	     * Java.net Homepage @
	       http://jaxb.dev.java.net

	     * Developer interest list @
	       https://jaxb.dev.java.net/servlets/ProjectMailingListList

	     * FAQ @
	       https://jaxb.dev.java.net/faq/index.html


									07 Aug 2006								     xjc(1)
