wxPython-tools(1)							 wxWidgets							  wxPython-tools(1)



NAME
       img2py - wxPython tools.
       img2png - wxPython tools.
       img2xpm - wxPython tools.
       pycrust - wxPython tools.
       pyshell - wxPython tools.
       xrced - wxPython tools.
       helpviewer - wxPython tools.
       pyalacarte - wxPython tools.
       pyalamode - wxPython tools.
       pywrap - wxPython tools.
       pywxrc - wxPython tools.


DESCRIPTION
       The real documentation for these tools is available in pydoc format.


SEE ALSO
       pydoc(1)


COPYRIGHT
       This  manpage  was  written  by Ron Lee <ron@debian.org> for the Debian GNU/Linux distribution of wxWidgets.  It may be freely distributed by anyone
       insane enough to find it useful.




Debian GNU/Linux							 3 Jan 2003							  wxPython-tools(1)
