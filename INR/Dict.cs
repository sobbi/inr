﻿using System;
using System.Collections.Generic;

namespace INR
{
    //Dict klasse speichert eine liste mit einem element pro docid/term mit der information der häufigkeit des terms.

    public class Dict
    {
        public static List<DictEntry> dict = new List<DictEntry>();

        public static void addEntry(DictEntry entry)
        {
            dict.Add(entry);
            PostingFile.addDocIdToTerm(entry.term, entry.docId, entry.positions);
        }

        public static void addEntry(string term, int docId, List<int> positions)
        {
            addEntry(new DictEntry(term, docId, positions));
        }

        public static void sortDict()
        {
            dict.Sort();
        }
    }

    public class DictEntry : IComparable {

        public string term;
        public int docId;
        public List<int> positions;

        public DictEntry(string term, int docId, List<int> positions)
        {
            this.term = term;
            this.docId = docId;
            this.positions = positions;
        }
     
        public int CompareTo(object obj)
        {
            DictEntry cur = (DictEntry)obj;
            int compVal = this.term.CompareTo(cur.term);
            if(compVal == 0)
            {
                return this.docId - cur.docId;
            }
            else
            {
                return compVal;
            }
        }
    }
}
