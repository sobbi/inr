﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace INR
{
    class Docs
    {
        public List<Doc> docs = new List<Doc>();
        private int nextDocId = 0;
        public int count = 0;

        private Thread importThread;
        private Queue<string> processPaths = new Queue<string>();
        //private bool run = true;

        public Docs()
        {
            //importThread.Start();
        }

        public void addPathToDocs(string path)
        {
            Form1.form.setGroupVisable(Form1.form.groupBox4, false);
            if (Directory.Exists(path) && !File.Exists(path))
            {
                string[] filePaths = Directory.GetFiles(path);
                foreach (string filePath in filePaths)
                {
                    processPaths.Enqueue((string)filePath.Clone());
                }
            }
            else
            {
                processPaths.Enqueue(path);
            }
            importThread = new Thread(threadFunction);
            importThread.Start();
        }

        private void threadFunction()
        {
            DateTime start = DateTime.Now;
            while (processPaths.Count > 0)
            {
                string debugOut = "Pre-Count: " + processPaths.Count + " ";
                string val = processPaths.Dequeue();
                debugOut += "Val: " + val + " Post-Count: " + processPaths.Count;
                addDoc(val);
            }
            Form1.form.updateAddArea(true);
            Form1.form.setGroupVisable(Form1.form.groupBox2, true);
            Form1.form.setGroupVisable(Form1.form.groupBox3, true);
            Dict.sortDict();
            Tokenizer.UpdateIndices();
            Cosine.calcWtds();
            PostingFile.initDocHashSet();
            Form1.form.setGroupVisable(Form1.form.groupBox5, true);
            if (Form1.form.runOgawa)
            {
                Ogawa.CalculateJaccard();
                Ogawa.CreateFuzzyValues();
                Form1.form.setGroupVisable(Form1.form.groupBox4, true);
            }
            Form1.form.updateAddArea();
            Form1.form.setGroupVisable(Form1.form.groupBox4, true);
            DateTime end = DateTime.Now;
            Console.WriteLine("Started in: " + (end - start));
        }

        public void killThread()
        {
            if (importThread != null && importThread.IsAlive)
                importThread.Abort();
        }

        private void addDoc(string path)
        {
            if (!(path.EndsWith(".txt") || path.EndsWith("pdf")))
            {
                Console.WriteLine("No compatible File");
                return;
            }
            if (docs.Contains(new Doc(0, path)))
            {
                Console.WriteLine("Already exist: " + path);
                return;
            }
            docs.Add(new Doc(nextDocId, path));
            count = docs.Count;
            List<string> tokens = Tokenizer.tokenize(path, nextDocId);
            SortedList<string, List<int>> positions = new SortedList<string, List<int>>();
            for (int i = 0; i < tokens.Count; i++)
            {
                if (!positions.ContainsKey(tokens[i]))
                {
                    List<int> list = new List<int>();
                    positions.Add(tokens[i], list);
                }
                positions[tokens[i]].Add(i);
            }
            foreach (KeyValuePair<string, List<int>> entry in positions)
            {
                Dict.addEntry(entry.Key, nextDocId, entry.Value);
            }
            //Dict.sortDict();
            nextDocId++;
            Form1.form.updateAddArea();
        }

        public void removeDoc(int docId)
        {
            foreach (Doc doc in docs)
            {
                if (doc.id == docId)
                {
                    docs.Remove(doc);
                    count = docs.Count;
                    return;
                }
            }
        }

        public int getIdToName(string name)
        {
            foreach (Doc doc in docs)
            {
                if (doc.name == name)
                    return doc.id;
            }
            return -1;
        }

        public string getDocName(int docId)
        {
            foreach (Doc doc in docs)
            {
                if (doc.id == docId)
                {
                    return doc.name;
                }
            }
            return "Not Found";
        }

        public void openDoc(int docId)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                foreach (Doc doc in docs)
                {
                    if (doc.id == docId)
                    {
                        Process.Start("explorer.exe", doc.fullPath);
                    }
                }
            }
            else if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                foreach (Doc doc in docs)
                {
                    if (doc.id == docId)
                    {
                        string tmp = (string)doc.fullPath.Clone();
                        Console.WriteLine(tmp);
                        var process = new Process()
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = "xdg-open",
                                Arguments = $"\"{tmp}\"",
                                RedirectStandardOutput = true,
                                UseShellExecute = false,
                                CreateNoWindow = false,
                            }
                        };
                        process.Start();
                    }
                }
            }
        }

        public void showDoc(int docId)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                foreach (Doc doc in docs)
                {
                    if (doc.id == docId)
                    {
                        Process.Start("explorer.exe", "/select," + doc.fullPath);
                    }
                }
            }
            else if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                foreach (Doc doc in docs)
                {
                    if (doc.id == docId)
                    {
                        string tmp = (string)doc.path.Replace(doc.name, "").Clone();
                        Console.WriteLine(tmp);
                        var process = new Process()
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = "xdg-open",
                                Arguments = $"\"{tmp}\"",
                                RedirectStandardOutput = true,
                                UseShellExecute = false,
                                CreateNoWindow = false,
                            }
                        };
                        process.Start();
                    }
                }
            }
        }

        public LinkedList<int> getDocIdList()
        {
            LinkedList<int> result = new LinkedList<int>();
            foreach (Doc doc in docs)
            {
                result.AddLast(doc.id);
            }
            return result;
        }

    }

    class Doc
    {
        public int id;
        public string name;
        public string path;
        public string fullPath { get { return path + name; } }

        public Doc(int id, string path)
        {
            this.id = id;
            string[] parts = path.Split(new string[] { "/", "\\" }, StringSplitOptions.RemoveEmptyEntries);
            this.name = parts[parts.Length - 1];
            this.path = path.Replace(this.name, "");
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            Doc doc = (Doc)obj;
            return doc.fullPath == this.fullPath;
        }

        public override int GetHashCode()
        {
            return -1757656154 + EqualityComparer<string>.Default.GetHashCode(fullPath);
        }
    }
}
