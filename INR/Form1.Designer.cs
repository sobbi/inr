﻿using System;

namespace INR
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.addDocBtn = new System.Windows.Forms.Button();
            this.docPathField = new System.Windows.Forms.TextBox();
            this.docCountLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownB1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownB2 = new System.Windows.Forms.NumericUpDown();
            this.ClusterBtn = new System.Windows.Forms.Button();
            this.runOgawaBox = new System.Windows.Forms.CheckBox();
            this.showAllBtn = new System.Windows.Forms.Button();
            this.ogawaTresholdtextfield = new System.Windows.Forms.TextBox();
            this.ogawaTresholdLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.p1Example4Btn = new System.Windows.Forms.Button();
            this.p1Example3Btn = new System.Windows.Forms.Button();
            this.p1Example2Btn = new System.Windows.Forms.Button();
            this.p1Example1Btn = new System.Windows.Forms.Button();
            this.boolSearchBtn = new System.Windows.Forms.Button();
            this.boolenRequestField = new System.Windows.Forms.TextBox();
            this.groupBox0 = new System.Windows.Forms.GroupBox();
            this.showExplorerBtn = new System.Windows.Forms.Button();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.resultListBox = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.p2Example4Btn = new System.Windows.Forms.Button();
            this.p2Example3Btn = new System.Windows.Forms.Button();
            this.p2Example2Btn = new System.Windows.Forms.Button();
            this.p2Example1Btn = new System.Windows.Forms.Button();
            this.posSearchBtn = new System.Windows.Forms.Button();
            this.posRequestField = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.histogrammJaccard = new System.Windows.Forms.Button();
            this.histogramFuzzyMembership = new System.Windows.Forms.Button();
            this.p3Example5Btn = new System.Windows.Forms.Button();
            this.p3Example4Btn = new System.Windows.Forms.Button();
            this.p3Example3Btn = new System.Windows.Forms.Button();
            this.p3Example2Btn = new System.Windows.Forms.Button();
            this.p3Example1Btn = new System.Windows.Forms.Button();
            this.fuzzySearchBtn = new System.Windows.Forms.Button();
            this.fuzzyRequestField = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.p4Example6Btn = new System.Windows.Forms.Button();
            this.p4Example5Btn = new System.Windows.Forms.Button();
            this.p4Example4Btn = new System.Windows.Forms.Button();
            this.p4Example3Btn = new System.Windows.Forms.Button();
            this.p4Example2Btn = new System.Windows.Forms.Button();
            this.p4Example1Btn = new System.Windows.Forms.Button();
            this.cosineSearchBtn = new System.Windows.Forms.Button();
            this.cosRequestField = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.clusterSearchBtn = new System.Windows.Forms.Button();
            this.clusterRequestField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB2)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox0.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // addDocBtn
            // 
            this.addDocBtn.Location = new System.Drawing.Point(700, 17);
            this.addDocBtn.Name = "addDocBtn";
            this.addDocBtn.Size = new System.Drawing.Size(106, 62);
            this.addDocBtn.TabIndex = 0;
            this.addDocBtn.Text = "Hinzufügen";
            this.addDocBtn.UseVisualStyleBackColor = true;
            this.addDocBtn.Click += new System.EventHandler(this.addDocBtn_Click);
            // 
            // docPathField
            // 
            this.docPathField.Location = new System.Drawing.Point(12, 25);
            this.docPathField.Name = "docPathField";
            this.docPathField.Size = new System.Drawing.Size(679, 26);
            this.docPathField.TabIndex = 1;
            // 
            // docCountLabel
            // 
            this.docCountLabel.AutoSize = true;
            this.docCountLabel.Location = new System.Drawing.Point(14, 65);
            this.docCountLabel.Name = "docCountLabel";
            this.docCountLabel.Size = new System.Drawing.Size(189, 20);
            this.docCountLabel.TabIndex = 2;
            this.docCountLabel.Text = "Anzahl Dokumente: 0000";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numericUpDownB1);
            this.groupBox1.Controls.Add(this.numericUpDownB2);
            this.groupBox1.Controls.Add(this.ClusterBtn);
            this.groupBox1.Controls.Add(this.runOgawaBox);
            this.groupBox1.Controls.Add(this.showAllBtn);
            this.groupBox1.Controls.Add(this.ogawaTresholdtextfield);
            this.groupBox1.Controls.Add(this.ogawaTresholdLabel);
            this.groupBox1.Controls.Add(this.docPathField);
            this.groupBox1.Controls.Add(this.docCountLabel);
            this.groupBox1.Controls.Add(this.addDocBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(813, 145);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dokumente hinzufügen";
            // 
            // numericUpDownB1
            // 
            this.numericUpDownB1.Location = new System.Drawing.Point(348, 97);
            this.numericUpDownB1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDownB1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownB1.Name = "numericUpDownB1";
            this.numericUpDownB1.Size = new System.Drawing.Size(82, 26);
            this.numericUpDownB1.TabIndex = 15;
            this.numericUpDownB1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownB2
            // 
            this.numericUpDownB2.Location = new System.Drawing.Point(489, 97);
            this.numericUpDownB2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDownB2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownB2.Name = "numericUpDownB2";
            this.numericUpDownB2.Size = new System.Drawing.Size(86, 26);
            this.numericUpDownB2.TabIndex = 14;
            this.numericUpDownB2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ClusterBtn
            // 
            this.ClusterBtn.Location = new System.Drawing.Point(582, 89);
            this.ClusterBtn.Name = "ClusterBtn";
            this.ClusterBtn.Size = new System.Drawing.Size(110, 40);
            this.ClusterBtn.TabIndex = 13;
            this.ClusterBtn.Text = "Clustern";
            this.ClusterBtn.UseVisualStyleBackColor = true;
            this.ClusterBtn.Click += new System.EventHandler(this.ClusterBtn_Click);
            // 
            // runOgawaBox
            // 
            this.runOgawaBox.AutoSize = true;
            this.runOgawaBox.Checked = true;
            this.runOgawaBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.runOgawaBox.Location = new System.Drawing.Point(196, 100);
            this.runOgawaBox.Name = "runOgawaBox";
            this.runOgawaBox.Size = new System.Drawing.Size(22, 21);
            this.runOgawaBox.TabIndex = 12;
            this.runOgawaBox.UseVisualStyleBackColor = true;
            this.runOgawaBox.CheckedChanged += new System.EventHandler(this.runOgawaBox_CheckedChanged);
            // 
            // showAllBtn
            // 
            this.showAllBtn.Location = new System.Drawing.Point(700, 85);
            this.showAllBtn.Name = "showAllBtn";
            this.showAllBtn.Size = new System.Drawing.Size(106, 52);
            this.showAllBtn.TabIndex = 3;
            this.showAllBtn.Text = "Alle anzeigen";
            this.showAllBtn.UseVisualStyleBackColor = true;
            this.showAllBtn.Click += new System.EventHandler(this.showAllBtn_Click);
            // 
            // ogawaTresholdtextfield
            // 
            this.ogawaTresholdtextfield.Location = new System.Drawing.Point(151, 98);
            this.ogawaTresholdtextfield.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ogawaTresholdtextfield.Name = "ogawaTresholdtextfield";
            this.ogawaTresholdtextfield.Size = new System.Drawing.Size(38, 26);
            this.ogawaTresholdtextfield.TabIndex = 4;
            this.ogawaTresholdtextfield.Text = "0.4";
            // 
            // ogawaTresholdLabel
            // 
            this.ogawaTresholdLabel.AutoSize = true;
            this.ogawaTresholdLabel.Location = new System.Drawing.Point(17, 99);
            this.ogawaTresholdLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ogawaTresholdLabel.Name = "ogawaTresholdLabel";
            this.ogawaTresholdLabel.Size = new System.Drawing.Size(126, 20);
            this.ogawaTresholdLabel.TabIndex = 3;
            this.ogawaTresholdLabel.Text = "Ogawa Schwelle";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.p1Example4Btn);
            this.groupBox2.Controls.Add(this.p1Example3Btn);
            this.groupBox2.Controls.Add(this.p1Example2Btn);
            this.groupBox2.Controls.Add(this.p1Example1Btn);
            this.groupBox2.Controls.Add(this.boolSearchBtn);
            this.groupBox2.Controls.Add(this.boolenRequestField);
            this.groupBox2.Location = new System.Drawing.Point(14, 163);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(813, 111);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "P1-Boolean-Anfragen";
            // 
            // p1Example4Btn
            // 
            this.p1Example4Btn.Location = new System.Drawing.Point(508, 58);
            this.p1Example4Btn.Name = "p1Example4Btn";
            this.p1Example4Btn.Size = new System.Drawing.Size(186, 43);
            this.p1Example4Btn.TabIndex = 5;
            this.p1Example4Btn.Text = "or König and not Hexe";
            this.p1Example4Btn.UseVisualStyleBackColor = true;
            this.p1Example4Btn.Click += new System.EventHandler(this.p1Example4Btn_Click);
            // 
            // p1Example3Btn
            // 
            this.p1Example3Btn.Location = new System.Drawing.Point(236, 58);
            this.p1Example3Btn.Name = "p1Example3Btn";
            this.p1Example3Btn.Size = new System.Drawing.Size(268, 43);
            this.p1Example3Btn.TabIndex = 4;
            this.p1Example3Btn.Text = "or Frosch and König and Tellerlein";
            this.p1Example3Btn.UseVisualStyleBackColor = true;
            this.p1Example3Btn.Click += new System.EventHandler(this.p1Example3Btn_Click);
            // 
            // p1Example2Btn
            // 
            this.p1Example2Btn.Location = new System.Drawing.Point(96, 58);
            this.p1Example2Btn.Name = "p1Example2Btn";
            this.p1Example2Btn.Size = new System.Drawing.Size(132, 43);
            this.p1Example2Btn.TabIndex = 3;
            this.p1Example2Btn.Text = "and Prinzessin";
            this.p1Example2Btn.UseVisualStyleBackColor = true;
            this.p1Example2Btn.Click += new System.EventHandler(this.p1Example2Btn_Click);
            // 
            // p1Example1Btn
            // 
            this.p1Example1Btn.Location = new System.Drawing.Point(15, 58);
            this.p1Example1Btn.Name = "p1Example1Btn";
            this.p1Example1Btn.Size = new System.Drawing.Size(75, 43);
            this.p1Example1Btn.TabIndex = 2;
            this.p1Example1Btn.Text = "Hexe";
            this.p1Example1Btn.UseVisualStyleBackColor = true;
            this.p1Example1Btn.Click += new System.EventHandler(this.p1Example1Btn_Click);
            // 
            // boolSearchBtn
            // 
            this.boolSearchBtn.Location = new System.Drawing.Point(700, 25);
            this.boolSearchBtn.Name = "boolSearchBtn";
            this.boolSearchBtn.Size = new System.Drawing.Size(106, 75);
            this.boolSearchBtn.TabIndex = 1;
            this.boolSearchBtn.Text = "Suchen";
            this.boolSearchBtn.UseVisualStyleBackColor = true;
            this.boolSearchBtn.Click += new System.EventHandler(this.boolSearchBtn_Click);
            // 
            // boolenRequestField
            // 
            this.boolenRequestField.Location = new System.Drawing.Point(15, 26);
            this.boolenRequestField.Name = "boolenRequestField";
            this.boolenRequestField.Size = new System.Drawing.Size(680, 26);
            this.boolenRequestField.TabIndex = 0;
            // 
            // groupBox0
            // 
            this.groupBox0.Controls.Add(this.showExplorerBtn);
            this.groupBox0.Controls.Add(this.openFileBtn);
            this.groupBox0.Controls.Add(this.resultListBox);
            this.groupBox0.Location = new System.Drawing.Point(842, 12);
            this.groupBox0.Name = "groupBox0";
            this.groupBox0.Size = new System.Drawing.Size(465, 740);
            this.groupBox0.TabIndex = 7;
            this.groupBox0.TabStop = false;
            this.groupBox0.Text = "Suche-Ergebnis";
            // 
            // showExplorerBtn
            // 
            this.showExplorerBtn.Location = new System.Drawing.Point(240, 29);
            this.showExplorerBtn.Name = "showExplorerBtn";
            this.showExplorerBtn.Size = new System.Drawing.Size(216, 49);
            this.showExplorerBtn.TabIndex = 2;
            this.showExplorerBtn.Text = "Im Explorer anzeigen";
            this.showExplorerBtn.UseVisualStyleBackColor = true;
            this.showExplorerBtn.Click += new System.EventHandler(this.showExplorerBtn_Click);
            // 
            // openFileBtn
            // 
            this.openFileBtn.Location = new System.Drawing.Point(6, 29);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(214, 49);
            this.openFileBtn.TabIndex = 1;
            this.openFileBtn.Text = "Datei öffnen";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.openFileBtn_Click);
            // 
            // resultListBox
            // 
            this.resultListBox.FormattingEnabled = true;
            this.resultListBox.ItemHeight = 20;
            this.resultListBox.Location = new System.Drawing.Point(6, 85);
            this.resultListBox.Name = "resultListBox";
            this.resultListBox.Size = new System.Drawing.Size(450, 424);
            this.resultListBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.p2Example4Btn);
            this.groupBox3.Controls.Add(this.p2Example3Btn);
            this.groupBox3.Controls.Add(this.p2Example2Btn);
            this.groupBox3.Controls.Add(this.p2Example1Btn);
            this.groupBox3.Controls.Add(this.posSearchBtn);
            this.groupBox3.Controls.Add(this.posRequestField);
            this.groupBox3.Location = new System.Drawing.Point(15, 280);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(812, 118);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "P2-Positional-Index-Anfragen";
            // 
            // p2Example4Btn
            // 
            this.p2Example4Btn.Location = new System.Drawing.Point(500, 58);
            this.p2Example4Btn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.p2Example4Btn.Name = "p2Example4Btn";
            this.p2Example4Btn.Size = new System.Drawing.Size(194, 46);
            this.p2Example4Btn.TabIndex = 5;
            this.p2Example4Btn.Text = "und /100 die";
            this.p2Example4Btn.UseVisualStyleBackColor = true;
            this.p2Example4Btn.Click += new System.EventHandler(this.p2Example4Btn_Click);
            // 
            // p2Example3Btn
            // 
            this.p2Example3Btn.Location = new System.Drawing.Point(321, 58);
            this.p2Example3Btn.Name = "p2Example3Btn";
            this.p2Example3Btn.Size = new System.Drawing.Size(170, 46);
            this.p2Example3Btn.TabIndex = 4;
            this.p2Example3Btn.Text = "Kuchen /5 Wein";
            this.p2Example3Btn.UseVisualStyleBackColor = true;
            this.p2Example3Btn.Click += new System.EventHandler(this.p2Example3Btn_Click);
            // 
            // p2Example2Btn
            // 
            this.p2Example2Btn.Location = new System.Drawing.Point(148, 58);
            this.p2Example2Btn.Name = "p2Example2Btn";
            this.p2Example2Btn.Size = new System.Drawing.Size(166, 46);
            this.p2Example2Btn.TabIndex = 3;
            this.p2Example2Btn.Text = "Hänsel und Gretel";
            this.p2Example2Btn.UseVisualStyleBackColor = true;
            this.p2Example2Btn.Click += new System.EventHandler(this.p2Example2Btn_Click);
            // 
            // p2Example1Btn
            // 
            this.p2Example1Btn.Location = new System.Drawing.Point(10, 58);
            this.p2Example1Btn.Name = "p2Example1Btn";
            this.p2Example1Btn.Size = new System.Drawing.Size(130, 46);
            this.p2Example1Btn.TabIndex = 2;
            this.p2Example1Btn.Text = "sieben Zwerge";
            this.p2Example1Btn.UseVisualStyleBackColor = true;
            this.p2Example1Btn.Click += new System.EventHandler(this.p2Example1Btn_Click);
            // 
            // posSearchBtn
            // 
            this.posSearchBtn.Location = new System.Drawing.Point(700, 26);
            this.posSearchBtn.Name = "posSearchBtn";
            this.posSearchBtn.Size = new System.Drawing.Size(106, 78);
            this.posSearchBtn.TabIndex = 1;
            this.posSearchBtn.Text = "Suchen";
            this.posSearchBtn.UseVisualStyleBackColor = true;
            this.posSearchBtn.Click += new System.EventHandler(this.posSearchBtn_Click);
            // 
            // posRequestField
            // 
            this.posRequestField.Location = new System.Drawing.Point(10, 26);
            this.posRequestField.Name = "posRequestField";
            this.posRequestField.Size = new System.Drawing.Size(684, 26);
            this.posRequestField.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.histogrammJaccard);
            this.groupBox4.Controls.Add(this.histogramFuzzyMembership);
            this.groupBox4.Controls.Add(this.p3Example5Btn);
            this.groupBox4.Controls.Add(this.p3Example4Btn);
            this.groupBox4.Controls.Add(this.p3Example3Btn);
            this.groupBox4.Controls.Add(this.p3Example2Btn);
            this.groupBox4.Controls.Add(this.p3Example1Btn);
            this.groupBox4.Controls.Add(this.fuzzySearchBtn);
            this.groupBox4.Controls.Add(this.fuzzyRequestField);
            this.groupBox4.Location = new System.Drawing.Point(14, 405);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(813, 112);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "P3-Fuzzy(Ogawa)-Anfragen";
            // 
            // histogrammJaccard
            // 
            this.histogrammJaccard.Location = new System.Drawing.Point(645, 26);
            this.histogrammJaccard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.histogrammJaccard.Name = "histogrammJaccard";
            this.histogrammJaccard.Size = new System.Drawing.Size(45, 32);
            this.histogrammJaccard.TabIndex = 10;
            this.histogrammJaccard.Text = "HJ";
            this.histogrammJaccard.UseVisualStyleBackColor = true;
            this.histogrammJaccard.Click += new System.EventHandler(this.HistogrammJaccard_Click);
            // 
            // histogramFuzzyMembership
            // 
            this.histogramFuzzyMembership.Location = new System.Drawing.Point(592, 26);
            this.histogramFuzzyMembership.Name = "histogramFuzzyMembership";
            this.histogramFuzzyMembership.Size = new System.Drawing.Size(45, 32);
            this.histogramFuzzyMembership.TabIndex = 9;
            this.histogramFuzzyMembership.Text = "HF";
            this.histogramFuzzyMembership.UseVisualStyleBackColor = true;
            this.histogramFuzzyMembership.Click += new System.EventHandler(this.histogramFuzzyMembership_Click);
            // 
            // p3Example5Btn
            // 
            this.p3Example5Btn.Location = new System.Drawing.Point(488, 58);
            this.p3Example5Btn.Name = "p3Example5Btn";
            this.p3Example5Btn.Size = new System.Drawing.Size(206, 43);
            this.p3Example5Btn.TabIndex = 6;
            this.p3Example5Btn.Text = "and Prinzessin or Frosch";
            this.p3Example5Btn.UseVisualStyleBackColor = true;
            this.p3Example5Btn.Click += new System.EventHandler(this.p3Example5Btn_Click);
            // 
            // p3Example4Btn
            // 
            this.p3Example4Btn.Location = new System.Drawing.Point(294, 58);
            this.p3Example4Btn.Name = "p3Example4Btn";
            this.p3Example4Btn.Size = new System.Drawing.Size(186, 43);
            this.p3Example4Btn.TabIndex = 5;
            this.p3Example4Btn.Text = "Prinzessin and Frosch";
            this.p3Example4Btn.UseVisualStyleBackColor = true;
            this.p3Example4Btn.Click += new System.EventHandler(this.p3Example4Btn_Click);
            // 
            // p3Example3Btn
            // 
            this.p3Example3Btn.Location = new System.Drawing.Point(202, 58);
            this.p3Example3Btn.Name = "p3Example3Btn";
            this.p3Example3Btn.Size = new System.Drawing.Size(86, 43);
            this.p3Example3Btn.TabIndex = 4;
            this.p3Example3Btn.Text = "or Wald";
            this.p3Example3Btn.UseVisualStyleBackColor = true;
            this.p3Example3Btn.Click += new System.EventHandler(this.p3Example3Btn_Click);
            // 
            // p3Example2Btn
            // 
            this.p3Example2Btn.Location = new System.Drawing.Point(96, 58);
            this.p3Example2Btn.Name = "p3Example2Btn";
            this.p3Example2Btn.Size = new System.Drawing.Size(100, 43);
            this.p3Example2Btn.TabIndex = 3;
            this.p3Example2Btn.Text = "and Wald";
            this.p3Example2Btn.UseVisualStyleBackColor = true;
            this.p3Example2Btn.Click += new System.EventHandler(this.p3Example2Btn_Click);
            // 
            // p3Example1Btn
            // 
            this.p3Example1Btn.Location = new System.Drawing.Point(15, 58);
            this.p3Example1Btn.Name = "p3Example1Btn";
            this.p3Example1Btn.Size = new System.Drawing.Size(75, 43);
            this.p3Example1Btn.TabIndex = 2;
            this.p3Example1Btn.Text = "Hexe";
            this.p3Example1Btn.UseVisualStyleBackColor = true;
            this.p3Example1Btn.Click += new System.EventHandler(this.p3Example1Btn_Click);
            // 
            // fuzzySearchBtn
            // 
            this.fuzzySearchBtn.Location = new System.Drawing.Point(700, 25);
            this.fuzzySearchBtn.Name = "fuzzySearchBtn";
            this.fuzzySearchBtn.Size = new System.Drawing.Size(106, 75);
            this.fuzzySearchBtn.TabIndex = 1;
            this.fuzzySearchBtn.Text = "Suchen";
            this.fuzzySearchBtn.UseVisualStyleBackColor = true;
            this.fuzzySearchBtn.Click += new System.EventHandler(this.fuzzySearchBtn_Click);
            // 
            // fuzzyRequestField
            // 
            this.fuzzyRequestField.Location = new System.Drawing.Point(15, 26);
            this.fuzzyRequestField.Name = "fuzzyRequestField";
            this.fuzzyRequestField.Size = new System.Drawing.Size(566, 26);
            this.fuzzyRequestField.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.p4Example6Btn);
            this.groupBox5.Controls.Add(this.p4Example5Btn);
            this.groupBox5.Controls.Add(this.p4Example4Btn);
            this.groupBox5.Controls.Add(this.p4Example3Btn);
            this.groupBox5.Controls.Add(this.p4Example2Btn);
            this.groupBox5.Controls.Add(this.p4Example1Btn);
            this.groupBox5.Controls.Add(this.cosineSearchBtn);
            this.groupBox5.Controls.Add(this.cosRequestField);
            this.groupBox5.Location = new System.Drawing.Point(14, 522);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(813, 112);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "P4-Cosine-Score";
            // 
            // p4Example6Btn
            // 
            this.p4Example6Btn.Location = new System.Drawing.Point(542, 58);
            this.p4Example6Btn.Name = "p4Example6Btn";
            this.p4Example6Btn.Size = new System.Drawing.Size(152, 43);
            this.p4Example6Btn.TabIndex = 7;
            this.p4Example6Btn.Text = "Doc : 17 (Peter)";
            this.p4Example6Btn.UseVisualStyleBackColor = true;
            this.p4Example6Btn.Click += new System.EventHandler(this.p4Example6Btn_Click);
            // 
            // p4Example5Btn
            // 
            this.p4Example5Btn.Location = new System.Drawing.Point(398, 58);
            this.p4Example5Btn.Name = "p4Example5Btn";
            this.p4Example5Btn.Size = new System.Drawing.Size(138, 43);
            this.p4Example5Btn.TabIndex = 6;
            this.p4Example5Btn.Text = "Doc : 0 (Aladin)";
            this.p4Example5Btn.UseVisualStyleBackColor = true;
            this.p4Example5Btn.Click += new System.EventHandler(this.p4Example5Btn_Click);
            // 
            // p4Example4Btn
            // 
            this.p4Example4Btn.Location = new System.Drawing.Point(294, 58);
            this.p4Example4Btn.Name = "p4Example4Btn";
            this.p4Example4Btn.Size = new System.Drawing.Size(98, 43);
            this.p4Example4Btn.TabIndex = 5;
            this.p4Example4Btn.Text = "Wolf";
            this.p4Example4Btn.UseVisualStyleBackColor = true;
            this.p4Example4Btn.Click += new System.EventHandler(this.p4Example4Btn_Click);
            // 
            // p4Example3Btn
            // 
            this.p4Example3Btn.Location = new System.Drawing.Point(202, 58);
            this.p4Example3Btn.Name = "p4Example3Btn";
            this.p4Example3Btn.Size = new System.Drawing.Size(86, 43);
            this.p4Example3Btn.TabIndex = 4;
            this.p4Example3Btn.Text = "Frosch";
            this.p4Example3Btn.UseVisualStyleBackColor = true;
            this.p4Example3Btn.Click += new System.EventHandler(this.p4Example3Btn_Click);
            // 
            // p4Example2Btn
            // 
            this.p4Example2Btn.Location = new System.Drawing.Point(96, 58);
            this.p4Example2Btn.Name = "p4Example2Btn";
            this.p4Example2Btn.Size = new System.Drawing.Size(100, 43);
            this.p4Example2Btn.TabIndex = 3;
            this.p4Example2Btn.Text = "Prinzessin";
            this.p4Example2Btn.UseVisualStyleBackColor = true;
            this.p4Example2Btn.Click += new System.EventHandler(this.p4Example2Btn_Click);
            // 
            // p4Example1Btn
            // 
            this.p4Example1Btn.Location = new System.Drawing.Point(15, 58);
            this.p4Example1Btn.Name = "p4Example1Btn";
            this.p4Example1Btn.Size = new System.Drawing.Size(75, 43);
            this.p4Example1Btn.TabIndex = 2;
            this.p4Example1Btn.Text = "Aladdin";
            this.p4Example1Btn.UseVisualStyleBackColor = true;
            this.p4Example1Btn.Click += new System.EventHandler(this.p4Example1Btn_Click);
            // 
            // cosineSearchBtn
            // 
            this.cosineSearchBtn.Location = new System.Drawing.Point(700, 25);
            this.cosineSearchBtn.Name = "cosineSearchBtn";
            this.cosineSearchBtn.Size = new System.Drawing.Size(106, 75);
            this.cosineSearchBtn.TabIndex = 1;
            this.cosineSearchBtn.Text = "Suchen / Vergleichen";
            this.cosineSearchBtn.UseVisualStyleBackColor = true;
            this.cosineSearchBtn.Click += new System.EventHandler(this.cosineSearchBtn_Click);
            // 
            // cosRequestField
            // 
            this.cosRequestField.Location = new System.Drawing.Point(15, 26);
            this.cosRequestField.Name = "cosRequestField";
            this.cosRequestField.Size = new System.Drawing.Size(679, 26);
            this.cosRequestField.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.clusterSearchBtn);
            this.groupBox6.Controls.Add(this.clusterRequestField);
            this.groupBox6.Location = new System.Drawing.Point(14, 640);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(813, 112);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "P5-Cluster";
            // 
            // clusterSearchBtn
            // 
            this.clusterSearchBtn.Location = new System.Drawing.Point(700, 25);
            this.clusterSearchBtn.Name = "clusterSearchBtn";
            this.clusterSearchBtn.Size = new System.Drawing.Size(106, 75);
            this.clusterSearchBtn.TabIndex = 1;
            this.clusterSearchBtn.Text = "Suchen / Vergleichen";
            this.clusterSearchBtn.UseVisualStyleBackColor = true;
            this.clusterSearchBtn.Click += new System.EventHandler(this.ClusterSearchBtn_Click);
            // 
            // clusterRequestField
            // 
            this.clusterRequestField.Location = new System.Drawing.Point(15, 26);
            this.clusterRequestField.Name = "clusterRequestField";
            this.clusterRequestField.Size = new System.Drawing.Size(679, 26);
            this.clusterRequestField.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(456, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "B2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(316, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "B1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 775);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox0);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Information Retrieval";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownB2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox0.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addDocBtn;
        private System.Windows.Forms.TextBox docPathField;
        private System.Windows.Forms.Label docCountLabel;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.GroupBox groupBox0;
        private System.Windows.Forms.ListBox resultListBox;
        private System.Windows.Forms.Button boolSearchBtn;
        private System.Windows.Forms.TextBox boolenRequestField;
        private System.Windows.Forms.Button showExplorerBtn;
        private System.Windows.Forms.Button openFileBtn;
        private System.Windows.Forms.Button showAllBtn;
        private System.Windows.Forms.Button p1Example1Btn;
        private System.Windows.Forms.Button p1Example2Btn;
        private System.Windows.Forms.Button p1Example3Btn;
        private System.Windows.Forms.Button p1Example4Btn;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button p2Example3Btn;
        private System.Windows.Forms.Button p2Example2Btn;
        private System.Windows.Forms.Button p2Example1Btn;
        private System.Windows.Forms.Button posSearchBtn;
        private System.Windows.Forms.TextBox posRequestField;
        private System.Windows.Forms.Button p2Example4Btn;
        public System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button p3Example4Btn;
        private System.Windows.Forms.Button p3Example3Btn;
        private System.Windows.Forms.Button p3Example2Btn;
        private System.Windows.Forms.Button p3Example1Btn;
        private System.Windows.Forms.Button fuzzySearchBtn;
        private System.Windows.Forms.TextBox fuzzyRequestField;
        private System.Windows.Forms.Button p3Example5Btn;
        private System.Windows.Forms.Button histogramFuzzyMembership;
        private System.Windows.Forms.TextBox ogawaTresholdtextfield;
        private System.Windows.Forms.Label ogawaTresholdLabel;
        private System.Windows.Forms.Button histogrammJaccard;
        public System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button p4Example6Btn;
        private System.Windows.Forms.Button p4Example5Btn;
        private System.Windows.Forms.Button p4Example4Btn;
        private System.Windows.Forms.Button p4Example3Btn;
        private System.Windows.Forms.Button p4Example2Btn;
        private System.Windows.Forms.Button p4Example1Btn;
        private System.Windows.Forms.Button cosineSearchBtn;
        private System.Windows.Forms.TextBox cosRequestField;
        private System.Windows.Forms.CheckBox runOgawaBox;
        private System.Windows.Forms.Button ClusterBtn;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button clusterSearchBtn;
        private System.Windows.Forms.TextBox clusterRequestField;
        private System.Windows.Forms.NumericUpDown numericUpDownB1;
        private System.Windows.Forms.NumericUpDown numericUpDownB2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

