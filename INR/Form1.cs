﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace INR
{
    public partial class Form1 : Form
    {
        public static Form1 form;
        Docs d;
        public bool runOgawa = true;

        public Form1()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);
            Form1.form = this;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                this.docPathField.Text = "C:\\Users\\Uni\\Documents\\Source\\Repos\\INR\\INR\\CorpusUTF8";
            else
                this.docPathField.Text = "/home/carsten/Projects/InformationRetreival/INR/CorpusUTF8";
            d = new Docs();

            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                groupBox2.Visible = false;
                groupBox3.Visible = false;
                groupBox4.Visible = false;
                groupBox5.Visible = false;
                groupBox6.Visible = false;
                groupBox0.Visible = false;
            }
        }

        #region Events

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            d.killThread();
            Application.Exit();
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            for (int i = 0; i < files.Length; i++)
            {
                d.addPathToDocs(files[i]);
            }
        }

        public void setGroupVisable(GroupBox g, bool val)
        {
            if (Environment.OSVersion.Platform != PlatformID.Win32NT)
                return;
            g.Invoke((MethodInvoker)delegate
            {
                g.Visible = val;
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (Environment.OSVersion.Platform != PlatformID.Win32NT)
                return;

            this.MinimumSize = new Size((int)(groupBox1.Width * 1.05f), (int)(groupBox1.Height * 1.5f));

            // no larger than screen size
            this.MaximumSize = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            groupBox0.AutoSize = true;
            groupBox0.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        }

        private void runOgawaBox_CheckedChanged(object sender, EventArgs e)
        {
            Form1.form.runOgawa = runOgawaBox.Checked;
        }

        #endregion

        #region Documents

        private void addDocBtn_Click(object sender, EventArgs e)
        {
            string path = docPathField.Text;
            if (path.CompareTo("") == 0)
                return;
            if (!Directory.Exists(path) && !File.Exists(path))
            {
                docPathField.ForeColor = Color.Red;
                return;
            }

            float ogawaTreshold = 0.4f;
            try
            {
                ogawaTreshold = Convert.ToSingle(ogawaTresholdtextfield.Text.Replace(".", ","));
            }
            catch (Exception ex) { Console.WriteLine("Invalid treshold value, using 0.4 instead"); }
            Ogawa.treshold = ogawaTreshold;

            d.addPathToDocs(path);
            docPathField.ForeColor = Color.Black;
            updateAddArea();
        }

        public static void updateResultList(LinkedList<string> result)
        {
            Form1.form.groupBox0.Visible = true;
            Form1.form.resultListBox.BeginUpdate();
            Form1.form.resultListBox.Items.Clear();
            foreach (string value in result)
            {
                Form1.form.resultListBox.Items.Add(value);
            }
            if (result.Count == 0)
            {
                Form1.form.resultListBox.Items.Add("Kein Ergebniss");
            }
            Form1.form.resultListBox.EndUpdate();

            //resize
            int elementCount = Math.Max(3, Math.Min(30, result.Count + 2));
            int newHeight = elementCount * Form1.form.resultListBox.ItemHeight;
            Form1.form.resultListBox.Height = newHeight;

            if (Environment.OSVersion.Platform != PlatformID.Win32NT) {
                int minSize = Form1.form.groupBox6.Height + Form1.form.groupBox6.Location.Y;
                Form1.form.groupBox0.Height = newHeight + 60;
                Form1.form.Height = Math.Max(Form1.form.groupBox0.Height + Form1.form.groupBox0.Location.Y, minSize) + 50;
            }
        }

        public void updateAddArea(bool jacc = false)
        {
            this.docCountLabel.Invoke((MethodInvoker)delegate
            {
                if (jacc)
                {
                    docCountLabel.Text = "Anzahl Dokumente: " + d.count + " - updating JaccardValues...";
                }
                else
                {
                    docCountLabel.Text = "Anzahl Dokumente: " + d.count;
                }
            });
        }

        private void openFileBtn_Click(object sender, EventArgs e)
        {
            string s = Form1.form.resultListBox.GetItemText(Form1.form.resultListBox.SelectedItem);
            s = s.Split('|')[0].Trim();
            d.openDoc(d.getIdToName(s));
        }

        private void showExplorerBtn_Click(object sender, EventArgs e)
        {
            string s = Form1.form.resultListBox.GetItemText(Form1.form.resultListBox.SelectedItem);
            s = s.Split('|')[0].Trim();
            d.showDoc(d.getIdToName(s));
        }

        private void showAllBtn_Click(object sender, EventArgs e)
        {
            LinkedList<string> allList = new LinkedList<string>();
            foreach (Doc doc in d.docs)
            {
                allList.AddLast(doc.name + " | Id: " + doc.id);
            }
            Form1.updateResultList(allList);
        }


        #endregion

        #region p1

        void boolSearchBtn_Click(object sender, EventArgs e)
        {
            LinkedList<PostingListElement> result = BooleanRetreival.parseAndCalculate(boolenRequestField.Text);
            LinkedList<string> resultDocIds = new LinkedList<string>();
            foreach (PostingListElement entry in result)
            {
                resultDocIds.AddLast(d.getDocName(entry.docId) + " | Id: " + entry.docId);
            }
            updateResultList(resultDocIds);
        }

        private void p1Example1Btn_Click(object sender, EventArgs e)
        {
            boolenRequestField.Text = "Hexe";
        }

        private void p1Example2Btn_Click(object sender, EventArgs e)
        {
            boolenRequestField.Text = "Hexe \\and Prinzessin";
        }

        private void p1Example3Btn_Click(object sender, EventArgs e)
        {
            boolenRequestField.Text = "(Hexe \\and Prinzessin) \\or (Frosch \\and König \\and Tellerlein)";
        }

        private void p1Example4Btn_Click(object sender, EventArgs e)
        {
            boolenRequestField.Text = "(Hexe \\and Prinzessin) \\or (König \\and \\not Hexe)";
        }


        #endregion

        #region p2

        void posSearchBtn_Click(object sender, EventArgs e)
        {
            LinkedList<Tuple<int, Tuple<int, int>>> result = PositionalIndex.parseAndCalculate(posRequestField.Text);
            LinkedList<string> printList = new LinkedList<string>();
            foreach (Tuple<int, Tuple<int, int>> t in result)
            {
                printList.AddLast(d.getDocName(t.Item1) + " | Id: " + t.Item1 + ", Pos: " + t.Item2.Item1 + " - " + t.Item2.Item2);
            }
            Form1.updateResultList(printList);
        }

        private void p2Example1Btn_Click(object sender, EventArgs e)
        {
            posRequestField.Text = "sieben Zwerge";
        }

        private void p2Example2Btn_Click(object sender, EventArgs e)
        {
            posRequestField.Text = "Hänsel und Gretel";
        }

        private void p2Example3Btn_Click(object sender, EventArgs e)
        {
            posRequestField.Text = "Kuchen /5 Wein";
        }

        private void p2Example4Btn_Click(object sender, EventArgs e)
        {
            posRequestField.Text = "und /100 die";
        }


        #endregion

        #region p3

        private void fuzzySearchBtn_Click(object sender, EventArgs e)
        {
            double[] result = Fuzzy2.parseAndCalculate(fuzzyRequestField.Text);
            List<Tuple<int, double>> sortedResultList = new List<Tuple<int, double>>();
            for (int c = 0; c < result.Length; c++)
            {
                int docId = 0;
                for (int i = 1; i < result.Length; i++)
                {
                    if (result[docId] < result[i])
                    {
                        docId = i;
                    }
                }
                sortedResultList.Add(new Tuple<int, double>(docId, result[docId]));
                result[docId] = -1;
            }
            LinkedList<string> resultList = new LinkedList<string>();
            foreach (Tuple<int, double> r in sortedResultList)
            {
                resultList.AddLast(d.getDocName(r.Item1) + " | WTD: " + r.Item2);
            }
            updateResultList(resultList);
        }

        private void p3Example1Btn_Click(object sender, EventArgs e)
        {
            fuzzyRequestField.Text = "Hexe";
        }

        private void p3Example2Btn_Click(object sender, EventArgs e)
        {
            fuzzyRequestField.Text = "Hexe \\and Wald";
        }

        private void p3Example3Btn_Click(object sender, EventArgs e)
        {
            fuzzyRequestField.Text = "Hexe \\or Wald";
        }

        private void p3Example4Btn_Click(object sender, EventArgs e)
        {
            fuzzyRequestField.Text = "Prinzessin \\and Frosch";
        }

        private void p3Example5Btn_Click(object sender, EventArgs e)
        {
            fuzzyRequestField.Text = "Hexe \\and Prinzessin \\or Frosch";
        }

        private void histogramFuzzyMembership_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(Ogawa.w, "FuzzyMembership");
            form2.Show();
        }

        private void HistogrammJaccard_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(Ogawa.c, "Jaccard");
            form2.Show();
        }

        #endregion

        #region p4

        private void cosineSearchBtn_Click(object sender, EventArgs e)
        {
            List<Tuple<int, double>> result;
            result = CosineScoreParser.parseAndExecute(cosRequestField.Text);
            LinkedList<string> resultList = new LinkedList<string>();
            foreach(Tuple<int,double> elem in result)
            {
                resultList.AddLast(d.getDocName(elem.Item1) + " | Id: " + elem.Item1 + ", Score: " + elem.Item2);
            }
            updateResultList(resultList);
        }

        private void p4Example1Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text += "Aladdin ";
        }

        private void p4Example2Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text += "Prinzessin ";
        }

        private void p4Example3Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text += "Frosch ";
        }

        private void p4Example4Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text += "Wolf ";
        }

        private void p4Example5Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text = 0.ToString();
        }

        private void p4Example6Btn_Click(object sender, EventArgs e)
        {
            cosRequestField.Text = 17.ToString();
        }


        #endregion

        private void ClusterBtn_Click(object sender, EventArgs e)
        {
            int b1 = (int)Form1.form.numericUpDownB1.Value;
            Clustering.initClusters(b1);
            Form1.form.setGroupVisable(Form1.form.groupBox6, true);
        }

        private void ClusterSearchBtn_Click(object sender, EventArgs e)
        {
            int b2 = (int)Form1.form.numericUpDownB2.Value;
            Dictionary<int, double> resultList = ClusteringParser.parse(clusterRequestField.Text, b2);
            LinkedList<string> results = new LinkedList<string>();
            foreach(KeyValuePair<int, double> kv in resultList)
            {
                results.AddLast(d.getDocName(kv.Key) + " | Id: " + kv.Key + ", Score: " + kv.Value);
            }
            updateResultList(results);
        }
    }
}