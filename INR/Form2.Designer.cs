﻿namespace INR
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picHisto = new System.Windows.Forms.PictureBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.sizeField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picHisto)).BeginInit();
            this.SuspendLayout();
            // 
            // picHisto
            // 
            this.picHisto.Location = new System.Drawing.Point(9, 8);
            this.picHisto.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.picHisto.Name = "picHisto";
            this.picHisto.Size = new System.Drawing.Size(517, 276);
            this.picHisto.TabIndex = 0;
            this.picHisto.TabStop = false;
            this.picHisto.Paint += new System.Windows.Forms.PaintEventHandler(this.picHisto_Paint);
            this.picHisto.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picHisto_MouseMove);
            // 
            // sizeField
            // 
            this.sizeField.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.sizeField.Location = new System.Drawing.Point(251, 289);
            this.sizeField.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sizeField.Name = "sizeField";
            this.sizeField.Size = new System.Drawing.Size(100, 20);
            this.sizeField.TabIndex = 1;
            this.sizeField.Text = "0,1";
            this.sizeField.TextChanged += new System.EventHandler(this.sizeField_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 291);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Bin-Breite";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 318);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sizeField);
            this.Controls.Add(this.picHisto);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form2";
            this.Text = "Ogawa-Histogram";
            this.Resize += new System.EventHandler(this.Form2_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.picHisto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picHisto;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TextBox sizeField;
        private System.Windows.Forms.Label label2;
    }
}