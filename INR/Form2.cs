﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INR
{
    public partial class Form2 : Form
    {
        public static Form2 form;
        public int[] _val = new int[10];
        private int maxVal = 0;
        private double[,] usedMatrix;

        Matrix transformation;

        public Form2()
        {
            InitializeComponent();
            Form2.form = this;
            this.Text = "Histogram FuzzyMembership | Ogawa Schwellwert: " + Math.Round(Ogawa.treshold, 2);
            getBarsCount(10);
        }

        public Form2(double[,] usedMatrix, string desc)
        {
            this.usedMatrix = usedMatrix;
            InitializeComponent();
            Form2.form = this;
            this.Text = "Histogram " + desc + " | Ogawa Schwellwert: " + Math.Round(Ogawa.treshold, 2);
            getBarsCount(10);
        }

        private void picHisto_Paint(object sender, PaintEventArgs e)
        {
            DrawHistogram(e.Graphics, picHisto.BackColor, _val, picHisto.ClientSize.Width, picHisto.ClientSize.Height);
        }

        private void DrawHistogram(Graphics gr, Color backColor, int[] val, int width, int height)
        {
            Color[] colors = new Color[] { Color.Red, Color.Green, Color.Blue, Color.Orange, Color.Purple, Color.Yellow };

            gr.Clear(backColor);

            RectangleF dataBounds = new RectangleF(0, 0, val.Length, maxVal);
            PointF[] points = { new PointF(0, height), new PointF(width, height), new PointF(0, 0) };
            transformation = new Matrix(dataBounds, points);
            gr.Transform = transformation;

            using (Pen thinPen = new Pen(Color.Black, 0))
            {
                for (int i = 0; i < val.Length; i++)
                {
                    RectangleF rect = new RectangleF(i, 0, 1, Convert.ToSingle(val[i]));

                    using (Brush brush = new SolidBrush(colors[i % colors.Length]))
                    {
                        gr.FillRectangle(brush, rect);
                        gr.DrawRectangle(thinPen, rect.X, rect.Y, rect.Width, rect.Height);
                    }
                }
            }

            gr.ResetTransform();
            gr.DrawRectangle(Pens.Black, 0, 0, width - 1, height - 1);
        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            picHisto.Refresh();
        }

        string tipText = "";

        private void picHisto_MouseMove(object sender, MouseEventArgs e)
        {
            if (transformation == null)
                return;
            float barWidth = picHisto.ClientSize.Width / _val.Length;
            int barNumber = (int)(e.X / barWidth);
            if (barNumber >= _val.Length)
                return;

            PointF[] points = { new PointF(0, Convert.ToSingle(_val[barNumber])) };
            transformation.TransformPoints(points);

            string tip = "";
                tip = "Value = " + _val[barNumber];
            if (tip != tipText)
            {
                tipText = tip;
                toolTip.SetToolTip(picHisto, tip);
            }
        }

        private void sizeField_TextChanged(object sender, EventArgs e)
        {
            int barNumber = 10;
            try
            {
                float textValue = Convert.ToSingle(sizeField.Text.Replace(".", ","));
                if (textValue <= 1)
                    barNumber = (int)Math.Ceiling(1.0f / textValue);
                else
                    barNumber = (int)Math.Floor(textValue);
            }
            catch (Exception ex)
            {
                return;
            }
            if (barNumber <= 0 || barNumber > 500) return;

            getBarsCount(barNumber);

            picHisto.Refresh();
        }

        private void getBarsCount(int barNumber)
        {
            _val = new int[barNumber];
            maxVal = 0;
            for (int i = 0; i < _val.Length; i++)
            {
                _val[i] = 0;
            }

            double barSize = 1f / (float)barNumber;
            double[,] ogawa = this.usedMatrix;
            maxVal = ogawa.GetLength(0) * ogawa.GetLength(1);
            for (int i = 0; i < ogawa.GetLength(0); i++)
            {
                for (int j = 0; j < ogawa.GetLength(1); j++)
                {
                    int pos = (int)Math.Floor(ogawa[i, j] / barSize);
                    if (pos == barNumber)
                        pos--;
                    _val[pos]++;
                    if (maxVal < _val[pos])
                        maxVal = _val[pos];
                }
            }
        }
    }
}
