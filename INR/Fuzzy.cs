﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INR
{
    class Fuzzy
    {

        public static LinkedList<PostingListElement> parseAndCalculate(string query)
        {
            string[] ors = query.ToLower().Replace("(", "").Replace(")", "").Split(new string[] { "\\or" }, StringSplitOptions.RemoveEmptyEntries);
            LinkedList<PostingListElement> resultAll = new LinkedList<PostingListElement>();
            for (int i = 0; i < ors.Length; i++)
            {
                string[] ands = ors[i].Split(new string[] { "\\and" }, StringSplitOptions.RemoveEmptyEntries);
                LinkedList<PostingListElement> resultAnds = PostingFile.get(ands[0].Trim()).list;
                int firstIndex = PostingFile.postingFile.IndexOfKey(ands[0].Trim());

                for (int j = 1; j < ands.Length; j++)
                {
                    LinkedList<PostingListElement> currentAnd = PostingFile.get(ands[j].Replace("\\not", "").Trim()).list;
                    int currentIndex = PostingFile.postingFile.IndexOfKey(ands[j].Replace("\\not", "").Trim());
                    if (ands[j].Trim().StartsWith("\\not"))
                    {
                        resultAnds = Fuzzy.andNot(resultAnds, firstIndex, currentAnd, currentIndex);
                    }
                    else
                    {
                        resultAnds = Fuzzy.and(resultAnds, firstIndex, currentAnd, currentIndex);
                    }
                }
                resultAll = Fuzzy.or(resultAll, resultAnds);
            }
            return resultAll;
        }

        public static LinkedList<PostingListElement> and(LinkedList<PostingListElement> a, int aIndex, LinkedList<PostingListElement> b, int bIndex)
        {
            LinkedList<PostingListElement> result = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;
            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId == p2.Value.docId)
                {
                    PostingListElement copy = p1.Value.clone();
                    copy.wtd = Math.Min(Ogawa.w[aIndex, p1.Value.docId], Ogawa.w[bIndex, p2.Value.docId]);
                    result.AddLast(copy);
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else if (p1.Value.docId < p2.Value.docId)
                {
                    p1 = p1.Next;
                }
                else
                {
                    p2 = p2.Next;
                }
            }
            return result;
        }

        public static LinkedList<PostingListElement> andNot(LinkedList<PostingListElement> a, int aIndex, LinkedList<PostingListElement> b, int bIndex)
        {
            Console.WriteLine("calling andNot");
            LinkedList<PostingListElement> result = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;
            while (p1 != null && p2 != null)
            {
                if (p2.Value.docId == p1.Value.docId)
                {
                    PostingListElement copy = p1.Value.clone();
                    copy.wtd = Math.Min(p1.Value.wtd, 1-p2.Value.wtd);
                    result.AddLast(copy);
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else if (p1.Value.docId < p2.Value.docId)
                {
                    result.AddLast(p1.Value);
                    p1 = p1.Next;
                }
                else
                {
                    PostingListElement copy = p1.Value.clone();
                    copy.wtd = 1 - p2.Value.wtd;
                    result.AddLast(copy);
                    p2 = p2.Next;
                }
            }
            while (p1 != null)
            {
                result.AddLast(p1.Value);
                p1 = p1.Next;
            }
            return result;
        }

        public static LinkedList<PostingListElement> or(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b)
        {
            LinkedList<PostingListElement> answer = new LinkedList<PostingListElement>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;

            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId < p2.Value.docId)
                {
                    answer.AddLast(p1.Value);
                    p1 = p1.Next;
                }
                else if (p1.Value.docId > p2.Value.docId)
                {
                    answer.AddLast(p2.Value);
                    p2 = p2.Next;
                }
                else
                {
                    PostingListElement copy = p1.Value.clone();
                    copy.wtd = Math.Max(p1.Value.wtd, p2.Value.wtd);
                    answer.AddLast(copy);
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
            }

            while (p1 != null)
            {
                answer.AddLast(p1.Value);
                p1 = p1.Next;
            }
            while (p2 != null)
            {
                answer.AddLast(p2.Value);
                p2 = p2.Next;
            }
            return answer;
        }

    }
}
