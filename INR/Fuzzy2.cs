﻿using System;
using System.Collections.Generic;

namespace INR
{
    public class Fuzzy2
    { 

        public static double[] parseAndCalculate(string query) {
            List<int[]> currentValues = new List<int[]>();

            string[] ors = query.ToLower().Replace("(", "").Replace(")", "").Split(new string[] { "\\or" }, StringSplitOptions.RemoveEmptyEntries);
            double[] resultAll = new double[Tokenizer.wordsInDocs.Count];
            for (int i = 0; i < ors.Length; i++)
            {
                string[] ands = ors[i].Split(new string[] { "\\and" }, StringSplitOptions.RemoveEmptyEntries);
                double[] resultAndWtd = Ogawa.getAllWtdValuesForTerm(ands[0].Trim());
                for (int j = 1; j < ands.Length; j++)
                {
                    double[] currentAndWtd = Ogawa.getAllWtdValuesForTerm(ands[j].Replace("\\not", "").Trim());
                    if (ands[j].Trim().StartsWith("\\not"))
                    {
                        resultAndWtd = Fuzzy2.andNot(resultAndWtd, currentAndWtd);
                    }
                    else
                    {
                        resultAndWtd = Fuzzy2.and(resultAndWtd, currentAndWtd);
                    }
                }
                resultAll = Fuzzy2.or(resultAll, resultAndWtd);
            }
            return resultAll;
        }


        public static double[] and(double[] currentValues, double[] currentValues2)
        {
            for(int docId = 0; docId < currentValues.Length; docId++)
            {
                currentValues[docId] = Math.Min(currentValues[docId], currentValues2[docId]);
            }
            return currentValues;
        }

        public static double[] or(double[] currentValues, double[] currentvalues2)
        {
            for(int docId = 0; docId < currentValues.Length; docId++)
            {
                currentValues[docId] = Math.Max(currentValues[docId], currentvalues2[docId]);
            }
            return currentValues;
        }

        public static double[] andNot(double[] currentValues, double[] currentValues2)
        {
            for(int docId = 0; docId < currentValues2.Length; docId++)
            {
                currentValues2[docId] = 1 - currentValues2[docId];
            }
            return Fuzzy2.and(currentValues, currentValues2);
        }

    }
}
