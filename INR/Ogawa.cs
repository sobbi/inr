using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace INR
{
    public class Ogawa
    {
        public static double[,] c;
        public static double[,] w;

        public static double treshold = 0.4;

        public static double GetJaccardValue(string term1, string term2)
        {
            int i1 = PostingFile.postingFile.IndexOfKey(term1);
            int i2 = PostingFile.postingFile.IndexOfKey(term2);
            return c[i1, i2];
        }

        public static double[] getAllWtdValuesForTerm(string term)
        {
            int indexOfTerm = PostingFile.postingFile.IndexOfKey(term);
            int rank = w.Rank;
            double[] result = new double[w.GetLength(w.Rank - 1)];
            if (indexOfTerm < 0)
                return result;
            for(int docId = 0; docId < result.Length; docId++)
            {
                result[docId] = w[indexOfTerm, docId];
            }
            return result;
        }

        public static void CalculateJaccard()
        {
            DateTime t1 = DateTime.UtcNow;
            int numOfTerms = PostingFile.postingFile.Count;
            c = new double[numOfTerms, numOfTerms];
            int commonFreq = 0;
            for(int i = 0; i < numOfTerms; i++) { 
                c[i, i] = 1.0;
                LinkedList<PostingListElement> l1 = PostingFile.postingFile.Values[i].list;
                for (int j = 0; j < i; j++)
                {
                    LinkedList<PostingListElement> l2 = PostingFile.postingFile.Values[j].list;
                    commonFreq = CalculateCommonFreq(l1, l2);
                    int list1Freq = l1.Count;
                    int list2Freq = l2.Count;
                    double result = (double)commonFreq / (list1Freq + list2Freq - commonFreq);
                    if (result > treshold)
                    {
                        c[i, j] = result;
                        c[j, i] = result;
                    }
                }
            }
            DateTime t2 = DateTime.UtcNow;
            Console.WriteLine("Time taken for calculateJaccardValues: " + (t2 - t1));
        }

        private static int CalculateCommonFreq(LinkedList<PostingListElement> l1, LinkedList<PostingListElement> l2)
        {
            int commonFreq = 0;
            LinkedListNode<PostingListElement> p1 = l1.First;
            LinkedListNode<PostingListElement> p2 = l2.First;
            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId == p2.Value.docId)
                {
                    p1 = p1.Next;
                    p2 = p2.Next;
                    commonFreq++;
                }
                else if (p1.Value.docId > p2.Value.docId)
                {
                    p2 = p2.Next;
                }
                else
                {
                    p1 = p1.Next;
                }
            }
            return commonFreq;
        }

        public static void CreateFuzzyValues()
        {
            DateTime t1 = DateTime.UtcNow;
            int numOfWords = PostingFile.postingFile.Count;
            int numOfDocs = Tokenizer.wordsInDocs.Count;
            w = new double[numOfWords, numOfDocs];
            Parallel.For(0, numOfWords, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount - 1 }, i =>
            {
                string currentTerm = PostingFile.postingFile.Values[i].term;
                LinkedList<PostingListElement> currentTermPostingList = PostingFile.postingFile.Values[i].list;
            for(int docId = 0; docId < numOfDocs; docId++)
                {
                    List<WordInDoc> wordsInCurrentDoc = Tokenizer.wordsInDocs[docId];
                    double wtd = 0;
                    foreach (WordInDoc word in wordsInCurrentDoc)
                    {
                        wtd += Math.Log((1.0 - c[i, word.id]), 2);
                    }
                    wtd = 1.0 - Math.Pow(2, wtd);
                    w[i, docId] = wtd;
                }
            });
            DateTime t2 = DateTime.UtcNow;
            Console.WriteLine("Time Taken for calculateFuzzyValues: " + (t2-t1));
        }

    }
}
