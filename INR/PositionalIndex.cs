﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace INR
{
    public class PositionalIndex
    {

        public static LinkedList<Tuple<int, Tuple<int, int>>> parseAndCalculate(string query)
        {
            LinkedList<Tuple<int, Tuple<int, int>>> r = new LinkedList<Tuple<int, Tuple<int, int>>>();
            List<LinkedList<Tuple<int, Tuple<int, int>>>> results = new List<LinkedList<Tuple<int, Tuple<int, int>>>>();
            if (!query.Contains("/"))
            {
                string[] terms = query.Split(' ');
                for (int i = 0; i < terms.Length - 1; i++)
                {
                    try
                    {
                        results.Add(positionalIntersect(PostingFile.postingFile[terms[i].ToLower().Trim()].list, PostingFile.postingFile[terms[i + 1].ToLower().Trim()].list));
                    }
                    catch (KeyNotFoundException ex)
                    {
                        results.Add(new LinkedList<Tuple<int, Tuple<int, int>>>());
                    }

                }
                if (results.Count > 1)
                    r = mergeResults(results[0], results[1]);
                else
                    r = results[0];
            }
            else
            {
                Match match = Regex.Match(query, @"/[0-9]*");
                if (match.Captures.Count != 1)
                {
                    throw new Exception("Syntax Error");
                }
                int k = System.Convert.ToInt32(match.Value.Trim('/'));
                string[] terms = Regex.Split(query, @"/[0-9]*");
                try
                {
                    r = positionalIntersect(PostingFile.postingFile[terms[0].ToLower().Trim()].list, PostingFile.postingFile[terms[1].ToLower().Trim()].list, k);
                } catch(KeyNotFoundException ex)
                {
                    r = new LinkedList<Tuple<int, Tuple<int, int>>>();
                }
            }
            /**Testing Jaccard Implementation:**/
            /*string term1 = "zwerg";
            string term2 = "nase";
            Console.Write(term1 + ": ");
            foreach(PostingListElement e in PostingFile.postingFile[term1].list)
            {
                Console.Write(e.docId + "-");
            }
            Console.WriteLine("");
            Console.Write(term2 + ": ");
            foreach (PostingListElement e in PostingFile.postingFile[term2].list)
            {
                Console.Write(e.docId + "-");
            }
            Console.WriteLine("");
            Console.WriteLine("JaccardValue for " + term1 + " and " + term2 + ": " + Ogawa.GetJaccardValue(term1, term2));
            Console.WriteLine("JaccardValue for " + term2 + " and " + term1 + ": " + Ogawa.GetJaccardValue(term2, term1));
            Console.WriteLine("JaccardValue for " + term1 + " and " + term1 + ": " + Ogawa.GetJaccardValue(term1, term1));
            Console.WriteLine("JaccardValue for " + term2 + " and " + term2 + ": " + Ogawa.GetJaccardValue(term2, term2));
            */
            /**Testing WTD-Implementation:**/
            /*
            string term = "nase";
            int index = PostingFile.postingFile.IndexOfKey("nase");
            for(int i = 0; i < Tokenizer.wordsInDocs.Count; i++)
            {
                Console.WriteLine("    Document " + i + ": wtd = " + Ogawa.w[index, i]);
            }
            */

            return r;
        }

        public static LinkedList<Tuple<int, Tuple<int, int>>> mergeResults(LinkedList<Tuple<int, Tuple<int, int>>> a, LinkedList<Tuple<int, Tuple<int, int>>> b)
        {
            LinkedList<Tuple<int, Tuple<int, int>>> result = new LinkedList<Tuple<int, Tuple<int, int>>>();
            LinkedListNode<Tuple<int, Tuple<int, int>>> p1 = a.First;
            LinkedListNode<Tuple<int, Tuple<int, int>>> p2 = b.First;
            while (p1 != null && p2 != null)
            {
                if (p1.Value.Item1.Equals(p2.Value.Item1))
                {
                    if (p1.Value.Item2.Item2.Equals(p2.Value.Item2.Item1))
                    {
                        Tuple<int, int> pos = new Tuple<int, int>(p1.Value.Item2.Item1, p2.Value.Item2.Item2);
                        result.AddLast(new Tuple<int, Tuple<int, int>>(p1.Value.Item1, pos));
                        p1 = p1.Next;
                        p2 = p2.Next;
                    }
                    else if (p1.Value.Item2.Item2 < p2.Value.Item2.Item1)
                    {
                        p1 = p1.Next;
                    }
                    else
                    {
                        p2 = p2.Next;
                    }
                }
                else if (p1.Value.Item1 < p2.Value.Item1)
                {
                    p1 = p1.Next;
                }
                else
                {
                    p2 = p2.Next;
                }
            }
            return result;
        }


        public static LinkedList<Tuple<int, Tuple<int, int>>> positionalIntersect(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b)
        {
            LinkedList<Tuple<int, Tuple<int, int>>> result = new LinkedList<Tuple<int, Tuple<int, int>>>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;

            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId == p2.Value.docId)
                {
                    List<int> l = new List<int>();
                    LinkedListNode<int> pp1 = p1.Value.positions.First;
                    LinkedListNode<int> pp2 = p2.Value.positions.First;
                    while (pp1 != null)
                    {
                        while (pp2 != null)
                        {
                            if (Math.Abs(pp1.Value - pp2.Value) <= 1)
                            {
                                l.Add(pp2.Value);
                            }
                            else
                            {
                                if (pp2.Value > pp1.Value)
                                {
                                    break;
                                }
                            }
                            pp2 = pp2.Next;
                        }
                        while (l.Count > 0 && (Math.Abs(l[0] - pp1.Value) > 1))
                        {
                            l.RemoveAt(0);
                        }
                        foreach (int ps in l)
                        {
                            result.AddLast(new Tuple<int, Tuple<int, int>>(p1.Value.docId, new Tuple<int, int>(pp1.Value, ps)));
                        }
                        pp1 = pp1.Next;
                    }
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else
                {
                    if (p1.Value.docId < p2.Value.docId)
                        p1 = p1.Next;
                    else
                        p2 = p2.Next;
                }
            }
            return result;
        }


        public static LinkedList<Tuple<int, Tuple<int, int>>> positionalIntersect(LinkedList<PostingListElement> a, LinkedList<PostingListElement> b, int k)
        {
            LinkedList<Tuple<int, Tuple<int, int>>> result = new LinkedList<Tuple<int, Tuple<int, int>>>();
            LinkedListNode<PostingListElement> p1 = a.First;
            LinkedListNode<PostingListElement> p2 = b.First;

            while (p1 != null && p2 != null)
            {
                if (p1.Value.docId == p2.Value.docId)
                {
                    List<int> l = new List<int>();
                    LinkedListNode<int> pp1 = p1.Value.positions.First;
                    LinkedListNode<int> pp2 = p2.Value.positions.First;
                    while (pp1 != null)
                    {
                        while (pp2 != null)
                        {
                            if (Math.Abs(pp1.Value - pp2.Value) <= k)
                            {
                                l.Add(pp2.Value);
                            }
                            else
                            {
                                if (pp2.Value > pp1.Value)
                                {
                                    break;
                                }
                            }
                            pp2 = pp2.Next;
                        }
                        while (l.Count > 0 && (Math.Abs(l[0] - pp1.Value) > k))
                        {
                            l.RemoveAt(0);
                        }
                        foreach (int ps in l)
                        {
                            result.AddLast(new Tuple<int, Tuple<int, int>>(p1.Value.docId, new Tuple<int, int>(pp1.Value, ps)));
                        }
                        pp1 = pp1.Next;
                    }
                    p1 = p1.Next;
                    p2 = p2.Next;
                }
                else
                {
                    if (p1.Value.docId < p2.Value.docId)
                        p1 = p1.Next;
                    else
                        p2 = p2.Next;
                }
            }
            return result;
        }
    }
}
