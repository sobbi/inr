﻿using System;
using System.Collections.Generic;

namespace INR
{
    //Postingfile klasse speichert eine liste mit postinglist elementen
    //postinglist klasse ist definiert durch einen term und beinhaltet alle docIds, in denen der term vorkommt

    public class PostingFile
    {

        public static SortedList<string, PostingList> postingFile = new SortedList<string, PostingList>();

        public static void initDocHashSet()
        {
            DateTime start = DateTime.Now;
            foreach(PostingList entry in postingFile.Values)
            {
                foreach(PostingListElement e in entry.list)
                {
                    if (!entry.docs.ContainsKey(e.docId))
                    {
                        entry.docs.Add(e.docId, e);
                    }
                }
            }
            DateTime end = DateTime.Now;
            Console.WriteLine("DocHashSet created in: " + (end - start));
        }

        public static void addDocIdToTerm(string term, int docId, List<int> positions)
        {
            PostingList current;
            if(postingFile.TryGetValue(term, out current))
            {
                current.addDocument(docId, positions);
            }
            else
            {
                postingFile.Add(term, new PostingList(term, docId, positions));
            }
        }

        public static PostingList get(string key)
        {
            if(postingFile.ContainsKey(key)) {
                return postingFile[key];
            }
            return new PostingList(key);
        }

        public static PostingList getPostingList(string term)
        {
            PostingList result = null;
            bool correctionNeeded = false;
            try
            {
                result = postingFile[term];
                if (result.list.Count < 5)
                    correctionNeeded = true;
            } catch (KeyNotFoundException ex)
            {
                correctionNeeded = true;
            }
            if (correctionNeeded)
            {
                result = ToleranceMatching.startCorrection(term);
            }
            return result;
        }

    }

    public class PostingList
    {
        public string term;
        public int freq = 0;
        public LinkedList<PostingListElement> list = new LinkedList<PostingListElement>();
        public Dictionary<int, PostingListElement> docs = new Dictionary<int, PostingListElement>();

        public PostingList(string term)
        {
            this.term = term;
        }

        public PostingList(string term, int docId, List<int> positions)
        {
            this.term = term;
            addDocument(docId, positions);
        }

        public void addDocument(int docId, List<int> positions)
        {
            this.addDocument(new PostingListElement(docId, positions));
        }

        public void addDocument(PostingListElement entry)
        {
            list.AddLast(entry);
            this.freq = list.Count;
        }

    }

    public class PostingListElement
    {
        public int docId;
        public LinkedList<int> positions;
        public double wtd;

        public PostingListElement(int docId, List<int> positions)
        {
            this.docId = docId;
            this.positions = new LinkedList<int>(positions);
        }

        //positions will not be deep cloned
        public PostingListElement clone()
        {
            return (PostingListElement)this.MemberwiseClone();
        }
    }
}
