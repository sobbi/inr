﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace INR
{
    public class Tokenizer
    {

        public static Dictionary<int, List<WordInDoc>> wordsInDocs = new Dictionary<int, List<WordInDoc>>();
        public static Dictionary<string, List<string>> kGramIndices = new Dictionary<string, List<string>>();
        public static Dictionary<string, List<string>> biGramIndices = new Dictionary<string, List<string>>();


        public static List<string> tokenize(string path, int docId) {
            List<string> tokens = new List<string>();
            wordsInDocs.Add(docId, new List<WordInDoc>());
            foreach (string line in File.ReadLines(path))
            {
                string[] currentTokens = line.ToLower().Split(new string[] { " ", ".", ",", ";", ":", "!", "?", "\"", "-" }, StringSplitOptions.RemoveEmptyEntries);
                tokens.AddRange(currentTokens);
                foreach(string token in currentTokens)
                {
                    wordsInDocs[docId].Add(new WordInDoc(token));
                    kGramIndices = addToKGram(kGramIndices, token, 3);
                    biGramIndices = addToKGram(biGramIndices, token, 2);
                }
            }
            return tokens;
        }

        private static Dictionary<string, List<string>> addToKGram(Dictionary<string, List<string>> result, string token, int k)
        {
            List<string> kgrams = createKGrams(token, k);
            foreach (string current in kgrams)
            {
                if (result.ContainsKey(current))
                {
                    result[current].Add(token);
                }
                else
                {
                    List<string> initList = new List<string>();
                    initList.Add(token);
                    result.Add(current, initList);
                }
            }
            return result;
        }

        public static List<string> createKGrams(string term, int k)
        {
            string tokenD = "$" + term + "$";
            List<string> kgrams = new List<string>();
            for (int i = 0; i < tokenD.Length - k; i++)
            {
                string current = tokenD.Substring(i, k);
                kgrams.Add(current);
            }
            return kgrams;
        }

        public static void UpdateIndices()
        {
            DateTime t1 = DateTime.UtcNow;
            for(int wordId = 0; wordId < PostingFile.postingFile.Count; wordId++) {
                PostingList p = PostingFile.postingFile.Values[wordId];
                string currentTerm = p.term;
                Parallel.ForEach(p.list, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount - 1 }, node =>
                {
                    int currentDocId = node.docId;
                    Parallel.ForEach(wordsInDocs[currentDocId], new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount - 1 }, word => {
                        if (word.term.Equals(currentTerm))
                        {
                            word.id = wordId;
                        }
                    });
                });
            }
            DateTime t2 = DateTime.UtcNow;
            Console.WriteLine("Time Taken for updating Indices: " + (t2 - t1));
        }

    }

    public class WordInDoc
    {

        public string term;
        public int id;

        public WordInDoc(string term, int id)
        {
            this.term = term;
            this.id = id;
        }

        public WordInDoc(string term)
        {
            this.term = term;
        }

        public override bool Equals(object obj)
        {
            var doc = obj as WordInDoc;
            return doc != null &&
                   term == doc.term;
        }

        public override int GetHashCode()
        {
            return 1403212655 + EqualityComparer<string>.Default.GetHashCode(term);
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
