﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INR
{
    class ToleranceMatching
    {
        private static float BIGRAM_TRESHOLD = 0.66f;

        public static PostingList startCorrection(string term)
        {
            List<string> biGrams = Tokenizer.createKGrams(term, 2);
            Dictionary<string, int> counter = new Dictionary<string, int>();

            foreach (string biGram in biGrams)
            {
                if (Tokenizer.biGramIndices.ContainsKey(biGram))
                {
                    List<string> tmpBiGrams = Tokenizer.biGramIndices[biGram];
                    foreach (string tmpBiGram in tmpBiGrams)
                    {
                        if (counter.ContainsKey(tmpBiGram))
                        {
                            counter[tmpBiGram]++;
                        }
                        else
                        {
                            counter.Add(tmpBiGram, 1);
                        }
                    }
                }
            }
            List<string> termList = new List<string>();
            foreach(KeyValuePair<string, int> c in counter)
            {
                if (c.Value / biGrams.Count > BIGRAM_TRESHOLD)
                {
                    termList.Add(c.Key);
                }
            }

            //TRIGRAM Replace with global Variable.
            List<string> kGramsQueryTerm = Tokenizer.createKGrams(term, 3);
            Dictionary<string, double> jaccardValues = new Dictionary<string, double>();
            foreach(string t in termList)
            {
                List<string> tmpList = Tokenizer.createKGrams(t, 3);
                int tmpLen = kGramsQueryTerm.Count + tmpList.Count;
                int distLen = kGramsQueryTerm.Concat(tmpList).Distinct().ToList().Count;
                int nenner = tmpLen - distLen;
                int zaehler = distLen;
                jaccardValues.Add(t, (double)zaehler/(double)nenner);
            }
            jaccardValues = jaccardValues.OrderBy(t => t.Value).Take(10).ToDictionary(t => t.Key, t => t.Value);
            int bestLevenshteinDist = int.MaxValue;
            string resultTerm = null;
            foreach(KeyValuePair<string, double> kv in jaccardValues)
            {
                int currentLevenshteinDist = CalcLevenshteinDistance(term, kv.Key);
                Console.WriteLine("LevenshteinDistance called with string " + term + " and " + kv.Key + " calculated Distance: " + currentLevenshteinDist);
                if (currentLevenshteinDist <= bestLevenshteinDist)
                {
                    bestLevenshteinDist = currentLevenshteinDist;
                    resultTerm = kv.Key;
                }
            }
            Console.WriteLine("Term " + term + " wird ersetzt durch den Term " + resultTerm);
            return PostingFile.postingFile[resultTerm];
        }



        private static int CalcLevenshteinDistance(string a, string b)
        {
            if (String.IsNullOrEmpty(a) && String.IsNullOrEmpty(b))
            {
                return 0;
            }
            if (String.IsNullOrEmpty(a))
            {
                return b.Length;
            }
            if (String.IsNullOrEmpty(b))
            {
                return a.Length;
            }
            int lengthA = a.Length;
            int lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
            for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

            for (int i = 1; i <= lengthA; i++)
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = b[j - 1] == a[i - 1] ? 0 : 1;
                    distances[i, j] = Math.Min
                        (
                        Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                        distances[i - 1, j - 1] + cost
                        );
                }
            return distances[lengthA, lengthB];
        }
    }
}
